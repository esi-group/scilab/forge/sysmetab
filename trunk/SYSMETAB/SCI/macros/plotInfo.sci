function plotInfo(outputname,resid)
  doc=xmlRead(outputname+'.params.xml');
	params=xmlXPath(doc,'//smtb:params',['smtb','http://www.utc.fr/sysmetab']);
  s=size(params(1).children,"*")
  for i=1:s
    name=params(1).children(i).attributes.name
    content=params(1).children(i).content
    xstring(0,1-i/s,sprintf("%s=%s",name,content))
  end
  title(sprintf("Results for the network %s.fml (residual=%f)",outputname,resid))
  set(get(gca(),'title'),'font_style',8)
endfunction  
  
