function dy_dp_reshaped=NSDerScales(y,y_unscaled,dy_dp,dy_unscaled_dp,auto_scales_ind)
  //
  // Finish the computation of dy_dp for observations belonging to groups with scale="auto"
  // and reshapes the hypermatrix by stacking vertically matrices dy_dp(:,:,j), j=1..nbMeas
  //
  [sizeMeas,nbParam,nbMeas]=size(dy_dp);    
  for i=auto_scales_ind // groups with scale="auto"
    indg=meas_ind_group(i);
    domega_i_dp=zeros(1,nbParam);     
    for j=1:nbMeas
      domega_i_dp=domega_i_dp+((2*y(indg,j)-ymeas(indg,j)).*Sypm2(indg,j))'*dy_unscaled_dp(indg,:,j)
    end
    domega_i_dp=-domega_i_dp/norm(y_unscaled(indg,:)./Sy(indg,:),"fro")^2
    for j=1:nbMeas
      dy_dp(indg,:,j)=dy_dp(indg,:,j)+y_unscaled(indg,j)*domega_i_dp
    end
  end

  dy_dp_reshaped=zeros(sizeMeas*nbMeas,nbParam);

  for j=1:nbMeas
    dy_dp_reshaped((j-1)*sizeMeas+(1:sizeMeas),:)=dy_dp(:,:,j)
  end
endfunction
//
function [dgdv_nz,db_dx_nz]=forwardNSDerivatives(v,X,dgdv,db_dx)
    //
    // Derivatives needed for the forward computation of gradient
    //
    dgdv_nz=dgdv.t*(X(dgdv.m1,:).*X(dgdv.m2,:));
    db_dx_nz=list();
    for i=2:length(db_dx)
        db_dx_nz(i)=db_dx(i).t*spdiag(v(db_dx(i).m2))*X(db_dx(i).m1,:);
    end
    //
endfunction
//
function [db_dxt_nz]=adjointNSDerivatives(v,X,dgdvt,db_dxt)
    //
    // Derivatives needed for the adjoint computation of gradient
    //
    db_dxt_nz=list();
    for i=1:length(db_dxt)
        db_dxt_nz(i)=db_dxt(i).t*spdiag(v(db_dxt(i).m2))*X(db_dxt(i).m1,:);
    end
    //
endfunction
//
function y=meas_v(v,m,Xinp,M,b,cum,ind_meas,t,pool_ind_weight)
  [residual,X,y,h,omega,Sypm2_e_label]=scheme.solveNS(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight)
  y=y(:)
endfunction
//
function y=meas_m(m,v,Xinp,M,b,cum,ind_meas,t,pool_ind_weight)
  [residual,X,y,h,omega,Sypm2_e_label]=scheme.solveNS(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight)
  y=y(:)
endfunction
//
function J=cost_v(v,m,Xinp,M,b,cum,ind_meas,t,pool_ind_weight)
  [residual,X,y,h,omega,Sypm2_e_label]=scheme.solveNS(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight)
  J=sum(residual);
endfunction
//
function J=cost_m(m,v,Xinp,M,b,cum,ind_meas,t,pool_ind_weight)
  [residual,X,y,h,omega,Sypm2_e_label]=scheme.solveNS(v,m,Xinp,M,b,cum,t,ind_meas,pool_ind_weight)
  J=sum(residual);
endfunction
//
function checkGradient(x)
  function d=myNumderivative(i)
    J=all_stuffNonStat(x0+h*((1:length(x0))==i)',"")    
    d=(J-J0)/h
    unix(":")
  endfunction
  eps_phi=1e-3
  eps_reg=1e-3
  
  for i=1:5
    reldiff=[]
    x0=x;
    x0(abs(x0)<%eps)=mean(abs(x))
    x0=x0.*(1+rand(x))
    [J0,grad0]=all_stuffNonStat(x0,"")    
    for j=-5:5 // make h vary from the classical value
      h=sqrt(%eps*J0*10^j)
      d=parallel_run(1:length(x0),myNumderivative)
      denom=d
      denom(denom==0)=1
      reldiff=[reldiff abs((d(:)-grad0)./denom(:))]
    end
    rdisc=min(reldiff,"c") // take the best error for each derivative
    printf("Rel. discrepancy : mean=%f, max=%f \n",mean(rdisc),max(rdisc))
  end 
endfunction
//
function checkDerivative(x)
  function d=myNumderivative(i)
    x=x0+h*((1:length(x0))==i)' 
    [residual,X,y]=scheme.solveNS(Phi(W*x(1:nff)+w0,0),x(nff+1:$),Xinp,M,b,cum,time_struct,pool_ind_weight,%t)
    d=(y(:)-y0(:))/h
    unix(":")
  endfunction
  nff=length(ff)
  for i=1:5
    reldiff=[]
    x0=x;
    x0(abs(x0)<%eps)=mean(abs(x))
    x0=x0.*(1+rand(x))
    [v0,dv0_dw]=Phi(W*x0(1:nff)+w0,0)
    [residual,X,y0,dy_dp0,omega]=scheme.solveNSDer(v0,x0(nff+1:$),Xinp,M,b,cum,time_struct,pool_ind_weight,dP_dpool,dv0_dw,W)
    for j=-5:5 // make h vary from the classical value
      h=sqrt(%eps*mean(y0)*10^j)
      d=parallel_run(1:length(x0),myNumderivative,length(y0))
      denom=1e-6+max(abs(d(:)),abs(dy_dp0(:)))
      reldiff=[reldiff abs((d(:)-dy_dp0(:))./denom(:))]
    end
    rdisc=min(reldiff,"c") // take the best error for each derivative
    printf("Rel. discrepancy : mean=%f, max=%f \n",mean(rdisc),max(rdisc))
  end 
endfunction
