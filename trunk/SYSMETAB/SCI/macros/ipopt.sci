function oj=ipopt_obj(x,x_new) 
    // Computation of the cost function 
    // Note : we compute here all that is necessary to ipopt, namely the function
    // giving the satisfaction of constraints and their gradient respectively.
    // Since in practice we compute simultaneously the cost function and its 
    // gradient, this allows to not make unnecessary computations, using the 
    // function x_new of the ipopt toolbox, which allows to know if the 
    // current x have changed or not.
    if x_new
        [all_obj,all_grobj]=all_stuff(x,'ipopt');
        oj=all_obj;
        [all_obj,all_grobj]=resume(all_obj,all_grobj);
    else
        oj=all_obj;
    end
endfunction

function goj=ipopt_grobj(x,x_new)
    // gradient computation of the cost function
    if x_new
        [all_obj,all_grobj]=all_stuff(x,'ipopt');
        goj=all_grobj;
        [all_obj,all_grobj]=resume(all_obj,all_grobj);
    else
        goj=all_grobj;
    end
endfunction

function c=ipopt_cntr(x,x_new)
    // Compute of the function giving satisfaction to the constraints.
    // pause;
    c=Cq*x-bq;
    if x_new
        [all_obj,all_grobj]=all_stuff(x,'ipopt');
        [all_obj,all_grobj]=resume(all_obj,all_grobj);
    end
endfunction

function dc=ipopt_dcntr(x,x_new)
    // derivative computation of the function giving satisfaction to the constraints.
    // called only once
    dc=Cq(sp_dcntr_vect);
endfunction

function [spmin,k]=SparseMatrixIndices(A)
    // give the indicies of non zeros elements in a matrix A and put it in a matrix
    // with two colomn (row,colomn)=(i,j) where i and j are obtained from
    [i,j]=find(A);
    spmin=[i' j'];
    k=find(A);
endfunction



