function [residual,X,y,y_unscaled,MList,omega,Sypm2_e_label,Y,Mass,err]=solveNSRKInterp(v,m,Xinp,M,b,cum,time_struct,pool_ind_weight,free)
    //
    // Solve the state equation cascade, SDRIK scheme + non uniform time grid
    // we suppose that a(i,i)==gam, i=1...s and A_s=b (b is equal to the last line of A)
    // c vector of Butcher tableau is not used as the ode is autonomous
    //
    gam=scheme.gamma
    alpha=scheme.alpha
    errc=scheme.err
    s=size(alpha,1) // number of RK stages
    N=length(cum)
    nX=size(Xinp,1)
    //
    MList=list()
    MX=list()
    Mass=zeros(nX,1)
    
    // initial stepsize, initial factorization
    
    stepsize=time_struct.t(2)-time_struct.t(1)
    for n=1:N    
      Mn_sp=sparse(M(n).ij,M(n).t*v(M(n).m1),M(n).size)
      mcn=m(pool_ind_weight(n))
      MList(n)=struct('Mn',Mn_sp,'MnImp_handle',umf_lufact(sparse(spdiag(mcn)-stepsize*gam*Mn_sp)),'Bnv',b(n).t*spdiag(v(b(n).m3)))
      Mass(cum(n))=mcn
    end

    G=zeros(nX,1)    
    Z=zeros(nX,s)

    kstart=[1;time_struct.ind(1:$-1,3)+1] // ranges of state integration followed by measurement interpolation‡
    kend=time_struct.ind(:,3)
    kmeas=time_struct.ind(:,1:2)
    shape=time_struct.shape
    
    nt=kend($)+1
    X=repmat(Xinp,1,nt)
    Xi=zeros(nX,size(shape,2))
    err=zeros(1,nt-1)    

    Y=list()
    for i=1:s
      Y(i)=zeros(nX,nt) // to store RK stage vectors for the adjoint state
    end
    for l=1:size(kend,1) // loop on k ranges

      for k=kstart(l):kend(l)  // loop on discrete time ranges

        Xk=X(:,k)
        Yi=Xk 
      
        newstepzize=time_struct.t(k+1)-time_struct.t(k)
        if clean(newstepzize-stepsize)
          stepsize=newstepzize
          for n=1:N    
            umf_ludel(MList(n).MnImp_handle)
            MList(n).MnImp_handle=umf_lufact(sparse(spdiag(m(pool_ind_weight(n)))-stepsize*gam*MList(n).Mn))
          end
        end
      
        // rK stage 1

        for n=1:N // loop on cumomer weight -> solving cascade for Z1
          cumn=cum(n)
          MX(n)=MList(n).Mn*Xk(cumn)
          Z(cumn,1)=umf_lusolve(MList(n).MnImp_handle,stepsize*gam*(MX(n)+spset(b(n).value,MList(n).Bnv*(Yi(b(n).m1).*Yi(b(n).m2)))))
          Yi(cumn)=Xk(cumn)+Z(cumn,1)
        end
      
        Y(1)(:,k)=Yi        
      
        for i=2:s // loop on subsequent RK stages

          G=Mass.*(Z(:,1:i-1)*alpha(i,1:i-1)')
          Yi=Xk        
          for n=1:N // loop on cumomer weight -> solving cascade for Zi
            cumn=cum(n)
            Z(cumn,i)=umf_lusolve(MList(n).MnImp_handle,G(cumn)+stepsize*gam*(MX(n)+spset(b(n).value,MList(n).Bnv*(Yi(b(n).m1).*Yi(b(n).m2)))))
            Yi(cumn)=Xk(cumn)+Z(cumn,i)
          end
        
          Y(i)(:,k)=Yi        

        end // loop on RK stage

        X(:,k+1)=Yi // special case d=b*A^-1 and A_s=b (b is equal to the last line of A) hence d_s=1 and d_i=0, i<s      
        err(k)=sqrt(mean((Z*errc).^2))

      end // loop on discrete time values

      // Compute the interpolated states
      k1=kmeas(l,1)
      k2=kmeas(l,2)
      Xi(:,k1:k2)=repmat(Xk,1,k2-k1+1)+Z*shape(:,k1:k2)
    
    end // loop on discrete time ranges

    omega=ones(nb_group,1)
    y_unscaled=Cx*Xi
    for i=auto_scales_ind // for groups with scale="auto"
      ind=meas_ind_group(i)
      yui=y_unscaled(ind,:)./Sy(ind,:)
      ymi=ymeas(ind,:)./Sy(ind,:)
      omega(i)=yui(:)'*ymi(:)/norm(yui(:))^2
    end
    y=spdiag(omega(omega_ind_meas))*y_unscaled
    //
    // Compute the residual
    //
    try
      e_label=y-ymeas
      Sypm2_e_label=Sypm2.*e_label
      residual=Sypm2_e_label.*e_label
    catch
      e_label=[]
      Sypm2_e_label=[]
      residual=[]
    end

    for i=1:length(cum)
      umf_ludel(MList(i).MnImp_handle)
    end

endfunction