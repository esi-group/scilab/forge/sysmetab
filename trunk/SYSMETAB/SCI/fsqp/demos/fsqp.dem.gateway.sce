mode(-1)
//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//

demopath = get_absolute_file_path("fsqp.dem.gateway.sce");


subdemolist = ['Quadratic problem with non-linear constraints'
	       'Min Max problem'
	       'Non linear problem with  non-linear constraints'
	       'Sequentially related nonlinear inequality contraints'
	       'Discretized semi-infinite program'];
T= 'Example'+string(1:5)';
subdemolist(:,2) = demopath+T+'/'+T+'.sce'
exec(demopath + 'fsqp_demo_gui.sci')


