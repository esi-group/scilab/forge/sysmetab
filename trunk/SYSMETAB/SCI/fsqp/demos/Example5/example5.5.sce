//[2] fsqp with  lists 
//=======================================================

// One regular objective 0 SR objective
function fj=f_1(x), fj=(x(1)^2)/3+x(2)^2+x(1)/2;endfunction 
list_obj=list(list(f_1),list()); 
function gr=grf_1(x), gr=[2/3*x(1)+0.5,2*x(2)];endfunction
list_grobj=list(list(grf_1),list());

//One non linear SR inequality constraint R^101 valued (G_1(x))
function c=G_1(j,x),c=(1-(t*x(1))^2)^2-x(1)*t2+x(2)-x(2)^2;endfunction
list_cntr=list(list(),list(G_1),list(),list(),list(),list());
function g=grG_1(j,x),
  g=[-4*t2.*(1-(t*x(1))^2)^2*x(1)-t2,-2*x(2)+ones(t)];
endfunction
list_grcn=list(list(),list(grG_1),list(),list(),list(),list());

x0=[-1;-2];
t=(0:0.001:1)';t2=t.*t;
// fsqp parameters obtained by findparam
x=x0;
[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0);

modefsqp=100;iprint=1;miter=500;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

srpar=[nfsr,ncsrl,ncsrn];

bigbnd=1.e10;eps=1.e-4;epsneq=0.e0;udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=[-bigbnd;-bigbnd];
bu=-bl;

x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)



