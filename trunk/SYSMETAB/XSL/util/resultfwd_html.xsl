<?xml version="1.0" encoding="UTF-8" ?>

<!-- 
    Authors :   Stéphane Mottelet
    Project :   PIVERT/Metalippro-PL1

    xsltproc --stringparam fwdfile e_coli_ns_2.fwd resultfwd_html.xsl e_coli_ns_1.fwd 
    
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:fwd="http://www.13cflux.net/fwdsim"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt fwd">

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
    
  <xsl:strip-space elements="*"/>

	<xsl:param name="fwdfile"/>

	<xsl:key name="poolvalue" match="fwd:flux" use="@id"/>
  <xsl:key name="INPUT" match="f:input" use="@pool"/>

  <xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>
  

  <xsl:template match="fwd:fwdsim/fwd:stoichiometry">
  
  
  <table>  
    <tr>
      <td>Name</td>
      <td>Err (%)</td>
      <td>Hausd (%)</td>
    </tr>
  <xsl:for-each  select="fwd:flux/*[@type='free']">
    <tr>
      <td>    
        <xsl:value-of select="../@id"/>
      </td>

      <xsl:variable name="value1" select="@value"/>
      <xsl:variable name="value2" select="document($fwdfile,/)/fwd:fwdsim/fwd:stoichiometry/fwd:flux[@id=current()/../@id]/*[@type='free']/@value"/>        
      <xsl:variable name="diff" select="(($value1)-($value2))div($value2)"/>

      <td>
        <xsl:value-of select="math:abs($diff)*1e2"/>
      </td>       
    
      <xsl:variable name="sigma1" select="@stddev"/>
      <xsl:variable name="sigma2" select="document($fwdfile,/)/fwd:fwdsim/fwd:stoichiometry/fwd:flux[@id=current()/../@id]/*[@type='free']/@stddev"/>        
      <xsl:variable name="moins1" select="($value1)-2*($sigma1)"/>
      <xsl:variable name="moins2" select="($value2)-2*($sigma2)"/>
    
      <xsl:variable name="min1">
        <xsl:choose>
          <xsl:when test="$moins1>=0">
            <xsl:value-of select="$moins1"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="0"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
    
      <xsl:variable name="min2">
       <xsl:choose>
          <xsl:when test="$moins2>=0">
            <xsl:value-of select="$moins2"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="0"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      
      <xsl:variable name="max1" select="($value1)+2*($sigma1)"/>
      <xsl:variable name="max2" select="($value2)+2*($sigma2)"/>

      <xsl:variable name="diff1">
        <xsl:choose>
          <xsl:when test="($min1)-($min2)>=0">
            <xsl:value-of select="($min1)-($min2)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="-($min1)+($min2)"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:variable name="diff2">
        <xsl:choose>
          <xsl:when test="($max1)-($max2)>=0">
            <xsl:value-of select="($max1)-($max2)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="-($max1)+($max2)"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      
      <td>        
      <xsl:choose>
        <xsl:when test="$diff2>=$diff1">
          <xsl:value-of select="($diff2)div(($max2)-$min2)*1e2"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="($diff1)div(($max2)-$min2)*1e2"/>
        </xsl:otherwise>
      </xsl:choose>
      </td>

    </tr>                              
  </xsl:for-each> 
  </table>

</xsl:template>

  <xsl:template match="fwd:fwdsim/fwd:simulation">
  
  <table>  
    <tr>
      <td>Name</td>
      <td>Err (%)</td>
      <td>Hausd (%)</td>
    </tr>

  <xsl:for-each  select="fwd:pool[not(@stddev=0)]">       
    <tr>  
        
      <td>
        <xsl:value-of select="@id"/>
      </td>
      
      <xsl:variable name="value1" select="@value"/>
      <xsl:variable name="value2" select="document($fwdfile,/)/fwd:fwdsim/fwd:simulation/fwd:pool[@id=current()/@id]/@value"/>
      <xsl:variable name="diff" select="(($value1)-($value2))div($value2)"/>

      <td>
      <!--  
      <xsl:choose>
        <xsl:when test="$diff>=0">
          <xsl:value-of select="$diff"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="-$diff"/>
        </xsl:otherwise>
      </xsl:choose>
      -->
        <xsl:value-of select="math:abs($diff)*1e2"/>
      </td>       
      
      <xsl:variable name="sigma1" select="@stddev"/>
      <xsl:variable name="sigma2" select="document($fwdfile,/)/fwd:fwdsim/fwd:simulation/fwd:pool[@id=current()/@id]/@stddev"/>
      <xsl:variable name="moins1" select="($value1)-2*($sigma1)"/>
      <xsl:variable name="moins2" select="($value2)-2*($sigma2)"/>

      <xsl:variable name="min1">
        <xsl:choose>
          <xsl:when test="$moins1>=0">
            <xsl:value-of select="$moins1"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="0"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      
      <xsl:variable name="min2">
        <xsl:choose>
          <xsl:when test="$moins2>=0">
            <xsl:value-of select="$moins2"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="0"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
        
      <xsl:variable name="max1" select="($value1)+2*($sigma1)"/>
      <xsl:variable name="max2" select="($value2)+2*($sigma2)"/>
      
      <xsl:variable name="diff1">
        <xsl:choose>
          <xsl:when test="($min1)-($min2)>=0">
            <xsl:value-of select="($min1)-($min2)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="-($min1)+($min2)"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <xsl:variable name="diff2">
        <xsl:choose>
          <xsl:when test="($max1)-($max2)>=0">
            <xsl:value-of select="($max1)-($max2)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="-($max1)+($max2)"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>

      <td>          
      <xsl:choose>
        <xsl:when test="$diff2>=$diff1">
          <xsl:value-of select="($diff2)div(($max2)-$min2)*1e2"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="($diff1)div(($max2)-$min2)*1e2"/>
        </xsl:otherwise>
      </xsl:choose>
      </td>  
  
    </tr>  
  </xsl:for-each>
  </table> 
</xsl:template>
<!--
  <xsl:template match="f:variables">
  		<variables xmlns="http://www.13cflux.net/fluxml">
  			<xsl:for-each select="document($fwdfile,/)/fwd:fwdsim/fwd:stoichiometry/fwd:flux/*[@type='free']">
  				<fluxvalue flux="{../@id}" type="{name()}" xmlns="http://www.13cflux.net/fluxml">
  					<xsl:value-of select="@value"/>
  				</fluxvalue>
  			</xsl:for-each>
        <xsl:choose>
          <xsl:when test="//f:fluxml/f:configuration[@stationary='false'] and document($fwdfile,/)/fwd:fwdsim/fwd:simulation/fwd:pool[@value]">
  			      <xsl:for-each select="//f:fluxml/f:reactionnetwork/f:metabolitepools/f:pool[not(key('INPUT',@id))]">
  				      <poolvalue pool="{@id}" xmlns="http://www.13cflux.net/fluxml">
                  <xsl:if test="key('poolvalue',@id)/@lo">
                    <xsl:attribute name="lo">
                      <xsl:value-of select="key('poolvalue',@id)/@lo"/>
                    </xsl:attribute>
                  </xsl:if>
                  <xsl:if test="key('poolvalue',@id)/@hi">
                    <xsl:attribute name="hi">
                      <xsl:value-of select="key('poolvalue',@id)/@hi"/>
                    </xsl:attribute>
                  </xsl:if>
  					      <xsl:value-of select="document($fwdfile,/)/fwd:fwdsim/fwd:simulation/fwd:pool[@id=current()/@id]/@value"/>
  				      </poolvalue>
  			      </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="f:poolvalue"/>
          </xsl:otherwise>
        </xsl:choose>
  		</variables>
	</xsl:template>

  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>

-->
<xsl:template match="text()"/>
</xsl:stylesheet>
