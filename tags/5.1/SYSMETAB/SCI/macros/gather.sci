function Mstruct=gather(M_m,nrow,ncol,storage)
  // group matrix contributions to same (i,j) term
  if typeof(M_m)=="string"
    try
      M_m=fscanfMat(M_m)
    catch
      M_m=[]
    end
  end
  if isempty(M_m)
    Mstruct=struct('size',[nrow,ncol],'value',spzeros(nrow,ncol),'ij',[],'t',[],'m1',[],'m2',[],'m3',[])
  else 
    m=size(M_m,1);
    if storage==1 // CSR storage (Scilab)    
      [ij,ind]=gsort(M_m(:,1:2),"lr","i");  // lexicographic sort of rows
      [uij,k]=s_unique(ij);                 // unique (row,col) pairs        
    elseif storage==2 // CSC storage (Julia)  
      [ij,ind]=gsort(M_m(:,[2 1]),"lr","i");  // lexicographic sort of rows
      [uij,k]=s_unique(ij(ind,[2 1]));         // unique (row,col) pairs        
    end

    M_s=M_m(:,3);
    M_i=zeros(m,1);
    n=length(k);
    for i=1:n-1
        M_i(k(i):k(i+1)-1)=i;
    end
    if n>0
      M_i(k(n):$)=n
    end

    M=sparse(uij,ones(n,1),[nrow,ncol]);
    M_t=sparse([M_i ind],M_s(ind),[n,m]);
    if n==1 | m==1
        M_t=full(M_t);
    end

    Mstruct=struct('size',[nrow,ncol],'value',M,'ij',uij,'t',M_t,'m1',[],'m2',[],'m3',[])

    for j=4:size(M_m,2)
      execstr(sprintf("Mstruct.m%d=M_m(:,%d)",j-3,j))
    end
  end

endfunction

function [uij,k]=s_unique(ij)
    // computes unique lines of ij. Index k contains first occurences of unique lines 
    n=size(ij,1)
    if n==0
      uij=[]
      k=[]
    else
      k=zeros(n,1)
      ind=1;
      k(ind)=1
      for i=2:n
          if or(ij(i,:)<>ij(k(ind),:))
              ind=ind+1
              k(ind)=i
          end
      end
      k=k(1:ind)
      uij=ij(k,:)
    end  
endfunction
