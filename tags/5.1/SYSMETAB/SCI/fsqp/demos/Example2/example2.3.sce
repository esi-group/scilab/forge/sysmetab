 mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 2 of the fsqp documentation

exec(get_absolute_file_path("example2.3.sce")+'/example2-src/loader.sce') 

//Same example but Calling SR-fsqp (see example 2 of fsqp doc).

modefsqp=111; 
iprint=0;
miter=500;
bigbnd=1.e10;
eps=1.e-8;
epsneq=0.e0;
udelta=0.e0;
nf=1;
nfsr=1;
mesh_pts=163;
neqn=0;
nineqn=0;
nineq=7;
neq=0;
ncsrl=0;ncsrn=0;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
rpar=[bigbnd,eps,epsneq,udelta];
srpar=[nfsr,ncsrl,ncsrn];

x0=[0.5;1;1.5;2;2.5;3]; 
bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);

cd=[%pi,0.425]; 
//The gradient are estimated by finite differences grobfd and grcnfd C
//codes included in cfsqp.c

// Using C functions: cd must be given as last argument of fsqp
mprintf('%s\n',['---------------------------MinMax problem with srfsqp(C code)-----------------------------------------'
		'Initial values'
		'   Value of the starting point:     '+sci2exp(x0,0)])
timer();
[x,info,f,g]=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],'objmad','cnmad','grobfd','grcnfd',cd)
t=timer();
mprintf('%s\n',['Final values'
		'   Value of the solution:           '+sci2exp(x,0)
		'   Value of the objective function: '+sci2exp(f(1),0)
		'   Time :                           '+sci2exp(t,0)
	       ])

