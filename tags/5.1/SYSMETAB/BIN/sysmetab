#!/bin/bash
#
# Author   :   Stephane Mottelet and Georges Sadaka
# Date     :   Lun 13 avr 2015 16:23:00 CEST
# Project  :   SYSMETAB/MetaLipPro-PL1/PL2
#
# Generation program of resolution/optimization, steady case
#
SYSMETAB=$( cd "$( dirname "$0" )/.." && pwd )
XSLSYSPREP=$SYSMETAB/XSL/fmlsys_prep
XSLSYSGEN=$SYSMETAB/XSL/fmlsys_gen
XSLCOMMON=$SYSMETAB/XSL/common
BIN=$SYSMETAB/BIN

function help()
{
  echo ""
  echo "Usage: sysmetab [options] filename (without extension)"
  echo ""
  echo "Description : Generation program of resolution/optimization Scilab code, stationary case"
  echo ""
  echo "Options (for multiple choice default value is the first)"
  echo ""
  echo "    --output=filename                        output filename (without extension)"
  echo "    --full                                   no cumomer graph simplification"
  echo "    --nocalc                                 generate Scilab code but do not execute it."
  echo "    --nooptimize                             no optimization (forward simulation only)"
  echo "    --scheme=sdirk1|sdirk2a|sdirk3a|sdirk4b  ode scheme"
  echo "    --odetol                                 tolerance for local ode error control (default=1e-3)"
  echo "    --steps                                  (maximum when --adapt=yes) number of ode time steps (default=20)"
  echo "    --adapt=yes|[no]                         stepsize adaptation (before optimization), no=fixed-step"
  echo "    --extrap                                 estimate flux and pool error by step halving"
  #    echo "    --gradient=adjoint|direct                method for computing the gradient"    
  echo "    --freefluxes=user|auto[:n]|multi:n       freefluxes initialization"
  echo "    --stats=lin|mc:n                         linear or Monte Carlo statistics"
  echo "    --conflevel=LEVEL                        confidence level for fluxes intervals (default is 0.95)"
  echo "    --nc=N                                   number of allocated processor cores (by default, all available cores are used)"
  echo "    --seed=N                                 initialize random generator with seed N (default is number of seconds since the Epoch)"
  echo "    --scaling=[user]|yes|no                  scaling of measurement groups"
  echo "    --reg=REG                                set regularization parameter of cost function (default=1e-6)"
  echo "    --regphi=TOL                             set smoothing parameter for the (net,xch)->(f,b) transformation (default=0)"
  echo "    --toldir=TOL                             set upper bound for the norm of optimizer search direction (default=1e-6)"
  echo "    --tolx=TOL                               set upper bound for the norm of optimizer increment (default=1e-5)"
  echo "    --maxiter=N                              set maximum number of iterations of optimizer (default=1000)"
  echo "    --maxxch=MAXXCH                          maximum value for xch flux (default=1e3)"
  echo "    --maxnet=MAXNET                          maximum value for net flux (default=1e3)"
  echo "    --mininout=MININOUT                      minimum value for input/output net flux (default=0)"
  echo "    --minnr=MINNR                            minimum value for non-reversible reactions net flux (default=0, means no lower bound)"
  echo "    --rebuild                                rebuild Scilab code from scratch"
  echo "    --verbose 1|0                            verbosity level"
  echo "    --plot                                   plot results in pdf file"    
  echo "    --update                                 update variables in fml file after optimization"    
  echo "    -h, --help                               show this help message and exit"
  echo ""
}

tab=( $@ ); # put all parameters entries in a vector

if [[ $1 == "-h" || $1 == "--help" || $1 == "" ]]
then
  help
  exit
else
  file_name=${tab[$#-1]};
  output=${tab[$#-1]};
fi

if [ $(uname) == "Darwin" ]
then
  nc=`sysctl -n hw.ncpu`
else
  nc=`nproc`
fi
seed=`date +%s`
timer="no"
maxweight="999"
verb_level="1"
gradient="adjoint"
minimize="yes"
optimize="yes"
scaling="user"
zc="no"
optmeth="fsqp"
max_xch="999"
max_net="1000"
min_inout="0"
min_nr="-1000"
calc="yes"
eps_grad="1e-6"
eps_x="1e-5"
eps_reg="0"
eps_phi="0"
max_iter="1000"
minstdev="-1"
stats="lin"
conflevel=0.95
freefluxes="user"
steps="20"
odetol="1e-3"
adapt="no"
scheme="sdirk4b"
extrap="no"
rebuild="no"
update="no"
plot="no"
file_name=${tab[$#-1]};
output=${tab[$#-1]};

#
# Preliminary transformation
#

for ((i=0; i<$#-1 ; i++));
do
  case ${tab[i]} in
    -h | --help)
    help
    exit
    ;;
    --maxweight=*)
    maxweight="${tab[i]#*=}"
    ;;
    #        --gradient=*)
    #            gradient="${tab[i]#*=}"
    #            ;;
    --full)
    minimize="no"
    ;;
    --timer)
    timer="yes"
    calc="no"
    ;;
    --nooptimize)
    optimize="no"
    ;;
    --scaling=*)
    scaling="${tab[i]#*=}"
    ;;
    --nocalc)
    calc="no"
    ;;
    --rebuild)
    rebuild="yes"
    ;;
    --output=*)
    output="${tab[i]#*=}"
    ;;
    --algorithm=*)
    optmeth="${tab[i]#*=}"
    ;;
    --reg=*)
    eps_reg="${tab[i]#*=}"
    ;;
    --regphi=*)
    eps_phi="${tab[i]#*=}"
    ;;
    --toldir=*)
    eps_grad="${tab[i]#*=}"
    ;;
    --tolx=*)
    eps_x="${tab[i]#*=}"
    ;;
    --maxxch=*)
    max_xch="${tab[i]#*=}"
    ;;
    --maxnet=*)
    max_net="${tab[i]#*=}"
    ;;
    --mininout=*)
    min_inout="${tab[i]#*=}"
    ;;
    --minnr=*)
    min_nr="${tab[i]#*=}"
    ;;
    --maxiter=*)
    max_iter="${tab[i]#*=}"
    ;;
    --verbose=*)
    verb_level="${tab[i]#*=}"
    ;;
    --stats=*)
    stats="${tab[i]#*=}"
    ;;
    --conflevel=*)
    conflevel="${tab[i]#*=}"
    ;;
    --seed=*)
    seed="${tab[i]#*=}"
    ;;
    --nc=*)
    ncu="${tab[i]#*=}"
    nc=$((ncu<nc ? ncu : nc))
    ;;
    --zc)
    zc="yes"
    ;;
    --freefluxes=*)
    freefluxes="${tab[i]#*=}"
    ;;
    --steps=*)
    steps="${tab[i]#*=}"
    ;;    
    --odetol=*)
    odetol="${tab[i]#*=}"
    ;;    
    --adapt=*)
    adapt="${tab[i]#*=}"
    ;;    
    --scheme=*)
    scheme="${tab[i]#*=}"
    ;;    
    --extrap)
    extrap="yes"
    ;;
    --plot)
    plot="yes"
    ;;
    --update)
    update="yes"
    ;;
    *)
    echo "ERROR: unknown parameter \"${tab[i]}\""
    help
    exit 1
    ;;
  esac
  #    shift
done

dev=/dev/stdout
if [ "$verb_level" == "0" ]
then
  dev="/dev/null"
fi
	
if [ ! -f $file_name.fml ]
then
  echo "$file_name.fml : file not found\n"
  exit
fi

# XML file of parameters

xsltproc -o $output.params.xml \
  --stringparam file `basename $file_name` \
  --stringparam sysmetab $SYSMETAB\
  --stringparam timer $timer\
  --stringparam minimize $minimize\
  --stringparam maxweight $maxweight\
  --stringparam verb_level $verb_level\
  --stringparam output $output\
  --stringparam eps_grad $eps_grad\
  --stringparam eps_x $eps_x\
  --stringparam eps_reg $eps_reg\
  --stringparam eps_phi $eps_phi\
  --stringparam max_iter $max_iter\
  --stringparam max_xch $max_xch\
  --stringparam max_net $max_net\
  --stringparam min_inout $min_inout\
  --stringparam min_nr $min_nr\
  --stringparam optimize $optimize\
  --stringparam optimize_method $optmeth\
  --stringparam scaling $scaling\
  --stringparam gradient $gradient\
  --stringparam computation $calc\
  --stringparam stats $stats\
  --stringparam conflevel $conflevel\
  --stringparam freefluxes $freefluxes\
  --stringparam odeparams "$scheme,$steps,$odetol,$adapt,$extrap"\
  --stringparam nc $nc\
  --stringparam seed $seed\
  --stringparam plot $plot\
  --stringparam date "$(date)"\
  $XSLSYSGEN/fmlsys_gen_params.xsl\
  $file_name.fml

if [ "$rebuild" == "yes" ]
then
  rm -f $file_name.old
fi

md5cmd="md5sum" # md5sum is named differently under MacOSX 
if [ $(uname) == "Darwin" ]
then
  md5cmd="md5"
fi

RN_LM=$(xsltproc $XSLSYSGEN/RN_LM.xsl $file_name.fml | $md5cmd)
if [ ! -f $file_name.old ]
then
  RN_LM_OLD=""
else
  RN_LM_OLD=$(xsltproc $XSLSYSGEN/RN_LM.xsl $file_name.old | $md5cmd)
fi

# Preliminary (cheap) transformations

xsltproc -o $file_name.step0.xml --stringparam output $output $XSLSYSPREP/fmlsys_prep_0.xsl $file_name.fml

#
# Transformation needed when the network topology has changed ------------------------------------
#
if [ "$RN_LM" != "$RN_LM_OLD" ]
then
  cp $file_name.fml $file_name.old
  #
  xsltproc -o $file_name.step2.xml\
    --stringparam verb_level $verb_level\
    --stringparam output $output\
    --maxdepth 8192 $XSLSYSPREP/fmlsys_prep_1.xsl $file_name.step0.xml
  #
  if test $minimize = yes # Cumomer graph simplification by "backtracing"
  then
    xsltproc -o $file_name.graph\
      --stringparam verb_level $verb_level\
      --stringparam output $output\
      $XSLSYSGEN/fmlsys_gen_adj.xsl $file_name.step2.xml
    xsltproc -o $file_name.trace.sci\
      --stringparam verb_level $verb_level\
      --stringparam output\
      $output $XSLCOMMON/common_sys_text.xsl $file_name.graph
    scilab -nwni -nb -f $file_name.trace.sci > $dev 2>&1
  fi
  #
  xsltproc -o $file_name.step3.xml\
    --stringparam verb_level $verb_level\
    --stringparam output $output\
    $XSLSYSPREP/fmlsys_prep_3.xsl $file_name.step2.xml
  #
  # Final generation of specific Scilab functions for the network
  #
  xsltproc -o $file_name.state.xml\
    --stringparam verb_level $verb_level\
    --stringparam output $output\
    $XSLSYSGEN/fmlsys_gen_mathml_network.xsl $file_name.step3.xml
  #
  xsltproc -o $file_name.network.sci\
    --stringparam verb_level $verb_level\
    --stringparam output $output\
    $XSLCOMMON/common_sys_text.xsl $file_name.state.xml
fi
#
# End of transformations due to network topology changes -------------------------------------------
#
xsltproc -o $output.step4.xml\
  --stringparam output $output\
    $XSLSYSGEN/fmlsys_gen_mathml_main.xsl $file_name.step3.xml
#
xsltproc -o $output.main.sci\
  --stringparam verb_level $verb_level\
  --stringparam output $output\
  $XSLCOMMON/common_sys_text.xsl $output.step4.xml
#
xsltproc --stringparam output $output\
  -o $output.data.xml\
  $XSLSYSGEN/fmlsys_gen_data.xsl $file_name.step0.xml
#
xsltproc -o $output.data.sci\
  --stringparam verb_level $verb_level\
  --stringparam output $output\
  $XSLCOMMON/common_sys_text.xsl $output.data.xml
#
if test $calc = yes
then
  scilab -nw -nb -f $output.main.sci > $dev 2>&1
  if [ "$output.fwd" -nt "$file_name.step0.xml" ]
  then
    xsltproc $XSLSYSGEN/fwd_report.xsl $output.fwd > $dev 2>&1
    if test $update = yes
    then
      $SYSMETAB/BIN/setvariables $output.fwd $file_name.fml
    fi
  fi
else 
  echo "Everything is OK."
  echo "Now you must run \"${output}.main.sce\" in scilab in order to see all results."
  echo ""
fi
