Sysmetab Version 5.1 (Thu Jul  7 09:50:40 CEST 2016)

Sysmetab is an integrated software solving 13C metabolic flux identification problem, with automated Scilab code generation using XML/XSL technology.

Please see the project page at 

http://forge.scilab.org/index.php/p/sysmetab/

for more information.
