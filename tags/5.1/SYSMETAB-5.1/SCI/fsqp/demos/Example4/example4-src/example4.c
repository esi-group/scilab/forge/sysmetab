#include "cfsqpusr.h"

void obj(nparam,j,x,fj,cd)
     int nparam,j;
     double *x,*fj;
     void *cd;
{
  *fj=x[9];
  return;
}

void
grob(nparam,j,x,gradfj,dummy,cd)
     int nparam,j;
     double *x,*gradfj;
     void (* dummy)(),*cd;
{
  gradfj[0]=0.e0;
  gradfj[1]=0.e0;
  gradfj[2]=0.e0;
  gradfj[3]=0.e0;
  gradfj[4]=0.e0;
  gradfj[5]=0.e0;
  gradfj[6]=0.e0;
  gradfj[7]=0.e0;
  gradfj[8]=0.e0;
  gradfj[9]=1.e0;
  return;
}

void  
cntr(nparam,j,x,gj,cd)
     int nparam,j;
     double *x,*gj;
     void *cd;
{
  double t,z,s1,s2;
  int k,r;

  r=100;
  s1=s2=0.e0;
  if (j<=r) t=3.14159265358979312e0*(j-1)*0.025e0;
  else {
    if (j<=2*r) t=3.14159265358979312e0*(j-1-r)*0.025e0;
    else t=3.14159265358979312e0*(1.2e0+(j-1-2*r)*0.2e0)*0.25e0;
  }
  for (k=1; k<=9; k++) {
    s1=s1+x[k-1]*cos(k*t);
    s2=s2+x[k-1]*sin(k*t);
  }
  z=s1*s1 + s2*s2;
  if (j<=r) *gj=(1.e0-x[9])*(1.e0-x[9])-z;
  else {
    if (j<=2*r) *gj=z-(1.e0+x[9])*(1.e0+x[9]);
    else *gj=z-x[9]*x[9];
  }
  return;
}

/** for fort calling from Scilab **/

void cntr3(j,x,gj) 
     int *j;
     double *x,*gj;
{
  int nparam=0;
  cntr(&nparam,*j,x,gj,(void *) 0);
}
