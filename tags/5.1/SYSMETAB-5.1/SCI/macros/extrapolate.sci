function [flux,pool]=extrapolate(q1,m1,ts,flux,pool)
  n1=length(ts.t)-2
  n2=floor(n1*1.5)
  printf("Extrapolation from %d to %d time steps:\n",n1,n2)
  time_struct=time_discr(scheme,tmeas,n2);
  [q2,m2]=optimizeNonStat("fsqp",[q_start;m_start],[q_min;m_min],[q_max;m_max],C,d,me,1e-5,1e-6,0,0,1000,iprint);
  flux.err=abs(W*(q2-q1))/(1-(n1/n2)^scheme.order)
  pool.err=abs(m2-m1)/(1-(n1/n2)^scheme.order)
endfunction
