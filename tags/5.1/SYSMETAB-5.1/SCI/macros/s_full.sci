function a=s_full(ijv,n,m)
  if typeof(ijv)=='string'
    ijv=fscanfMat(ijv)
  end
  a=full(sparse(ijv(:,1:2),ijv(:,3),[n m]))
endfunction
