function [q,m,f,info,mess,ncallf]=optimizeNonStat(algorithm,xstart,xmin,xmax,C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,iprint,task,ntasks)

    // Error estimation by Richardson extrapolation 

    function [_q,_m,_f,_info,_iter,_time]=optimize_ex(i)
        time_struct=time_discr(scheme,tmeas,stepstab(i))
        [_q,_m,_f,_info,_iter,_time]=optimizeNonStat_single(algorithm,xstart,xmin,xmax,...
        C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,0);
        printf("task=%3d, info=%d, iterations=%5d, residual=%8g\n",i,_info,_iter,_f)         
        unix(':') // to circumvent OSX parallel_run bug
    endfunction


    // Monte-Carlo method 

    function [_q,_m,_f,_info,_iter,_time]=optimize_mc(i)
        ymeas=ymeastab(i);
        wmeas=wmeastab(i);        
        [_q,_m,_f,_info,_iter,_time]=optimizeNonStat_single(algorithm,xstart,xmin,xmax,...
        C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,0);
        printf("task=%3d, info=%d, iterations=%5d, residual=%8g\n",i,_info,_iter,_f)         
        unix(':') // to circumvent OSX parallel_run bug
    endfunction

    // multistart

    function [_q,_m,_f,_info,_iter,_time]=optimize_multi(i)
        [_q,_m,_f,_info,_iter,_time]=optimizeNonStat_single(algorithm,xstarttab(:,i),xmin,xmax,...
            C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,0);
        printf("task=%3d, info=%d, iterations=%5d, residual=%8g\n",i,_info,_iter,_f)
        unix(':') // to circumvent OSX parallel_run bug
    endfunction

    time=0;
    eps_reg=max(1e-14,eps_reg) // safety reg (helps sometimes)

    if exists('task','local')

      params=init_param('dynamic_scheduling',1,'nb_workers',nb_cores)

      if task=="extra"
        n1=length(time_struct.t)-2
        ntasks=4
        r=1.5^(1/(ntasks-1))
        stepstab=round(n1*r.^(0:ntasks-1)) 
        printf("\n%d tasks started with steps=",ntasks)
        for i=1:ntasks
          printf("%d, ",stepstab(i))
        end
        printf("please wait.\n\n")    
        [q,m,f,info,iter,time]=parallel_run(1:ntasks,optimize_ex,[length(ff) nb_pool 1 1 1 1]',params);	

      elseif task=='mc'
        [q,m,f,info,iter,time,mess]=optimizeNonStat_single(algorithm,xstart,xmin,xmax,C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,iprint);
        if info>=0
          w=W*q+w0;
          [residual,X,y,h,omega,Sypm2_e_label]=scheme.solveNS(Phi(w,eps_phi),m,Xinp,M,b,cum,time_struct,pool_ind_weight) 
          ymeastab=list();
          wmeastab=list();        
          for i=1:ntasks
              ymeastab(i)=max(y+rand(ymeas,'normal')./sqrt(Sypm2),0)
              wmeastab(i)=max(E*w+rand(wmeas,'normal')./sqrt(Svpm2),0);
          end
          printf("\n%d tasks started, please wait.\n\n",ntasks)    

          xstart=[q;m]

          tic
          [qmc,mmc,fmc,infomc,itermc,timemc]=parallel_run(1:ntasks,optimize_mc,[length(ff) nb_pool 1 1 1 1]',params);	
          time=toc()
          
          k=find(infomc==0 | infomc==3 | infomc==8); // keep semi-successfull optimization runs: 0=OK, 3=max. iter. reached, 8=should not be present (because of specific tolX handling)

          qmc=qmc(:,k);mmc=mmc(:,k);fmc=fmc(k);infomc=infomc(k);
          qt=quart(fmc);
          printf("\nResidual min=%f, q1=%f, median=%f, mean=%f, q3=%f, max=%f, std=%f\n",min(fmc),qt(1),qt(2),mean(fmc),qt(3),max(fmc),stdev(fmc))        

          q=[q qmc]
          m=[m mmc]
          f=[f fmc]
          info=[info infomc]
          iter=[iter itermc]

        end
      elseif task=="multi"
    
        xstarttab=rand(length(xstart),ntasks)
        
        printf("\n%d tasks started, please wait\n\n",ntasks)

        [q,m,f,info,iter,time]=parallel_run(1:ntasks,optimize_multi,[length(ff) nb_pool 1 1 1 1]',params);
      
        k=find(info==0 | info==3 | info==8); // keep semi-successfull optimization runs: 0=OK, 3=max. iter. reached, 8=should not be present (because of specific tolX handling)

        if ~isempty(k)
          q=q(:,k);m=m(:,k);f=f(k);info=info(k);iter=iter(k);time=time(k);
          printf("\nResidual best=%f, mean=%f, median=%f, std=%f\n",min(f),mean(f),median(f),stdev(f))
          [bc,bk]=min(f);
          q=q(:,bk);
          m=m(:,bk);
          f=f(bk);
          info=info(bk);
          iter=iter(bk);
          time=time(bk);
        else // in order to have a more detailed error message, since string output is complicated with parallel_run... 
          [q,m,f,info,iter,time,mess]=optimizeNonStat_single(algorithm,xstart,xmin,xmax,C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,0);
        end
      // write best free fluxes set in original file ?
      end   
  else
    [q,m,f,info,iter,time,mess,ncallf]=optimizeNonStat_single(algorithm,xstart,xmin,xmax,C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,iprint);
  end

  mess.iter=iter(1);
  mess.time=time(1);
  infoStrings=['Normal termination'
  'Cannot find a feasible initial guess'
  'Cannot find a feasible initial guess'
  'Maximum number of iteration has been reached'
  'Step size smaller than machine precision'
  'Failure in constructing d0'
  'Failure in constructing d1'
  ''
  'New iterate is equal to previous iterate to machine precision'];
  if info($)==-1
    mess.info="Error during optimization"
  else
      mess.info=infoStrings(info($)+1);
  end
endfunction

function [q,m,f,info,iter,time,mess,ncallf]=optimizeNonStat_single(algorithm,xstart,xmin,xmax,C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,iprint)
    // w0=flux.values verify the equality constrains (CS) and the inequality
    // CS that why we reduce our problem by neglecting equality CS and working
    // with only inequality CS. In the other hand, we remove all evident 
    // inequality and zeros line of C(me+1:$,:)*W from inequality CS. 
    
    global lastobj iteration lastx message userstop fcount
    
    lastx=[]
    lastobj=%inf
    fcount=0
    
    nff=size(W,2)
    
    function [lubound,Cq,dq,nineq]=boundary(xmin,xmax,C,d,me,W,w0)
      Cineq=C(me+1:$,:); dineq=d(me+1:$);
      Cq=Cineq*W;
      dq=dineq-Cineq*w0;
      ineq_constr=find(clean(sum(abs(Cq),'col'))~=0);
      Cq=clean(Cq(ineq_constr,:));
      dq=clean(dq(ineq_constr,:));

      // detection of pure bound constraints to treat as such

      bc=[];
      for i=1:size(Cq,1);
          j=find(Cq(i,:));
          if length(j)==1
              bc=[bc i];
              if Cq(i,j)<0 then
                  xmin(j)=max(xmin(j),dq(i)/Cq(i,j));
              else
                  xmax(j)=min(xmax(j),dq(i)/Cq(i,j));
              end
          end
      end
      
      Cq(bc,:)=[];
      dq(bc)=[];

      nineq=size(Cq,1); // is the number of (non-trivial) linear inequalities

      Cq=[Cq zeros(nineq,length(xstart)-nff)];

      // xmin(size(W,2)+1:$)=0;
      lower=[xmin]; // lower bounds
      upper=[xmax]; // upper bounds 
      lubound=[lower,upper];
    endfunction


    [lubound,Cq,dq,nineq]=boundary(xmin,xmax,C,d,me,W,w0);
    if algorithm=='fsqp'

        nf=1; // is the number of cost function
        nineqn=0; // is the number of nonlinear inequalities
        neqn=0; // is the number of nonlinear equalities
        neq=0; // is the number of linear equalities
        modefsqp=110; // see documentation of fsqp
        ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,4*miter,0];
        bigbnd=1.e10; // play the role of infinity
        epsneq=0.e0; // Maximum violation of nonlinear equality constraints allowed 
                     // by the user at an optimal point
        udelta=0.e0; // The perturbation size the user suggests to use in 
                     // approximating gradients by finite difference
        rpar=[bigbnd,max(eps_grad,1e-15),epsneq,udelta];
        // the input _obj,_cntr,_grobj and _grcntr must be defined as a function
        // inform is a parameter indicating the status of the execution of fsqp
        // f is a value of the cost function at q at the end of the execution
        // lambda are the values of the Lagrange multipliers at q 
        // at the end of execution
        // g are the values of all constraints at q at the end of execution
    
        inform=-1;
        f=0;
        lastobj=%inf
        iteration=0
        userstop=-1
        lastq=[]
        
        tic();
        [x,info,f]=fsqp([xstart],ipar,rpar,lubound,_objNonStat,_cntrNonStat,_grobjNonStat,_grcntrNonStat);
        time=toc();
        ncallf=fcount
        if userstop>=0
          info=userstop
        end
        q=x(1:nff)
        m=x(nff+1:$)

    end
    
    iter=iteration
    mess=message
    
    if ~isempty(message)
      info=-1
      f=%nan
    else
      f=f-eps_reg*norm(x)^2;
    end
    
endfunction

function [value,gradient]=all_stuffNonStat(_x,_algorithm)

  global lastobj iteration lastx message userstop fcount
        
    //W=constraint.param.matrix;
    //w0=constraint.param.point;
    //
    //mmeas=configuration.pool.data;
    //Em=configuration.pool.matrix;
    //Smpm2=configuration.pool.stddev;
    //mmat=simulation.pool.matrix;
    //
    //wmeas=configuration.flux.data;
    //E=configuration.flux.matrix;
    //Svpm2=configuration.flux.stddev;
    //
    //eps_phi=simulation.eps.phi;
    //Xinp=configuration.input;
    //M=network.matrix;
    //b=network.vector;
    //cum=network.cumomer;

    fcount=fcount+1

    _q=_x(1:size(W,2));
    _m=_x(size(W,2)+1:$);

    // We build in the function all cost function and its gradient

    // Computation of pool size observation residual

    e_pool=Em*_m-mmeas;
    pool_error=Smpm2.*e_pool.^2;

    // Computation of a net/xch flux vector w=W*q+w0 using free fluxes.

    w=W*_q+w0;

		// Computation of flux observation residual

    e_flux=E*w-wmeas;
    flux_error=Svpm2.*e_flux.^2;

		// Computation of a fwd/rev flux vector

    [v,dv_dw]=Phi(w,eps_phi);

		// Solve the state equation and compute the label observation residual and gradient



      if argn(1)==1 // no gradient required
        [label_error,X,y,MList]=scheme.solveNS(v,_m,Xinp,M,b,cum,time_struct,pool_ind_weight,%t)
        value=sum(label_error)+sum(flux_error)+sum(pool_error)+eps_reg*norm(_x)^2;
      else  // cost and gradient  computation
        [label_error,gradq,gradm,X,y]=scheme.solveNSGrad(v,_m,Xinp,M,b,cum,time_struct,pool_ind_weight,dP_dpool,dP_dpool_t,dv_dw,W);
        value=sum(label_error)+sum(flux_error)+sum(pool_error)+eps_reg*norm(_x)^2;
        gradient=[gradq+2*W'*(E'*(Svpm2.*e_flux))
                  gradm+2*(Em'*(Smpm2.*e_pool))]+2*eps_reg*_x;
      end
      
      if value<=lastobj // Iteration report and stopping criterion
        lastobj=value;
        if iprint>0 & _algorithm=='fsqp' & lastx<>[]
            printf("iter=%5d, cost=%14.8e, errx=%14.8e\n",iteration,value,norm(_x-lastx));
        end
        if norm(_x-lastx)<eps_x
          gradient=%nan+zeros(_x)
          userstop=0          
        elseif iteration>=miter
          gradient=%nan+zeros(_x)
          userstop=3          
        end
        iteration=iteration+1;
      end
      lastx=_x
  


   
endfunction
