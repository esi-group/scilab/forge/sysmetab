function [t,y,h0,MList,Mass]=solveNSRKAdap(v,m,Xinp,M,b,cum,tmeas,pool_ind_weight,TOL,nt,newstepsize)
    //
    // Solve the state equation cascade, SDRIK scheme
    // we suppose that a(i,i)==gam, i=1...s and A_s=b (b is equal to the last line of A)
    // c vector of Butcher tableau is not used as the ode is autonomous
    //

    //Y=zeros(size(Xinp,1),50,size(scheme.a,1));
    X=zeros(size(Xinp,1),nt);

    gam=scheme.gamma
    alpha=scheme.alpha
    errc=scheme.err
    q=scheme.order
    s=size(alpha,1) // number of RK stages
    N=length(cum)
    nX=size(Xinp,1)
    //
    MList=list()
    MX=list()
    Mass=zeros(nX,1)
    err=zeros(1,nt-1)
    y=zeros(nX,length(tmeas))
    t=zeros(1,nt)

    for n=1:N    
      Mn_sp=sparse(M(n).ij,M(n).t*v(M(n).m1),M(n).size)
      MList(n)=struct('Mn',Mn_sp,'Bnv',b(n).t*spdiag(v(b(n).m3)),'MnImp_handle',[])
      Mass(cum(n))=m(pool_ind_weight(n))
    end

    Yi=Xinp  
    G=zeros(nX,1)    
    Z=zeros(nX,s)

    FACMAX=4 // parameters for step control mechanism
    FACMIN=1/100
    MINSTEP=1e-6
    tend=tmeas($)
    if ~exists("newstepsize","local") // newstepsize is the suggested value for the first step
       // estimation of initial stepsize based on the (q+1)th derivative of X1 at t=0
       // defaut mechanism can be quite conservative hence we put the factor length(y0)/nX on the mean
       y0=umfpack(MList(1).Mn,"\",-full(spset(b(1).value,b(1).t*(Xinp(b(1).m1).*Xinp(b(1).m2).*v(b(1).m3)))));
       m0=m(pool_ind_weight(1))
       m0(clean(m0)==0)=1
       for i=1:q+1
         y0=(MList(1).Mn*y0)./m0(pool_ind_weight(1))/i
       end
       newstepsize=(TOL/sqrt(mean(y0.^2)*length(y0)/nX))^(1/(q+1))
    end
    stepsize=%nan      
    
    X(:,1)=Xinp
        
    for k=1:nt-1 // loop on discrete time values

      Xk=Yi // Yi=X(:,k) of previous time step
      Yi=Xinp

      for n=1:N // computed once for all
        MX(n)=MList(n).Mn*Xk(cum(n))
      end
      
      while %t

        if newstepsize <> stepsize
          stepsize=newstepsize
          for n=1:N    
            MList(n).MnImp_handle=umf_lufact(sparse(spdiag(m(pool_ind_weight(n)))-stepsize*gam*MList(n).Mn))
          end
        end
        
        // rK stage 1
        for n=1:N // loop on cumomer weight -> solving cascade for Z1
          cumn=cum(n)
          MX(n)=MList(n).Mn*Xk(cumn)
          Z(cumn,1)=umf_lusolve(MList(n).MnImp_handle,stepsize*gam*(MX(n)+spset(b(n).value,MList(n).Bnv*(Yi(b(n).m1).*Yi(b(n).m2)))))
          Yi(cumn)=Xk(cumn)+Z(cumn,1)
        end
        //Y(:,k,1)=Yi        
      
        for i=2:s // loop on subsequent RK stages
          G=Mass.*(Z(:,1:i-1)*alpha(i,1:i-1)')
          Yi=Xinp   
          for n=1:N // loop on cumomer weight -> solving cascade for Zi
            cumn=cum(n)
            Z(cumn,i)=umf_lusolve(MList(n).MnImp_handle,G(cumn)+stepsize*gam*(MX(n)+spset(b(n).value,MList(n).Bnv*(Yi(b(n).m1).*Yi(b(n).m2)))))
            Yi(cumn)=Xk(cumn)+Z(cumn,i)
          end
          //Y(:,k,i)=Yi        
        end // loop on RK stage
              
        // step control mechanism (local truncation error is kept below TOL)
      
        err(k)=sqrt(mean((Z*errc).^2))
        FAC=max(FACMIN,(TOL/err(k))^(1/(q+1)))
        if FAC<1 
          newstepsize=0.5*FAC*stepsize
          printf("Rejected step at t=%f, FAC=%f\n",t(k),FAC)
          for n=1:N    
            umf_ludel(MList(n).MnImp_handle)
          end
          if newstepsize<MINSTEP
            error(sprintf("\nSysmetab error: singularity likely at t=%f, or --odetol (%e) too small\n\n",t(k),TOL))
          end
        else        
          if FAC>1.3 & (t(k)+2*stepsize<tend) // to prevent too frequent LU refactorization (it really helps!)
            newstepsize=1.1*FAC*stepsize
            for n=1:N    
              umf_ludel(MList(n).MnImp_handle)
            end
          end
          break // the while %t
        end 
        
      end // while step is not accepted

      X(:,k+1)=Yi // special case d=b*A^-1 and A_s=b (b is equal to the last line of A) hence d_s=1 and d_i=0, i<s
      t(k+1)=t(k)+stepsize
      
      kmeas=find(tmeas>=t(k) & tmeas<t(k+1)) // dense output computation
      if ~isempty(kmeas)
        theta=(tmeas(kmeas)-t(k))/stepsize
        y(:,kmeas)=repmat(Xk,1,length(kmeas))+Z*(scheme.d*cumprod(repmat(theta(:)',q,1),1))
      end
        
      if t(k+1)>tend
        break // the for k=0:nt-1
      end
      
    end // loop on discrete time values

    t=t(:,1:k+1)
    err=err(1:k)
    h0=t(2)-t(1) // initial stepsize for eventual reuse

    if umf_luinfo(MList(1).MnImp_handle)
      for n=1:N    
        umf_ludel(MList(n).MnImp_handle)
      end
    end
    
    if t(k+1)<tend
      error(sprintf("\nSysmetab error: increase --odetol (%e) or --steps (%d)\n\n",TOL,nt))
    else
      printf("Successful adaptation (%d steps) for odetol=%e\n",k,TOL)      
    end

endfunction


