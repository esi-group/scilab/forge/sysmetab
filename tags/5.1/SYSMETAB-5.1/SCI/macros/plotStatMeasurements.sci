function plotStatMeasurements(name,sysmetab)
  d=driver()
  driver pdf
  scf(0)
  plotInfo(name,sum(label_error)+sum(flux_error))
  xs2pdf(0,sprintf("%s00.pdf",name))
  for i=1:nb_group
      clf
      bar(1:length(meas_ind_group(i)),[ymeas(meas_ind_group(i)),label_meas.values(meas_ind_group(i))]/scale_opt.values(i))
      a=gca()
      a.sub_ticks=[0,1]
  //    a.x_ticks.labels=measurement_ids(meas_ind_group(i))
      str=tokens(group_textual(i),["[","]"])
      if size(str,"*")==3 // MS measurements
        head=tokens(group_textual(i),"#")
        title(sprintf("%s, residual=%f, scale=%f",head(1),sum(label_error(meas_ind_group(i))),scale_opt.values(i)))
        str=tokens(str(3),"M")
        str=tokens(str(2),",")
        for j=1:size(str,"*")
          a.x_ticks.labels(j)="M+"+str(j)
        end
      else // RMN or arbitraty labelling
        str=tokens(group_textual(i),[";"," ",","])
        head=tokens(str(1),"#")
        title(sprintf("%s, residual=%f, scale=%f",head(1),sum(label_error(meas_ind_group(i))),scale_opt.values(i)))
        for j=1:size(str,"*")
          label=tokens(str(j),"#")
          a.x_ticks.labels(j)=label(2) 
        end
        if size(str,"*")>=10
        a.font_size=.25
        end
      end  
      a.data_bounds(3:4)=[0 1]
      legend("measurement","simulation",-2)
      xs2pdf(0,sprintf("%s%02d.pdf",name,i))
  end
  for j=1:size(wmeas,1)
    clf
    wsim=E*(W*q+w0)
    bar(1,[wmeas(j) wsim(j)])
    a=gca()
    a.x_ticks.labels(1)=fluxmeasurement_textual(j)
    a.sub_ticks=[0,1]
    title(sprintf("flux measurement %s, residual=%e",fluxmeasurement_id(j),flux_error(j)))
    legend("measurement","simulation",-1)
    xs2pdf(0,sprintf("%s%02d.pdf",name,i+j))
  end
  driver(d)
  pdftk="pdftk"
  if unix_g("echo $(uname)")=="Darwin"
    pdftk=sysmetab+"/TOOLS/pdftk/bin/pdftk"
  end    
  unix_g(pdftk+" "+name+"??.pdf cat output "+name+".pdf")
  unix_g("rm "+name+"??.pdf")
endfunction

