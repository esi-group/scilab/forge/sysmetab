<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   Stéphane Mottelet
    Date    :   Wed Mar 21 11:13:38 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de générer un fichier XML avec une structure
    permettant de générer les équations dans un langage quelconque. Il résume les
    informations sur les différents flux (connus ou pas), et sur les espèces (connues
    ou pas + équations des bilans au format "Content MathML" :
    
    <carbon-labeling-system>
        <listOfReactions>
            <reaction id="re1" known="yes"/>
            .
            .
            .
        </listOfReactions>
        <listOfSpecies>
            <species id="s1" type="input" known="yes"/>
            <species id="s2" name="Glucose6P" type="intermediate">
                <equations>
                    <apply xmlns:m="http://www.w3.org/1998/Math/MathML">
                    .
                    .
                    .
                    </apply>
                    <apply xmlns:m="http://www.w3.org/1998/Math/MathML">
                    .
                    .
                    .
                    </apply>
                    .
                    .
                    .
                </equations>
            </species>
    </carbon-labeling-system>
    
-->

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"
    xmlns:math="http://exslt.org/math"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:f="http://www.13cflux.net/fluxml"
  exclude-result-prefixes="smtb sbml celldesigner m str exslt xhtml math xsl f">
       
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

    <xsl:param name="verbose">no</xsl:param>
    <xsl:param name="type">stationnaire</xsl:param>
    <xsl:variable name="maxweight" select="math:max(//smtb:cumomer-contribution/@weight)"/>
<!-- 
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>
-->
    <!-- find all pool -->
    <xsl:key name="POOL" match="f:pool" use="@id"/>
  <!-- find all f:reduct from f:reaction -->
  <xsl:key name="REDUCT" match="f:reaction/f:reduct" use="@id"/>

  <!-- find all f:rproduct from f:reaction -->
  <xsl:key name="RPRODUCT" match="f:reaction/f:rproduct" use="@id"/>

  <!-- find all smtb:cumomer from f:pool -->
  <xsl:key name="CUMOMERS" match="f:pool/smtb:cumomer" use="@id"/>
  
    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
          <xsl:apply-templates/>
    </xsl:template>


  <xsl:template match="f:fluxml">
    <carbon-labeling-system >
      <xsl:apply-templates select="f:reactionnetwork/smtb:listOfIntermediateCumomers"/>
    </carbon-labeling-system>
  </xsl:template>
  
    
  <xsl:template match="smtb:listOfIntermediateCumomers">
    <equations>
      <xsl:for-each select="smtb:listOfCumomers">
        <xsl:for-each select="smtb:cumomer">
          <equation weight="{@weight}" cumomer="{@id}">
            <xsl:call-template name="iteration"/>                                    
          </equation>
        </xsl:for-each>
      </xsl:for-each>
    </equations>
  </xsl:template>
   

<xsl:template name="bilan">

    <xsl:variable name="name" select="@id"/>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <cn>0</cn>
        <apply>
            <minus/>   

            <apply>
                <plus/>
                <xsl:for-each select="key('RPRODUCT',@id)">
                    <ci>
                        <xsl:value-of select="../@id"/>
                    </ci>
                </xsl:for-each>
            </apply>
            
            <xsl:call-template name="bilan-outflux">
                <xsl:with-param name="id" select="@id"/>
            </xsl:call-template>
                 
        </apply>
    
    </apply>

</xsl:template>

<xsl:template name="bilan-outflux">
    <xsl:param name="id"/>

   
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <plus/>
        <xsl:for-each select="key('REDUCT',$id)">
          <xsl:if test="not(key('POOL',../f:rproduct/@id)/@size='0')"> <!-- no outflux to pool with size=0 (simulation of dilution)-->
            <ci>
                <xsl:value-of select="../@id"/>
            </ci>
          </xsl:if>            
        </xsl:for-each>
    </apply>

</xsl:template>




<xsl:template name="iteration">

  <xsl:variable name="carbons">
    <xsl:copy-of select="key('CUMOMERS',@id)/smtb:carbon"/>
  </xsl:variable>
  
  <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <cn>0</cn>
      <apply>
          <minus/>   
            <apply>
                <plus/>
                <xsl:for-each select="key('RPRODUCT',@pool)"> 
                    <xsl:variable name="id" select="@id"/>
                    <xsl:variable name="occurrence" select="count(preceding-sibling::f:rproduct)+1"/>
                    <xsl:variable name="influx">
                      <xsl:for-each select="../f:reduct[smtb:carbon[(@id=$id) and (@occurrence=$occurrence)]]">
                        <xsl:variable name="somme">
                            <xsl:for-each select="smtb:carbon[(@id=$id) and (@occurrence=$occurrence)]">
                                <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                                    <token><xsl:value-of select="@position"/></token>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:variable>
                        <xsl:if test="sum(exslt:node-set($somme)/*)&gt;0">
                          <!-- We sum up the <token>, this is a way to know if there is at least one ... -->
                          <!-- We generate a new element <token> with a weight attribut specifying the weight of concerned 
                          cumomer (the number of <token> in $somme), the type of the concerned pool (intermediate or input) 
                          and its identifier, which is obtained by the sum of <token>. -->
                          <token weight="{count(exslt:node-set($somme)/*)}">
                            <xsl:value-of select="concat(@id,'_',sum(exslt:node-set($somme)/*))"/>
                          </token>
                        </xsl:if>
                      </xsl:for-each>
                    </xsl:variable>
                    <xsl:if test="(count(exslt:node-set($influx)/*)&gt;0)">
                      <apply>
                        <times/>
                        <xsl:for-each select="exslt:node-set($influx)/*">
                          <ci>
                            <xsl:value-of select="."/>
                          </ci>
                        </xsl:for-each>
                        <ci>
                          <xsl:value-of select="../@id"/>
                        </ci>
                      </apply>
                    </xsl:if>                    
                </xsl:for-each>
            </apply>

            <apply>
              <times/>
              <ci>
                   <xsl:value-of select="@id"/>
              </ci>

            <xsl:call-template name="bilan-outflux">
                <xsl:with-param name="id" select="@pool"/>
            </xsl:call-template>

            </apply>
        </apply>
        
    </apply>
    
</xsl:template>

  <xsl:template match="node()"/>

</xsl:stylesheet>
