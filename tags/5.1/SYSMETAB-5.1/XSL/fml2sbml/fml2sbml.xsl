<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
	Auteur  :   Stéphane Mottelet et Georges Sadaka
	Date    :   Mon Sep 30 15:00:00 CET 2013
	Projet  :   PIVERT
-->

<!-- 
	This stylesheet is intended to transform a fml file to a sbml file 
	by doing some transorfmation as in the sequel:
	
	
    1 - transform all f:pool in f:reactionnetwork/f:metabolitepools into 
    sbml:species in sbml:listOfSpecies, example for: 
		
    <f:metabolitepools>
      <pool atoms="2" id="A"/>
    </f:metabolitepools>
		
    this give :

    <sbml:listOfSpecies>
      <sbml:species compartment="default" id="A" name="A"/>
    </sbml:listOfSpecies>


    2 - verify for the existance of BIOMASS is a species which don't exists
    as f:rproduct in f:reaction and in this case add a "Aux" prefix to the 
    BIOMASS id in sbml:species and also add a sbml:speciesReference in 
    sbml:listOfProducts with the last id name, example for :
	
	<f:reaction id="v5">
      <f:reduct cfg="IJ" id="F"/>
    </f:reaction>
    
    this give :
    <sbml:listOfSpecies>
      <sbml:species compartment="default" id="FAux" name="FAux"/>
    </sbml:listOfSpecies>
    and
    <sbml:reaction id="v5" name="v5" reversible="false">
      <sbml:listOfReactants>
        <sbml:speciesReference species="F"/>
      </sbml:listOfReactants>
      <sbml:listOfProducts>
        <sbml:speciesReference species="FAux"/>
      </sbml:listOfProducts>
    </sbml:reaction>  


    3 - verify for the existance of f:reaction/f:reduct or f:rproduct 
    in f:reactionnetwork and in this case add a sbml:reaction/
    sbml:listOfReactants or sbml:listOfProducts to the sbml:listOfReactions,
    and put the f:reduct/@cfg or f:rproduct/@cfg in the notes of the 
    sbml:reaction, example for:
    
    <f:reactionnetwork>  
      <f:reaction id="v6">
        <f:reduct cfg="IJ" id="A_out"/>
        <f:rproduct cfg="IJ" id="A"/>
      </f:reaction>
    <f:reactionnetwork>
    
    this give:
    <sbml:listOfReactions>
      <sbml:reaction id="v6" name="v6" reversible="false">
        <sbml:notes>
          <xhtml:html xmlns="http://www.w3.org/1999/xhtml">
            <xhtml:body>IJ &gt; IJ</xhtml:body>
          </xhtml:html>
        </sbml:notes>
        <sbml:listOfReactants>
          <sbml:speciesReference species="A_out"/>
        </sbml:listOfReactants>
        <sbml:listOfProducts>
          <sbml:speciesReference species="A"/>
        </sbml:listOfProducts>
      </sbml:reaction>
    </sbml:listOfReactions>


    4 - verify for the existance of f:constraints/f:xch and in this case 
    add a <<reversible="false">> to the corresponding sbml:reaction, 
    example for:
    
    <f:constraints>
      <f:xch>
        <m:math xmlns="http://www.w3.org/1998/Math/MathML">
          <m:apply>
            <m:eq/>
            <m:ci>v5</m:ci>
            <m:cn>0</m:cn>
          </m:apply>
        </m:math>
      </f:xch>
    </f:constraints>
    
    this give :
    <sbml:reaction id="v5" name="v5" reversible="false"/>
    
    
    5 - verify for the existance of f:constraints/f:net and in this case 
    add a sbml:listOfRules/sbml:algebraicRule to the sbml:model, example for:
    
    <f:constraints>
      <f:net>
        <m:math xmlns="http://www.w3.org/1998/Math/MathML">
          <m:apply>
            <m:eq/>
            <m:apply>
              <m:minus/>
              <m:apply>
                <m:plus/>
                <m:ci>v1</m:ci>
                <m:ci>v2</m:ci>
              </m:apply>
              <m:ci> v4 </m:ci>
            </m:apply>  
            <m:cn>0</m:cn>
          </m:apply>  
        </m:math>
      </f:net>  
    </f:constraints>
    which means v1+v2-v4=0
    
    this give :
    
    <sbml:listOfRules>
      <sbml:algebraicRule>
        <m:math xmlns="http://www.w3.org/1998/Math/MathML">
          <m:apply>
            <m:minus/>
            <m:apply>
              <m:plus/>
              <m:ci> v1 </m:ci>
              <m:ci> v2 </m:ci>
            </m:apply>
            <m:ci> v4 </m:ci>
          </m:apply>
        </m:math>
      </sbml:algebraicRule>
    </sbml:listOfRules>
    which means v1+v2-v4
    
    Note that if we have a non zero constant after the equality for example
    v1+v2-v4=0.2 it will transfer to v1+v2-v4-0.2 such as
    
    <f:constraints>
      <f:net>
        <m:math xmlns="http://www.w3.org/1998/Math/MathML">
          <m:apply>
            <m:eq/>
            <m:apply>
              <m:minus/>
              <m:apply>
                <m:plus/>
                <m:ci>v1</m:ci>
                <m:ci>v2</m:ci>
              </m:apply>
              <m:ci> v4 </m:ci>
            </m:apply>  
            <m:cn>0.2</m:cn>
          </m:apply>  
        </m:math>
      </f:net>  
    </f:constraints>
    
    to
    
    <sbml:listOfRules>
      <sbml:algebraicRule>
        <m:math xmlns="http://www.w3.org/1998/Math/MathML">
          <m:apply>
            <m:minus/>
            <m:apply>
              <m:minus/>
              <m:apply>
                <m:plus/>
                <m:ci> v1 </m:ci>
                <m:ci> v2 </m:ci>
              </m:apply>
              <m:ci> v4 </m:ci>
            </m:apply>              
            <m:cn>0.2</m:cn>
          </m:apply>
        </m:math>
      </sbml:algebraicRule>
    </sbml:listOfRules>
    
    
    6 - verify for the existance of f:configuration/f:input and in this case 
    add a sbml:notes to the sbml:species in the sbml:listOfSpecies using 
    the value of f:label/@cfg, example for:
    
    <f:configuration name="default" stationary="true">
      <f:input pool="A_out" type="isotopomer">
        <f:label cfg="10"/>
        <f:label cfg="01"/>
        <f:label cfg="11"/>
      </f:input>
    </f:configuration>
    
    this give :
    
    <smbl:species compartment="default" id="A_out" name="A_out">
      <sbml:notes>
        <xhtml:html xmlns="http://www.w3.org/1999/xhtml">
          <xhtml:body>
            LABEL_INPUT 01,10,11
          </xhtml:body>
        </xhtml:html>
      </sbml:notes>
    </sbml:species>
  
  
    7 - verify for the existance of f:labelingmeasurement in 
    f:measurement/f:model and in this case add a sbml:notes to the sbml:species
    in the sbml:listOfSpecies, here we must note that there is two kind of 
    f:labelingmeasurement the first is "F[2]#M1" which is also called by 
    Mass Stocheometry and the second is "F#1x" which is called Label 
    Measurement and to transform them inside the notes we use here two method
    by defining at the begining a parameter "MS2LM" which can have two 
    value the first is yes if we want to transform the MS to LM and the 
    second is no if we want only to copy the MS in the notes and not
    transform the MS to LM. By default, " MS2LM = no " and we can change 
    this parameter either at the begining of this script either by using
    the command line "xsltproc -stringparam MS2LM no fml2sbml.xsl toto.fml"
    in a terminal. For example, if we have :
    
    <f:measurement>
      <f:model>
        <f:labelingmeasurement>
          <f:group id="F_10" scale="auto">
            <f:textual>F#1x</f:textual>
          </f:group>
          <f:group id="F_01" scale="auto">
            <f:textual>F[2]#M1</f:textual>
          </f:group>
          <f:group id="F_11" scale="auto">
            <f:textual>F[1,2]#M2</f:textual>
          </f:group>
        </f:labelingmeasurement>
      </f:model>
    </f:measurement>
    
    this give, if " MS2LM = yes "
    <sbml:species compartment="default" id="F" name="F">
      <sbml:notes>
        <xhtml:html xmlns="http://www.w3.org/1999/xhtml">
          <xhtml:body>
            LABEL_MEASUREMENT 1x;x1;11
          </xhtml:body>
        </xhtml:html>
      </sbml:notes>
    </sbml:species>
    and if " MS2LM = no "
    <sbml:species compartment="default" id="F" name="F">
      <sbml:notes>
        <xhtml:html xmlns="http://www.w3.org/1999/xhtml">
          <xhtml:body>
            LABEL_MEASUREMENT 1x;[2]#M1;[1,2]#M0,1,2
          </xhtml:body>
        </xhtml:html>
      </sbml:notes>
    </sbml:species>
      
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:math="http://exslt.org/math"
  xmlns:exslt="http://exslt.org/common"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"    
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:f="http://www.13cflux.net/fluxml"
  exclude-result-prefixes="xsl xhtml str sbml celldesigner f m math exslt">
  
  <xsl:param name="MS2LM">no</xsl:param>

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <xsl:strip-space elements="*"/> <!-- preserve the white space-->

  <!-- find all irreversible reaction from f:constraints/f:xch -->
  <xsl:key name="IRREVERSIBLE" match="m:apply[(m:eq) and (m:cn=0)]" use="m:ci"/>

  <!-- find all label input from f:configuration -->
  <xsl:key name="INPUT" match="f:input" use="@pool"/>
  
  <!-- find all Label Measurement (Mass Stocheometry) from f:configuration -->
  <xsl:key name="MEASUREMENTMS" match="f:labelingmeasurement/f:group/f:textual" use="substring-before(.,'[')"/>  

  <!-- find all Label Measurement (LM) from f:configuration -->
  <xsl:key name="MEASUREMENTLM" match="f:labelingmeasurement/f:group/f:textual" use="substring-before(.,'#')"/>  

  <xsl:variable name="maxAtoms" select="math:max(//f:pool/@atoms)"/>

  <xsl:variable name="list">
    <xsl:for-each select="str:split(str:padding($maxAtoms,'.'),'')">  
      <!--a way to make a loop from 1 to $maxAtoms-->
      <listOfIsotopomers atoms="{position()}">
        <xsl:call-template name="iteration-cumomers">
          <xsl:with-param name="max" select="math:power(2,position())-1"/>
          <xsl:with-param name="number" select="str:padding(position(),'0')"/>
        </xsl:call-template>
      </listOfIsotopomers>
    </xsl:for-each>
  </xsl:variable> 

  <xsl:template match="/">
     <xsl:apply-templates/>
  </xsl:template>

  <!-- create model id of the document -->
  <xsl:template match="f:fluxml">
    <sbml xmlns="http://www.sbml.org/sbml/level2/version4" >     
      <model id="{f:info/f:name}">
        <xsl:apply-templates/>
      </model>
    </sbml>
  </xsl:template>

  <!-- creation of algebric rule from f:constraints/f:net -->
  <xsl:template match="f:reactionnetwork">
    <xsl:apply-templates select="f:metabolitepools"/>
    <listOfRules xmlns="http://www.sbml.org/sbml/level2/version4">
      <xsl:for-each select="../f:constraints/f:net/m:math/m:apply[m:cn[normalize-space(.)!='0']]"> 
        <algebraicRule xmlns="http://www.sbml.org/sbml/level2/version4">
          <math xmlns="http://www.w3.org/1998/Math/MathML">
            <apply>
              <minus/>
              <xsl:copy-of select="*[2]"/>
              <xsl:copy-of select="*[3]"/>
            </apply> 
          </math> 
        </algebraicRule>  
      </xsl:for-each>
      <xsl:for-each select="../f:constraints/f:net/m:math/m:apply[(m:eq) and (m:cn[normalize-space(.)='0'])]"> 
        <algebraicRule xmlns="http://www.sbml.org/sbml/level2/version4">
          <math xmlns="http://www.w3.org/1998/Math/MathML">
            <apply>
              <xsl:copy-of select="m:apply/*"/>
            </apply>  
          </math> 
        </algebraicRule>  
      </xsl:for-each>  
    </listOfRules>
    <listOfReactions xmlns="http://www.sbml.org/sbml/level2/version4" >
      <xsl:apply-templates select="f:reaction"/>
    </listOfReactions>
  </xsl:template>

  <!-- creation of List of species and adding the BIOMASS species -->
  <xsl:template match="f:metabolitepools">
    <listOfSpecies xmlns="http://www.sbml.org/sbml/level2/version4" >
      <xsl:for-each select="../f:reaction[not(f:rproduct)]">
        <species compartment="default" id="{concat(f:reduct/@id,'Aux')}" name="{concat(f:reduct/@id,'Aux')}"/>
      </xsl:for-each>
      <xsl:apply-templates/>
    </listOfSpecies>  
  </xsl:template>

  <!-- creation of Label Input and Label Measurement -->
  <xsl:template match="f:pool">
    <species compartment="default" id="{@id}" name="{@id}" xmlns="http://www.sbml.org/sbml/level2/version4">
      <xsl:variable name="ID" select="@id"/> 
      <xsl:choose>
        <xsl:when test="key('INPUT',@id)">        
          <notes>
            <html xmlns="http://www.w3.org/1999/xhtml">
              <body>
                <xsl:text>LABEL_INPUT </xsl:text>
                <xsl:for-each select="key('INPUT',@id)/f:label">
                  <xsl:value-of select="@cfg"/>
                  <xsl:if test="position()&lt;last()">
                    <xsl:text>,</xsl:text>
                  </xsl:if>
                </xsl:for-each>
              </body>
            </html>  
          </notes>
        </xsl:when>
        <xsl:when test="key('MEASUREMENTMS',@id)|key('MEASUREMENTLM',@id)">
          <notes>
            <html xmlns="http://www.w3.org/1999/xhtml">
              <body>
                <xsl:text>LABEL_MEASUREMENT </xsl:text>
                <!-- if $MS2LM = yes the Mass spectometry will transform to Label Mesurement -->
                <xsl:if test="$MS2LM='yes'">
                  <xsl:for-each select="key('MEASUREMENTLM',@id)">
                    <xsl:value-of select="substring-after(.,'#')"/> 
                    <xsl:if test="position()&lt;last()">
                      <xsl:value-of select="';'"/>
                    </xsl:if>
                  </xsl:for-each>
                  <xsl:if test="key('MEASUREMENTMS',@id) and key('MEASUREMENTLM',@id)">
                    <xsl:value-of select="';'"/>
                  </xsl:if>  
                  <xsl:variable name="atoms" select="@atoms"/>  
                  <xsl:for-each select="key('MEASUREMENTMS',@id)">
                    <xsl:variable name="measurement" select="."/>
                    <xsl:variable name="fragment" select="str:split(substring-before(substring-after($measurement,'['),']'),',')"/>
                    <xsl:variable name="before" select="str:padding($fragment[1]-1,'x')"/>
                    <xsl:variable name="after" select="str:padding(($atoms)-$fragment[last()],'x')"/>
                    <xsl:for-each select="str:split(substring-after($measurement,'#M'),',')"> 
                    <xsl:for-each select="exslt:node-set($list)/listOfIsotopomers[@atoms=count($fragment)]/isotopomer[@weight=current()]">
                      <xsl:value-of select="concat($before,@pattern,$after)"/>
                      <xsl:if test="position()&lt;last()">
                        <xsl:value-of select="'+'"/>
                      </xsl:if>
                    </xsl:for-each>
                      <xsl:if test="position()&lt;last()">
                        <xsl:value-of select="';'"/>
                      </xsl:if>
                    </xsl:for-each>
                    <xsl:if test="position()&lt;last()">
                      <xsl:text>;</xsl:text>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:if>
                <!-- if $MS2LM = no the Mass spectometry will be copied in the Label Mesurement -->
                <xsl:if test="$MS2LM='no'">
                  <xsl:for-each select="key('MEASUREMENTLM',@id)">
                    <xsl:value-of select="substring-after(.,'#')"/> 
                    <xsl:if test="position()&lt;last()">
                      <xsl:value-of select="';'"/>
                    </xsl:if>
                  </xsl:for-each>
                  <xsl:if test="key('MEASUREMENTMS',@id) and key('MEASUREMENTLM',@id)">
                    <xsl:value-of select="';'"/>
                  </xsl:if>  
                  <xsl:for-each select="key('MEASUREMENTMS',@id)">
                    <xsl:value-of select="substring-after(.,$ID)"/> 
                    <xsl:if test="position()&lt;last()">
                      <xsl:value-of select="';'"/>
                    </xsl:if>
                  </xsl:for-each>  
                </xsl:if>  
              </body>
            </html>
          </notes>  
        </xsl:when>  
      </xsl:choose>  
    </species>
  </xsl:template>

  <!-- add notes and reversibility for each species and add a speciesReference for BIOMASS -->
  <xsl:template match="f:reaction">
    <reaction id="{@id}" name="{@id}" xmlns="http://www.sbml.org/sbml/level2/version4" >
      <xsl:if test="key('IRREVERSIBLE',@id) or not(f:rproduct) or (key('INPUT',f:reduct[1]/@id))">
        <xsl:attribute name="reversible">false</xsl:attribute>
      </xsl:if>
      <notes>
        <html xmlns="http://www.w3.org/1999/xhtml">
          <body>
            <xsl:for-each select="f:reduct">
              <xsl:value-of select="@cfg"/>
              <xsl:if test="position()&lt;last()">
                <xsl:text>+</xsl:text>
              </xsl:if>
            </xsl:for-each>
            <xsl:text> &gt; </xsl:text>
	        <xsl:if test="not(f:rproduct)">
              <xsl:value-of select="f:reduct/@cfg"/>
            </xsl:if>
            <xsl:for-each select="f:rproduct">
              <xsl:value-of select="@cfg"/>
              <xsl:if test="position()&lt;last()">
                <xsl:text>+</xsl:text>
              </xsl:if>
            </xsl:for-each>
          </body>
        </html>
      </notes>
      <listOfReactants>
        <xsl:apply-templates select="f:reduct"/>
      </listOfReactants>
      <listOfProducts>
        <xsl:if test="not(f:rproduct)">
          <speciesReference species="{concat(f:reduct/@id,'Aux')}"/>
        </xsl:if>
        <xsl:apply-templates select="f:rproduct"/>
      </listOfProducts>
    </reaction>  
  </xsl:template>

  <!-- create speciesReference -->
  <xsl:template match="f:reduct|f:rproduct">
     <speciesReference species="{@id}" xmlns="http://www.sbml.org/sbml/level2/version4" />
  </xsl:template>

  <xsl:template name="iteration-cumomers">        
    <xsl:param name="max"/>
    <xsl:param name="i" select="0"/>
    <xsl:param name="number"/>      
    <xsl:if test="$i&lt;=$max">
      <isotopomer weight="{string-length(translate($number,'0',''))}" pattern="{$number}"/>
      <xsl:call-template name="iteration-cumomers">
        <xsl:with-param name="max" select="$max"/>
        <xsl:with-param name="i" select="($i)+1"/>
        <xsl:with-param name="number">
          <xsl:call-template name="ripple-carry">  
            <xsl:with-param name="number" select="$number"/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="ripple-carry">  
    <xsl:param name="k" select="0"/>
    <xsl:param name="number"/>
    <xsl:variable name="fragment" select="substring($number,1,string-length($number)-($k)-1)"/>
    <xsl:choose>
      <xsl:when test="substring($number,string-length($number)-$k,1)='1'">
        <xsl:call-template name="ripple-carry">
          <xsl:with-param name="k" select="($k)+1"/>
          <xsl:with-param name="number" select="concat($fragment,str:padding(($k)+1,'0'))"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat($fragment,'1',str:padding($k,'0'))"/> <!-- fin de la propagation-->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="node()"/>  

</xsl:stylesheet>