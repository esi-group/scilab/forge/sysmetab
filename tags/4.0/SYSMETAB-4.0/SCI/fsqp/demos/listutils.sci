
function [nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0)
//                  OBJECTIVES

//functions f_i(x), F_i(x) and grf_i(x) grF_i(x).
//Each f_i, F_i should return a column vector and gr_fi, gr_Fi a matrix
//with nparam columns and same row dim as the corresponding f_i or F_i.
//(f_i may represent several regular objectives stacked together).

//lists of objectives should be: (use empty list when necessary: list() )
//list_obj=list(...
//               list(f_1,...,f_nf0),...     //regular objectives
//		 list(F_1,...,F_nfsr)        //sr objectives
//              )

//list of gradients:
//list_grobj=list(...
//               list(grf_1,...,grf_nf0),...  //grad of regular objectives
//		 list(grF_1,...,grF_nfsr)     //grad of sr objectives
//              )

  nf0=length(list_obj(1));
  nfsr=length(list_obj(2));
  nb_reg_obj=0;
  for k=1:nf0
    f_k=null();
    f_k=list_obj(1)(k);
    nb_reg_obj=nb_reg_obj+size(f_k(x0),1);
  end
  nf=nb_reg_obj+nfsr;

  mesh_pts=[];
  for k=1:nfsr
    F_k=null();
    F_k=list_obj(2)(k);
    mesh_pts=[mesh_pts,size(F_k(x0),1)];
  end

  //                    CONSTRAINTS

  //Six types of fonctions: g_i(x), G_i(x), c_i(x), C_i(x), h_i(x) and A_i(x)
  //which each return a column vector of constraints.
  //Regular constraints can be stacked together in a single column vector 
  //ie always possible to use just one vector valued g_, (c_, h_, A_) function.  
  //list_cntr=list(...
  //		list(g_1,...,g_ng0),...     //regular nonlinear inequality
  //              list(G_1,...,G_ncsrn),...   //sr      nonlinear inequality
  //              list(c_1,...,c_nc0),...     //regular linear    inequality
  //              list(C_1,...,C_ncsrl),...   //sr      linear    inequality
  //              list(h_1,...,h_nh0),...     //nonlinear         equality
  //              list(A_1,...,A_na0)         //linear            equality
  //              )

  //Define also the list of gradient functions. Each output of gr-function
  //is a matrix with same number of rows as the corresponding function
  //and nparam (=dim(x)) columns.

  //list_grcn=list(...
  //      list(grg_1,...,grg_ng0),...     //grad regular nonlinear inequality
  //      list(grG_1,...,grG_ncsrn),...   //grad sr      nonlinear inequality
  //      list(grc_1,...,grc_nc0),...     //grad regular linear    inequality
  //      list(grC_1,...,grC_ncsrl),...   //grad sr      linear    inequality
  //      list(grh_1,...,grh_nh0),...     //grad nonlinear         equality
  //      list(grA_1,...,grA_na0)         //grad linear            equality
  //              )

  ng0=length(list_cntr(1));
  ncsrn=length(list_cntr(2));

  nb_reg_nonlin_ineq=0;
  for k=1:ng0;
    g_k=null();
    g_k=list_cntr(1)(k);
    nb_reg_nonlin_ineq=nb_reg_nonlin_ineq+size(g_k(x0),1);
  end

  for k=1:ncsrn
    G_k=null();
    G_k=list_cntr(2)(k);
    mesh_pts=[mesh_pts,size(G_k(x0),1)];
  end

  nineqn=ncsrn+nb_reg_nonlin_ineq;

  nc0=length(list_cntr(3));
  ncsrl=length(list_cntr(4));

  nb_reg_lin_ineq=0;
  for k=1:nc0;
    c_k=null();
    c_k=list_cntr(3)(k);
    nb_reg_lin_ineq=nb_reg_lin_ineq+size(c_k(x0),1);
  end

  nineq=nineqn+ncsrl+nb_reg_lin_ineq;

  for k=1:ncsrl
    C_k=null();
    C_k=list_cntr(4)(k);
    mesh_pts=[mesh_pts,size(C_k(x0),1)];
  end
  nh0=length(list_cntr(5));

  neqn=0;
  for k=1:nh0;
    h_k=null();
    h_k=list_cntr(5)(k);
    neqn=neqn+size(h_k(x0),1);
  end

  nb_lin_eq=0
  na0=length(list_cntr(6));
  for k=1:na0
    A_k=null();
    A_k=list_cntr(6)(k);
    nb_lin_eq=nb_lin_eq+size(A_k(x0),1);
  end
  neq=neqn+nb_lin_eq;
endfunction


//ipar=[nf,nineqn,nineq,neqn,neq,...];
//srpar=[nfsr,ncsrl,ncsrn];


function f=allobj(x)
  f=[];
  for k=1:nf0;
    f_k=null();
    f_k=list_obj(1)(k);
    f=[f;f_k(x)];
  end
  for k=1:nfsr;
    F_k=null();
    F_k=list_obj(2)(k)
    f=[f;F_k(x)];
  end
endfunction



function ct=allcntr(x)
  ct=[];
  for k=1:ng0;
    g_k=null();
    g_k=list_cntr(1)(k);
    ct=[ct;g_k(x)];
  end
  for k=1:ncsrn
    G_k=null();
    G_k=list_cntr(2)(k);
    ct=[ct;G_k(x)];
  end
  for k=1:nc0
    c_k=null();
    c_k=list_cntr(3)(k);
    ct=[ct;c_k(x)];
  end
  for k=1:ncsrl
    C_k=null();
    C_k=list_cntr(4)(k);
    ct=[ct;C_k(x)];
  end
  for k=1:nh0
    h_k=null();
    h_k=list_cntr(5)(k);
    ct=[ct;h_k(x)];
  end
  for k=1:na0
    A_k=null();
    A_k=list_cntr(6)(k);
    ct=[ct;A_k(x)];
  end
endfunction



function fp=allgrob(x)
  fp=[];
  for k=1:nf0;
    grf_k=null();
    grf_k=list_grobj(1)(k);
    fp=[fp;grf_k(x)];
  end
  for k=1:nfsr;
    grF_k=null();
    grF_k=list_grobj(2)(k);
    fp=[fp;grF_k(x)];
  end
endfunction




function ctp=allgrcn(x)
  ctp=[];
  for k=1:ng0;
    grg_k=null();
    grg_k=list_grcn(1)(k);
    ctp=[ctp;grg_k(x)];
  end
  for k=1:ncsrn
    grG_k=null();
    grG_k=list_grcn(2)(k);
    ctp=[ctp;grG_k(x)];
  end
  for k=1:nc0
    grc_k=null();
    grc_k=list_grcn(3)(k);
    ctp=[ctp;grc_k(x)];
  end
  for k=1:ncsrl
    grC_k=null();
    grC_k=list_grcn(4)(k);
    ctp=[ctp;grC_k(x)];
  end
  for k=1:nh0
    grh_k=null();
    grh_k=list_grcn(5)(k);
    ctp=[ctp;grh_k(x)];
  end
  for k=1:na0
    grA_k=null();
    grA_k=list_grcn(6)(k);
    ctp=[ctp;grA_k(x)];
  end
endfunction



function fj=obj(j,x)
  if x_is_new() then
    //    Common part  
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    //
    fj=all_objectives(j);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    fj=all_objectives(j);
  end
endfunction




function cj=cntr(j,x)
  if x_is_new() then
    //    Common part
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    //
    cj=all_constraints(j);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    cj=all_constraints(j);
  end
endfunction



function gj=grob(j,x)
  if x_is_new() then
    //    Common part
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    //
    gj=all_gradobjectives(j,:);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    gj=all_gradobjectives(j,:);
  end
endfunction



function gj=grcn(j,x)
  if x_is_new() then
    //    Common part
    all_objectives=allobj(x);
    all_constraints=allcntr(x);
    all_gradobjectives=allgrob(x);
    all_gradconstraints=allgrcn(x);
    //
    gj=all_gradconstraints(j,:);
    set_x_is_new(0);  //Use resume to define global Scilab variables
    [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
	resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
  else
    gj=all_gradconstraints(j,:);
  end
endfunction


