mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 4 of the fsqp documentation

// sr-fsqp with C functions
//=======================================================
exec(get_absolute_file_path('example4.3.sce')+'example4-src/loader.sce') 
r=100;
t1=%pi*0.025*((1:r)-1)';
t2=%pi*0.025*((1:r)-1)';
t3=%pi*0.25*(1.2+0.2*((1:1.5*r)-1))';
t=[t1;t2;t3];

modefsqp=100;
iprint=1;
miter=500;
bigbnd=1.e10;
eps=1.e-7;
epsneq=0.e0;
nf=1;
neqn=0;
nineqn=3;nineq=3;ncsrn=3;
ncsrl=0;
r=100;
mesh_pts=[r,r,3*r/2];
neq=0;nfsr=0;

x0=[0.1*ones(9,1);1];
bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
bigbnd=1.e10; eps=1.e-8; epsneq=0.e0; udelta=0.e0;
 
udelta=0.00001;
rpar=[bigbnd,eps,epsneq,udelta];

srpar=[nfsr,ncsrl,ncsrn];

//Constraints evaluated by scilab fct cntr1
function ztx=z(t,x)
  k=1:9;
  tk=t*k;
  ztx=(cos(tk)*x(k))^2+(sin(tk)*x(k))^2
endfunction
function gj=cntr1(j,x)
  if j<= r
    gj=(1-x(10))^2-z(t(j),x);
  elseif j<= 2*r 
    gj=z(t(j),x)-(1+x(10))^2;
  elseif j<= 3.5*r
    gj=z(t(j),x)-(x(10))^2;
  end
endfunction

x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],'obj',cntr1,'grob','grcnfd')
