Sysmetab Version 4.0 (Wed Jul 22 10:21:55 CEST 2015)

Sysmetab is an integrated software solving 13C metabolic flux identification problem, with automated Scilab code generation using XML/XSL technology.

Please see the project page at 

http://forge.scilab.org/index.php/p/sysmetab/

for more information.
