<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   2013/2014
    Project :   PIVERT/Metalippro-PL1
-->

<!-- This style sheet contains functions used for the generation of the isotopomer and cumomer list for each
     pool, for example for the pool A, we obtain :

     <pool atoms="2" id="A" type="intermediate">
       <cumomer xmlns="http://www.utc.fr/sysmetab" id="A_1" pool="A" subscript="1" weight="1" pattern="x1">
         <carbon position="1" index="0"/>
       </cumomer>
       <cumomer xmlns="http://www.utc.fr/sysmetab" id="A_2" pool="A" subscript="2" weight="1" pattern="1x">
         <carbon position="2" index="1"/>
       </cumomer>
       <cumomer xmlns="http://www.utc.fr/sysmetab" id="A_3" pool="A" subscript="3" weight="2" pattern="11">
         <carbon position="1" index="0"/>
         <carbon position="2" index="1"/>
       </cumomer>
     </pool>
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:str="http://exslt.org/strings"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl str smtb math f exslt">

  <!-- Maximum weight of all cumomers inside the network -->
  <xsl:variable name="maxweight" select="math:max(//f:pool/@atoms)"/>

  <xsl:variable name="list">
    <!--a way to make a loop from 1 to $maxweight-->
    <xsl:for-each select="str:split(str:padding($maxweight,'.'),'')">
      <listOfCumomers atoms="{position()}">
        <xsl:call-template name="iteration-cumomers-string">
          <xsl:with-param name="max" select="math:power(2,position())-1"/>
          <xsl:with-param name="number" select="str:padding(position(),'x')"/>
        </xsl:call-template>
      </listOfCumomers>
    </xsl:for-each>
  </xsl:variable> 

  <!-- Computation of binomial coefficients, which are used to compute the cumomer contribution -->
  <xsl:variable name="Cnp">
    <xsl:call-template name="iteration-Cnp">
      <xsl:with-param name="max" select="$maxweight"/>
      <xsl:with-param name="coeffs">
        <c n="0" p="0">1</c>
      </xsl:with-param>    
    </xsl:call-template>
  </xsl:variable> 

  <xsl:template name="iteration-Cnp">        
    <xsl:param name="max"/>
    <xsl:param name="n" select="'1'"/>
    <xsl:param name="coeffs"/>      
    <xsl:copy-of select="$coeffs"/>
    <xsl:if test="$n&lt;=$max">
      <xsl:call-template name="iteration-Cnp">
        <xsl:with-param name="max" select="$max"/>
        <xsl:with-param name="n" select="($n)+1"/>
        <xsl:with-param name="coeffs">
          <c n="{$n}" p="0">1</c>
            <xsl:for-each select="str:split(str:padding(($n)-1,'.'),'')">
              <c n="{$n}" p="{position()}">
                <xsl:variable name="p" select="position()"/>
                <xsl:value-of select="exslt:node-set($coeffs)/c[$p]+exslt:node-set($coeffs)/c[1+$p]"/>
              </c>
            </xsl:for-each>
          <c n="{$n}" p="{$n}">1</c>
       </xsl:with-param>
      </xsl:call-template> 
    </xsl:if>
  </xsl:template>

  <!-- These two templates allows to generate a list (see the "list" global variable above) of cumomers
       of size 1 to N where N is the maximum number of carbon atoms among all molecules in the network. -->
  <xsl:template name="iteration-cumomers-string">        
    <xsl:param name="max"/>
    <xsl:param name="i" select="0"/>
    <xsl:param name="number"/>      
    <xsl:if test="$i&lt;=$max">
      <cumomer number="{$i}" pattern="{$number}" weight="{string-length(translate($number,'x',''))}" />
      <xsl:call-template name="iteration-cumomers-string">
        <xsl:with-param name="max" select="$max"/>
        <xsl:with-param name="i" select="($i)+1"/>
        <xsl:with-param name="number">
          <xsl:call-template name="ripple-carry">  
            <xsl:with-param name="number" select="$number"/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="ripple-carry">  
    <xsl:param name="k" select="0"/>
    <xsl:param name="number"/>
    <xsl:variable name="fragment" select="substring($number,1,string-length($number)-($k)-1)"/>
    <xsl:choose>
      <xsl:when test="substring($number,string-length($number)-$k,1)='1'">
        <xsl:call-template name="ripple-carry">
          <xsl:with-param name="k" select="($k)+1"/>
          <xsl:with-param name="number" select="concat($fragment,str:padding(($k)+1,'x'))"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat($fragment,'1',str:padding($k,'x'))"/> <!-- the end of the propagation -->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- This template allows to express label measurements as linear combination of cumomers. A brute force 
       (but efficient) method is used. -->
  <xsl:template name="disassemble-cumomer">
    <xsl:param name="string"/>
    <xsl:param name="i" select="string-length($string)"/>
    <xsl:param name="id" select="'0'"/>
    <xsl:param name="sign" select="'1'"/>
    <xsl:choose>
      <xsl:when test="$i=0">
        <cumomer-contribution subscript="{$id}" string="{$string}" weight="{string-length(translate($string,'x',''))}"  sign="{$sign}" xmlns="http://www.utc.fr/sysmetab"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="substring($string,$i,1)='x'">
            <xsl:call-template name="disassemble-cumomer">
              <xsl:with-param name="i" select="($i)-1"/>
              <xsl:with-param name="string" select="$string"/>
              <xsl:with-param name="id" select="$id"/>
              <xsl:with-param name="sign" select="$sign"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="substring($string,$i,1)='1'">
            <xsl:call-template name="disassemble-cumomer">
              <xsl:with-param name="i" select="($i)-1"/>
              <xsl:with-param name="string" select="$string"/>
              <xsl:with-param name="id" select="$id + math:power(2,string-length($string)-$i)"/>
              <xsl:with-param name="sign" select="$sign"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="substring($string,$i,1)='0'">
            <xsl:call-template name="disassemble-cumomer">
              <xsl:with-param name="i" select="$i"/>
              <xsl:with-param name="string" select="concat(substring($string,1,($i)-1),'x',substring($string,($i)+1))"/>
              <xsl:with-param name="id" select="$id"/>
              <xsl:with-param name="sign" select="$sign"/>
            </xsl:call-template>
            <xsl:call-template name="disassemble-cumomer">
              <xsl:with-param name="i" select="$i"/>
              <xsl:with-param name="string" select="concat(substring($string,1,($i)-1),'1',substring($string,($i)+1))"/>
              <xsl:with-param name="id" select="$id"/>
              <xsl:with-param name="sign" select="-($sign)"/>
            </xsl:call-template>
          </xsl:when>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- This template generates the cumomers of a pool. For a given pool "P", e.g. with 2 carbon atoms we get
  
       <cumomer id="P_1" pool="P" subscript="1" weight="1" pattern="x1">
          <carbon position="1" index="0"/>
       </cumomer>
       <cumomer id="P_2" pool="P" subscript="2" weight="1" pattern="1x">
          <carbon position="2" index="1"/>
       </cumomer>
       <cumomer id="P_3" pool="P" subscript="3" weight="2" pattern="11">
          <carbon position="1" index="0"/>
          <carbon position="2" index="1"/>
       </cumomer>
       
       The <carbon> elements represent the 13-neutrons isotopes and the @index and @position attributes verify @position=2^@index -->
  <xsl:template name="iteration-cumomers">       
    <xsl:param name="nbCumomer"/>
    <xsl:param name="i"/>
    <xsl:param name="carbons"/>
    <xsl:if test="$i&lt;=$nbCumomer">
      <cumomer id="{concat(@id,'_',$i)}" pool="{@id}" subscript="{$i}" weight="{count(exslt:node-set($carbons)/smtb:carbon)}" xmlns="http://www.utc.fr/sysmetab">
        <xsl:attribute name="pattern">
          <xsl:variable name="atoms" select="@atoms"/>
          <xsl:for-each select="str:tokenize(str:padding(@atoms,'x'),'')">
            <xsl:variable name="position" select="($atoms)-position()"/>
            <xsl:choose>
              <xsl:when test="exslt:node-set($carbons)/smtb:carbon[@index=$position]">
                <xsl:value-of select="'1'"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="'x'"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </xsl:attribute>
        <xsl:copy-of select="$carbons"/>
      </cumomer>
      <xsl:call-template name="iteration-cumomers">
        <xsl:with-param name="nbCumomer" select="$nbCumomer"/>
        <xsl:with-param name="i" select="($i)+1"/>
        <xsl:with-param name="carbons">
          <xsl:call-template name="plusone">
            <xsl:with-param name="j" select="'1'"/>
            <xsl:with-param name="index" select="'0'"/>
            <xsl:with-param name="carbons" select="$carbons"/>
          </xsl:call-template>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!-- This recursive template provides the marked carbons of a cumomer according to the marked carbons of the 
       previous cumomer. 
       In practice, it suffice to add 1 to a binary number and to propagate the carry if required.
       For example, if in the input ("carbons" parameter), we have the fragment
         <carbon position="1"/>
         <carbon position="2"/>
       which corresponds to the binary number 11, we obtain in the output 100, or the fragment
         <carbon position="4"/>
  -->
  <xsl:template name="plusone">
    <xsl:param name="j"/>
    <xsl:param name="index"/>
    <xsl:param name="carbons"/>
    <xsl:choose>
      <xsl:when test="(exslt:node-set($carbons)/smtb:carbon[1]/@position)=$j">
        <xsl:call-template name="plusone">
          <xsl:with-param name="j" select="($j)*2"/>
          <xsl:with-param name="index" select="($index)+1"/>
          <xsl:with-param name="carbons">
            <xsl:copy-of select="exslt:node-set($carbons)/smtb:carbon[position()&gt;1]"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <carbon position="{$j}" index="{$index}" xmlns="http://www.utc.fr/sysmetab"/>
        <xsl:copy-of select="$carbons"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>