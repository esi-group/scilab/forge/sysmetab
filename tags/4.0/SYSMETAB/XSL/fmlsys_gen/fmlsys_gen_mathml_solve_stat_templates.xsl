<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt">
  
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <!-- Max weight, by default automatically set -->

  <xsl:variable name="maxweight">
	  <xsl:variable name="maxweight_tokens">
			<token>
				<xsl:value-of select="exslt:node-set($params)[@name='maxweight']"/>
			</token>
	  	<token>
				<xsl:value-of select="math:max(//smtb:listOfIntermediateCumomers/smtb:listOfCumomers/@weight)"/>
			</token>
		</xsl:variable>
		<xsl:value-of select="math:min(exslt:node-set($maxweight_tokens)/*)"/>
	</xsl:variable>

  <!-- Solve cumomers, cost function and gradient of the cost function for Direct and Adjoint method -->

  <xsl:template name="solveCumomersAndGrad">
    <xsl:param name="method"/>
    <function xmlns="http://www.utc.fr/sysmetab">
      <!-- functions name -->
      <xsl:choose>
        <xsl:when test="$method='none'">
          <ci xmlns="http://www.w3.org/1998/Math/MathML">solveCumomers</ci>
        </xsl:when>
        <xsl:when test="$method='derivative'">
          <ci xmlns="http://www.w3.org/1998/Math/MathML">solveCumomersAndDerivative</ci>
        </xsl:when>
        <xsl:otherwise>
          <ci xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:value-of select="concat('solveCumomersAndGrad',$method)"/>
          </ci>
        </xsl:otherwise>
      </xsl:choose>
      <!-- functions input -->
      <input>
        <list xmlns="http://www.w3.org/1998/Math/MathML">
          <ci>v</ci>
          <ci>omega</ci>
					<ci>Xinp</ci>
        </list>
      </input>
      <!-- functions output -->
      <output>
				<xsl:choose>
					<xsl:when test="exslt:node-set($params)[@name='timer']='yes'">
						<ci>t</ci>
					</xsl:when>
					<xsl:otherwise>
		        <list xmlns="http://www.w3.org/1998/Math/MathML">
	            <ci>residual</ci>
		          <xsl:if test="($method='Direct') or ($method='Adjoint')">
		            <ci>gradv</ci>
		            <ci>gradomega</ci>
		          </xsl:if>
		          <ci>X</ci>
		          <ci>y</ci>
		          <xsl:if test="$method='derivative'">
		            <ci>dy_dv</ci>
		            <ci>dy_domega</ci>
		          </xsl:if>
		        </list>
					</xsl:otherwise>
				</xsl:choose>
      </output>
      <!-- functions body -->
      <body>

        <comment>Solve the state equation cascade</comment>

				<xsl:if test="exslt:node-set($params)[@name='timer']='yes'">
					<apply xmlns="http://www.w3.org/1998/Math/MathML">
						<eq/>
						<ci>t</ci>
						<vector/>
					</apply>
					<apply xmlns="http://www.w3.org/1998/Math/MathML">
						<fn>
							<ci>timer</ci>
						</fn>
					</apply>
				</xsl:if>

        <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$maxweight]">
          <xsl:variable name="current_weight" select="@weight"/>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <list xmlns="http://www.w3.org/1998/Math/MathML" separator=",">
              <ci>X</ci>
              <ci>
                <xsl:value-of select="concat('M',@weight,'_handle')"/>
              </ci>
            </list>
            <apply>
              <fn>
                <ci>
                  <xsl:value-of select="concat('solve_weight_',@weight)"/>
                </ci>
              </fn>
              <ci>v</ci>
							<xsl:choose>
								<xsl:when test="@weight='1'">
									<ci>Xinp</ci>
								</xsl:when>
								<xsl:otherwise>
									<ci>X</ci>
								</xsl:otherwise>
							</xsl:choose>
            </apply>
          </apply>
        </xsl:for-each>

        <comment>Compute the simulated measurements</comment>

        <!-- Numeric observation  -->
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>y_unscale</ci>
          <apply>
                <times/>
                <ci>Cx</ci>
                <ci>X</ci>
          </apply>
        </apply>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>Omega</ci>
          <apply>
            <selector/>
						<ci type="vector">omega</ci>
						<apply>
							<selector/>
							<ci type="matrix">dy_domega_ij</ci>
							<cn>:</cn>
							<cn>2</cn>
            </apply>
					</apply>
        </apply>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>y</ci>
          <apply>
            <times type="array"/>
            <ci>Omega</ci>
            <ci>y_unscale</ci>    
          </apply>
        </apply>

        <comment>Compute the residual</comment>

        <!-- e_label=y-ymeas; -->
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>e_label</ci>
          <apply>
            <minus/>
            <ci>y</ci>
            <ci>ymeas</ci>
          </apply>
        </apply>
        <!-- Sypm2.*e_label -->
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>Sypm2_e_label</ci>
            <apply>
              <times type="array"/>
              <ci>Sypm2</ci>
              <ci>e_label</ci>
            </apply>
          </apply>
        <!-- residual=Sypm2.*e_label.^2 -->
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>residual</ci>
            <apply>
              <times type="array"/>
              <ci>Sypm2_e_label</ci>
              <ci>e_label</ci>
            </apply>
          </apply>

					<xsl:call-template name="timer"/>

          <!-- Gradient computation -->
        <xsl:choose>
          <xsl:when test="($method='Direct') or ($method='derivative')">
            
            <comment>Compute the state derivatives</comment>

            <!-- System resolution for dx_weight/dv -->
            <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$maxweight]">
              <xsl:variable name="current_weight" select="@weight"/>
	            <comment>
								<xsl:value-of select="concat('Weight ',@weight)"/>
							</comment>
							<apply xmlns="http://www.w3.org/1998/Math/MathML">
	              <eq/>
	              <ci>
	                <xsl:value-of select="concat('dgdv',@weight)"/>
	              </ci>
								<apply>
									<fn>
										<ci>spset</ci>
									</fn>
		              <ci>
		                <xsl:value-of select="concat('dgdv',@weight)"/>
		              </ci>
									<apply>
										<times/>
			              <ci>
			                <xsl:value-of select="concat('dgdv',@weight,'_t')"/>
			              </ci>
										<apply>
											<apply>
												<times type="array"/>
												<apply>
													<selector/>
													<ci type="vector">X</ci>
													<ci>
														<xsl:value-of select="concat('dgdv',@weight,'_m2')"/>
													</ci>										
												</apply>
												<apply>
													<selector/>
													<ci type="vector">X</ci>
													<ci>
														<xsl:value-of select="concat('dgdv',@weight,'_m3')"/>
													</ci>										
												</apply>
											</apply>
										</apply>
									</apply>
								</apply>
							</apply>
						
	            <xsl:for-each select="../smtb:listOfCumomers[@weight&lt;current()/@weight]">
	              <xsl:sort select="@weight" order="ascending" data-type="number"/>
								<xsl:variable name="nc2" select="number(@last)-number(@first)+1"/>
								<xsl:variable name="id" select="concat('db',$current_weight,'_dx',@weight)"/>
								<apply xmlns="http://www.w3.org/1998/Math/MathML">
		              <eq/>
									<ci>
	                	<xsl:value-of select="$id"/>
	              	</ci>
									<apply>
										<fn>
											<ci>spset</ci>
										</fn>
			              <ci>
			                <xsl:value-of select="$id"/>
			              </ci>
										<apply>
											<times/>
				              <ci>
				                <xsl:value-of select="concat($id,'_t')"/>
				              </ci>
											<apply>									
												<apply>
													<times type="array"/>
													<apply>
														<selector/>
														<ci type="vector">X</ci>
														<ci>
															<xsl:value-of select="concat($id,'_m1')"/>
														</ci>										
													</apply>
													<apply>
														<selector/>
														<ci type="vector">v</ci>
														<ci>
															<xsl:value-of select="concat($id,'_m2')"/>
														</ci>										
													</apply>
												</apply>
											</apply>
										</apply>
									</apply>
								</apply>
	            </xsl:for-each>

              <apply xmlns="http://www.w3.org/1998/Math/MathML">
                <eq/>
                <ci>
                  <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                </ci>
                <apply>
                  <minus/>
                  <apply>
                    <fn><ci>umf_lusolve</ci></fn>
                    <ci>
                      <xsl:value-of select="concat('M',@weight,'_handle')"/>
                    </ci>
                    <apply xmlns="http://www.w3.org/1998/Math/MathML">
                      <plus/>
                      <xsl:choose>
                        <xsl:when test="position()=1">
                          <apply>
                            <fn><ci>full</ci></fn>
                            <ci>
                              <xsl:value-of select="concat('dgdv',@weight)"/>
                            </ci>
                          </apply>
                        </xsl:when>
                        <xsl:otherwise>
                          <ci>
                            <xsl:value-of select="concat('dgdv',@weight)"/>
                          </ci>
                        </xsl:otherwise>
                      </xsl:choose>
                      <xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
                        <apply>
                          <times/>
                          <ci>
                            <xsl:value-of select="concat('db',$current_weight,'_dx',@weight)"/>
                          </ci>
                          <ci>
                            <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                          </ci>
                        </apply>
                      </xsl:for-each>
                    </apply>
                  </apply>
                </apply>
              </apply>
            </xsl:for-each>
            
						<xsl:call-template name="timer"/>
						
            <xsl:choose>
              <xsl:when test="$method='derivative'">            
                <comment>Compute the derivative (forward method)</comment>
                <apply xmlns="http://www.w3.org/1998/Math/MathML">
                  <eq/>
                  <ci>dy_dv</ci>
                  <apply>
                    <times/>
                    <apply>
                      <fn><ci>spdiag</ci></fn>
                      <apply>
                        <ci>Omega</ci>
                      </apply>
                    </apply>
                    <apply>
                      <plus/>
                      <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$maxweight]">
                        <apply>
                          <times/>
                          <ci>
                            <xsl:value-of select="concat('C',@weight)"/>
                          </ci>
                          <ci>
                            <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                          </ci>
                        </apply>
                      </xsl:for-each>
                    </apply>
                  </apply>
                </apply>
              </xsl:when>
              <xsl:otherwise>
                <comment>Compute the gradient (forward method)</comment>
                
                <apply xmlns="http://www.w3.org/1998/Math/MathML">
                  <eq/>
                  <ci>Omega_Sypm2_e_label</ci>
                  <apply>
                    <times type="array"/>            
                    <ci>Omega</ci>
                    <ci>Sypm2_e_label</ci>                  
                  </apply>
                </apply>
                
                <apply xmlns="http://www.w3.org/1998/Math/MathML">
                  <eq/>
                  <ci>gradv</ci>
                  <apply>
                    <times/>
                    <cn>2</cn>
                    <apply>
                      <transpose/>
                      <apply>
                        <plus/>
                        <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$maxweight]">
                          <apply>
                            <times/>
                            <apply>
                              <transpose/>
                              <ci>Omega_Sypm2_e_label</ci>
                            </apply>
                            <ci>
                              <xsl:value-of select="concat('C',@weight)"/>
                            </ci>
                            <ci>
                              <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                            </ci>
                          </apply>
                        </xsl:for-each>
                      </apply>
                    </apply>
                  </apply>
                </apply>
        
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>

          <xsl:when test="$method='Adjoint'">
                 
            <comment>Compute the adjoint states</comment>

            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>Omega_Sypm2_e_label</ci>
              <apply>
                <times type="array"/>
                <ci type="matrix">Omega</ci>
                <ci type="matrix">Sypm2_e_label</ci>
              </apply>
            </apply>
            <!-- System resolution for p_weight -->
            <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$maxweight]">
              <xsl:sort select="@weight" order="descending" data-type="number"/>
              <xsl:variable name="current_weight" select="@weight"/>
	            <comment>
								<xsl:value-of select="concat('Weight ',@weight)"/>
							</comment>

	            <xsl:for-each select="../smtb:listOfCumomers[@weight&gt;current()/@weight]">
	              <xsl:sort select="@weight" order="ascending" data-type="number"/>
								<xsl:variable name="nc2" select="number(@last)-number(@first)+1"/>
								<xsl:variable name="id" select="concat('db',@weight,'_dx',$current_weight)"/>
								<apply xmlns="http://www.w3.org/1998/Math/MathML">
		              <eq/>
									<ci>
	                	<xsl:value-of select="concat($id,'t')"/>
	              	</ci>
									<apply>
										<fn>
											<ci>spset</ci>
										</fn>
			              <ci>
		                	<xsl:value-of select="concat($id,'t')"/>
			              </ci>
										<apply>
											<times/>
				              <ci>
				                <xsl:value-of select="concat($id,'t_t')"/>
				              </ci>
											<apply>									
												<apply>
													<times type="array"/>
													<apply>
														<selector/>
														<ci type="vector">X</ci>
														<ci>
															<xsl:value-of select="concat($id,'_m1')"/>
														</ci>										
													</apply>
													<apply>
														<selector/>
														<ci type="vector">v</ci>
														<ci>
															<xsl:value-of select="concat($id,'_m2')"/>
														</ci>										
													</apply>
												</apply>
											</apply>
										</apply>
									</apply>
								</apply>
	            </xsl:for-each>

              <apply xmlns="http://www.w3.org/1998/Math/MathML">
                <eq/>
                <ci>
                  <xsl:value-of select="concat('p',@weight)"/>
                </ci>
                <apply>
                  <fn><ci>umf_lusolve</ci></fn>
                  <ci>
                    <xsl:value-of select="concat('M',@weight,'_handle')"/>
                  </ci>
                  <apply xmlns="http://www.w3.org/1998/Math/MathML">
	                  <xsl:if test="following-sibling::smtb:listOfCumomers">
	                    <minus/>
	                  </xsl:if>
	                  <apply>
	                    <times/>
	                    <ci>
	                      <xsl:value-of select="concat('C',@weight,'t')"/>
	                    </ci>
	                    <ci type="matrix">Omega_Sypm2_e_label</ci>
	                  </apply>
	                  <xsl:if test="following-sibling::smtb:listOfCumomers[@weight&lt;=$maxweight]">
	                    <apply>
	                      <plus/>
	                      <xsl:for-each select="following-sibling::smtb:listOfCumomers[@weight&lt;=$maxweight]">
	                        <apply>
	                          <times/>
	                          <ci>
	                            <xsl:value-of select="concat('db',@weight,'_dx',$current_weight,'t')"/>
	                          </ci>
	                          <apply>
	                            <ci>
	                              <xsl:value-of select="concat('p',@weight)"/>
	                            </ci>
	                          </apply>
	                        </apply>
	                      </xsl:for-each>
	                    </apply>
	                  </xsl:if>
                  </apply>
                  <string xmlns="http://www.utc.fr/sysmetab">A&apos;&apos;x=b</string>
                </apply>
              </apply>
            </xsl:for-each>
            
						<comment/>
						
            <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$maxweight]">
							<apply xmlns="http://www.w3.org/1998/Math/MathML">
	              <eq/>
	              <ci>
	                <xsl:value-of select="concat('dgdv',@weight,'t')"/>
	              </ci>
								<apply>
									<fn>
										<ci>spset</ci>
									</fn>
		              <ci>
		                <xsl:value-of select="concat('dgdv',@weight,'t')"/>
		              </ci>
									<apply>
										<times/>
			              <ci>
			                <xsl:value-of select="concat('dgdv',@weight,'t_t')"/>
			              </ci>
										<apply>
											<apply>
												<times type="array"/>
												<apply>
													<selector/>
													<ci type="vector">X</ci>
													<ci>
														<xsl:value-of select="concat('dgdv',@weight,'_m2')"/>
													</ci>										
												</apply>
												<apply>
													<selector/>
													<ci type="vector">X</ci>
													<ci>
														<xsl:value-of select="concat('dgdv',@weight,'_m3')"/>
													</ci>										
												</apply>
											</apply>
										</apply>
									</apply>
								</apply>
							</apply>
	          </xsl:for-each>
					
						<xsl:call-template name="timer"/>
						
            <comment>Compute the gradient (adjoint method)</comment>
            
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>gradv</ci>            
              <apply>
                <times/>
                <cn>-2</cn>
                <apply xmlns="http://www.w3.org/1998/Math/MathML">
                  <plus/>
                  <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$maxweight]">
                    <apply>
                      <times/>
                      <ci>
                        <xsl:value-of select="concat('dgdv',@weight,'t')"/>
                      </ci>                        
                      <ci>
                        <xsl:value-of select="concat('p',@weight)"/>
                      </ci>
                    </apply>
                  </xsl:for-each>
                </apply>
              </apply>
            </apply>

          </xsl:when>
        </xsl:choose>
    
        <xsl:if test="$method!='none'"> <!-- gradient computation for all but solveCumomers() function -->
        					
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
						<eq/>
						<ci>dy_domega</ci>
						<apply>
							<fn>
								<ci>s_full</ci>
							</fn>
							<ci>dy_domega_ij</ci>
							<ci>y_unscale</ci>
								<cn>
									<xsl:value-of select="count(f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']/smtb:measurement)"/>
								</cn>
								<cn>
									<xsl:value-of select="count(f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group)"/>
								</cn>
						</apply>
					</apply>
        
          <xsl:if test="$method != 'derivative'">        
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>gradomega</ci>            
              <apply>
                <times/>
                <cn>2</cn>
                <apply>
                  <times/>
                  <apply>
                    <transpose/>
                    <ci>dy_domega</ci>
                  </apply>
                  <ci>Sypm2_e_label</ci>
                </apply>
              </apply>
            </apply>
          </xsl:if>
					
					<xsl:call-template name="timer"/>

        </xsl:if>

        <comment>Free the memory occupied by the LU factors</comment>

        <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$maxweight]">
          <xsl:sort select="@weight" order="descending" data-type="number"/>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>err</ci>
            <apply>
              <fn><ci>umf_ludel</ci></fn>
              <ci>
                <xsl:value-of select="concat('M',@weight,'_handle')"/>
              </ci>
            </apply>
          </apply>
        </xsl:for-each>
      </body>
    </function>
  </xsl:template>

  <xsl:template name="timer">
		<xsl:if test="exslt:node-set($params)[@name='timer']='yes'">
			<apply xmlns="http://www.w3.org/1998/Math/MathML">
				<eq/>
				<ci>t</ci>
				<vector>
					<ci>t</ci>
					<apply>
						<fn><ci>timer</ci></fn>
					</apply>
				</vector>
			</apply>
		</xsl:if>
	</xsl:template>

</xsl:stylesheet>