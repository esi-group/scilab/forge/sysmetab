<?xml version="1.0" encoding="UTF-8" ?>

<!-- 
    Authors :   Stéphane Mottelet
    Project :   PIVERT/Metalippro-PL1
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:fwd="http://www.13cflux.net/fwdsim"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt">

  <xsl:template match="/">
		<xsl:apply-templates/>
	</xsl:template>

  <xsl:template match="fwd:fwdsim">
		<xsl:apply-templates/>
    <xsl:if test="fwd:optimization/fwd:param[@name='error']">
      <xsl:message>
        <xsl:text>&#xA;Scilab returned the following error message : </xsl:text>
        <xsl:value-of select="fwd:optimization/fwd:param[@name='error']"/>
				<xsl:text>.&#xA;&#xA;</xsl:text>
      </xsl:message>
    </xsl:if>
	</xsl:template>

  <xsl:template match="fwd:stoichiometry">

			<xsl:if test="fwd:flux/*[@stddev='Inf']">
				<xsl:message>
					<xsl:text>Jacobian matrix is singular, the following fluxes are (locally) non-identifiable: &#xA;&#xA;</xsl:text>
					<xsl:for-each select="fwd:flux/*[@stddev='Inf']">
						<xsl:value-of select="concat(../@id,'_',name())"/>
						<xsl:if test='position()&lt;last()'>
							<xsl:text>, </xsl:text>
						</xsl:if>
					</xsl:for-each>
					<xsl:text>.&#xA;&#xA;</xsl:text>
				</xsl:message>
			</xsl:if>

			<xsl:message>
        <xsl:for-each select="fwd:flux/*[(math:abs(@value)&gt;0) and (@stddev &gt; math:abs(@value))]">
          <xsl:if test="position()=1">
  					<xsl:text>The following fluxes are poorly determined (stddev>abs(value)): &#xA;&#xA;</xsl:text>
          </xsl:if>

					<xsl:value-of select="concat(../@id,'_',name())"/>
          <xsl:choose>
            <xsl:when test='position()&lt;last()'>
						  <xsl:text>, </xsl:text>
					  </xsl:when>
            <xsl:otherwise>
              <xsl:text>.&#xA;&#xA;</xsl:text>
    					<xsl:if test="../fwd:optimization/fwd:param[@name='stats']='lin'">
    						<xsl:text>You should compute Monte-Carlo statistics (--stats=mc:n) in order to confirm or infirm this.&#xA;&#xA;</xsl:text>
    					</xsl:if>
            </xsl:otherwise>
          </xsl:choose>
				</xsl:for-each>
			</xsl:message>

      <xsl:variable name="eps_phi" select="../fwd:optimization/fwd:param[@name='eps_phi']"/>
      
				<xsl:message>
					<xsl:for-each select="fwd:flux/fwd:net[math:abs(@value) &lt; number($eps_phi)]">
            <xsl:if test="position()=1">
    					<xsl:value-of select="concat('Warning: the following net fluxes have absolute values lower that regphi (',$eps_phi,'): &#xA;&#xA;')"/>
            </xsl:if>
						<xsl:value-of select="concat(../@id,'_net : ',@value)"/>
            <xsl:choose>
              <xsl:when test='position()&lt;last()'>
							  <xsl:text>, </xsl:text>
						  </xsl:when>
              <xsl:otherwise>
                <xsl:text>.&#xA;&#xA;</xsl:text>
                <xsl:text>As contraints can be violated, you should decrease the value of the --regphi options or add contraints for these fluxes and run sysmetab without --regphi.&#xA;&#xA;</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
					</xsl:for-each>
				</xsl:message>


	</xsl:template>  

  <xsl:template match="node()"/>

</xsl:stylesheet>
