<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
-->

<!-- 
    System resolution for x_weight
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt">

  <!-- Output file name -->
  <xsl:param name="output"/>

  <xsl:param name="verb_level">1</xsl:param>

  <xsl:variable name="params" select="document(concat($output,'.params.xml'),/)/smtb:params/smtb:param"/>

  <!-- find all f:rproduct from f:reaction -->
  <xsl:key name="RPRODUCT" match="f:reaction/f:rproduct" use="@id"/>

  <!-- find all f:reduct from f:reaction -->
  <xsl:key name="REDUCT" match="f:reaction/f:reduct" use="@id"/>

  <!-- find all f:pool -->
  <xsl:key name="POOL" match="f:pool" use="@id"/>

  <!-- find all smtb:cumomer from smtb:listOfInputCumomers -->
  <xsl:key name="ALL_CUMOMER" match="smtb:listOfInputCumomers//smtb:cumomer | smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>

  <!-- find all smtb:cumomer from smtb:listOfInputCumomers -->
  <xsl:key name="INPUT_CUMOMER" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>

  <!-- find all smtb:cumomer from smtb:listOfIntermediateCumomers -->
  <xsl:key name="INTERMEDIATE_CUMOMER" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>

  <!-- find all smtb:cumomer from f:pool -->
  <xsl:key name="CUMOMERS" match="f:pool/smtb:cumomer" use="@id"/>

  <!-- find all f:datum -->
  <xsl:key name="DATUM" match="f:datum" use="@id"/>

  <!-- find all f:group -->
  <xsl:key name="GROUP" match="f:group" use="@id"/>

	<xsl:variable name="nX" select="1+count(//smtb:listOfIntermediateCumomers//smtb:cumomer | //smtb:listOfInputCumomers//smtb:cumomer)"/>
  <xsl:variable name="nY" select="count(//f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']/smtb:measurement)"/>
  <xsl:variable name="nv" select="count(//f:reactionnetwork/f:reaction)"/>

	<!-- sparse storage : 1 (csr) for Scilab and 2 (csc) for Julia -->

  <xsl:variable name="storage">1</xsl:variable>


  <xsl:template match="/">
    <xsl:if test="$verb_level&gt;0">
      <xsl:message>Generating state equation code</xsl:message>
    </xsl:if>
    <carbon-labeling-system>
      <xsl:apply-templates/>
    </carbon-labeling-system>
  </xsl:template>

  <xsl:template match="f:fluxml">

    <!-- System resolution for x_weight -->

    <xsl:call-template name="solve_functions"/>

    <xsl:call-template name="construct_matrix_C"/>

    <!-- Building index matrix for dy_domega -->

    <xsl:call-template name="groups"/>

  </xsl:template>
  
  <!-- All functions to find the x_weight -->
  <xsl:template name="solve_functions">
    <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
      <xsl:variable name="current_weight" select="@weight"/>
       <function xmlns="http://www.utc.fr/sysmetab">
        <!-- functions name -->
        <ci xmlns="http://www.w3.org/1998/Math/MathML">
          <xsl:value-of select="concat('solve_weight_',@weight)"/>
        </ci>
        <!-- functions input -->
        <input>
          <list xmlns="http://www.w3.org/1998/Math/MathML">
            <ci>v</ci>
            <ci>X</ci>
          </list>
        </input>
        <!-- functions output -->
        <output>
          <list xmlns="http://www.w3.org/1998/Math/MathML">
            <ci>X</ci>
            <ci>
              <xsl:value-of select="concat('M',@weight,'_handle')"/>
            </ci>
          </list>
        </output>
        <!-- functions body -->
        <body>
          <xsl:variable name="nc" select="number(@last)-number(@first)+1"/>
            <comment>Weight <xsl:value-of select="@weight"/> cumomers</comment>

            <!-- For each cumomer, we call the template generating various assignments, to the matrix in the 
            right hand side of the system whose solution is the cumomer vector of the current weight.
            The template "iteration" is in the style sheet fml_common_gen_mathml.xsl -->
						
						<apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>X</ci>
              <ci>X</ci>
						</apply>
						<apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>
                <xsl:value-of select="concat('M',@weight)"/>
              </ci>
							<apply>
								<fn>
									<ci>sparse</ci>
								</fn>
	              <ci>
	                <xsl:value-of select="concat('M',@weight,'_ij')"/>
	              </ci>
								<apply>
									<times/>
									<ci>
	                	<xsl:value-of select="concat('M',@weight,'_t')"/>
	              	</ci>
									<ci>
	                	<xsl:value-of select="concat('v(M',@weight,'_m2)')"/>
	              	</ci>
								</apply>
                <vector>
                  <ci><xsl:value-of select="$nc"/></ci>
                  <ci><xsl:value-of select="$nc"/></ci>
                </vector>
							</apply>
						</apply>

						<apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>
                <xsl:value-of select="concat('b',@weight)"/>
              </ci>
							<apply>
								<fn>
									<ci>spset</ci>
								</fn>
	              <ci>
	                <xsl:value-of select="concat('b',@weight)"/>
	              </ci>
								<apply>
									<times/>
									<ci>
										<xsl:value-of select="concat('b',@weight,'_t')"/>
									</ci>
									<apply>
  									<apply>
  										<times type="array"/>
  										<apply>
  											<selector/>
  											<ci type="vector">X</ci>
  											<ci>
  												<xsl:value-of select="concat('b',@weight,'_m1')"/>
  											</ci>										
  										</apply>
                      
                      <xsl:if test="@weight&gt;1">                       
                        <apply>
  											  <selector/>
  											  <ci type="vector">X</ci>
  											  <ci>
  												  <xsl:value-of select="concat('b',@weight,'_m2')"/>
  											  </ci>										
  										  </apply>
                      </xsl:if>
                      
                      <apply>
  											<selector/>
  											<ci type="vector">v</ci>
  											<ci>
  												<xsl:value-of select="concat('b',@weight,'_m3')"/>
  											</ci>										
  										</apply>
  									</apply>
  								</apply>
  							</apply>
							</apply>
						</apply>						
						
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <ci>
                <xsl:value-of select="concat('M',@weight,'_handle')"/>
              </ci>
              <apply>
                <fn>
                  <ci>umf_lufact</ci>
                </fn>
                <ci>
                  <xsl:value-of select="concat('M',@weight)"/>
                </ci>
              </apply>
            </apply>
            <!-- System resolution for x_weight -->
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <eq/>
              <apply>
								<selector/>
								<ci type="vector">X</ci>
								<ci>
									<xsl:value-of select="concat('cum',@weight)"/>
								</ci>
							</apply>
              <apply>
                <fn>
                  <ci>umf_lusolve</ci>
                </fn>
                <ci>
                  <xsl:value-of select="concat('M',@weight,'_handle')"/>
                </ci>
                <apply>
                  <minus/>
                  <ci>
                    <xsl:value-of select="concat('full(b',@weight,')')"/>
                  </ci>
                </apply>
              </apply>
            </apply>						

        </body>
      </function>
    </xsl:for-each>
		
    <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
      <xsl:variable name="current_weight" select="@weight"/>
      <xsl:variable name="nc" select="number(@last)-number(@first)+1"/>

			<comment xmlns="http://www.utc.fr/sysmetab">Weight <xsl:value-of select="@weight"/> cumomers</comment>

			<apply xmlns="http://www.w3.org/1998/Math/MathML">
				<eq/>
				<ci><xsl:value-of select="concat('cum',@weight)"/></ci>
				<cn><xsl:value-of select="concat(@first,':',@last)"/></cn>
			</apply>
			
			<optimize xmlns="http://www.utc.fr/sysmetab">
				<single-matrix-open  id="{concat('M',@weight,'_m')}" cols="4"/>				
				<single-matrix-open  id="{concat('b',@weight,'_m')}" cols="5"/>
				<single-matrix-open  id="{concat('dgdv',@weight,'_m')}" cols="5"/>
        <xsl:for-each select="../smtb:listOfCumomers[@weight&lt;current()/@weight]">
          <xsl:sort select="@weight" order="ascending" data-type="number"/>
          <single-matrix-open id="{concat('db',$current_weight,'_dx',@weight,'_m')}" cols="4"/>
        </xsl:for-each>


			<!-- For each cumomer, we call the template generating various assignments, to the matrix in the 
			right hand side of the system whose solution is the cumomer vector of the current weight.
			The template "iteration" is in the style sheet fml_common_gen_mathml.xsl -->

				<xsl:for-each select="smtb:cumomer">			
					<xsl:call-template name="iteration"/>
				</xsl:for-each>

				<single-matrix-close id="{concat('M',@weight,'_m')}"/>
				<apply xmlns="http://www.w3.org/1998/Math/MathML">
					<eq/>
					<list separator=",">
						<ci><xsl:value-of select="concat('M',@weight)"/></ci>
						<ci><xsl:value-of select="concat('M',@weight,'_t')"/></ci>
						<ci><xsl:value-of select="concat('M',@weight,'_ij')"/></ci>
					</list>
					<apply>
						<fn><ci>gather</ci></fn>
						<ci><xsl:value-of select="concat('M',@weight,'_mij')"/></ci>
            <cn><xsl:value-of select="$nc"/></cn><cn><xsl:value-of select="$nc"/></cn>
						<cn><xsl:value-of select="$storage"/></cn>
						<ci><xsl:value-of select="concat('M',@weight,'_m1')"/></ci>						
					</apply>
				</apply>
        
				
				<single-matrix-close id="{concat('b',@weight,'_m')}"/>
				<apply xmlns="http://www.w3.org/1998/Math/MathML">
					<eq/>
					<list separator=",">
						<ci><xsl:value-of select="concat('b',@weight)"/></ci>
						<cn><xsl:value-of select="concat('b',@weight,'_t')"/></cn>
					</list>
					<apply>
						<fn><ci>gather</ci></fn>
						<ci><xsl:value-of select="concat('b',@weight,'_mij')"/></ci>
            <cn><xsl:value-of select="$nc"/></cn><cn>1</cn>
						<cn><xsl:value-of select="$storage"/></cn>
					</apply>
				</apply>

				<single-matrix-close id="{concat('dgdv',@weight,'_m')}"/>
				<apply xmlns="http://www.w3.org/1998/Math/MathML">
					<eq/>
					<list separator=",">
						<ci><xsl:value-of select="concat('dgdv',@weight)"/></ci>
						<cn><xsl:value-of select="concat('dgdv',@weight,'_t')"/></cn>
					</list>
					<apply>
						<fn><ci>gather</ci></fn>
						<ci><xsl:value-of select="concat('dgdv',@weight,'_mij')"/></ci>
            <cn><xsl:value-of select="$nc"/></cn><cn><xsl:value-of select="$nv"/></cn>
						<cn><xsl:value-of select="$storage"/></cn>
						<ci><xsl:value-of select="concat('dgdv',@weight,'_m1')"/></ci>						
					</apply>
				</apply>

				<apply xmlns="http://www.w3.org/1998/Math/MathML">
					<eq/>
					<list separator=",">
						<ci><xsl:value-of select="concat('dgdv',@weight,'t')"/></ci>
						<cn><xsl:value-of select="concat('dgdv',@weight,'t_t')"/></cn>
					</list>
					<apply>
						<fn><ci>gather</ci></fn>
						<apply>
							<selector/>
							<ci type="matrix"><xsl:value-of select="concat('dgdv',@weight,'_mij')"/></ci>
							<cn>:</cn>
							<vector><cn>2</cn><cn>1</cn></vector>
						</apply>	
            <cn><xsl:value-of select="$nv"/></cn>
						<cn><xsl:value-of select="$nc"/></cn>
						<cn><xsl:value-of select="$storage"/></cn>
						<ci><xsl:value-of select="concat('dgdv',@weight,'_m1')"/></ci>						
					</apply>
				</apply>

        <xsl:for-each select="../smtb:listOfCumomers[@weight&lt;current()/@weight]">
          <xsl:sort select="@weight" order="ascending" data-type="number"/>
					<xsl:variable name="nc2" select="number(@last)-number(@first)+1"/>
          <single-matrix-close id="{concat('db',$current_weight,'_dx',@weight,'_m')}"/>
					<apply xmlns="http://www.w3.org/1998/Math/MathML">
						<eq/>
						<list separator=",">
							<ci><xsl:value-of select="concat('db',$current_weight,'_dx',@weight)"/></ci>
							<cn><xsl:value-of select="concat('db',$current_weight,'_dx',@weight,'_t')"/></cn>
						</list>
						<apply>
							<fn><ci>gather</ci></fn>
							<ci><xsl:value-of select="concat('db',$current_weight,'_dx',@weight,'_mij')"/></ci>
              <cn><xsl:value-of select="$nc"/></cn><cn><xsl:value-of select="$nc2"/></cn>
							<cn><xsl:value-of select="$storage"/></cn>
						</apply>
					</apply>

					<apply xmlns="http://www.w3.org/1998/Math/MathML">
						<eq/>
						<list separator=",">
							<ci><xsl:value-of select="concat('db',$current_weight,'_dx',@weight,'t')"/></ci>
							<cn><xsl:value-of select="concat('db',$current_weight,'_dx',@weight,'t_t')"/></cn>
						</list>
						<apply>
							<fn><ci>gather</ci></fn>
							<apply>
								<selector/>
								<ci type="matrix"><xsl:value-of select="concat('db',$current_weight,'_dx',@weight,'_mij')"/></ci>
								<cn>:</cn>
								<vector><cn>2</cn><cn>1</cn></vector>
							</apply>	
							<cn><xsl:value-of select="$nc2"/></cn>
              <cn><xsl:value-of select="$nc"/></cn>
							<cn><xsl:value-of select="$storage"/></cn>
						</apply>
					</apply>

        </xsl:for-each>

		  </optimize>
			
    </xsl:for-each>
		
  </xsl:template>


  <xsl:template name="iteration">
    <!-- Beware the context node is an element <smtb:cumomer>.
    The variable "carbons" contains the carbons 13 of the current pool -->
    <xsl:variable name="carbons">
      <xsl:copy-of select="key('CUMOMERS',@id)/smtb:carbon"/>
    </xsl:variable>
    <xsl:variable name="weight" select="../@weight"/>
    <xsl:variable name="position" select="@local-number"/>
    <xsl:variable name="global-number" select="@number"/>
    <!-- influx rule : the most difficult, but also the most interesting, it is here that truly creates 
    supplementary information compared to the simple stoichiometry.
    We loop all the "rproduct" elements that have the current cumomer pool as a product, so in the for-each the
    contextual node is of reaction/rproduct type. -->
    
    <xsl:for-each select="key('RPRODUCT',@pool)">
    
      <!-- Now, We try to find occurrences marked carbons of reactant of the current reaction: it needs work -->
    
      <xsl:variable name="id" select="@id"/>
      <xsl:variable name="reaction" select="../@position"/>
      <xsl:variable name="occurrence" select="count(preceding-sibling::f:rproduct)+1"/>
      
      <xsl:variable name="influx">
        <!-- This is where the serious stuff begins. We loop all the reactants which have carbons atoms which 
        "point" to the pool. -->
        <xsl:for-each select="../f:reduct[smtb:carbon[(@id=$id) and (@occurrence=$occurrence)]]">
          
          <xsl:variable name="somme">
            <!-- So now, we loop all the carbons of reactant. If a carbon of reactant point to a carbon 13 of 
            cumomer of product, we note its number in an element <token/> -->
            <xsl:for-each select="smtb:carbon[(@id=$id) and (@occurrence=$occurrence)]">
              <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                <token>
                  <xsl:value-of select="@position"/>
                </token>
              </xsl:if>
            </xsl:for-each>
            <!-- At the end of this loop, the "somme" variable contains a number, possibly zero, of elements
            <token> which identify without ambiguity the concerned cumomer. -->
          </xsl:variable>
          
          <xsl:if test="sum(exslt:node-set($somme)/token)&gt;0">
            <!-- We sum up the <token>, this is a way to know if there is at least one ... -->
            <!-- We generate a new element <token> with a weight attribut specifying the weight of concerned 
            cumomer (the number of <token> in $somme), the type of the concerned pool (intermediate or input) 
            and its identifier, which is obtained by the sum of <token>. -->
            <token weight="{count(exslt:node-set($somme)/token)}" type="{key('POOL',@id)/@type}">
              <xsl:value-of select="concat(@id,'_',sum(exslt:node-set($somme)/token))"/>
            </token>
          </xsl:if>
        </xsl:for-each>
        <!-- At the end of this loop, the "influx" variable conteins a number of <token>. According to the type
        and the weight of concerned cumomers. It is either unknown, if the weight is the current weight $weight,
        or involved quantities in the right hand side as product form, whose the sum of weight is equal to the 
        current weight $weight. -->
      </xsl:variable>
      
      <!-- Here, we genrate the code which forms the matrix and the right hand side of the system which allows 
      to obtain the cumomers of weight $weight -->
      <xsl:choose>
        <xsl:when test="(count(exslt:node-set($influx)/token)=1) and (exslt:node-set($influx)/token/@weight=$weight) and (exslt:node-set($influx)/token/@type='intermediate')">

          <!-- We only have one term of the current weight, so the matrix is assembled so that its derivative -->

					<single-matrix-row id="{concat('M',$weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
						<ci>
							<xsl:value-of select="$position"/>
						</ci>
						<ci>
							<xsl:value-of select="key('INTERMEDIATE_CUMOMER',$influx)/@local-number"/>
						</ci>
						<ci>1</ci>
						<ci><xsl:value-of select="$reaction"/></ci>
					</single-matrix-row>

					<single-matrix-row id="{concat('dgdv',$weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
						<ci><xsl:value-of select="$position"/></ci>
						<ci><xsl:value-of select="$reaction"/></ci>
						<ci>1</ci>
						<ci><xsl:value-of select="key('INTERMEDIATE_CUMOMER',$influx)/@number"/></ci>
						<ci>1</ci>
					</single-matrix-row>

        </xsl:when>
				
        <xsl:when test="count(exslt:node-set($influx)/token)&gt;=1">
          <!-- We have one input term or a product of two terms of lower weight, thus we assembles the right hand 
          side and the Jacobien of the right hand side in comparison of cumomers of lower weight. -->
					
					<single-matrix-row id="{concat('b',$weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
						<ci>
							<xsl:value-of select="$position"/>
						</ci>
						<ci>1</ci>
						<ci>
							<xsl:value-of select="key('ALL_CUMOMER',exslt:node-set($influx)/token[1])/@number"/>
						</ci>
						<xsl:choose>
							<xsl:when test="exslt:node-set($influx)/token[2]">
								<ci>
									<xsl:value-of select="key('ALL_CUMOMER',exslt:node-set($influx)/token[2])/@number"/>
								</ci>
							</xsl:when>
							<xsl:otherwise>
								<ci>1</ci>
							</xsl:otherwise>
						</xsl:choose>
						<ci>
							<xsl:value-of select="$reaction"/>
						</ci>
					</single-matrix-row>
					
					<single-matrix-row id="{concat('dgdv',$weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
						<ci><xsl:value-of select="$position"/></ci>
						<ci><xsl:value-of select="$reaction"/></ci>
						<ci>1</ci>
						<ci>
							<xsl:value-of select="key('ALL_CUMOMER',exslt:node-set($influx)/token[1])/@number"/>
						</ci>
						<xsl:choose>
							<xsl:when test="exslt:node-set($influx)/token[2]">
								<ci>
									<xsl:value-of select="key('ALL_CUMOMER',exslt:node-set($influx)/token[2])/@number"/>
								</ci>
							</xsl:when>
							<xsl:otherwise>
								<ci>1</ci>
							</xsl:otherwise>
						</xsl:choose>
					</single-matrix-row>

	        <xsl:if test="count(exslt:node-set($influx)/token)&gt;1">
						<xsl:variable name="id1" select="exslt:node-set($influx)/token[1]"/>
						<xsl:variable name="id2" select="exslt:node-set($influx)/token[2]"/>
							<xsl:if test="key('INTERMEDIATE_CUMOMER',$id1)">
								<single-matrix-row id="{concat('db',$weight,'_dx',key('INTERMEDIATE_CUMOMER',$id1)/@weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
									<ci><xsl:value-of select="$position"/></ci>
									<ci><xsl:value-of select="key('INTERMEDIATE_CUMOMER',$id1)/@local-number"/></ci>
									<ci><xsl:value-of select="key('ALL_CUMOMER',$id2)/@number"/></ci>
									<ci><xsl:value-of select="$reaction"/></ci>									
								</single-matrix-row>
							</xsl:if>
							<xsl:if test="key('INTERMEDIATE_CUMOMER',$id2)">
								<single-matrix-row id="{concat('db',$weight,'_dx',key('INTERMEDIATE_CUMOMER',$id2)/@weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
									<ci><xsl:value-of select="$position"/></ci>
									<ci><xsl:value-of select="key('INTERMEDIATE_CUMOMER',$id2)/@local-number"/></ci>
									<ci><xsl:value-of select="key('ALL_CUMOMER',$id1)/@number"/></ci>
									<ci><xsl:value-of select="$reaction"/></ci>									
								</single-matrix-row>
							</xsl:if>
					</xsl:if>
					
        </xsl:when>
      </xsl:choose>
    </xsl:for-each>
		
    <!-- outflux rule : the easiest part to generate (cf Wiechert paper) -->

    <xsl:for-each select="key('REDUCT',@pool)">
			<single-matrix-row id="{concat('M',$weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
				<ci>
					<xsl:value-of select="$position"/>
				</ci>
				<ci>
					<xsl:value-of select="$position"/>
				</ci>
				<ci>-1</ci>
	      <cn><xsl:value-of select="../@position"/></cn>
			</single-matrix-row>
			<single-matrix-row id="{concat('dgdv',$weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
				<ci>
					<xsl:value-of select="$position"/>
				</ci>
				<ci>
					<xsl:value-of select="../@position"/>
				</ci>
				<ci>-1</ci>
				<ci><xsl:value-of select="$global-number"/></ci>
				<ci>1</ci>
			</single-matrix-row>
    </xsl:for-each>

  </xsl:template>


  <xsl:template name="construct_matrix_C">

    <comment xmlns="http://www.utc.fr/sysmetab">Observation matrices (cumomers)</comment>


    <optimize xmlns="http://www.utc.fr/sysmetab">
	    <matrix-open id="Cx"
	                 rows="{$nY}" 
	                 cols="{$nX}" 
	                 type="sparse" 
	                 assignments="unique"
	                 xmlns="http://www.utc.fr/sysmetab"/>
      <!-- We open an item "optimize" in which we will gradually build these matrices -->
      <xsl:for-each select="f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']/smtb:measurement">
        <xsl:sort select="@id" data-type="text"/>
        <xsl:variable name="position" select="position()"/>
        <xsl:for-each select="smtb:cumomer-contribution">
          <xsl:choose>
            <xsl:when test="@weight=0">
              <matrix-assignment id="Cx" row="{$position}" col="1">
                <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
              </matrix-assignment>
            </xsl:when>
            <xsl:otherwise>  
              <matrix-assignment id="Cx" row="{$position}" col="{key('INTERMEDIATE_CUMOMER',concat(../../@id,'_',@subscript))/@number}">
                <cn xmlns="http://www.w3.org/1998/Math/MathML">
                  <xsl:value-of select="@sign"/>
                </cn>
              </matrix-assignment>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </xsl:for-each>
	    <matrix-close id="Cx" xmlns="http://www.utc.fr/sysmetab"/>
    </optimize>
		
		<xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
			<apply xmlns="http://www.w3.org/1998/Math/MathML">
				<eq/>
				<ci><xsl:value-of select="concat('C',@weight)"/></ci>
				<apply>
					<selector/>
					<ci type="matrix">Cx</ci>
					<cn>:</cn>
					<ci><xsl:value-of select="concat('cum',@weight)"/></ci>
				</apply>
			</apply>
			<apply xmlns="http://www.w3.org/1998/Math/MathML">
				<eq/>
				<ci><xsl:value-of select="concat('C',@weight,'t')"/></ci>
				<apply>
					<transpose/>
					<ci><xsl:value-of select="concat('C',@weight)"/></ci>
				</apply>
			</apply>

		</xsl:for-each>
  </xsl:template>
	
	<xsl:template name="groups">
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
			<eq/>
			<ci>dy_domega_ij</ci>
			<matrix>
				<xsl:for-each select="f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']/smtb:measurement">
        	<xsl:sort select="@id" data-type="text"/>
        	<matrixrow>
						<cn>
							<xsl:value-of select="position()"/>
						</cn>
						<cn>
							<xsl:value-of select="key('GROUP',@id)/@pos"/>
						</cn>
					</matrixrow>
      </xsl:for-each>
    </matrix>
	</apply>
	</xsl:template>


</xsl:stylesheet>
