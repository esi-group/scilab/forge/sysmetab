<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   Stéphane Mottelet
    Date    :   Wed Mar 21 11:13:38 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de générer un fichier XML avec une structure
    permettant de générer les équations dans un langage quelconque. Il résume les
    informations sur les différents flux (connus ou pas), et sur les espèces (connues
    ou pas + équations des bilans au format "Content MathML" :
    
    <carbon-labeling-system>
        <listOfReactions>
            <reaction id="re1" known="yes"/>
            .
            .
            .
        </listOfReactions>
        <listOfSpecies>
            <species id="s1" type="input" known="yes"/>
            <species id="s2" name="Glucose6P" type="intermediate">
                <equations>
                    <apply xmlns:m="http://www.w3.org/1998/Math/MathML">
                    .
                    .
                    .
                    </apply>
                    <apply xmlns:m="http://www.w3.org/1998/Math/MathML">
                    .
                    .
                    .
                    </apply>
                    .
                    .
                    .
                </equations>
            </species>
    </carbon-labeling-system>
    
-->

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"
    xmlns:math="http://exslt.org/math"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:f="http://www.13cflux.net/fluxml"
  exclude-result-prefixes="smtb sbml celldesigner m str exslt xhtml math xsl f">
       
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

    <xsl:param name="verbose">no</xsl:param>
    <xsl:param name="type">stationnaire</xsl:param>
    <xsl:variable name="maxweight" select="math:max(//smtb:cumomer-contribution/@weight)"/>
<!-- 
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>
-->
  <!-- find all f:reduct from f:reaction -->
  <xsl:key name="REDUCT" match="f:reaction/f:reduct" use="@id"/>

  <!-- find all f:rproduct from f:reaction -->
  <xsl:key name="RPRODUCT" match="f:reaction/f:rproduct" use="@id"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
          <xsl:apply-templates/>
    </xsl:template>


  <xsl:template match="f:fluxml">
    <carbon-labeling-system >
      <xsl:apply-templates select="f:reactionnetwork"/>
    </carbon-labeling-system>
  </xsl:template>
  
<!--
  <xsl:template match="f:fluxml">
      <xsl:apply-templates select="f:reactionnetwork"/>
  </xsl:template>
  <xsl:template match="f:reactionnetwork">
    <xsl:apply-templates select="f:reaction"/>
    <xsl:apply-templates select="f:metabolitepools"/>
  </xsl:template>
-->

  <xsl:template match="f:reactionnetwork">
      <xsl:apply-templates select="f:metabolitepools"/>
      <xsl:apply-templates select="f:reaction"/>            
  </xsl:template>

    <xsl:template match="f:reaction">
      <reaction id="{@id}">
        <xsl:copy-of select="@*"/>
      </reaction>
    </xsl:template>
    

    <xsl:template match="f:metabolitepools">
       <metabolitepools>
        <xsl:for-each select="f:pool">
        
            <pool>
               <xsl:copy-of select="@*"/>


                    <xsl:if test="@type='intermediate'">
                                                
                        <equations>
                        

                            <equation weight="0">
                                <xsl:call-template name="bilan"/>
                            </equation>
                            
                                 
                            <xsl:for-each select="smtb:cumomer[@weight&lt;=$maxweight]">
                                <equation weight="{@weight}" cumomer="{@id}">
                                    <xsl:call-template name="iteration"/>                                    
                                </equation>
                            </xsl:for-each>

                          </equations>

                    </xsl:if>


               
            </pool>
        </xsl:for-each>
       </metabolitepools>        
    </xsl:template>
   


<xsl:template name="bilan">

    <xsl:variable name="name" select="@id"/>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <cn>0</cn>
        <apply>
            <minus/>   
            
  
            <apply>
                <plus/>
                <xsl:for-each select="key('RPRODUCT',@id)">
                    <ci>
                        <xsl:value-of select="../@id"/>
                    </ci>
                </xsl:for-each>
            </apply>
            
            <xsl:call-template name="bilan-outflux">
                <xsl:with-param name="id" select="@id"/>
            </xsl:call-template>
                 
        </apply>
    
    </apply>

</xsl:template>

<xsl:template name="bilan-outflux">
    <xsl:param name="id"/>

   
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <plus/>
        <xsl:for-each select="key('REDUCT',$id)">
            <ci>
                <xsl:value-of select="../@id"/>
            </ci>
        </xsl:for-each>
    </apply>

</xsl:template>




<xsl:template name="iteration">

    
    
    <xsl:variable name="name" select="../@id"/>
    
    <xsl:variable name="carbons">
        <xsl:copy-of select="smtb:carbon"/>
    </xsl:variable>

   
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <xsl:choose>
            <xsl:when test="$type='stationnaire'">
                <cn>0</cn>
            </xsl:when>
            <xsl:when test="$type='dynamique'">
                <apply>
                    <times/>
                    <ci>
                        <xsl:value-of select="../@id"/>
                    </ci>
                    <apply>
                        <diff/>
                        <ci>
                            <xsl:value-of select="@id"/> 
                        </ci>
                    </apply>
                </apply>
            </xsl:when>
        </xsl:choose>

        <apply>
            <minus/>   

     
            <apply>
                <plus/>
                <xsl:for-each select="key('RPRODUCT',../@id)"> 

      
                    <xsl:variable name="id" select="@id"/>
                    <xsl:variable name="occurence" select="count(preceding-sibling::*[name()=name(current())])+1"/>

                    <apply>

                        <times/>


                        <xsl:for-each select="../f:reduct[smtb:carbon[(@id=$id) and (@occurence=$occurence)]]">

                            <xsl:variable name="somme">
                                <xsl:for-each select="smtb:carbon[(@id=$id) and (@occurence=$occurence)]">
                                    <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                                        <token>
                                        	<xsl:value-of select="@position"/>
                                        </token>
                                    </xsl:if>
                                </xsl:for-each>
                            </xsl:variable>
                            
                            <xsl:if test="sum(exslt:node-set($somme)/*)&gt;0">
                            
                                <ci>
                                    <xsl:value-of select="concat(@id,'_',sum(exslt:node-set($somme)/*))"/>
                                </ci>
                            </xsl:if>

                        </xsl:for-each>

                        <ci>
                            <xsl:value-of select="../@id"/>
                        </ci>

                    </apply>

                </xsl:for-each>
            </apply>


            <apply>
              <times/>
              <ci>
                   <xsl:value-of select="@id"/>
              </ci>

            <xsl:call-template name="bilan-outflux">
                <xsl:with-param name="id" select="../@id"/>
            </xsl:call-template>

            </apply>
        </apply>
        
    </apply>
    
</xsl:template>

  <xsl:template match="node()"/>

</xsl:stylesheet>
