<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   Stéphane Mottelet
    Date    :   Wed Mar 21 11:13:38 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de générer un fichier XML avec une structure
    permettant de générer les équations dans un langage quelconque. Il résume les
    informations sur les différents flux (connus ou pas), et sur les espèces (connues
    ou pas + équations des bilans au format "Content MathML" :
    
    <carbon-labeling-system>
        <listOfReactions>
            <reaction id="re1" known="yes"/>
            .
            .
            .
        </listOfReactions>
        <listOfSpecies>
            <species id="s1" type="input" known="yes"/>
            <species id="s2" name="Glucose6P" type="intermediate">
                <equations>
                    <m:apply xmlns:m="http://www.w3.org/1998/Math/MathML">
                    .
                    .
                    .
                    </m:apply>
                    <m:apply xmlns:m="http://www.w3.org/1998/Math/MathML">
                    .
                    .
                    .
                    </m:apply>
                    .
                    .
                    .
                </equations>
            </species>
    </carbon-labeling-system>
    
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"
    xmlns:math="http://exslt.org/math"
        version="1.0"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
       
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
 
    <xsl:param name="nnodes">100</xsl:param>

    <xsl:variable name="df">
        <value p="0.05"/>
        <value p="0.33"/>
        <value p="0.2"/>
        <value p="0.26"/>
        <value p="0.09"/>
        <value p="0.03"/>
        <value p="0.03"/>
        <value p="0.01"/>
    </xsl:variable>
    
    <xsl:variable name="icdf">
        <xsl:for-each select="exslt:node-set($df)/value">
            <value x="{sum(preceding-sibling::value/@p)+@p}"/>
            <xsl:message><xsl:value-of select="sum(preceding-sibling::value/@p)+@p"/></xsl:message>
        </xsl:for-each>
    </xsl:variable>

    <xsl:template match="/">
        <graph>
            <xsl:for-each select="str:split(str:padding($nnodes,'x'),'')">
                <node id="{position()}">
                    <xsl:if test="position()=1">
                        <xsl:attribute name="source">yes</xsl:attribute>
                    </xsl:if>
                    <xsl:variable name="rand" select="math:random()"/>
                    <xsl:for-each select="str:split(str:padding(count(exslt:node-set($icdf)/value[@x&lt;$rand]),'x'),'')">
                        <child node="{ceiling((math:random())*$nnodes)}"/>
                    </xsl:for-each>
                </node>
            </xsl:for-each>
        </graph>
    </xsl:template>

</xsl:stylesheet>
