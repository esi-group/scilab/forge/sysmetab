<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    version="1.0"
    exclude-result-prefixes="sbml m">
    <xsl:output method="xml" indent="no" encoding="ISO-8859-1"/>

    <xsl:key name="dynamicSpecies" match="sbml:species[(@constant='false') or not(@constant)]" use="@id"/>
    <xsl:key name="boundarySpecies" match="sbml:species[@boundaryCondition='true']" use="@id"/>
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:model">
        <sciml>
            <function-definition name="_rhs">
                <inputs>
                    <parm>_t</parm>
                    <parm>_x</parm>
                    <parm>_xdot</parm>
                </inputs>
                <outputs>
                    <parm>_res</parm>
                    <parm>_ires</parm>
                </outputs>
                <body>
                    <xsl:apply-templates mode="rhs"/>
                    <assign>
                        <lhs>_res</lhs>
                        <rhs>
                            <list>
                                <xsl:apply-templates mode="residual"/>
                            </list>
                        </rhs>
                    </assign>                
                    <assign>
                        <lhs>_ires</lhs>
                        <rhs>0</rhs>
                    </assign>                
                </body>
            </function-definition>

            <xsl:apply-templates mode="main"/>

            <assign>
                <lhs>_nt</lhs>
                <rhs>100</rhs>
            </assign>

            <assign>
                <lhs>_tmax</lhs>
                <rhs>80</rhs>
            </assign>

            <assign>
                <lhs>_t</lhs>
                <rhs>
                    <function-call name="linspace">
                        <inputs>
                            <parm>0</parm>
                            <parm>_tmax</parm>
                            <parm>_nt</parm>
                        </inputs>
                    </function-call>
                </rhs>
            </assign>
            <assign>
                <lhs>
                    <list>
                        <parm>_x</parm>
                    </list>
                </lhs>
                <rhs>
                    <function-call name="dassl">
                        <inputs>
                            <parm>_x0</parm>
                            <parm>0</parm>
                            <parm>_t</parm>
                            <parm>_rhs</parm>
                        </inputs>
                    </function-call>
                </rhs>
            </assign>
            
            <xsl:for-each select="sbml:listOfSpecies/sbml:species[key('dynamicSpecies',@id)]">
                <assign>
                    <lhs>
                        <xsl:value-of select="@id"/>
                    </lhs>
                    <rhs>
                        <select matrix="_x" row="{position()+1}" col=":"/>
                    </rhs>
                </assign>
            </xsl:for-each>

            <xsl:for-each select="sbml:listOfSpecies/sbml:species[not(key('dynamicSpecies',@id))]">
                <assign>
                    <lhs>
                        <xsl:value-of select="@id"/>
                    </lhs>
                    <rhs>
                        <select matrix="{@id}" row="1" col="ones(1,_nt)"/>
                    </rhs>
                </assign>
            </xsl:for-each>
            
            <macro-call name="plot">
                <inputs>
                    <xsl:for-each select="sbml:listOfSpecies/sbml:species">
                        <parm>_t</parm>
                        <parm>
                            <xsl:value-of select="@id"/>
                        </parm>
                    </xsl:for-each>
                    <parm>
                        <string>legend</string>
                    </parm>
                    <xsl:for-each select="sbml:listOfSpecies/sbml:species">
                        <parm>
                            <string>
                                <xsl:value-of select="@id"/>
                            </string>
                        </parm>
                    </xsl:for-each> 
                </inputs>
            </macro-call>
            
        </sciml>
    </xsl:template>

<!-- rhs mode: templates for the assembly of the rhs function used
     to compute the residual for dassl -->

    <xsl:template match="sbml:listOfReactions" mode="rhs">
        <xsl:apply-templates mode="rhs"/>                
    </xsl:template>
    
    <xsl:template match="sbml:reaction" mode="rhs">
        <xsl:apply-templates select="sbml:kineticLaw" mode="rhs"/>                
        <xsl:apply-templates select="*[not(self::sbml:kineticLaw)]" mode="rhs"/>                
    </xsl:template>

    <xsl:template match="sbml:kineticLaw" mode="rhs">
        <xsl:apply-templates select="sbml:listOfParameters"/>
        <assign>
            <lhs>rate</lhs>
            <rhs>
                <xsl:apply-templates select="m:math|@formula"/>
            </rhs>
        </assign>
    </xsl:template>

    <xsl:template match="sbml:model/sbml:listOfParameters" mode="rhs"/>

    <xsl:template match="sbml:listOfParameters" mode="rhs">
        <xsl:apply-templates/>        
    </xsl:template>

    <xsl:template match="sbml:listOfParameters">
        <xsl:apply-templates/>        
    </xsl:template>
    
    <xsl:template match="sbml:parameter">
        <assign>
            <lhs>
                <xsl:value-of select="@id"/>
            </lhs>
            <rhs>
                <xsl:value-of select="@value"/>
            </rhs>
        </assign>
    </xsl:template>

    <xsl:template match="sbml:listOfReactants" mode="rhs">
        <xsl:apply-templates select="sbml:speciesReference[key('dynamicSpecies',@species) and not(key('boundarySpecies',@species))]" mode="rhs">
            <xsl:with-param name="sign" select="'+'"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="sbml:listOfProducts" mode="rhs">
        <xsl:apply-templates select="sbml:speciesReference[key('dynamicSpecies',@species) and not(key('boundarySpecies',@species))]" mode="rhs"/>
    </xsl:template>

   <xsl:template match="sbml:speciesReference[@stoechiometry]" mode="rhs">
        <xsl:param name="sign" select="'-'"/>
        <assign>
            <lhs>
                <xsl:value-of select="concat('_r',@species)"/>
            </lhs>
            <rhs>
                <xsl:value-of select="concat('_r',@species,$sign,'rate/',@stoechiometry)"/>
            </rhs>
        </assign>
    </xsl:template>

    <xsl:template match="sbml:speciesReference" mode="rhs">
        <xsl:param name="sign" select="'-'"/>
        <assign>
            <lhs>
                <xsl:value-of select="concat('_r',@species)"/>
            </lhs>
            <rhs>
                <xsl:value-of select="concat('_r',@species,$sign,'rate')"/>
            </rhs>
        </assign>
    </xsl:template>

    <xsl:template match="sbml:listOfSpecies" mode="rhs">
        <xsl:apply-templates select="sbml:species[key('dynamicSpecies',@id)]" mode="rhs"/>        
    </xsl:template>
    
    <xsl:template match="sbml:species" mode="rhs">
        <assign>
            <lhs>
                 <xsl:value-of select="@id"/>
            </lhs>
            <rhs>
                <xsl:value-of select="concat('_x(',position(),')')"/>
            </rhs>
        </assign>    
        <assign>
            <lhs>
                 <xsl:value-of select="concat('_r',@id)"/>
            </lhs>
            <rhs>
                <xsl:value-of select="concat('_xdot(',position(),')')"/>
            </rhs>
        </assign>    
    </xsl:template>
    
    <xsl:template match="sbml:notes" mode="rhs"/>
    <xsl:template match="sbml:listOfCompartments" mode="rhs"/>
    <xsl:template match="sbml:listOfRules" mode="rhs"/>

<!-- residual mode: templates for the computation of the residual
     output for the dassl rhs -->

    <xsl:template match="sbml:listOfSpecies" mode="residual">
        <xsl:apply-templates select="sbml:species[key('dynamicSpecies',@id) and key('reactionSpecies',@id)]" mode="residual"/>        
    </xsl:template>

    <xsl:template match="sbml:species" mode="residual">
            <parm>
                <xsl:value-of select="concat('_r',@id)"/>
            </parm>
    </xsl:template>    
    
    <xsl:template match="sbml:listOfRules" mode="residual">
         <xsl:apply-templates mode="residual"/>
    </xsl:template>

    <xsl:template match="sbml:assignmentRule" mode="residual">
        <parm>
            <xsl:value-of select="concat(@variable,'-')"/>
            <xsl:apply-templates/>
        </parm>
    </xsl:template>

    <xsl:template match="sbml:algebraicRule" mode="residual">
        <parm>
            <xsl:apply-templates/>
        </parm>
    </xsl:template>

    <xsl:template match="sbml:listOfReactions" mode="residual"/>
    <xsl:template match="sbml:notes" mode="residual"/>

<!-- main mode: templates for the main program part of the sci file,
     global parameters, initial conditions, etc. -->

    <xsl:template match="sbml:listOfCompartments" mode="main">
        <xsl:apply-templates mode="main"/>
    </xsl:template>

    <xsl:template match="sbml:compartment[@size]" mode="main">
        <assign>
            <lhs><xsl:value-of select="@id"/></lhs>
            <rhs><xsl:value-of select="@size"/></rhs>
        </assign>
    </xsl:template>
    
    <xsl:template match="sbml:listOfSpecies" mode="main">

        <assign>
            <lhs>_x0</lhs>
            <rhs>
                <list sep=";">
                    <xsl:for-each select="sbml:species[key('dynamicSpecies',@id)]">
                        <parm>
                             <xsl:apply-templates select="." mode="main"/>
                        </parm>
                    </xsl:for-each>
                </list>
            </rhs>
        </assign>

        <xsl:for-each select="sbml:species[not(key('dynamicSpecies',@id))]">
            <assign>
                <lhs><xsl:value-of select="@id"/></lhs>
                <rhs>
                    <xsl:apply-templates select="." mode="main"/>
                </rhs>
            </assign>
        </xsl:for-each>

    </xsl:template>
    
    <xsl:template match="sbml:species[@initialConcentration]" mode="main">
            <xsl:value-of select="@initialConcentration"/>
    </xsl:template>

    <xsl:template match="sbml:species[@initialAmount]" mode="main">
            <xsl:value-of select="concat(@initialAmount,'/',@compartment)"/>
    </xsl:template>

    <xsl:template match="sbml:listOfParameters" mode="main">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="sbml:listOfRules" mode="main"/>
    <xsl:template match="sbml:listOfReactions" mode="main"/>

    <xsl:template match="sbml:notes" mode="main">
        <xsl:for-each select="xhtml:body//xhtml:p">
            <comment><xsl:value-of select="normalize-space(.)"/></comment>
        </xsl:for-each>
    </xsl:template>

<!-- elementary mathml templates -->

    <xsl:template match="m:math">
          <xsl:apply-templates/>
    </xsl:template>
    
<!--    <xsl:template match="m:math/m:apply">
          <xsl:apply-templates select="*[1]"/>
    </xsl:template>-->

    <xsl:template match="m:apply">
          <xsl:text>(</xsl:text>
          <xsl:apply-templates select="*[1]"/>
          <xsl:text>)</xsl:text>
    </xsl:template>
    
    <xsl:template match="m:plus">
          <xsl:for-each select="following-sibling::*">
                <xsl:apply-templates select="."/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text>+</xsl:text>
                </xsl:if>
          </xsl:for-each>
    </xsl:template>

    <xsl:template match="m:minus">
          <xsl:for-each select="following-sibling::*">
                <xsl:apply-templates select="."/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text>-</xsl:text>
                </xsl:if>
          </xsl:for-each>
    </xsl:template>

    <xsl:template match="m:times">
          <xsl:for-each select="following-sibling::*">
                <xsl:apply-templates select="."/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text>*</xsl:text>
                </xsl:if>
          </xsl:for-each>
    </xsl:template>

    <xsl:template match="m:divide">
          <xsl:for-each select="following-sibling::*">
                <xsl:apply-templates select="."/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text>/</xsl:text>
                </xsl:if>
          </xsl:for-each>
    </xsl:template>

    <xsl:template match="m:power">
          <xsl:for-each select="following-sibling::*">
                <xsl:apply-templates select="."/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text>^</xsl:text>
                </xsl:if>
          </xsl:for-each>
    </xsl:template>

    <xsl:template match="m:ci">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="m:cn">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>


</xsl:stylesheet>
