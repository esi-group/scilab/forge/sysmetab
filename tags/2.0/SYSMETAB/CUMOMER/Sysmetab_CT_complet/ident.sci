
[x1,dx1_dv]=solveCumomers(v,x1_input);

s1=svd([C1*dx1_dv(:,:,1)*V])
s2=svd([C1*dx1_dv(:,:,2)*V])
s3=svd([C1*dx1_dv(:,:,3)*V])
sensib=[C1*dx1_dv(:,:,1)*V;C1*dx1_dv(:,:,2)*V;C1*dx1_dv(:,:,3)*V];

s=svd(sensib);
gap=log(s(1:$-1)./s(2:$));
[g r]=sort(gap);

plot(log([s1/max(s1) s2/max(s2) s3/max(s3)]),'-o',log(s./max(s)),'-o','legend','Experience 1','Experience 2','Experience 3','Experiences 1+2+3')
title(sprintf('Log des valeurs singulieres des matrices de sensibilite, rangs probables = %d, %d',r(2),r(1)))
