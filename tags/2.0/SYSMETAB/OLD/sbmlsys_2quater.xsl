<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Thu Feb  8 09:50:47 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but d'ajouter explicitement, pour chaque
    r�action (sauf si elle est d�clar�e comme irreversible), la r�action
    inverse correspondante. Du point de vue des identificateurs, si par exemple
    une r�action a pour id "re1", cela donne deux nouvelles r�actions avec les
    id "re1_f" et "re1_b" (f pour forward et b pour backward).    
-->

<xsl:stylesheet  version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"   
    xmlns:math="http://exslt.org/math"   
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
    
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:variable name="maxweight" select="math:max(//sbml:species/smtb:cumomer/@weight)"/>
    
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>
    

    
    <xsl:template match="sbml:reaction[(@reversible!='false') or not(@reversible)]">
        <sbml:reaction id="{@id}_f" name="{@name} (forward)">
            <xsl:copy-of select="*"/>
        </sbml:reaction>
        <sbml:reaction id="{@id}_b" name="{@name} (backward)">
            <sbml:listOfReactants>
                <xsl:copy-of select="sbml:listOfProducts/*"/>
            </sbml:listOfReactants>
            <sbml:listOfProducts>
                <xsl:copy-of select="sbml:listOfReactants/*"/>
            </sbml:listOfProducts>
        </sbml:reaction>
    </xsl:template>

	<xsl:template match="sbml:species">
		<xsl:element name="sbml:species">
			<xsl:copy-of select="@*"/>
			<xsl:choose>
            	<xsl:when test="key('products',@id) and key('reactants',@id)">        
					<xsl:attribute name="type">intermediate</xsl:attribute>
				</xsl:when>
				<xsl:when test="not (key('products',@id))">
                    <xsl:attribute name="type">input</xsl:attribute>
                </xsl:when>

                <!-- Si l'esp�ce est un m�tabolite sortant -->

                <xsl:when test="not(key('reactants',@id))">
					<xsl:attribute name="type">output</xsl:attribute>    
                </xsl:when>
			</xsl:choose>
			<xsl:copy-of select="*"/>
		</xsl:element>
	</xsl:template>

   <xsl:template match="sbml:annotation"/>

    <xsl:template match="*|@*|text()|comment()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>


</xsl:stylesheet>
