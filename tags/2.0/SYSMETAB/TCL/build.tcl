# Installation of the loading part in the user's startup file (.scilab)

insert  [file join $SCIHOME .scilab] "*SYSMETAB*loader.sce*" "exec(\"[file join $SYSMETAB loader.sce]\",-1) // Load SYSMETAB"
insert  [file join $SCIHOME .scilab] "*OPTIM*loader.sce*" "exec(\"[file join $SYSMETAB OPTIM fsqp-1.5 loader.sce]\",-1) // Load FSQP"


set cmdfile [file join $SYSMETAB BIN gensolvestat] 
insert $cmdfile  "SYSMETAB=*" "SYSMETAB=$SYSMETAB"
file attributes $cmdfile -permissions a+x

set cmdfile [file join $SYSMETAB BIN gensolvedyn]
insert $cmdfile  "SYSMETAB=*" "SYSMETAB=$SYSMETAB"
file attributes $cmdfile -permissions a+x


