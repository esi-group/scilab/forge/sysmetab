<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet  version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"   
    xmlns:math="http://exslt.org/math">

    
  <xsl:output method="text" encoding="ISO-8859-1"/>

  <xsl:strip-space elements="*"/>
	
  <xsl:variable name="maxAtoms" select="9"/>
   
  <!-- Listes des isotopomères, pour un nombre d'atomes allant de 1 à $maxAtoms-->
  
  <xsl:variable name="list">
    <xsl:for-each select="str:split(str:padding($maxAtoms,'.'),'')"> <!-- une façon de faire une boucle de 1 à $maxAtoms-->
      <listOfIsotopomers atoms="{position()}">
         <xsl:call-template name="iteration-cumomers">
              <xsl:with-param name="max" select="math:power(2,position())-1"/>
              <xsl:with-param name="number" select="str:padding(position(),'0')"/>
          </xsl:call-template>
      </listOfIsotopomers>
    </xsl:for-each>
  </xsl:variable>  

  <xsl:template match="/">  

    <xsl:variable name="atoms" select="6"/>
    <xsl:variable name="measurement">Glu[2,3,4,5]#M0,1,2,3,4</xsl:variable>
    
    <xsl:variable name="fragment" select="str:split(substring-before(substring-after($measurement,'['),']'),',')"/>
    <xsl:variable name="before" select="str:padding($fragment[1]-1,'x')"/>
    <xsl:variable name="after" select="str:padding(($atoms)-$fragment[last()],'x')"/>
    
    <xsl:text>LABEL_MEASUREMENT </xsl:text>
    <xsl:for-each select="str:split(substring-after($measurement,'#M'),',')"> 
        <xsl:for-each select="exslt:node-set($list)/listOfIsotopomers[@atoms=count($fragment)]/isotopomer[@weight=current()]">
          <xsl:value-of select="concat($before,@pattern,$after)"/>
          <xsl:if test="position()&lt;last()">
            <xsl:value-of select="'+'"/>
          </xsl:if>
        </xsl:for-each>
        <xsl:if test="position()&lt;last()">
          <xsl:value-of select="','"/>
        </xsl:if>
    </xsl:for-each>
    <xsl:text>&#10;</xsl:text>
    
  </xsl:template>

  <xsl:template name="iteration-cumomers">          
      <xsl:param name="max"/>
      <xsl:param name="i" select="0"/>
      <xsl:param name="number"/>       
      <xsl:if test="$i&lt;=$max">
         <isotopomer weight="{string-length(translate($number,'0',''))}" pattern="{$number}"/>
         <xsl:call-template name="iteration-cumomers">
              <xsl:with-param name="max" select="$max"/>
              <xsl:with-param name="i" select="($i)+1"/>
              <xsl:with-param name="number">
                  <xsl:call-template name="ripple-carry">  
                      <xsl:with-param name="number" select="$number"/>
                  </xsl:call-template>
              </xsl:with-param>
          </xsl:call-template>
      </xsl:if>
  </xsl:template>
  
  <!-- Template de propagation de la retenue ou "ripple carry" -->
  
  <xsl:template name="ripple-carry">    
    <xsl:param name="k" select="0"/>
    <xsl:param name="number"/>
    <xsl:variable name="fragment" select="substring($number,1,string-length($number)-($k)-1)"/>
    <xsl:choose>
	    <xsl:when test="substring($number,string-length($number)-$k,1)='1'">
        	<xsl:call-template name="ripple-carry">
            	<xsl:with-param name="k" select="($k)+1"/>
            	<xsl:with-param name="number" select="concat($fragment,str:padding(($k)+1,'0'))"/>
        	</xsl:call-template>
	    </xsl:when>
	    <xsl:otherwise>
          <xsl:value-of select="concat($fragment,'1',str:padding($k,'0'))"/> <!-- fin de la propagation-->
	    </xsl:otherwise>
	  </xsl:choose>
  </xsl:template>

  <xsl:template match="node()"/>
  
</xsl:stylesheet>
