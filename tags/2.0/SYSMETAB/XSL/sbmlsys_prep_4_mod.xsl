<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Auteur  :   Stéphane Mottelet
    Date    :   Tue Mar 27 09:27:49 CEST 2007

    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but :

    1 - pour chaque espèce, de lui ajouter un attribut "type" pouvant etre
    "intermediate", "input" ou "output" suivant qu'il s'agit d'un métabolite
    du type correspondant.

    2 - d'ajouter à chaque réaction un attribut "position" donnant l'indice
    de la réaction dans la liste.
	
	5 - De traduire les observations sous forme de combinaisons linéaires des cumomères.

    REMARQUE : il semble que pour ce dernier point il y ait un problème de version
    de libxslt ou libxml sous MacOSX/Intel 10.4.9 qui fait que les position ne sont
    pas calculées comme il faut.
       
-->

<xsl:stylesheet  version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"   
    xmlns:math="http://exslt.org/math"   
    xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="math sbml celldesigner m str exslt xhtml smtb">

    <xsl:param name="experiences">1</xsl:param>
    
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

    <xsl:strip-space elements="*"/>
	
	<!-- Poids maximal des cumomères présents dans le réseau. -->
    
    <xsl:key name="species" match="sbml:species" use="@id"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>
    <xsl:key name="cumomer" match="smtb:cumomer" use="@id"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>
        
    <xsl:template match="*|@*|text()|comment()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>
    
  <xsl:template match="sbml:reaction[(@reversible!='false') or not(@reversible)]">
        <sbml:reaction id="{@id}_f" name="{@name} (forward)">
            <xsl:copy-of select="*"/>
        </sbml:reaction>
        <sbml:reaction id="{@id}_b" name="{@name} (backward)">
            <sbml:listOfReactants>
                <xsl:copy-of select="sbml:listOfProducts/*"/>
            </sbml:listOfReactants>
            <sbml:listOfProducts>
                <xsl:copy-of select="sbml:listOfReactants/*"/>
            </sbml:listOfProducts>
        </sbml:reaction>
    </xsl:template>

	<xsl:template match="sbml:species">
        <xsl:variable name="id" select="@id"/>
		<xsl:element name="sbml:species">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates select="*"/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="smtb:measurement">
    <smtb:measurement>
			<xsl:copy-of select="@*"/>
			<xsl:for-each select="smtb:token">
				<xsl:call-template name="disassemble-cumomer"/>
			</xsl:for-each>
		</smtb:measurement>
	</xsl:template>
	
	<!-- Template récursive de détermination des contributions des cumomères dans une observation donnée. Cela
	     sert plus tard pour la génération des matrices d'observation C1, C2, etc. -->

	<xsl:template name="disassemble-cumomer">
		<xsl:param name="i" select="string-length(.)"/>
		<xsl:param name="string" select="."/>
		<xsl:param name="id" select="'0'"/>
		<xsl:param name="sign" select="'1'"/>
		
		<xsl:choose>
			<xsl:when test="$i=0">
				<smtb:cumomer-contribution id="{concat(../../@id,'_',$id)}" string="{$string}" 
				               weight="{string-length(translate($string,'x',''))}" sign="{$sign}"/>
			</xsl:when>
			<xsl:otherwise>		
				<xsl:choose>
					<xsl:when test="substring($string,$i,1)='x'">
						<xsl:call-template name="disassemble-cumomer">
							<xsl:with-param name="i" select="($i)-1"/>
							<xsl:with-param name="string" select="$string"/>
							<xsl:with-param name="id" select="$id"/>
							<xsl:with-param name="sign" select="$sign"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="substring($string,$i,1)='1'">
						<xsl:call-template name="disassemble-cumomer">
							<xsl:with-param name="i" select="($i)-1"/>
							<xsl:with-param name="string" select="$string"/>
							<xsl:with-param name="id" select="$id + math:power(2,string-length(.)-$i)"/>
							<xsl:with-param name="sign" select="$sign"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="substring($string,$i,1)='0'">
						<xsl:call-template name="disassemble-cumomer">
							<xsl:with-param name="i" select="$i"/>
							<xsl:with-param name="string" select="concat(substring($string,1,($i)-1),'x',substring($string,($i)+1))"/>
							<xsl:with-param name="id" select="$id"/>
							<xsl:with-param name="sign" select="$sign"/>
						</xsl:call-template>
						<xsl:call-template name="disassemble-cumomer">
							<xsl:with-param name="i" select="$i"/>
							<xsl:with-param name="string" select="concat(substring($string,1,($i)-1),'1',substring($string,($i)+1))"/>
							<xsl:with-param name="id" select="$id"/>
							<xsl:with-param name="sign" select="-($sign)"/>
						</xsl:call-template>
					</xsl:when>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

   <xsl:template match="sbml:annotation"/>

</xsl:stylesheet>
