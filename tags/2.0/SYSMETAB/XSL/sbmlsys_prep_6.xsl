<?xml version="1.0" encoding="UTF-8"?>

<!-- 
    Auteur  :   Stéphane Mottelet, LMAC
    Date    :   
    Projet  :   GENESYS WP4
-->


<!-- Cette feuille de style a pour but :

       
-->

<xsl:stylesheet  version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:math="http://exslt.org/math"   
    xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="math sbml exslt smtb">
    
    <xsl:output method="xml" indent="yes" encoding="UTF-8"/>

    <xsl:strip-space elements="*"/>
	
	<!-- Poids maximal des cumomères présents dans le réseau. -->
	
    <xsl:param name="maxweight" select="math:max(//sbml:species/@carbons)"/>

    <xsl:variable name="mincumomers" select="document('graph.xml',/)/graph/*"/>
    
    <xsl:key name="cumomers" match="smtb:cumomer" use="@id"/>

    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>
        
    
     <xsl:template match="sbml:listOfSpecies">
        <sbml:listOfSpecies>
            <xsl:apply-templates/>
        </sbml:listOfSpecies>
        <xsl:message>
            <xsl:text>Cumomers : </xsl:text>
            <xsl:value-of select="concat('original=',count(sbml:species[@type='intermediate']/smtb:cumomer))"/>
            <xsl:value-of select="concat(', new=',count(exslt:node-set($mincumomers)[@type='intermediate']))"/>            
        </xsl:message>
        <smtb:listOfIntermediateCumomers>
            <xsl:call-template name="make-cumomer-list">
                <xsl:with-param name="type" select="'intermediate'"/>
            </xsl:call-template>                  
        </smtb:listOfIntermediateCumomers>
        <smtb:listOfInputCumomers>
            <xsl:call-template name="make-cumomer-list">
                <xsl:with-param name="type" select="'input'"/>
            </xsl:call-template>                  
        </smtb:listOfInputCumomers>
    </xsl:template>

    <xsl:template name="make-cumomer-list">
        <xsl:param name="w">1</xsl:param>
        <xsl:param name="type"/>
        <xsl:if test="$w&lt;=$maxweight">
            <xsl:if test="$type='intermediate'">
                <xsl:message>
                    <xsl:value-of select="concat('Weight ',$w,' :')"/>
                    <xsl:text> original=</xsl:text>
                    <xsl:value-of select="count(sbml:species[@type=$type]/smtb:cumomer[(@weight=$w)])"/>
                    <xsl:text>, new=</xsl:text>
                    <xsl:value-of select="count(exslt:node-set($mincumomers)[(@weight=$w) and (@type=$type)])"/>
                </xsl:message>
                <smtb:listOfCumomers weight="{$w}">
                    <xsl:for-each select="exslt:node-set($mincumomers)[(@weight=$w) and (@type=$type)]">
                        <xsl:sort select="@species" data-type="text"/>
                        <xsl:sort select="@subscript" data-type="number"/>
                        <smtb:cumomer id="{@id}" number="{position()}"/>
                    </xsl:for-each>
                </smtb:listOfCumomers>
                <xsl:call-template name="make-cumomer-list">
                    <xsl:with-param name="w" select="($w)+1"/>
                    <xsl:with-param name="type" select="$type"/>                
                </xsl:call-template>
            </xsl:if>
        </xsl:if>
    </xsl:template>      
    
    <xsl:template match="*|@*|text()|comment()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>

</xsl:stylesheet>
