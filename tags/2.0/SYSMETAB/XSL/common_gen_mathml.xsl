<?xml version="1.0" encoding="UTF-8" ?> 

<!--
Auteur: Florent Tixier
Date: lun mai 26 12:18 CEST 2008
Projet: SYSMETAB/Carnot
-->

<!--
Templates communes à la génération des différentes fonctions spécifiques d'un modèle donné sous forme SBML,
édité dans CellDesigner pour les cas stationnaire et non-stationnaire. Extraites telles quelles du fichier
initial sbmlsys_gen_mathml_solve_stat_4.xsl
-->

<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
  xmlns:m="http://www.w3.org/1998/Math/MathML" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:exslt="http://exslt.org/common" 
  xmlns:str="http://exslt.org/strings" 
  xmlns:math="http://exslt.org/math" 
  xmlns:smtb="http://www.utc.fr/sysmetab" 
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
 
     <xsl:template match="sbml:listOfSpecies" mode="stoichiometric-matrix">
		<smtb:optimize>
			<smtb:matrix-open type="s_full" id="N" 
				rows="{count(sbml:species[@type='intermediate'])}" 
				cols="{count(../sbml:listOfReactions/sbml:reaction)}"/>
	
	        <xsl:apply-templates select="sbml:species[@type='intermediate']" mode="stoichiometric-matrix"/>
			
			<smtb:matrix-close id="N"/>
	
		</smtb:optimize>
    </xsl:template>
	
	<xsl:template match="sbml:species" mode="stoichiometric-matrix">
        <xsl:variable name="position" select="position()"/>
        <xsl:for-each select="key('products',@id) | key('reactants',@id)">
			<smtb:matrix-assignment id="N" row="{$position}" col="{../../@position}">
                    <xsl:choose>
						<xsl:when test="parent::sbml:listOfProducts">
    						<m:cn>1</m:cn>
						</xsl:when>
						<xsl:when test="parent::sbml:listOfReactants">
                            <m:apply>
    						    <m:minus/>
                                <m:cn>1</m:cn>
                            </m:apply>
						</xsl:when>                   
                    </xsl:choose>
			</smtb:matrix-assignment>
        </xsl:for-each>
    </xsl:template> 

     <xsl:template match="sbml:listOfRules" mode="stoichiometric-matrix">
		<smtb:optimize>
			<smtb:matrix-open type="s_full" id="R" 
				rows="{count(sbml:algebraicRule)}" 
				cols="{count(../sbml:listOfReactions/sbml:reaction)}"/>
			
			<xsl:apply-templates select="sbml:algebraicRule" mode="stoichiometric-matrix"/>			
	
			<smtb:matrix-close id="R"/>
	
		</smtb:optimize>
    </xsl:template>

     <xsl:template match="sbml:algebraicRule" mode="stoichiometric-matrix">			
			<smtb:matrix-assignment id="R" row="{position()}" col="{key('reaction',m:math/m:apply[m:minus]/m:ci[1])/@position}">
				<m:cn>1</m:cn>
			</smtb:matrix-assignment>
			<smtb:matrix-assignment id="R" row="{position()}" col="{key('reaction',m:math/m:apply[m:minus]/m:ci[2])/@position}">
				<m:apply>
					<m:minus/>
					<m:cn>1</m:cn>
				</m:apply>
			</smtb:matrix-assignment>
     </xsl:template>


<xsl:template name="iteration">
    
    <!-- Attention le noeud contextuel est un élement <smtb:cumomer> -->

    <xsl:variable name="name" select="../@id"/>
    
	<!-- La variable "carbons" contient les carbones 13 de l'espèce courante -->
	
    <xsl:variable name="carbons">
        <xsl:copy-of select="smtb:carbon"/>
    </xsl:variable>
	
    <xsl:variable name="weight" select="@weight"/>
    <xsl:variable name="position" select="@position"/>

	<!-- influx rule : le plus chiant, mais aussi le plus intéressant. C'est là que se crée véritablement
     	l'information supplémentaire par rapport à la stoechiométrie simple. 

     On boucle sur tous les éléments <speciesReference> qui ont l'espèce du cumomère courant comme produit,
     donc dans le for-each le noeud contextuel est de type reaction/listOfProducts/speciesReference. -->

    	<xsl:for-each select="key('products',@species)"> 

        	<!-- Maintenant, on essaye de trouver des occurences des carbones marqués
            	 du reactant de la réaction courante : c'est du boulot... -->

        	<xsl:variable name="species" select="@species"/>
        	<xsl:variable name="reaction" select="../../@position"/>
        	<xsl:variable name="occurence" select="count(preceding-sibling::sbml:speciesReference)+1"/>

        	<xsl:variable name="influx">

            	<!-- C'est ici que les choses sérieuses commencent. On boucle sur tous les réactants qui ont des atomes de carbones
                	 qui "pointent" sur l'espèce -->

            	<xsl:for-each select="../../sbml:listOfReactants/sbml:speciesReference[smtb:carbon[(@species=$species) and (@occurence=$occurence)]]">

                	<xsl:variable name="somme">
					
						<!-- On boucle donc maintenant sur les carbones du reactant. Si un carbone du reactant pointe vers
					    	 vers un carbone 13 du cumomère du produit, on note son numéro dans un élément <token/> -->
                    
						<xsl:for-each select="smtb:carbon[(@species=$species) and (@occurence=$occurence)]">
                        	<xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                            	<token>
                                	<xsl:value-of select="@position"/>
                            	</token>
                        	</xsl:if>
                    	</xsl:for-each>

						<!-- A la fin de cette boucle la variable "somme" contient un certain nombre, éventuellement nul, d'éléments <token> 
							qui identifient sans abiguité le cumomère concerné. -->
						
                	</xsl:variable>

                	<xsl:if test="sum(exslt:node-set($somme)/token)&gt;0"> <!-- On fait la somme des <token>, ce qui est une façon de savoir s'il y en a au moins un... -->

						<!-- On génére un nouvel élément <token> avec un attribut weight précisant le poids du cumomère concerné (le nombre de <token> dans $somme), le
						     type de l'espèce concernée (intermédiaire ou entrée) et son identificateur, que l'on obtient en faisant la somme des <token>. --> 

                    	<token weight="{count(exslt:node-set($somme)/token)}" type="{key('species',@species)/@type}">
                        	<xsl:value-of select="concat(@species,'_',sum(exslt:node-set($somme)/token))"/>
                    	</token>

                	</xsl:if>
            	</xsl:for-each>

				<!-- A la fin de cette boucle la variable "influx" contient un certain nombre de <token>. Suivant le type et le poids des cumomères
				    concernés, il s'agit soit d'inconnues, si le poids est le poids courant $weight, soit de quantités intervenant au second membre
					sous forme de produit, dont la somme des poids est égale au poids courant $weight. -->

			</xsl:variable>

			<!-- Ici on génère le code qui forme la matrice et le second membre du système qui permet d'obtenir les
		    	 cumomères de poids $weight -->

			<xsl:choose>
				<xsl:when test="(count(exslt:node-set($influx)/token)=1) and (exslt:node-set($influx)/token/@weight=$weight) and (exslt:node-set($influx)/token/@type='intermediate')">
				
					<!-- On n'a qu'un seul terme du poids courant, on assemble donc la matrice ainsi que sa dérivée -->
				
					<smtb:matrix-assignment id="{concat('M',$weight)}" row="{$position}" col="{key('cumomer',$influx)/@position}">
                        <m:apply>
                            <m:selector/>
                            <m:ci type="vector">v</m:ci>
						    <m:cn>
                            	<!-- id de la réaction -->
	                            <xsl:value-of select="$reaction"/>
                            </m:cn>
                        </m:apply>
					</smtb:matrix-assignment>
					<smtb:matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{$reaction}">
						<m:apply>
							<m:selector/>
							<m:ci type="matrix">
								<xsl:value-of select="concat('x',$weight)"/>
							</m:ci>
							<m:cn>
								<xsl:value-of select="key('cumomer',$influx)/@position"/>
							</m:cn>
							<m:cn>:</m:cn>
						</m:apply>
					</smtb:matrix-assignment>
				</xsl:when>
				<xsl:otherwise>

					<!-- On a un produit de plusieurs termes, donc de poids inférieur, on assemble donc le second membre
				    	 ainsi que la jacobienne du second membre par rapport aux cumomères de poids inférieur  -->

					<xsl:variable name="influx-factors">
						<xsl:call-template name="iterate-influx">
							<xsl:with-param name="tokens">
								<xsl:copy-of select="$influx"/>
							</xsl:with-param>
						</xsl:call-template>
					</xsl:variable>

					<smtb:matrix-assignment id="{concat('b',$weight)}" row="{$position}" col="1">
						<m:apply>
							<m:times type="array"/>
						    <m:apply>
                                <m:selector/>
                                <m:ci type="vector">v</m:ci>
						        <m:cn>
                                <!-- id de la réaction -->
	                                <xsl:value-of select="$reaction"/>
                                </m:cn>
                            </m:apply>
							<xsl:copy-of select="$influx-factors"/>
						</m:apply>						
					</smtb:matrix-assignment>
					
					<smtb:matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{$reaction}">
						<m:apply>
							<m:times type="array"/>
							<xsl:copy-of select="$influx-factors"/>
						</m:apply>
					</smtb:matrix-assignment>					

					<xsl:call-template name="iterate-influx-jacobian-x">
						<xsl:with-param name="reaction" select="$reaction"/>
						<xsl:with-param name="weight" select="$weight"/>
						<xsl:with-param name="tokens">
							<xsl:copy-of select="$influx"/>
						</xsl:with-param>
						<xsl:with-param name="position" select="$position"/>
						<xsl:with-param name="i">1</xsl:with-param>
						<xsl:with-param name="n" select="count(exslt:node-set($influx)/token)"/>
					</xsl:call-template>

				</xsl:otherwise>
			</xsl:choose>

    	</xsl:for-each>

    	<!-- outflux rule : partie la plus simple à générer (voir papier Wiechert) -->

		<smtb:matrix-assignment id="{concat('M',$weight)}" row="{$position}" col="{$position}">
			<m:apply>
				<m:minus/>
	        	<xsl:call-template name="bilan-outflux">
    	        	<xsl:with-param name="id" select="@species"/>
        		</xsl:call-template>
			</m:apply>
    	</smtb:matrix-assignment>

        <xsl:for-each select="key('reactants',@species)">
			<smtb:matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{../../@position}">
				<m:apply>
					<m:minus/>
					<m:apply>
						<m:selector/>
						<m:ci type="matrix">
						    <xsl:value-of select="concat('x',$weight)"/>
						</m:ci>
						<m:cn>
						    <xsl:value-of select="$position"/>
						</m:cn>
						<m:cn>:</m:cn>
					</m:apply>
				</m:apply>
			</smtb:matrix-assignment>
        </xsl:for-each>

    
</xsl:template>
 
 
<xsl:template name="bilan-outflux">
    <xsl:param name="id"/>

    <!-- outflux rule -->

    <m:apply>
        <m:plus/>
        <xsl:for-each select="key('reactants',$id)">
            <m:apply>
                <m:selector/>
                <m:ci type="vector">v</m:ci>
			    <m:cn>
                    <!-- id de la réaction -->
	                <xsl:value-of select="../../@position"/>
                </m:cn>
            </m:apply>
        </xsl:for-each>
    </m:apply>

</xsl:template>
 
<xsl:template name="iterate-influx">
	<xsl:param name="tokens"/>
	<xsl:variable name="id" select="exslt:node-set($tokens)/token[1]"/>
	
	<xsl:choose>
		<xsl:when test="key('cumomer',$id)">
			<m:apply>
				<m:selector/>
				<m:ci type="matrix">
					<xsl:value-of select="concat('x',key('cumomer',$id)/@weight)"/>
				</m:ci>
				<m:cn>
					<xsl:value-of select="key('cumomer',$id)/@position"/>
				</m:cn>
				<m:cn>:</m:cn>
			</m:apply>
		</xsl:when>
		<xsl:when test="key('input-cumomer',$id)">
			<m:apply>
				<m:selector/>
				<m:ci type="matrix">
					<xsl:value-of select="concat('x',key('input-cumomer',$id)/@weight,'_input')"/>
				</m:ci>
				<m:cn>
					<xsl:value-of select="key('input-cumomer',$id)/@position"/>
				</m:cn>
				<m:cn>:</m:cn>
			</m:apply>
		</xsl:when>
	</xsl:choose>
	
	<xsl:if test="count(exslt:node-set($tokens)/token)&gt;1">
		<xsl:call-template name="iterate-influx">
			<xsl:with-param name="tokens">
				<xsl:copy-of select="exslt:node-set($tokens)/token[position()&gt;1]"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<xsl:template name="iterate-influx-jacobian-x">

	<!-- Generation des jacobiennes (dérivées) dbi/dxj -->

	<xsl:param name="reaction"/>
	<xsl:param name="weight"/>
	<xsl:param name="tokens"/>
	<xsl:param name="position"/>
	<xsl:param name="i"/>
	<xsl:param name="n"/>

	<xsl:variable name="id" select="exslt:node-set($tokens)/token[position()=$i]"/>
	
	<xsl:if test="key('cumomer',$id)">
		<smtb:matrix-assignment id="{concat('db',$weight,'_dx',key('cumomer',$id)/@weight)}" row="{$position}" col="{key('cumomer',$id)/@position}">
			<m:apply>
				<m:times type="array"/>
				<xsl:call-template name="iterate-influx">
				
					<!-- La dérivée est égale à tout ce qui est en facteur de la variable -->
				
					<xsl:with-param name="tokens">
						<xsl:copy-of select="exslt:node-set($tokens)/token[position()!=$i]"/>
					</xsl:with-param>
				
				</xsl:call-template>
                <m:apply>
                    <m:selector/>
                    <m:ci type="vector">v</m:ci>
					<m:cn>
	                    <xsl:value-of select="$reaction"/>
                    </m:cn>
                </m:apply>				
			</m:apply>
		</smtb:matrix-assignment>
	</xsl:if>

	<xsl:if test="$i&lt;$n">
		<xsl:call-template name="iterate-influx-jacobian-x">
			<xsl:with-param name="reaction" select="$reaction"/>
			<xsl:with-param name="weight" select="$weight"/>
			<xsl:with-param name="tokens">
				<xsl:copy-of select="$tokens"/>
			</xsl:with-param>
			<xsl:with-param name="position" select="$position"/>
			<xsl:with-param name="i" select="($i)+1"/>
			<xsl:with-param name="n" select="$n"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>


<!--Construction des matrices C-->

 <xsl:template name="observation-matrices-open">
	 <xsl:param name="i" select="'1'"/>
	 <xsl:param name="number-of-observations"/>
	 <smtb:matrix-open id="C{$i}" rows="{$number-of-observations}" cols="{count(smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight=$i]/smtb:cumomer)}" type="sparse"/>
	 <xsl:if test="$i&lt;$weight">
		 <xsl:call-template name="observation-matrices-open">
			 <xsl:with-param name="i" select="($i)+1"/>
			 <xsl:with-param name="number-of-observations" select="$number-of-observations"/>
		 </xsl:call-template>
	 </xsl:if>
 </xsl:template>

 <xsl:template name="observation-matrices-close">
	 <xsl:param name="i" select="'1'"/>
	 <smtb:matrix-close id="C{$i}"/>
	 <xsl:if test="$i&lt;$weight">
		 <xsl:call-template name="observation-matrices-close">
			 <xsl:with-param name="i" select="($i)+1"/>
		 </xsl:call-template>
	 </xsl:if>
 </xsl:template>

<xsl:template name="construct_matrix_C">
    <smtb:comment>Matrices d'observation (cumomères)</smtb:comment>
    <smtb:optimize> <!-- On ouvre un élément "optimize" dans lequel on va construire petit à petit ces matrices -->
    <xsl:call-template name="observation-matrices-open">
	    <xsl:with-param name="number-of-observations" select="count(sbml:listOfSpecies/sbml:species[@type='intermediate']/smtb:measurement)"/>
    </xsl:call-template>						

    <xsl:for-each select="sbml:listOfSpecies/sbml:species[@type='intermediate']/smtb:measurement">
	    <xsl:variable name="position" select="position()"/>
	    <xsl:for-each select="smtb:cumomer-contribution">
		    <smtb:matrix-assignment id="C{@weight}" row="{$position}" col="{key('cumomer',@id)/@position}">
				<xsl:choose>
					<xsl:when test="@sign='1'">
						<m:cn>1</m:cn>
					</xsl:when>
					<xsl:otherwise> <!--@sign="'-1'-->
						<m:apply>
							<m:minus/>
							<m:cn>1</m:cn>
						</m:apply>
					</xsl:otherwise>
				</xsl:choose>
		    </smtb:matrix-assignment>
	    </xsl:for-each>
    </xsl:for-each>

    <xsl:call-template name="observation-matrices-close"/>
    </smtb:optimize>
</xsl:template>


<!--Construction de la matrice E"-->
<xsl:template name="construct_matrix_E">
	<smtb:comment>Matrice d'observation (flux)</smtb:comment>
	<smtb:optimize> <!-- On ouvre un élément "optimize" dans lequel on va construire petit à petit la matrice E -->
			<!--Construction de la matrice E"-->
 	<smtb:matrix-open id="E" cols="{count(sbml:listOfReactions/sbml:reaction)}" 
			         rows="{count(sbml:listOfReactions/sbml:reaction[(@known='yes') or (@observation='yes')])}" 
				 type="sparse"/>
	<xsl:for-each select="sbml:listOfReactions/sbml:reaction[(@known='yes') or (@observation='yes')]">
		<smtb:matrix-assignment id="E" row="{position()}" col="{@position}"><m:cn>1</m:cn></smtb:matrix-assignment>
	</xsl:for-each>			
	<smtb:matrix-close id="E"/> <!-- Lorsque l'on va rencontrer cet élément dans la transformation qui a pour
					but de produire le code Scilab, on rassemble tous les ordres d'affectation
					à la matrice "E" et on crée cette matrice -->
	</smtb:optimize>
</xsl:template>


<!-- Construction des matrices d'entrée, permettant d'obtenir la contribution sur les cumomères des isotopomères en entrée. -->	
<xsl:template name="construct_matrix_D">

	<smtb:comment>Matrices d'entrée</smtb:comment>
	<smtb:optimize> <!-- On ouvre un élément "optimize" dans lequel on va construire petit à petit les matrices D1,D2,...,Dn
             		où "n" est le poids maximum, déterminé lors des transformations préalables en fonction des 
			 observations choisies -->
			
	<xsl:variable name="number-of-inputs" select="count(sbml:listOfSpecies/sbml:species[smtb:input]/smtb:input)"/>
	<xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers[(@weight&lt;=$weight) and smtb:cumomer]">
		<smtb:matrix-open id="D{@weight}" cols="{$number-of-inputs}" rows="{count(smtb:cumomer)}" type="sparse"/>		
		<xsl:variable name="current-weight" select="@weight"/>
		<!-- Pour chaque cumomère d'un métabolité d'entrée, on détermine quelles sont les contributions des différents
		isotopomères en entrée. -->
		<xsl:for-each select="smtb:cumomer">
		<!-- Attention ici l'attribut position de l'élément smtb:cumomer ne représente que le numéro du
		cumomère dans le vecteur des cumomères du poids courant -->
			<xsl:variable name="position" select="@position"/>
			<!-- La variable $carbons contient les éléments <smtb:carbon/> correspondant aux carbones marqués du cumomère courant. -->
			<xsl:variable name="carbons" select="smtb:carbon"/>
			<!-- On boucle sur tous les isotopomères des métabolites en entrée. -->
			<xsl:for-each select="../../../sbml:listOfSpecies/sbml:species[smtb:input]/smtb:input">
			<!-- Element contextuel : un isotopomère d'entrée.
			Ici la variable $string contient le pattern avec des 0 et des 1 suivant que le carbone
			correspondant est marqué ou non. -->
				<xsl:variable name="string" select="@string"/>
				<!-- Attention on se concentre : si toutes les positions marquées d'un cumomères sont occupées par un "1" dans
				l'isotopomère, alors l'isotopomère contribue au cumomère. On met donc un "1" dans la matrice D1 (par exemple,
				pour les poids 1) à la position ligne=$position (du cumomère), colonne=position() (de l'isotopomère), de manière
				 à pouvoir utiliser ensuite cette matrice pour écrire x1_input=D1*[s1_input;s2_input;...];
				-->
				<xsl:if test="count(exslt:node-set($carbons)[substring($string,string-length($string)-@index,1)='1'])=count(exslt:node-set($carbons))">
					<smtb:matrix-assignment id="D{$current-weight}" row="{$position}" col="{position()}">
						<m:cn>1</m:cn>
					</smtb:matrix-assignment> 
				</xsl:if>	
			</xsl:for-each>	
		</xsl:for-each>
		<smtb:matrix-close id="D{@weight}"/>
	</xsl:for-each>	
	</smtb:optimize>		 
</xsl:template>

<!--Contruction des matrice de noms et ids des fluxs -->
<xsl:template name="names_and_ids_fluxes">
	<!-- Matrice des noms des fluxs -->
	
	<m:apply>
		<m:eq/>
		<m:ci>flux_names</m:ci>
		<m:list separator=";">
			<xsl:for-each select="sbml:listOfReactions/sbml:reaction">
				<smtb:string>
					<xsl:value-of select='@name'/>
				</smtb:string>
			</xsl:for-each>
		</m:list>
	</m:apply>

	<!-- Matrice des ids des fluxs -->

	<m:apply>
		<m:eq/>
		<m:ci>flux_ids</m:ci>
		<m:list separator=";">
			<xsl:for-each select="sbml:listOfReactions/sbml:reaction">
				<smtb:string>
					<xsl:value-of select='@id'/>
				</smtb:string>
			</xsl:for-each>
		</m:list>
	</m:apply>
</xsl:template>

<!--Cumomères en entrée, calculés à partir des isotopomères des métabolites d'entrée.-->
<xsl:template name="input_cumomeres">
	<smtb:comment>Cumomères en entrée, calculés à partir des isotopomères des métabolites d'entrée.</smtb:comment>
	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
		<m:apply>
			<m:eq/>
			<m:ci><xsl:value-of select="concat('x',@weight,'_input')"/></m:ci>
			<m:apply>
				<m:times/>
				<m:ci><xsl:value-of select="concat('D',@weight)"/></m:ci>
				<m:list separator=";">
				<xsl:for-each select="../../sbml:listOfSpecies/sbml:species[smtb:input]">
					<m:ci><xsl:value-of select="concat(@id,'_input')"/></m:ci>
				</xsl:for-each>
				</m:list>
			</m:apply>
		</m:apply>
	</xsl:for-each>	
</xsl:template>

<xsl:template name="fluxSubspace">
        <smtb:function>
            <m:ci>fluxSubspace</m:ci>
	        <smtb:input>
		        <m:list>
			        <m:ci>_fluxes</m:ci>
		        </m:list>
	        </smtb:input>
	        <smtb:output>
		        <m:list>
			        <m:ci>A</m:ci>
			        <m:ci>b</m:ci>
			        <m:ci>N</m:ci>
			        <m:ci>H</m:ci>
			        <m:ci>w</m:ci>
		        </m:list>
	        </smtb:output>
	        <smtb:body>
                <smtb:comment>Construction des matrices (dont la matrice de stoechiométrie) permettant
                de calculer le changement de variable affine des flux.</smtb:comment>

                <xsl:apply-templates select="sbml:listOfSpecies" mode="stoichiometric-matrix"/>

				<!-- Prise en compte d'égalités imposées par exmple pour les éspèces symétriques -->
				
				<xsl:if test="sbml:listOfRules/sbml:algebraicRule">
	                <xsl:apply-templates select="sbml:listOfRules" mode="stoichiometric-matrix"/>
				</xsl:if>

		        <smtb:comment>Flux imposés dans l'interface</smtb:comment>

		        <m:apply>
			        <m:eq/>
			        <m:list separator=",">
				        <m:ci>H</m:ci>
				        <m:ci>w</m:ci>
			        </m:list>
			        <m:apply>
				        <m:fn><m:ci>fluxConstraints</m:ci></m:fn>
				        <m:ci>_fluxes</m:ci>
			        </m:apply>
		        </m:apply>
                <m:apply>
                    <m:eq/>
                    <m:ci>b</m:ci>
                    <m:list>
                        <m:apply>
				            <m:fn><m:ci>zeros</m:ci></m:fn>
				            <m:cn>
					            <xsl:value-of select="count(sbml:listOfSpecies/sbml:species[@type='intermediate'])+count(sbml:listOfRules/sbml:algebraicRule)"/>
				            </m:cn>
				            <m:cn>1</m:cn>
			            </m:apply>
                        <m:ci>w</m:ci>
                    </m:list>
                </m:apply>
		        <m:apply>
			        <m:eq/>
			        <m:ci>A</m:ci>
			        <m:list separator=";">
				        <m:ci>N</m:ci>

						<!-- Prise en compte d'égalités imposées par exmple pour les éspèces symétriques -->
						<xsl:if test="sbml:listOfRules/sbml:algebraicRule">
					        <m:ci>R</m:ci>
						</xsl:if>

				        <m:ci>H</m:ci>
			        </m:list>
		        </m:apply>
	        </smtb:body>
        </smtb:function>  
</xsl:template>
</xsl:stylesheet>
