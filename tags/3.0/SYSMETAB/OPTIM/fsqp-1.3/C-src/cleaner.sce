mode(-1)
curdir=pwd()
chdir(get_absolute_file_path('cleaner.sce'))
files=listfiles(['*','.libs/*'])';
tokeep=["cintfsqp3.c";
	"cintfsqp2.c";
	"cfsqp.c";
	"builder.sce";
	"cfsqp.c.empty";
	"README";
	"Copyright";
	"cfsqpusr.h";
	"libcfsqp1.c";
	"libcfsqp2.c";
	"libcfsqp.c";
	"qld.c"
	 "FTables0.h"
	 "FTables.h"
	"cleaner.sce"];
for k=tokeep',files(files==k)=[];end
for f=files,mdelete(f),end
chdir(curdir)

clear files f mdelete listfiles curdir tokeep
