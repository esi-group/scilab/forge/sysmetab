function fj=obj71(j,x)
  fj=x(1)*x(4)*sum(x(1:3))+x(3)
endfunction

function gradfj=grob71(j,x)
  s=sum(x(1:3));p=x(1)*x(4);
  gradfj=[x(4)*s+p,p,1+p,x(1)*s];
endfunction

function gj=cntr71(j,x)
  select j
   case 1
    gj=25-prod(x);
   case 2
    gj=norm(x)^2-40;
  end
endfunction

function gradgj=grcn71(j,x)
  select j
   case 1
    gradgj=-(prod(x)./x)';
   case 2
    gradgj=2*x';
  end
endfunction

