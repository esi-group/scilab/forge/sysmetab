function ct=allcntr(x)
  ct=[];
  for k=1:ng0;
    g_k=null();
    g_k=list_cntr(1)(k);
    ct=[ct;g_k(x)];
  end
  for k=1:ncsrn
    G_k=null();
    G_k=list_cntr(2)(k);
    ct=[ct;G_k(x)];
  end
  for k=1:nc0
    c_k=null();
    c_k=list_cntr(3)(k);
    ct=[ct;c_k(x)];
  end
  for k=1:ncsrl
    C_k=null();
    C_k=list_cntr(4)(k);
    ct=[ct;C_k(x)];
  end
  for k=1:nh0
    h_k=null();
    h_k=list_cntr(5)(k);
    ct=[ct;h_k(x)];
  end
  for k=1:na0
    A_k=null();
    A_k=list_cntr(6)(k);
    ct=[ct;A_k(x)];
  end
endfunction



