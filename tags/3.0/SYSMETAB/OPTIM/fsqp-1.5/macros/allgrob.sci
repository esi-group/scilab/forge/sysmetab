function fp=allgrob(x)
  fp=[];
  for k=1:nf0;
    grf_k=null();
    grf_k=list_grobj(1)(k);
    fp=[fp;grf_k(x)];
  end
  for k=1:nfsr;
    grF_k=null();
    grF_k=list_grobj(2)(k);
    fp=[fp;grF_k(x)];
  end
endfunction
