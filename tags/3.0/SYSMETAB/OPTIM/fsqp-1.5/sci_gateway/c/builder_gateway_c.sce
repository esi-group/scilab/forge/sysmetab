// ====================================================================
// Copyright (C) INRIA -  Serge Steer, Allan CORNET
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

path=get_absolute_file_path('builder_gateway_c.sce');
//edit original cfsqp.c file to replace fprintf by sciprint
ierr=execstr('txt=mgetl(path+''cfsqp.c'',-1)','errcatch');
if ierr==241 then
  mprintf("%s\n",['In order to make this toolbox run';
		  'You need to get cfsqp.c file']);
  error('File cfsqp.c is missing, take a look at the readme file')
  exit
end
// we create Ncfsqp.c
// make some modification in cfsqp.c file (replace fprintf by sciprint)
txt=strsubst(txt,'fprintf(io,','sciprint(');
txt=strsubst(txt,'fprintf(stderr,','sciprint(');
txt=strsubst(txt,'fprintf(glob_prnt.io,','sciprint(');
txt=strsubst(txt,'small(','smallf(');//to avoid a name conflict under Windows
//txt=strsubst(txt,'\n','\r\n');
mputl(txt,path+'Ncfsqp.c');

src_files=['cintfsqp2.c'  'Ncfsqp.c','qld.c'];

table =['set_x_is_new'	'cintfsqp0'
	'x_is_new'	'cintfsqp1';
	'fsqp'  	'cintfsqp2'
	'srfsqp'	'cintfsqp3'];

//next test to work around a Windows bug with CFLAGS handling
if getos()=="Windows" then
  tbx_build_gateway('fsqp',table,..
		  src_files, ..
                  path,[],"","-D__STD_C__")
else
  tbx_build_gateway('fsqp',table,..
		  src_files, ..
                  path,[],"","-D__USE_DEPRECATED_STACK_FUNCTIONS__ -D__STD_C__ -I"+path)
end


clear tbx_build_gateway path src_files table ierr txt
