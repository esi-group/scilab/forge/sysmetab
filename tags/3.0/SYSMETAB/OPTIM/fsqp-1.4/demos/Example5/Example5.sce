mode(-1)
//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//

demopath = get_absolute_file_path("Example5.sce");
  methods=['Scilab Code','Scilab Code 2','C code','C code with estimated gradients',..
	   'Using lists']
sz=[334 118];
fsqp_demo_gui(demopath,'example5',sz,methods)
