mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 2 of the fsqp documentation

//the objective function
function fj=objmad(j,x)
  pi=cd(1);
  sintheta=sin(pi*(8.5+j*0.5)/180);
  fj=1/15+2/15*(cos(7*pi*sintheta)+sum(cos(2*pi*sintheta*x)) ) 
endfunction

//The constraints function
function gj=cnmad(j,x)
  ss=cd(2);
  select j
   case 1
    gj=ss-x(1);
   case 2
    gj=ss+x(1)-x(2);
   case 3
    gj=ss+x(2)-x(3);
   case 4
    gj=ss+x(3)-x(4);
   case 5
    gj=ss+x(4)-x(5);
   case 6
    gj=ss+x(5)-x(6);
   case 7
    gj=ss+x(6)-3.5;
  end
endfunction

modefsqp=111;
iprint=0;
miter=500;
bigbnd=1.e10;
eps=1.e-8;
epsneq=0.e0;
udelta=0.e0;
nf=163;
neqn=0;
nineqn=0;
nineq=7;
neq=0;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
rpar=[bigbnd,eps,epsneq,udelta];

x0=[0.5;1;1.5;2;2.5;3]; 
bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);


cd=[%pi,0.425];  //cd is a (semi-global var. known by objmad,cnmad) variable
//The gradient are estimated by finite differences grobfd and grcnfd C
//codes included in cfsqp.c

f=objmad(1,x0)
C=[];for j=1:7,C=[C;cnmad(j,x0)];end
mprintf('%s\n',['---------------------------MinMax problem with fsqp(Scilab code)-----------------------------------------'
		'Initial values'
		'   Value of the starting point:     '+sci2exp(x0,0)
		'   Value of the objective function: '+sci2exp(f,0)
		'   Value of the constraints :       '+sci2exp(C,0)])
timer();
x=fsqp(x0,ipar,rpar,[bl,bu],objmad,cnmad,'grobfd','grcnfd')
t=timer();
f=objmad(1,x);
C=[];for j=1:7,C=[C;cnmad(j,x)];end
mprintf('%s\n',['Final values'
		'   Value of the solution:           '+sci2exp(x,0)
		'   Value of the objective function: '+sci2exp(f,0)
		'   Value of the constraints :       '+sci2exp(C,0)
		'   Time :                           '+sci2exp(t,0)
	       ])







  
