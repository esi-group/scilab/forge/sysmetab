function a=s_full(ij,v,dims)
	a=full(sparse(ij,v,dims));
endfunction

function [V,v0,ff]=freefluxes(A)
// Détermination des "free fluxes"
	n1=size(A,1);
	n2=size(A,2)-n1;
	[L,U,E]=lu(A');
	E1=E(1:n1,:);
	E2=E(n1+1:$,:);
	[s,i]=sort(-E2*(1:n1+n2)','r');
	ff=-s;
	E2=E2(i,:);
	E2=E(n1+1:$,:);
	C1=[A*E1'*E1;E2];
	C2=[-A*E2';eye(n2,n2)];
	V=clean(C1\C2);
endfunction

function [varargout]=admissibilityTest(w)
    [A,b]=fluxSubspace(w);
	V=kernel(A);
	v0=A\b;
    n=length(v0);
    ierr=execstr('[x,lagr,f]=linpro(-V''*zeros(n,1),-V,v0)','errcatch');
    if ierr~=0
        xmllab_error('Pas de flux admissible, les flux connus sont incohérents.')
	    v=zeros(n,1);
    else
	    v=clean(V*x+v0);
    end
   if argn(0)==1
	varargout(1)=v;
   else	
    for i=1:length(v)
        varargout(i)=v(i);
    end
   end
endfunction

function v=optimize()
	nf=1;
	nineqn=0; 
	nineq=0;
	neqn=0; 
	neq=size(A,1);
	modefsqp=100; 
	miter=1000; 
	iprint=1;
	ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
	bigbnd=1.e10; eps=1.e-10; epsneq=0.e0; udelta=0.e0;
	rpar=[bigbnd,eps,epsneq,udelta];

	n=size(A,2);

	bl=0*ones(n,1);
	bu=+bigbnd*ones(n,1);
	V=kernel(A);
	v0=linpro(rand(n,1),A,b,zeros(n,1),%inf*ones(n,1),neq);
	v=fsqp(v0,ipar,rpar,[bl bu],_obj,_cntr,_grobj,_grcntr);
	v=clean(v)
				
endfunction

function oj=_obj(j,_v)
	if x_is_new()
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+.5*epsilon*_v'*_v;
		all_grobj=(grad+epsilon*_v')';
		all_cntr=A*_v-b;
		all_grcntr=A';
		oj=all_obj;
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		oj=all_obj;
	end
endfunction

function goj=_grobj(j,_v)
	if x_is_new()
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+.5*epsilon*_v'*_v;
		all_grobj=(grad+epsilon*_v')';
		all_cntr=A*_v-b;
		all_grcntr=A';
		goj=all_grobj;
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		goj=all_grobj;
	end
endfunction

function cj=_cntr(j,_v)
	if x_is_new()
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+.5*epsilon*_v'*_v;
		all_grobj=(grad+epsilon*_v')';
		all_cntr=A*_v-b;
		all_grcntr=A';
		cj=all_cntr(j);
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		cj=all_cntr(j);
	end
endfunction

function gcj=_grcntr(j,_v)
	if x_is_new()
		[cost,grad]=costAndGrad(_v);
		all_obj=cost+.5*epsilon*_v'*_v;
		all_grobj=(grad+epsilon*_v')';
		all_cntr=A*_v-b;
		all_grcntr=A';
		gcj=all_grcntr(:,j);
		set_x_is_new(0);
		[all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
	else
		gcj=all_grcntr(:,j);
	end
endfunction


rand('seed',getdate('s'))
