<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Wed Mar 21 11:13:38 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer le fichier
    XMLlab d�crivant l'interface du logiciel de simulation/optimisation
    sp�cifique � un r�seau donn�.
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:math="http://exslt.org/math" version="1.0"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
       
    <xsl:output method="xml" doctype-system="http://www.xmllab.org/dtd/1.5/fr/simulation.dtd" doctype-public="-//UTC//DTD XMLlab V1.5//FR" indent="yes" encoding="ISO-8859-1"/>
 
    <xsl:param name="file"/>
    <xsl:param name="verbose">no</xsl:param>
    <xsl:param name="type">stationnaire</xsl:param>
    <xsl:param name="experiences">1</xsl:param>

	<xsl:key name="reaction" match="sbml:reaction" use="@id"/>
    <xsl:key name="species" match="sbml:species" use="@id"/>
    <xsl:key name="input-cumomer" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="cumomer" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

	<xsl:variable name="weight" select="math:max(//sbml:species[@type='intermediate']/smtb:measurement/smtb:cumomer-contribution/@weight)"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
        <simulation>
          <xsl:apply-templates/>
        </simulation>
    </xsl:template>

    <xsl:template match="sbml:model">
        <xsl:variable name="all-fluxes">
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="unknown-fluxes">
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction[not(@known)]">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="known-fluxes">
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction[@known='yes']">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="input-cumomers">
            <xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers/smtb:cumomer[@weight&lt;=$weight]">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="cumomers">
            <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers/smtb:cumomer[@weight&lt;=$weight]">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        
        <header>
            <title>R�seau m�tabolique <xsl:value-of select="@id"/></title>
			<author>St�phane Mottelet</author>
			<date>
				<xsl:value-of select="concat(date:date(),', at ',date:time())"/>
			</date>
            <script href="{$file}.sci"/>
            <script href="sysmetab.sci"/>
        </header>
        <parameters>
            <section>
                <title>Flux</title>
                <xsl:apply-templates select="sbml:listOfReactions/sbml:reaction"/>
            </section>

            <section>
                <title>Flux observ�s</title>
                <xsl:apply-templates select="sbml:listOfReactions"/>
            </section>


    		<section>
        		<title>Marquages observ�s</title>
        		<xsl:apply-templates select="sbml:listOfSpecies/sbml:species[smtb:measurement]"/>
    		</section>

    		<section>
        		<title>Marquages connus</title>
        		<xsl:apply-templates select="sbml:listOfSpecies/sbml:species[smtb:input]"/>
    		</section>

		
            <actions>
                <title>Simulation</title>
                <action>
                    <xsl:attribute name="update">
                        <xsl:value-of select="$unknown-fluxes"/>
                    </xsl:attribute>
                    <title>Admissibilit� des flux connus</title>
                    <script>
						<!-- vecteur de pond�ration pour le calcul d'un flux admissible -->
						<xsl:variable name="p">
							<xsl:for-each select="str:split($all-fluxes)">
								<xsl:value-of select="concat(.,'_coeff')"/>
								<xsl:if test="position()&lt;last()">
									<xsl:text>;</xsl:text>
								</xsl:if>
							</xsl:for-each>
						</xsl:variable>
                        <xsl:value-of select="concat('[',translate($all-fluxes,' ',','),']=admissibilityTest([',translate($known-fluxes,' ',';'),'],[',$p,'])')"/>
                    </script>
                </action>
                <action>
                    <xsl:attribute name="update">
                        <xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
                            <xsl:value-of select="concat(@id,'_obs')"/>
                            <xsl:if test="position()&lt;last()">
                                <xsl:text> </xsl:text>
                            </xsl:if>    
                        </xsl:for-each>
                    </xsl:attribute>
                    <title>Calcul des cumom�res</title>
                    <script href="{$file}_script.sci"/>
 					<xsl:document href="{$file}_script.sci" method="text" encoding="ISO-8859-1">
						<xsl:value-of select="'z=['"/>
						<xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:input]">
							<xsl:value-of select="concat(@id,'_input')"/>
							<xsl:if test="position()&lt;last()">
								<xsl:value-of select="';'"/>
							</xsl:if>
						</xsl:for-each>
                    	<xsl:text>];&#xA;</xsl:text>
                    	<xsl:text>[</xsl:text>
                    	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
                        	<xsl:value-of select="concat('x',position())"/>
                        	<xsl:if test="position()&lt;last()">
                            	<xsl:text>,</xsl:text>
                        	</xsl:if>
                    	</xsl:for-each>
                    	<xsl:text>]=solveCumomers(</xsl:text>
                    	<xsl:value-of select="concat('[',$all-fluxes,'],')"/>
        				<xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers[(@weight&lt;=$weight) and (smtb:cumomer)]">
							<xsl:value-of select="concat('D',@weight,'*z')"/>
                        	<xsl:if test="position()&lt;last()">
                            	<xsl:text>,</xsl:text>
                        	</xsl:if>
						</xsl:for-each>										
                    	<xsl:text>);&#xA;</xsl:text>
						<xsl:text>y=</xsl:text>
						<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
							<xsl:value-of select="concat('C',@weight,'*x',@weight)"/>
							<xsl:if test="position()&lt;last()">
								<xsl:text>+</xsl:text>
							</xsl:if>
						</xsl:for-each>
						<xsl:text>;&#xA;</xsl:text>
                    	<xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
							<xsl:variable name="n" select="count(smtb:measurement)"/>
							<xsl:variable name="start" select="count(preceding::smtb:measurement)"/>
                        	<xsl:value-of select="concat(@id,'_obs(1:',$n,',',2+$experiences,':',1+2*$experiences,')=y(',1+$start,':',$n+$start,',1:',$experiences,');&#xA;')"/>
                    	</xsl:for-each>
					</xsl:document>
                </action>
            </actions>    
        </parameters>
    </xsl:template>

	<xsl:template match="sbml:listOfReactions">
			<matrix label="flux_obs" rows="{count(sbml:reaction[(@known='yes') or (@observation='yes')])}" cols="{($experiences)+2}">
				<name>
					<xsl:value-of select="'Flux'"/>
				</name>
				<colheader>
					<col>
						<name>Ecart type</name>
					</col>
					<xsl:call-template name="iterate-observation-names">
						<xsl:with-param name="header" select="'Exp.'"/>
					</xsl:call-template>
					<col state="disabled">
						<name>Ident.</name>
					</col>
				</colheader>
        		<xsl:for-each select="sbml:reaction[(@known='yes') or (@observation='yes')]">
					<row>
						<name>
                    		<xsl:value-of select="@name"/>
                		</name>
						<value>1</value>
            		</row>
        		</xsl:for-each>
			</matrix>
	</xsl:template>

    <xsl:template match="sbml:species[smtb:measurement]">
			<matrix label="{@id}_obs" rows="{count(smtb:measurement)}" cols="{2*($experiences)+1}">
				<name>
					<xsl:value-of select="@name"/>
				</name>
				<colheader>
					<col>
						<name>Ecart type</name>
					</col>
					<xsl:call-template name="iterate-observation-names">
						<xsl:with-param name="header" select="'Exp.'"/>
					</xsl:call-template>
					<xsl:call-template name="iterate-observation-names">
						<xsl:with-param name="header" select="'Ident.'"/>
						<xsl:with-param name="state" select="'disabled'"/>
					</xsl:call-template>
				</colheader>
        		<xsl:for-each select="smtb:measurement">
					<row>
						<name>
                    		<xsl:value-of select="@string"/>
                		</name>
						<value>1</value>
            		</row>
        		</xsl:for-each>
			</matrix>
    </xsl:template>

    <xsl:template match="sbml:species[smtb:input]">
			<matrix label="{@id}_input" rows="{count(smtb:input)}" cols="{$experiences}">
				<name>
					<xsl:value-of select="@name"/>
				</name>
				<colheader>
					<xsl:call-template name="iterate-observation-names">
						<xsl:with-param name="header" select="'Exp.'"/>
					</xsl:call-template>
				</colheader>
        		<xsl:for-each select="smtb:input">
					<row>
						<name>
                    		<xsl:value-of select="@string"/>
                		</name>
            		</row>
        		</xsl:for-each>
			</matrix>
    </xsl:template>

	<xsl:template name="iterate-observation-names">
		<xsl:param name="header"/>
		<xsl:param name="state" select="'normal'"/>
		<xsl:param name="i">1</xsl:param>
		<col state="{$state}">
			<name>
				<xsl:value-of select="concat($header,'',$i)"/>
			</name>
		</col>
		<xsl:if test="$i&lt;$experiences">
			<xsl:call-template name="iterate-observation-names">
				<xsl:with-param name="i" select="($i)+1"/>
				<xsl:with-param name="header" select="$header"/>
				<xsl:with-param name="state" select="$state"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
    <xsl:template match="sbml:reaction[@known='yes']">
		<group>		
        	<scalar label="{@id}">
            	<name>
                	<xsl:value-of select="concat(@id,', ',@name)"/>
            	</name>
            	<value>100</value>
        	</scalar>
			<scalar label="{@id}_coeff">
				<name>Coeff.</name>
				<value>0</value>
			</scalar>
		</group>
    </xsl:template>

    <xsl:template match="sbml:reaction">
		<group>
        	<scalar label="{@id}" state="disabled">
            	<name>
                	<xsl:value-of select="concat(@id,', ',@name)"/>
            	</name>
        	</scalar>
			<scalar label="{@id}_coeff">
				<name>Coeff.</name>
				<value>0</value>
			</scalar>
		</group>
    </xsl:template>

</xsl:stylesheet>
