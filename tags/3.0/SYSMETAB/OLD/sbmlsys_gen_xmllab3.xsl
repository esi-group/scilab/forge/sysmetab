<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Wed Mar 21 11:13:38 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer le fichier
    XMLlab d�crivant l'interface du logiciel de simulation/optimisation
    sp�cifique � un r�seau donn�.
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:math="http://exslt.org/math" version="1.0"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
       
    <xsl:output method="xml" doctype-system="http://www.xmllab.org/dtd/1.6/fr/simulation.dtd" 
	            doctype-public="-//UTC//DTD XMLlab V1.6//FR" indent="yes" encoding="ISO-8859-1"/>
 
    <xsl:param name="file"/>
    <xsl:param name="verbose">no</xsl:param>
    <xsl:param name="type">stationnaire</xsl:param>
    <xsl:param name="experiences">1</xsl:param>

	<xsl:key name="reaction" match="sbml:reaction" use="@id"/>
    <xsl:key name="species" match="sbml:species" use="@id"/>
    <xsl:key name="input-cumomer" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="cumomer" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

	<xsl:variable name="weight" select="math:max(//sbml:species[@type='intermediate']/smtb:measurement/smtb:cumomer-contribution/@weight)"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
        <simulation>
          <xsl:apply-templates/>
        </simulation>
    </xsl:template>

    <xsl:template match="sbml:model">
        <xsl:variable name="all-fluxes">
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="unknown-fluxes">
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction[not(@known)]">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="known-fluxes">
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction[@known='yes']">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="input-cumomers">
            <xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers/smtb:cumomer[@weight&lt;=$weight]">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="cumomers">
            <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers/smtb:cumomer[@weight&lt;=$weight]">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        
        <header>
            <title>R�seau m�tabolique <xsl:value-of select="@id"/></title>
            <title lang="english">Metabolic network <xsl:value-of select="@id"/></title>
			<author>St�phane Mottelet</author>
			<date>
				<xsl:value-of select="concat(date:date(),', at ',date:time())"/>
			</date>
            <script href="{$file}.sci"/>
            <script href="sysmetab4.sci"/>
        </header>
        <parameters>
            <section>
                <title>Flux</title>
                <title lang="english">Flux</title>
				<matrix label="fluxes" rows="{count(sbml:listOfReactions/sbml:reaction)}" cols="2">
					<name/>
					<colheader>
						<col>
							<name>Valeur en %</name>
							<name lang="english">Value in %</name>
						</col>
						<col>
							<name>Valeur impos�e</name>
							<name lang="english">Constrained value</name>
						</col>
					</colheader>
					<xsl:for-each select="sbml:listOfReactions/sbml:reaction">
						<row>
				            <name>
                				 <xsl:value-of select="concat(@id,', ',@name)"/>
            				</name>
				            <name lang="english">
                				 <xsl:value-of select="concat(@id,', ',@name)"/>
            				</name>
							<value>0,-1</value>
						</row>
					</xsl:for-each>
				</matrix>
            </section>

            <section>
                <title>Flux observ�s</title>
                <title lang="english">Fluxes observations</title>
                <xsl:apply-templates select="sbml:listOfReactions"/>
            </section>

    		<section>
        		<title>Marquages observ�s</title>
        		<title lang="english">Label observation</title>
        		<xsl:apply-templates select="sbml:listOfSpecies/sbml:species[smtb:measurement]"/>
				<scalar label="label_error" state="disabled">
					<name>Erreur sur les marquages</name>
					<name lang="english">Label error contribution</name>
				</scalar>
    		</section>

    		<section>
        		<title>Marquages connus</title>
        		<title lang="english">Known label</title>
        		<xsl:apply-templates select="sbml:listOfSpecies/sbml:species[smtb:input]"/>
    		</section>

    		<section>
        		<title>Cumom�res</title>
        		<title lang="english">Cumomers</title>
	            <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers/smtb:cumomer[@weight&lt;=$weight]">
					
				</xsl:for-each>
    		</section>


			<section>
				<title>Calcul</title>
				<title lang="english">Computations</title>
				<subsection>
					<title>Fonction cout</title>
					<title lang="english">Cost function</title>
					<scalar label="epsilon" min="0" max="%inf">
						<name>Param�tre de r�gularisation</name>
						<name lang="english">Regularization parameter</name>
						<value>1e-5</value>	
					</scalar>
				</subsection>
				<subsection>
					<title>Optimisation (fsqp)</title>
					<title lang="english">Optimisation (fsqp)</title>
					<scalar label="epsgrad" min="1e-10" max="%inf">
						<name>Tol�rance pour la convergence (norme du gradient projet�)</name>
						<name lang="english">Tolerance for convergence (norm of projected gradient)</name>
						<value>1e-8</value>	
					</scalar>
					<scalar label="miter" min="1" max="%inf">
						<name>Nombre maximum d'it�rations pour une optimisation</name>
						<name lang="english">Maximum number of iterations for a single optimization</name>
						<value>1000</value>	
					</scalar>
					<scalar label="itmax" min="1" max="%inf">
						<name>Nombre de tirages al�atoires de conditions initiales</name>
						<name lang="english">Number of successive random initial conditions</name>
						<value>10</value>	
					</scalar>
				</subsection>
			</section>
		
            <actions>
                <title>Simulation</title>
                <title lang="english">Simulation</title>
                <action>
                    <xsl:attribute name="update">
                        <xsl:text>fluxes label_error</xsl:text>
						<xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
                            <xsl:value-of select="concat(' ',@id,'_obs')"/>
                        </xsl:for-each>
                    </xsl:attribute>
                    <title>Optimisation</title>
                    <title lang="english">Optimization</title>
                    <script href="{$file}_optim.sce"/>
                </action>
                <action>
                    <xsl:attribute name="update">
                        <xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
                            <xsl:value-of select="concat(' ',@id,'_obs')"/>
                        </xsl:for-each>
                    </xsl:attribute>
                    <title>Calcul direct des cumom�res</title>
                    <title lang="english">Direct computation of cumomers</title>
                    <script href="{$file}_direct.sce"/>
                </action>
            </actions> 		
        </parameters>

    </xsl:template>

	<xsl:template match="sbml:listOfReactions">
		<matrix label="flux_obs" rows="{count(sbml:reaction[(@known='yes') or (@observation='yes')])}" cols="2">
			<name/>
			<colheader>
				<col>
					<name>1/Ecart type</name>
					<name lang="english">1/std.deviation</name>
				</col>
				<col>
					<name>Valeur en %</name>
					<name lang="english">Value in %</name>
				</col>
			</colheader>
        	<xsl:for-each select="sbml:reaction[(@known='yes') or (@observation='yes')]">
				<row>
					<name>
                    	<xsl:value-of select="@name"/>
                	</name>
					<name lang="english">
                    	<xsl:value-of select="@name"/>
                	</name>
					<value>1</value>
            	</row>
        	</xsl:for-each>
		</matrix>
	</xsl:template>

    <xsl:template match="sbml:species[smtb:measurement]">
			<matrix label="{@id}_obs" rows="{count(smtb:measurement)}" cols="{2*($experiences)+1}">
				<name>
					<xsl:value-of select="@name"/>
				</name>
				<name lang="english">
					<xsl:value-of select="@name"/>
				</name>
				<colheader>
					<col>
						<name lang="english">1/std.deviation</name>
					</col>
					<xsl:call-template name="iterate-observation-names">
						<xsl:with-param name="header" select="'Exp.'"/>
					</xsl:call-template>
					<xsl:call-template name="iterate-observation-names">
						<xsl:with-param name="header" select="'Ident.'"/>
						<xsl:with-param name="state" select="'disabled'"/>
					</xsl:call-template>
				</colheader>
        		<xsl:for-each select="smtb:measurement">
					<row>
						<name>
                    		<xsl:value-of select="concat('#',@string)"/>
                		</name>
						<name lang="english">
                    		<xsl:value-of select="concat('#',@string)"/>
                		</name>
						<value>1</value>
            		</row>
        		</xsl:for-each>
			</matrix>
    </xsl:template>

    <xsl:template match="sbml:species[smtb:input]">
			<matrix label="{@id}_input" rows="{count(smtb:input)}" cols="{$experiences}">
				<name>
					<xsl:value-of select="@name"/>
				</name>				
				<name lang="english">
					<xsl:value-of select="@name"/>
				</name>
				<colheader>
					<xsl:call-template name="iterate-observation-names">
						<xsl:with-param name="header" select="'Exp.'"/>
					</xsl:call-template>
				</colheader>
        		<xsl:for-each select="smtb:input">
					<row>
						<name>
                    		<xsl:value-of select="concat('#',@string)"/>
                		</name>	
						<name lang="english">
                    		<xsl:value-of select="concat('#',@string)"/>
                		</name>
            		</row>
        		</xsl:for-each>
			</matrix>
    </xsl:template>

	<xsl:template name="iterate-observation-names">
		<xsl:param name="header"/>
		<xsl:param name="state" select="'normal'"/>
		<xsl:param name="i">1</xsl:param>
		<col state="{$state}">
			<name>
				<xsl:value-of select="concat($header,'',$i)"/>
			</name>
			<name lang="english">
				<xsl:value-of select="concat($header,'',$i)"/>
			</name>
		</col>
		<xsl:if test="$i&lt;$experiences">
			<xsl:call-template name="iterate-observation-names">
				<xsl:with-param name="i" select="($i)+1"/>
				<xsl:with-param name="header" select="$header"/>
				<xsl:with-param name="state" select="$state"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>
	
    <xsl:template match="sbml:reaction">
        <scalar label="{@id}">
            <name>
                <xsl:value-of select="concat(@id,', ',@name)"/>
            </name>
            <name lang="engllish">
                <xsl:value-of select="concat(@id,', ',@name)"/>
            </name>
        </scalar>
    </xsl:template>

</xsl:stylesheet>
