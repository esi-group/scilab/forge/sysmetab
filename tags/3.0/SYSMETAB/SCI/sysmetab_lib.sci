function oj=_obj(j,x)
    // Computation of the cost function 
    // Note : we compute here all that is necessary to fsqp, namely the function
    // giving the satisfaction of constraints and their gradient respectively.
    // Since in practice we compute simultaneously the cost function and its 
    // gradient, this allows to not make unnecessary computations, using the 
    // function x_is_new() of the fsqp toolbox, which allows to know if the 
    // current x have changed or not.
    if x_is_new()
      [all_obj,all_grobj,all_cntr,all_grcntr]=all_stuff(x);
        oj=all_obj;
        set_x_is_new(0);
        [all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
    else
        oj=all_obj;
    end
endfunction

function goj=_grobj(j,x)
    // gradient computation of the cost function
    if x_is_new()
      [all_obj,all_grobj,all_cntr,all_grcntr]=all_stuff(x);
      goj=all_grobj;
        set_x_is_new(0);
        [all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
    else
        goj=all_grobj;
    end
endfunction

function cj=_cntr(j,x)
    // Compute of the function giving satisfaction to the constraints.
    if x_is_new()
      [all_obj,all_grobj,all_cntr,all_grcntr]=all_stuff(x);
      cj=all_cntr(j);
        set_x_is_new(0);
        [all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
    else
        cj=all_cntr(j);
    end
endfunction

function gcj=_grcntr(j,x)

// gradient computation of the function giving satisfaction to the constraints.

    if x_is_new()
      [all_obj,all_grobj,all_cntr,all_grcntr]=all_stuff(x);
      gcj=all_grcntr(:,j);
        set_x_is_new(0);
        [all_obj,all_grobj,all_cntr,all_grcntr]=resume(all_obj,all_grobj,all_cntr,all_grcntr);
    else
        gcj=all_grcntr(:,j);
    end
endfunction

function [obj,grobj,cntr,grcntr]=all_stuff(q)
    // Computation of a first parameter w=W*q+w0 using free fluxes.
    w=W*q+w0;
    [cost,grad,meas]=costAndGrad(w);
    obj=cost+.5*epsilon*q'*q;
    grobj=W'*grad+epsilon*q;
    // In cntr : constrains(CS), we start to put nonlinear inequality CS, 
    // then linear inequality CS, followed by nonlinear equality CS and
    // linear equality.
    cntr=C(me+1:$,:)*w-b(me+1:$);
    // We remove all evident inequality from cntr
    cntr=cntr(ineq_constr);
    grcntr=(C(me+1:$,:)*W)';
    grcntr=grcntr(:,ineq_constr);
endfunction

function [f,q]=optimize(qstart)
    // w0 and flux.values verify the equality constrains (CS) and the inequality
    // CS that why we reduce our problem by neglecting equality CS and working
    // with only inequality CS. In the other hand, we remove all evident 
    // inequality and zeros line of C(me+1:$,:)*W from inequality CS. 
    ineq_constr=find(clean(sum(abs(C(me+1:$,:)*W),'col'))~=0);
    nf=1; // is the number of cost function
    nineqn=0; // is the number of nonlinear inequality equation
    neqn=0; // is the number of nonlinear equality equation
    neq=0; // is the number of linear equality equation
    nineq=length(ineq_constr); // is the number of linear inequation equation
    modefsqp=110; // see documentation of fsqp
    miter=500; // is the maximum number of iterations allowed by the user 
              // before termination of execution.
    iprint=2; // is Parameter indicating the desired output
    ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
    bigbnd=1.e10; // play the role of infinity
    eps=1.e-6; // Final norm requirement for the Newton direction.
    epsneq=0.e0; // Maximum violation of nonlinear equality constraints allowed 
                 // by the user at an optimal point
    udelta=0.e0; // The perturbation size the user suggests to use in 
                 // approximating gradients by finite difference
    rpar=[bigbnd,eps,epsneq,udelta];
    bl=-bigbnd*ones(size(qstart,1),1); // array containing lower bounds for the 
                                  // components of q
    bu=-bl; // array containing upper bounds for the components of q
    // the input _obj,_cntr,_grobj and _grcntr must be define as a function
    // inform is a parameter indicating the status of the execution of fsqp
    // f is a value of the cost function at q at the end of the execution
    // lambda are the values of the Lagrange multipliers at q 
    // at the end of execution
    // g are the values of all constraints at q at the end of execution
    [q,inform,f,g,lambda]=fsqp(qstart,ipar,rpar,[bl,bu],_obj,_cntr,_grobj,_grcntr);
    w=W*q+w0;
    f=tlist(['fluxes' 'names' 'values'],flux_ids,w);
endfunction

function a=s_full(ij,v,dims)
    // It is a function to build full matrix with the same description of sparse
    // matrix, i.e. an array (ij) contains pairs of row indecies and column 
    // also the array of corresponding values (v).
    // Attention : this is not equivalent to :
    // a=full(sparse(ij,v,dims))
    // because here it is assumed that there are no multiple entries (i,j)
    a=zeros(dims(1),dims(2));
    if isempty(v) & isempty(ij)
      return;
    else
      if dims(1)==1
        v=v(:)';
      end
      a(ij(:,1)+dims(1)*ij(:,2)-dims(1))=v;
    end

endfunction

function spm=spdiag(v)
    // return the components of a column array in a diagonal sparse matrix
    sv=size(v);
    spm=sparse([(1:sv(1))',(1:sv(1))'],v)
endfunction    

function %fluxes_p(f)
    // User friendly display of a fluxes set
    nfluxes=size(f.names,'*')/2;
    maxlength=max(length(f.names));
    printf("%"+string(maxlength)+"s : %10s, %10s\n\n",'flux','net','xch');
    for i=1:nfluxes
        fname=strsubst(f.names(i),'_f','');
        fdnet=ftype(i)
        fdxch=ftype(i+nfluxes);
        printf("%"+string(maxlength)+"s : %+10.5f (%s), %10.5f (%s)\n",...
        fname,f.values(i),fdnet,f.values(i+nfluxes),fdxch)
    end
endfunction

function %meas_p(m)
    // User friendly display of a measurement vector
    maxlength=max(length(m.names))+2;
    nmeas=size(m.values,'*')
    printf("%"+string(maxlength)+"s : %7s\n\n","isotopomer","value");
    for i=1:nmeas
        printf("%"+string(maxlength)+"s : %7.5f\n",m.names(i),m.values(i));
    end
endfunction


function [W,w0,ff,ftype]=freefluxes(A,b)
    // Determination of "free fluxes" and of setting of affine subspace define 
    // by A*w=b, as w=W*q+w0, where q is a subset of w component, the "free fluxes"
    W=[];
    w0=[];
    ff=[];
    n=size(A,2);
    n1=size(A,1);
    // We use QR factorisation of A : A*P=QR with P of size (n,n), R of size (n1,n)
    // and Q of size (n1,n1). We use here the fact that the permutation P ensures
    // that the principale submatrix (n1,n1) of A*P has full rank if rank(A)=n1.
    // Thus, we have, by putting B=A*P and E=P'
    // Aw=b <==> B*E*w=b <==> [B1 B2]*[E1*w;E2*w]=b <==> B1*E1*w = -B2*E2*w
    // if we add to this, the trivial equation E2*w=E2*w, we obtain
    // Aw=b <==> [B1*E1;E2]*w = [b;0]-[B2;I]*E2*w <==> w = inv([B1*E1;E2])*[b;0]-inv([B1*E1;E2])*[B2;I]*E2*w
    // which give the affine relation between fluxes and free fluxes, represented
    // here by E2*w. In the sequel, we set : 
    // w0=inv([B1*E1;E2])*[b;0] et W=-inv([B1*E1;E2])*[B2;I].
    // This works too when A is not full rank, but this has no interest unless 
    // when b is the image of A, which is verify above when solving linear program.
    [Q,R,P]=qr(A);
    E=P';
    n1=rank(A);
    n2=size(A,2)-n1;
    E1=E(1:n1,:);
    E2=E(n1+1:$,:);
    C1=[A*E1'*E1;E2];
    C2=[-A*E2';eye(n2,n2)];
    W=clean(C1\C2);
    [s,i]=gsort(-E2*(1:n1+n2)'); // On remet les free fluxes dans l'ordre.
    ff=-s;
    W=W(:,i);
    ftype=string(zeros(n,1));
    ftype(:)='d';
    ftype(ff)='f';
    ftype(find(sum(abs(W),'c')==0))='c';
    // Attention, the w0 obtained here may have negatives components, this is quite normal.
    w0=clean(C1\[b;zeros(n2,1)]);
endfunction

function [f]=qld_test(C,b,me,Flux_M,b_Flux)
    // To verify that the user does not impose incoherent constraints, we try to
    // solve a linear program whose cost function is identically zero, which 
    // tests the admissible set {[C(1:me,:),Flux_M]*w=[b(1:me),b_Flux]} 
    // and {C(me+1:$,:)*w<=b(me+1,:)} are non-empty.
    n=size(C,2);
    Q=zeros(n,n);
    p=zeros(n,1);
    try
        [w,lagr]=qld(Q,p,C,b,[],[],me);
        if argn(2)>3 then
            try
                [w,lagr]=qld(Q,p,[C(1:me,:);Flux_M;C(me:$,:)],[b(1:me);b_Flux;b(me:$)],[],[],me+size(Flux_M,1));
            catch
                error('The choice of free fluxes or their values are inconsistent');
            end
        end
    catch
        error('The constraints are inconsistent');
    end
    f=tlist(['fluxes' 'names' 'values'],flux_ids,w);
endfunction

function [v,Phiprime]=Phi(w,eps)
    // "mollification" of transformation (net,xch)->(forward,backward)
    //  v=[v_xch+max(0,v_net)
    //     v_xch-min(0,v_net)];
    n=length(w)/2;
    v_net=w(1:n,:);
    v_xch=w(n+1:$);
    v=[v_xch+phi(v_net,eps)
    v_xch+phi(-v_net,eps)];
    Phiprime=[spdiag(phiprime(v_net,eps)) ,speye(n,n);
             -spdiag(phiprime(-v_net,eps)),speye(n,n)];
endfunction

function y=phi(x,e)
    // A smooth function near (0,0)
    y=(x>-e & x<e).*(x+e)^2/4/e+(x>=e).*x;
endfunction

function y=phiprime(x,e)
    // The derivative of the smooth function near (0,0)
    y=(x>-e & x<e).*(x+e)/2/e+(x>=e);
endfunction

function [cost,grad,meas]=costAndGrad(w)
    // This function return the cost function and its gradient
    [v,dv_dw]=Phi(w,eps_phi);
    [cost,grad,y]=solveCumomersAndGradAdjoint(v);
    grad=dv_dw'*grad;
    meas=tlist(['meas' 'names' 'values'],measurement_ids,y);
endfunction

function [V]=variance(f)
    // This function return the variance of w=W*q+w0
    // we recall the residual r(q)=\sigma^-1*(ymeas-y(v))
    // where v=\Phi(w) and 
    // dr/dq = -\sigma^-1 * dy(v)/dq * d(\Phi(w))/dq * W
    // here d(\Phi(w))/dq = Phiprime obtained from
    w=f.values;
    [v,dv_dw]=Phi(w,eps_phi);
    // dy(v)/dq = dy_dv obtained from
    [cost,grad,y,dy_dv]=solveCumomersAndGradDirect(v);
    dr_dq=-spdiag(sqrt(Sypm2))*dy_dv*dv_dw*W;
    // Fisher matrix = (dr/dq)'*(dr/dq)
    Fisher=dr_dq'*dr_dq;
    // variance is the inverse of Fisher matrix
    V=inv(Fisher+%eps*eye(Fisher));
endfunction

function [cost,grad,meas]=costAndGradSaveData(w)
    // This function return the cost function and its gradient
    // We also save the measurment data in a .txt file
    [v,dv_dw]=Phi(w,eps_phi);
    [cost,grad,y]=solveCumomersAndGradDirect(v);
    grad=dv_dw'*grad;
    fid = mopen("extract_data_from_scilab.txt", "w");
    if (fid == -1)
      error('cannot open file for writing');
    end
    mfprintf(fid, "%s\n", "Ids");
    mfprintf(fid, "%s\n", measurement_ids);
    mfprintf(fid, "%s\n", "Values");
    mfprintf(fid, "%.25f\n", y);
    mclose(fid);
    meas=tlist(['meas' 'names' 'values'],measurement_ids,y);
endfunction
