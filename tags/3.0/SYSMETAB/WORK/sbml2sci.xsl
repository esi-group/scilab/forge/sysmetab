<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"    version="1.0" >
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
  <xsl:strip-space elements="*"/>
    
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="sbml:sbml">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="sbml:model">
    <simulation>
        <parameters>
            <xsl:apply-templates select="sbml:listOfParameters"/>
            <xsl:call-template name="sbml:simulationDuration"/>
        </parameters>
        <compute>
            <xsl:apply-templates select="sbml:listOfSpecies"/>
        </compute>
    </simulation>
  </xsl:template>
  
  <xsl:template match="sbml:listOfParameters">
        <section>
            <title>Param�tres du mod�le</title>
            <xsl:apply-templates/>  
        </section>
  </xsl:template>
  
  <xsl:template name="sbml:simulationDuration">
        <section>
            <title>Param�tres de simulation</title>
            <scalar label="Tf">
                <name>Temps final</name>
                <value>1</value>                
            </scalar>
            <scalar label="Tsteps">
                <name>Nombre de points</name>
                <value>100</value>                
            </scalar>
        </section>
  </xsl:template>
  
  <xsl:template match="parameter">
    <scalar label="{@id}">
        <name>
            <xsl:value-of select="@id"/>
        </name>
        <value>
            <xsl:value-of select="@value"/>
        </value>
    </scalar>
  </xsl:template>
  
  <xsl:template match="sbml:listOfSpecies">
    <ode label="model_ode">
        <defdomain1d label="t">
            <name>Temps</name>
            <interval steps="Tsteps">
                <initialvalue>0</initialvalue>
                <finalvalue>Tf</finalvalue>
            </interval>
            <states>
                <xsl:apply-templates select="sbml:species"/>
            </states>
        </defdomain1d>
    </ode>
  </xsl:template>

  <xsl:template match="sbml:species">
    <xsl:choose>
        <xsl:when test="//sbml:listOfReactions//sbml:speciesReference[@species=current()/@id]">
            <state label="{@id}">
                <derivative>
                    <xsl:for-each select="//sbml:listOfReactants/sbml:speciesReference[@species=current()/@id]">
                    -
                    </xsl:for-each>
                    <xsl:for-each select="//sbml:listOfProducts/sbml:speciesReference[@species=current()/@id]">
                    +
                    </xsl:for-each>               
                    </derivative>
                <initialcondition>
                    <xsl:value-of select="@initialConcentration"/>
                </initialcondition>
            </state>
        </xsl:when>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="speciesReference" mode="build_ode_rhs">
    
  </xsl:template>


</xsl:stylesheet>
