<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Thu Feb  8 09:50:47 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer les �quations du syst�me
    sous forme de texte lisible par un humain. La sortie n'est pas destin�e
    � un logiciel particulier, mais on pourra s'inspirer du code ci-dessous
    pour �crire une feuille de style destin�e par exemple � Maple ou autre.
-->


<xsl:stylesheet   version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"  
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="smtb sbml celldesigner m str exslt xhtml">

    <xsl:param name="weight">1</xsl:param>

    <xsl:output method="text"  encoding="iso-8859-1"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="carbon-labeling-system">
        <xsl:apply-templates/>
    
        <!-- ranking -->
        
        <xsl:text>&#xA;&#xA;elimiden:=</xsl:text>
        <xsl:text>[</xsl:text>
        
        <xsl:for-each select="listOfSpecies/species[not(@measurement)]/smtb:cumomer[@weight&lt;=$weight]">
            <xsl:value-of select="@id"/>
            <xsl:text>,</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="listOfSpecies/species[@measurement]/smtb:cumomer[@weight&lt;=$weight]">
            <xsl:value-of select="@id"/>
            <xsl:text>,</xsl:text>
        </xsl:for-each>
        <xsl:for-each select="listOfReactions/reaction[@known='yes']">
                    <xsl:value-of select="@id"/>
            <xsl:if test="position()&lt;last()">
                <xsl:text>,</xsl:text>
            </xsl:if>
        </xsl:for-each>
                
        <xsl:text>],{</xsl:text>
        <xsl:for-each select="listOfReactions/reaction[not(@known)]">
                    <xsl:value-of select="@id"/>
            <xsl:if test="position()&lt;last()">
                <xsl:text>,</xsl:text>
            </xsl:if>
        </xsl:for-each>
        <xsl:text>};</xsl:text>
        
    </xsl:template>    
    
    <xsl:template match="listOfReactions"/>

    <xsl:template match="listOfSpecies">
        <xsl:apply-templates select="//equation">
            <xsl:sort select="@weight"/>
        </xsl:apply-templates>
        <xsl:text>&#xA;sys:=[</xsl:text>
        <xsl:apply-templates select="//equation" mode="sys">
            <xsl:sort select="@weight"/>
        </xsl:apply-templates>
        <xsl:text>];</xsl:text>
    </xsl:template>
       
    <xsl:template match="species"/>

    
        <xsl:template match="m:apply[m:eq]">
        <xsl:if test="ancestor::smtb:body">
    		<xsl:text>    </xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>=</xsl:text>
        <xsl:apply-templates select="*[3]"/>
		<xsl:text>;&#xA;</xsl:text>
    </xsl:template>
    
    <xsl:template match="equation">
        <xsl:value-of select="concat('p',position())"/>
        <xsl:text>:=</xsl:text>
        <xsl:apply-templates select="m:apply/*[3]"/>   
        <xsl:text>;&#xA;</xsl:text>     
    </xsl:template>


    <xsl:template match="equation" mode="sys">
        <xsl:value-of select="concat('p',position())"/>
        <xsl:if test="position()&lt;last()">
            <xsl:text>,</xsl:text>
        </xsl:if>
    </xsl:template>

    
    <xsl:template match="m:apply[m:plus]">
        <xsl:for-each select="*[position()&gt;1]">
            <xsl:apply-templates select="."/>
            <xsl:if test="position()&lt;last()">
                <xsl:text>+</xsl:text>
            </xsl:if>
        </xsl:for-each> 
    </xsl:template>

    <xsl:template match="m:apply[m:plus and ((preceding-sibling::m:times) or (preceding-sibling::m:minus)) and (count(*)&gt;2)]">
        <xsl:text>(</xsl:text>
        <xsl:for-each select="*[position()&gt;1]">
            <xsl:apply-templates select="."/>
            <xsl:if test="position()&lt;last()">
                <xsl:text>+</xsl:text>
            </xsl:if>
        </xsl:for-each> 
        <xsl:text>)</xsl:text>
    </xsl:template>


    <xsl:template match="m:apply[m:minus and (count(*)=3)]">
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>-</xsl:text>
        <xsl:apply-templates select="*[3]"/>
    </xsl:template>

    <xsl:template match="m:apply[m:minus and (count(*)=2)]">
        <xsl:text>-</xsl:text>
        <xsl:apply-templates select="*[2]"/>
    </xsl:template>

   
   <xsl:template match="m:apply[m:times]">
        <xsl:for-each select="*[position()&gt;1]">
            <xsl:apply-templates select="."/>
            <xsl:if test="position()&lt;last()">
                <xsl:text>*</xsl:text>
            </xsl:if>
        </xsl:for-each> 
    </xsl:template>
    

    <xsl:template match="m:apply[m:diff]">
        <xsl:text>d(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)/dt</xsl:text>
    </xsl:template>



    <xsl:template match="m:apply[m:selector and m:ci[@type='vector']]">
		<xsl:apply-templates select="m:ci[1]"/>
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="m:cn[1]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <xsl:template match="m:apply[m:selector and m:ci[@type='matrix']]">
		<xsl:apply-templates select="m:ci[1]"/>
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="m:cn[1]"/>
		<xsl:text>,</xsl:text>
        <xsl:apply-templates select="m:cn[2]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>

    <xsl:template match="m:apply[m:backslash]">
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>\</xsl:text>
        <xsl:apply-templates select="*[3]"/>
    </xsl:template>

    <xsl:template match="m:apply[m:fn]">
        <xsl:apply-templates select="m:fn/m:ci"/>
		<xsl:text>(</xsl:text>
		<xsl:for-each select="*[not(self::m:fn)]">
			<xsl:apply-templates select="."/>
			<xsl:if test="position()&lt;last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>)</xsl:text>
    </xsl:template>

	<xsl:template match="smtb:input/m:list">
		<xsl:text>(</xsl:text>
		<xsl:for-each select="*">
			<xsl:apply-templates select="."/>
			<xsl:if test="position()&lt;last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>)</xsl:text>
	</xsl:template>


	<xsl:template match="smtb:output/m:list">
		<xsl:text>[</xsl:text>
		<xsl:for-each select="*">
			<xsl:apply-templates select="."/>
			<xsl:if test="position()&lt;last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>]</xsl:text>
	</xsl:template>


	<xsl:template match="m:list">
		<xsl:text>[</xsl:text>
		<xsl:for-each select="*">
			<xsl:apply-templates select="."/>
			<xsl:if test="position()&lt;last()">
				<xsl:text>;</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>]</xsl:text>
	</xsl:template>

	<xsl:template match="smtb:function">
		<xsl:text>function </xsl:text>
		<xsl:apply-templates select="smtb:output"/>
		<xsl:text>=</xsl:text>
		<xsl:apply-templates select="m:ci"/>
		<xsl:apply-templates select="smtb:input"/>
		<xsl:text>&#xA;</xsl:text>
		<xsl:apply-templates select="smtb:body"/>
		<xsl:text>endfunction&#xA;</xsl:text>
	</xsl:template>
	
	<xsl:template match="smtb:output">
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="smtb:input">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="smtb:body">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="smtb:comment">
        <xsl:if test="ancestor::smtb:body">
    		<xsl:text>    </xsl:text>
        </xsl:if>		
        <xsl:text>// </xsl:text>
        <xsl:value-of select="."/>
		<xsl:text>&#xA;</xsl:text>
	</xsl:template>
	
    <xsl:template match="m:ci">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="m:cn">
        <xsl:value-of select="."/>
    </xsl:template>


</xsl:stylesheet>
