<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet, LMAC
    Date    :   
    Projet  :   GENESYS WP4
-->

<!-- Cette feuille de style a pour but de g�n�rer un fichier XML avec une structure
    permettant de ...
    
    <graph>
    </graph>
    
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:math="http://exslt.org/math" version="1.0"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml exslt smtb math">
       
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
 
    <xsl:param name="maxweight" select="math:max(//sbml:species/@carbons)"/>

    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
        <graph>
          <xsl:apply-templates/>
        </graph>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:model">
            <xsl:apply-templates select="sbml:listOfSpecies"/>
    </xsl:template>

    <xsl:template match="sbml:listOfSpecies">
        
        <!-- Boucle sur les esp�ces qui sont soit des produits (products) soit des substrats (educts) : -->        
        
        <xsl:for-each select="sbml:species[key('reactionSpecies',@id)]"> 

           <xsl:for-each select="smtb:cumomer[@weight&lt;=$maxweight]">
				<xsl:element name="node">
					<xsl:copy-of select="@id"/>
					<xsl:copy-of select="@species"/>
					<xsl:copy-of select="@subscript"/>
					<xsl:copy-of select="@weight"/>
					<xsl:copy-of select="../@type"/>										
					<xsl:if test="../smtb:measurement/smtb:cumomer-contribution[@id=current()/@id]">
						<xsl:attribute name="source">yes</xsl:attribute>
					</xsl:if>
					<xsl:if test="../@type='intermediate'">
    					<xsl:call-template name="iteration"/>
    			    </xsl:if>
                </xsl:element>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:template>
   
<!-- Template de d�termination des cumom�res intervenant au second membre de l'�quation de bilan -->

<xsl:template name="iteration">

    <!-- Attention le noeud contextuel est un �lement <smtb:cumomer> -->

    <xsl:variable name="name" select="../@id"/>
    
    <xsl:variable name="carbons">
        <xsl:copy-of select="smtb:carbon"/>
    </xsl:variable>

    <xsl:for-each select="key('products',../@id)"> 

        <!-- Maintenant, on essaye de trouver des occurences des carbones marqu�s
             du reactant de la r�action courante : c'est du boulot... -->

        <xsl:variable name="species" select="@species"/>
        <xsl:variable name="occurence" select="count(preceding-sibling::sbml:speciesReference)+1"/>

            <!-- C'est ici que les choses s�rieuses commencent. On boucle sur tous les r�actants de la r�action courante : -->

        <xsl:for-each select="../../sbml:listOfReactants/sbml:speciesReference[smtb:carbon[(@species=$species) and (@occurence=$occurence)]]">

            <xsl:variable name="somme">
                <xsl:for-each select="smtb:carbon[(@species=$species) and (@occurence=$occurence)]">
                    <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                        <token>
                            <xsl:value-of select="@position"/>
                        </token>
                    </xsl:if>
                </xsl:for-each>
            </xsl:variable>

            <xsl:if test="sum(exslt:node-set($somme)/token)&gt;0">
                <child node="{concat(@species,'_',sum(exslt:node-set($somme)/token))}"/>
            </xsl:if>

        </xsl:for-each>
    </xsl:for-each>
    
</xsl:template>

</xsl:stylesheet>
