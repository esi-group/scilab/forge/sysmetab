<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Mon May 21 16:41:34 CEST 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer 

    1 - une fonction C permettant de remplir les matrices et les vecteurs
    permettant de calculer les diff�rentes fractions des cumom�res en fonction des flux
    et des fractions des cumom�res des entr�es. Exemple pour le "branching network"
    du papier "part II" de Wiechert :
    

    Ici on fait abstraction des identificateurs originaux des cumom�res et des flux.
    
    Ici on g�n�re en fait cette fonction d�crite en MathML, il faut enchainer avec
    une derni�re transformation pour avoir le code C
        
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"
    xmlns:math="http://exslt.org/math"
        version="1.0"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
       
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
 
    <xsl:param name="verbose">no</xsl:param>
    <xsl:param name="type">stationnaire</xsl:param>
    <xsl:param name="weight">2</xsl:param>

	<xsl:key name="reaction" match="sbml:reaction" use="@id"/>
    <xsl:key name="species" match="sbml:species" use="@id"/>
    <xsl:key name="input-cumomer" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="cumomer" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
        <carbon-labeling-system>
          <xsl:apply-templates/>
        </carbon-labeling-system>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:model">

	    <xsl:apply-templates select="smtb:listOfIntermediateCumomers"/>
                               
    </xsl:template>


    <xsl:template match="smtb:listOfIntermediateCumomers">
        
        <xsl:for-each select="smtb:listOfCumomers[@weight&lt;=$weight]">    

			<!-- D�claration et initialisation matrice et vecteur -->
            
            
            
			
            <smtb:comment>Cumom�res de poids <xsl:value-of select="@weight"/></smtb:comment>

			<m:apply>
				<m:eq/>
				<m:ci>
					<xsl:value-of select="concat('M',@weight)"/>
				</m:ci>
				<m:apply>
					<m:fn><m:ci>zeros</m:ci></m:fn>
					<m:cn>
						<xsl:value-of select="count(smtb:cumomer)"/>
					</m:cn>
					<m:cn>
						<xsl:value-of select="count(smtb:cumomer)"/>
					</m:cn>
				</m:apply>
			</m:apply>     

			<m:apply>
				<m:eq/>
				<m:ci>
					<xsl:value-of select="concat('b',@weight)"/>
				</m:ci>
				<m:apply>
					<m:fn><m:ci>zeros</m:ci></m:fn>
					<m:cn>
						<xsl:value-of select="count(smtb:cumomer)"/>
					</m:cn>
					<m:cn>1</m:cn>
				</m:apply>
			</m:apply>     



            <xsl:for-each select="smtb:cumomer">
                    <xsl:call-template name="iteration"/>                                    
            </xsl:for-each>

            <smtb:comment>Ici on est sens� r�soudre et obtenir x<xsl:value-of select="@weight"/></smtb:comment>
			
        </xsl:for-each>
        
		
    </xsl:template>
   

<xsl:template name="bilan-outflux">
    <xsl:param name="id"/>

    <!-- outflux rule -->

    <m:apply>
        <m:plus/>
        <xsl:for-each select="key('reactants',$id)">
            <m:apply>
                <m:selector/>
                <m:ci type="vector">v</m:ci>
			    <m:cn>
                <!-- id de la r�action -->
	                <xsl:value-of select="key('reaction',../../@id)/@position"/>
                </m:cn>
            </m:apply>
        </xsl:for-each>
    </m:apply>

</xsl:template>


<xsl:template name="iteration">
    
    <!-- Attention le noeud contextuel est un �lement <smtb:cumomer> -->

    <xsl:variable name="name" select="../@id"/>
    
    <xsl:variable name="carbons">
        <xsl:copy-of select="smtb:carbon"/>
    </xsl:variable>
	
    <xsl:variable name="weight" select="@weight"/>
    <xsl:variable name="position" select="@position"/>

    <!-- outflux rule : partie la plus simple � g�n�rer (voir papier Wiechert) -->
			
    <m:apply>
		<m:eq/>
		<m:apply>
			<m:selector/>
			<m:ci type="vector">
				<xsl:value-of select="concat('i',$weight)"/>
			</m:ci>
			<m:ci>k</m:ci>
		</m:apply>
		<m:cn>
			<xsl:value-of select="($position)-1"/> <!-- -1 � cause du C -->
		</m:cn>
    </m:apply>

    <m:apply>
		<m:eq/>
		<m:apply>
			<m:selector/>
			<m:ci type="vector">
				<xsl:value-of select="concat('j',$weight)"/>
			</m:ci>
			<m:ci>k</m:ci>
		</m:apply>
		<m:cn>
			<xsl:value-of select="($position)-1"/> <!-- -1 � cause du C -->
		</m:cn>
    </m:apply>

    <m:apply>
		<m:eq/>
		<m:apply>
			<m:selector/>
			<m:ci type="vector">
				<xsl:value-of select="concat('v',$weight)"/>
			</m:ci>
			<m:ci>k++</m:ci>
		</m:apply>
		<m:apply>
			<m:minus/>
	        <xsl:call-template name="bilan-outflux">
    	        <xsl:with-param name="id" select="@species"/>
        	</xsl:call-template>
		</m:apply>
    </m:apply>






	<!-- influx rule : le plus chiant, mais aussi le plus int�ressant. C'est l� que se cr�e v�ritablement
     	l'information suppl�mentaire par rapport � la stoechiom�trie simple. 

     On boucle sur tous les �l�ments <speciesReference> qui ont l'esp�ce courante comme produit,
     donc dans le for-each le noeud contextuel est de type reaction/listOfProducts/speciesReference. -->

    <xsl:for-each select="key('products',@species)"> 

        <!-- Maintenant, on essaye de trouver des occurences des carbones marqu�s
             du reactant de la r�action courante : c'est du boulot... -->

        <xsl:variable name="species" select="@species"/>
        <xsl:variable name="occurence" select="count(preceding-sibling::sbml:speciesReference)+1"/>

        <xsl:variable name="influx">

            <!-- C'est ici que les choses s�rieuses commencent. On boucle sur tous les r�actants : -->

            <xsl:for-each select="../../sbml:listOfReactants/sbml:speciesReference[smtb:carbon[(@species=$species) and (@occurence=$occurence)]]">
                <xsl:variable name="somme">
                    <xsl:for-each select="smtb:carbon[(@species=$species) and (@occurence=$occurence)]">
                        <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                            <token>
                                <xsl:value-of select="@position"/>
                            </token>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:variable>
                <xsl:if test="sum(exslt:node-set($somme)/token)&gt;0">
                    <token weight="{count(exslt:node-set($somme)/token)}" type="{key('species',@species)/@type}">
                        <xsl:value-of select="concat(@species,'_',sum(exslt:node-set($somme)/token))"/>
                    </token>
                </xsl:if>
            </xsl:for-each>

		</xsl:variable>
        
		<xsl:choose>
			<xsl:when test="(count(exslt:node-set($influx)/token)=1) and (exslt:node-set($influx)/token/@weight=$weight) and (exslt:node-set($influx)/token/@type='intermediate')">
				<m:apply>
						<m:eq/>
						<m:apply>
							<m:selector/>
							<m:ci type="vector">
								<xsl:value-of select="concat('i',$weight)"/>
							</m:ci>
                            <m:ci>k</m:ci>
                        </m:apply>
						<m:cn>
							<xsl:value-of select="($position)-1"/> <!-- -1 � cause du C -->
						</m:cn>
				</m:apply>
                <m:apply>
                    <m:eq/>
					<m:apply>
						<m:selector/>
						<m:ci type="vector">
							<xsl:value-of select="concat('j',$weight)"/>
						</m:ci>
                        <m:ci>k</m:ci>
                    </m:apply>
                    <m:cn>
						<xsl:value-of select="(key('cumomer',$influx)/@position)-1"/> <!-- -1 � cause du C -->
					</m:cn>    
				</m:apply>
                <m:apply>
                    <m:eq/>
					<m:apply>
						<m:selector/>
						<m:ci type="vector">
							<xsl:value-of select="concat('v',$weight)"/>
						</m:ci>
                        <m:ci>k++</m:ci>
                    </m:apply>                
                    <m:apply>
                        <m:selector/>
                        <m:ci type="vector">v</m:ci>
						<m:cn>
                        <!-- id de la r�action -->
	                        <xsl:value-of select="key('reaction',../../@id)/@position"/>
                        </m:cn>
                    </m:apply>
				</m:apply>
			</xsl:when>
			<xsl:otherwise>
				<m:apply>
					<m:eqplus/>
					<m:apply>
						<m:selector/>
						<m:ci type="vector">
							<xsl:value-of select="concat('b',$weight)"/>
						</m:ci>
						<m:cn>
							<xsl:value-of select="$position"/>
						</m:cn>
					</m:apply>
					<m:apply>
						<m:times/>
						<m:apply>
                            <m:selector/>
                            <m:ci type="vector">v</m:ci>
						    <m:cn>
                            <!-- id de la r�action -->
	                            <xsl:value-of select="key('reaction',../../@id)/@position"/>
                            </m:cn>
                        </m:apply>
						<xsl:call-template name="iterate-influx">
							<xsl:with-param name="tokens">
								<xsl:copy-of select="$influx"/>
							</xsl:with-param>
						</xsl:call-template>
					</m:apply>						
				</m:apply>
			</xsl:otherwise>
		</xsl:choose>

    </xsl:for-each>

    
</xsl:template>


<xsl:template name="iterate-influx">
	<xsl:param name="tokens"/>
	<xsl:variable name="id" select="exslt:node-set($tokens)/token[1]"/>
	<xsl:choose>
		<xsl:when test="key('cumomer',$id)">
			<m:apply>
				<m:selector/>
				<m:ci type="vector">
					<xsl:value-of select="concat('x',key('cumomer',$id)/@weight)"/>
				</m:ci>
				<m:cn>
					<xsl:value-of select="key('cumomer',$id)/@position"/>
				</m:cn>
			</m:apply>
		</xsl:when>
		<xsl:when test="key('input-cumomer',$id)">
			<m:apply>
				<m:selector/>
				<m:ci type="vector">x_input</m:ci>
				<m:cn>
					<xsl:value-of select="key('input-cumomer',$id)/@position"/>
				</m:cn>
			</m:apply>
		</xsl:when>
	</xsl:choose>
	<xsl:if test="count(exslt:node-set($tokens)/token)&gt;1">
		<xsl:call-template name="iterate-influx">
			<xsl:with-param name="tokens">
				<xsl:copy-of select="exslt:node-set($tokens)/token[position()&gt;1]"/>
			</xsl:with-param>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>
