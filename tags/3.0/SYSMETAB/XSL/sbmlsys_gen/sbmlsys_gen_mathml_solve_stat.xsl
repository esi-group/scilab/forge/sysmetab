<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   hu Mar 27 14:52:34 CET 2008
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer diff�rentes fonctions sp�cifiques
	d'un mod�le donn� sous forme SBML, �dit� dans CellDesigner. On prend ici l'exemple
	du "Branching Network" de l'article de N. Isermann, W. Wiechert :
	
	Metabolic isotopomer labeling systems. Part II: structural flux identifiability
	analysis.

            A_out
	         |
			 |v6
			 |
			 v
	_________A _______
	|        |       |
	|        |v2     |
	|v3      |       |
	|        v       |
	|        D       |v1
	|        |       |
	|        |v4     |
	|        |       |
	|        v       |
	_______> F <______
	         |
			 |v5
			 |
			 v
			 G

    Ici on fait abstraction des identificateurs originaux des cumom�res et des flux. On a dans cet
	exemple (les cumom�res des m�tabolites d'entr�e et de sortie ne font pas partie de l'�tat) :
	
	x1=[A$01;A$10;D$1;F$01;F$10];
	x2=[A$11;F$11];
	
	x1_input=[A_out$01;A_out$10];
	x2_input=[A_out$11];
	
	v=[v1;v2;v3;v4;v5;v6];
    
    Ici on g�n�re en fait des fonctions d�crites en MathML, il faut enchainer avec
    une derni�re transformation pour avoir le code Scilab (sbml_sys_text.xsl) :


*** 1 - une fonction Scilab permettant
    calculer les diff�rentes fractions des cumom�res en fonction des flux
    et des fractions des cumom�res des entr�es. 
	On g�n�re aussi le code des blocs non nuls de la jacobienne des �tats par rapport � v
	
	
	Exemple pour le "branching network"
    du papier "part II" de Wiechert :
	
	function [x1,x2,dx1_dv,dx2_dv]=solveCumomers(v,x1_input,x2_input)

		n1=5;

		// Cumom�res de poids 1

		M1_ijv=[1,1,-(v(1)+v(2)+v(3))
		2,2,-(v(1)+v(2)+v(3))
		3,2,v(2)
		3,1,v(2)
		3,3,-(v(4)+v(4))
		4,1,v(1)
		4,2,v(3)
		4,3,v(4)
		4,4,-v(5)
		5,2,v(1)
		5,1,v(3)
		5,3,v(4)
		5,5,-v(5)];
		M1=sparse(M1_ijv(:,1:2),M1_ijv(:,3),[n1,n1]);

		b1_ijv=[1,1,v(6).*x1_input(1,:)
		2,1,v(6).*x1_input(2,:)];
		b1_1=s_full(b1_ijv(:,1:2),b1_ijv(:,3),[n1,1]);

		[M1_handle,M1_rank]=lufact(M1);
		x1=lusolve(M1_handle,-[b1_1]);

		dg1_dv_ijv=[1,6,x1_input(1,:)
		1,1,-x1(1,:)
		1,2,-x1(1,:)
		1,3,-x1(1,:)
		2,6,x1_input(2,:)
		2,1,-x1(2,:)
		2,2,-x1(2,:)
		2,3,-x1(2,:)
		3,2,x1(2,:)
		3,2,x1(1,:)
		3,4,-x1(3,:)
		3,4,-x1(3,:)
		4,1,x1(1,:)
		4,3,x1(2,:)
		4,4,x1(3,:)
		4,5,-x1(4,:)
		5,1,x1(2,:)
		5,3,x1(1,:)
		5,4,x1(3,:)
		5,5,-x1(5,:)];
		dg1_dv_1=s_full(dg1_dv_ijv(:,1:2),dg1_dv_ijv(:,3),[n1,6]);
		dx1_dv(:,:,1)=lusolve(M1_handle,-dg1_dv_1);

		ludel(M1_handle);

		n2=2;

		// Cumom�res de poids 2

		M2_ijv=[1,1,-(v(1)+v(2)+v(3))
		2,1,v(1)
		2,1,v(3)
		2,2,-v(5)];
		M2=sparse(M2_ijv(:,1:2),M2_ijv(:,3),[n2,n2]);

		b2_ijv=[1,1,v(6).*x2_input(1,:)
		2,1,v(4).*x1(3,:).*x1(3,:)];
		b2_1=s_full(b2_ijv(:,1:2),b2_ijv(:,3),[n2,1]);

		[M2_handle,M2_rank]=lufact(M2);
		x2=lusolve(M2_handle,-[b2_1]);

		dg2_dv_ijv=[1,6,x2_input(1,:)
		1,1,-x2(1,:)
		1,2,-x2(1,:)
		1,3,-x2(1,:)
		2,1,x2(1,:)
		2,3,x2(1,:)
		2,4,x1(3,:).*x1(3,:)
		2,5,-x2(2,:)];
		dg2_dv_1=s_full(dg2_dv_ijv(:,1:2),dg2_dv_ijv(:,3),[n2,6]);
		db2_dx1_ijv=[2,3,x1(3,:).*v(4)
		2,3,x1(3,:).*v(4)];
		db2_dx1_1=sparse(db2_dx1_ijv(:,1:2),db2_dx1_ijv(:,3),[n2,n1]);
		dx2_dv=zeros(n2,6,1);
		dx2_dv(:,:,1)=lusolve(M2_handle,-(dg2_dv_1+db2_dx1_1*dx1_dv(:,:,1)));
		ludel(M2_handle);
	endfunction

2 *** - Une fonction permettant de calculer la fonction cout du probl�me d'identification
	ainsi que son gradient (sans utiliser d'�tat adjoint; on utilise directement les jacobiennes
	des �tats par rapport � v)


	function [cost,grad]=costAndGrad(v)
		[x1,x2,dx1_dv,dx2_dv]=solveCumomers(v,x1_input,x2_input);
		e_label=(C1*x1+C2*x2)-ymeas;
		e_flux=E*v-vobs;
		cost=0.5*(sum(Svpm2.*e_flux.^2)+sum(Sypm2(:,1).*e_label(:,1).^2));
		grad=(Svpm2.*e_flux)'*E+(Sypm2(:,1).*e_label(:,1))'*(C1*dx1_dv(:,:,1)+C2*dx2_dv(:,:,1));
	endfunction


3 *** - Les "matrices d'observation" correspondant aux cumom�res et aux fluxs observ�s

	1) isotopom�res en entr�e -> contributions aux cumom�res en entr�e, matrices D1,D2, etc.

	Exemple ici pour A_out (seul m�tabolite d'entr�e), on pr�cise dans les notes (dans CellDesigner)
	
	LABEL_INPUT 01,10,11

	ce qui signifie qu'on donne en entr�e les trois isotopom�res dans un vecteur A_out_input
	qui contient en fait [A_out#01;A_out#10;A_out#11]. Pour le calcul des cumom�res, on a besoin
	de connaitre les contributions aux vecteurs d'entr�es x1_input (cumom�res de poids 1) et
	x2_input (cumom�res de poids 2). On g�n�re donc les matrices D1 et D2 telles que
	
	x1_input=D1*A_out_input;
	x2_input=D2*A_out_input;


	// Matrices d'entr�e
	D1_ijv=[1,1,1
	1,3,1
	2,2,1
	2,3,1];
	D1=sparse(D1_ijv(:,1:2),D1_ijv(:,3),[2,3]);
	D2_ijv=[1,3,1];
	D2=sparse(D2_ijv(:,1:2),D2_ijv(:,3),[1,3]);
	
	2) cumom�res calcul�s -> isotopom�res observ�s

	Exemple ici pour F, dont on observe les cumom�res F$10,F$01,F$11, on a dans les notes

	LABEL_MEASUREMENT 1x,x1,11
	
	avec la convention du logiciel 13CFlux (1x=cumom�re 10, x1=cumom�re 01), on a besoin de
	pouvoir calculer ces observations en fonction des vecteurs des cumom�res calcul�s x1,x2,
	on g�n�re donc les matrices C1 et C2 telles que
	
	y=C1*x1+C2*x2

	et ici y contient finalement [F$10;F$01;F$11] car il n'y a pas d'autre observation dans 
	le mod�le.

	// Matrices d'observation (cumom�res)
	C1_ijv=[1,5,1
	2,4,1];
	C1=sparse(C1_ijv(:,1:2),C1_ijv(:,3),[3,5]);
	C2_ijv=[3,2,1];
	C2=sparse(C2_ijv(:,1:2),C2_ijv(:,3),[3,2]);


	3) flux -> flux observ�s
	
	Dans le Branching Network, l'exemple utilis�, on observe v5,v6. Dans le mod�le CellDesigner
	ces deux r�actions/flux sont identifi�s comme TRANSPORT ou KNOWN_TRANSITION_OMMITED


	// Matrice d'observation (flux)
	E_ijv=[1,5,1
	2,6,1];
	E=sparse(E_ijv(:,1:2),E_ijv(:,3),[2,6]);

    
    3 - une fonction Scilab [A,b]=fluxSubspace(_fluxes) calculant le param�trage du
    sous-espace des flux v v�riant
    
        Nv=0 
        Hv=w

    N est la matrice stoechiom�trique et Hv=w exprime des contraintes d'�galit�
    sur les flux, typiquement utilis�e lorsque des flux ont des valeurs connues.

	La partie Hv=w peut etre changee dans l'interface. La matrice _fluxes a deux colonnes
	dont la deuxi�me pr�cise si on impose un flux ou pas. La valeur est par d�faut -1 sinon
	on met une valeur positive ou nulle impos�e. La matrice H et le vecteur w sont ainsi r�cup�r�s
	apr�s appel � la fonction fluxConstraints (faisant partie de la biblioth�que de macros
	SYSMETAB).
        

	Ici on a finalement 
	
		A=[N;H] et b=[0;w]

	function [A,b,N,H,w]=fluxSubspace(_fluxes)
		// Construction des matrices (dont la matrice de stoechiom�trie) permettant
		// de calculer le changement de variable affine des flux.
		N=zeros(3,6);
		// A
        N_ijv=[1,1,-1
        1,2,-1
        1,3,-1
        1,6,1
        2,2,1
        2,2,1
        2,4,-1
        2,4,-1
        3,1,1
        3,3,1
        3,4,1
        3,5,-1];
		// Flux impos�s dans l'interface
		[H,w]=fluxConstraints(_fluxes);
		b=[zeros(3,1);w];
		A=[N;H];
	endfunction
    
-->

<xsl:stylesheet 
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:exslt="http://exslt.org/common" 
  xmlns:str="http://exslt.org/strings"
  xmlns:math="http://exslt.org/math"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
  
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
  
  <xsl:param name="file"/>
  <xsl:param name="verbose">no</xsl:param>
  <xsl:param name="type">stationnaire</xsl:param>
  <xsl:param name="experiences">1</xsl:param>
  <xsl:param name="method">direct</xsl:param>
  
  <xsl:variable name="quote">'</xsl:variable>
    
  <xsl:include href="common_gen_mathml.xsl"/> <!-- templates communes stationnaire/dynamique -->

  <xsl:key name="reaction" match="sbml:reaction" use="@id"/>
  <xsl:key name="species" match="sbml:species" use="@id"/>
  <xsl:key name="input-cumomer" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>
  <xsl:key name="cumomer" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>
  <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
  <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
  <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

  <xsl:strip-space elements="*"/>


  <!-- D�termination du poids maximum n�cessaire : on regarde le poids le plus grand dans les cumom�res contribuant aux observations -->

  <xsl:variable name="weight" select="math:max(//sbml:species[@type='intermediate']/smtb:measurement/smtb:cumomer-contribution/@weight)"/>

  <xsl:template match="/">
    <carbon-labeling-system>
      <xsl:apply-templates/>
    </carbon-labeling-system>
  </xsl:template>

  <xsl:template match="sbml:sbml">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="sbml:model">
    
  <!-- Construction de la fonction solveCumomers (calcul des cumom�res connaissant les cumom�res 
en entr�e et les flux) -->
    
    <function xmlns="http://www.utc.fr/sysmetab">
      <ci xmlns="http://www.w3.org/1998/Math/MathML">solveCumomersAndGrad</ci>
      <input>
        <list xmlns="http://www.w3.org/1998/Math/MathML">
          <ci>v</ci>
          <xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers[(@weight&lt;=$weight)]">
            <ci>
              <xsl:value-of select="concat('x',@weight,'_input')"/>
            </ci>
          </xsl:for-each>
        </list>
      </input>
      <output>
        <list xmlns="http://www.w3.org/1998/Math/MathML">
          <ci>cost</ci>
          <ci>grad</ci>
          <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
            <ci>
              <xsl:value-of select="concat('x',@weight)"/>
            </ci>
          </xsl:for-each>
          <xsl:if test="$method='direct'">
            <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
              <ci>
                <xsl:value-of select="concat('dx',@weight,'_dv')"/>
              </ci>
            </xsl:for-each>
          </xsl:if>
        </list>
      </output>
      <body>
        <xsl:apply-templates select="smtb:listOfIntermediateCumomers"/>
        <xsl:if test="$method='direct'">
          <comment>Forward computation of gradient</comment>
        </xsl:if>
        <xsl:if test="$method='adjoint'">
          <comment>Adjoint computation of gradient</comment>
        </xsl:if>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>e_label</ci>
          <apply>
            <minus/>
            <apply>
              <plus/>
              <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
                <apply>
                  <times/>
                  <ci>
                    <xsl:value-of select="concat('C',@weight)"/>
                  </ci>
                  <ci>
                    <xsl:value-of select="concat('x',@weight)"/>
                  </ci>
                </apply>
              </xsl:for-each>
            </apply>
            <ci>ymeas</ci>
          </apply>
        </apply>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>e_flux</ci>
          <apply>
            <minus/>
            <apply>
              <times/>
              <ci>E</ci>
              <ci>v</ci>
            </apply>
            <ci type="matrix">vobs</ci>
          </apply>
        </apply>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>flux_error</ci>
          <apply>
            <plus/>
            <apply>  <!-- contribution des observations des flux -->
              <times/>
              <apply>
                <fn>
                  <ci>sum</ci>
                </fn>
                <apply>
                  <times type="array"/>
                  <ci>Svpm2</ci>
                  <apply>
                    <power type="array"/>
                    <ci type="matrix">e_flux</ci>
                    <cn>2</cn>
                  </apply>
                </apply>
              </apply>
            </apply>
          </apply>
        </apply>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>label_error</ci>
          <apply>
            <plus/>
            <xsl:call-template name="iterate-cost"/>  <!-- contributions des observations  des marquages -->
          </apply>
        </apply>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>cost</ci>
          <apply>
            <times/>
            <cn>0.5</cn>
            <apply>
              <plus/>
              <ci>flux_error</ci>
              <ci>label_error</ci>
            </apply>
          </apply>
        </apply>
        <xsl:if test="$method='direct'">
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>grad</ci>
            <apply>
              <plus/>
              <apply>  <!-- contribution des observations des flux -->
                <times/>
                <apply>
                  <transpose/>
                  <apply>
                    <times type="array"/>
                    <ci>Svpm2</ci>
                    <ci>e_flux</ci>
                  </apply>
                </apply>
                <ci>E</ci>
              </apply>
              <xsl:call-template name="iterate-grad"/>  <!-- contributions des observations  des marquages -->
            </apply>
          </apply>
        </xsl:if>
        <xsl:if test="$method='adjoint'">
          <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
            <xsl:sort select="@weight" order="descending" type="number"/>    
            <!-- R�solution du syst�me pour p weight -->
            <hypermatrix id="{concat('p',@weight)}" rows="{concat('n',@weight)}" cols="{count(../../sbml:listOfReactions/sbml:reaction)}" versions="{$experiences}"/>
            <xsl:call-template name="solve-p"/>
          </xsl:for-each>
          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>grad</ci>
            <apply>
              <plus/>
              <apply>  <!-- contribution des observations des flux -->
                <times/>
                <apply>
                  <transpose/>
                  <apply>
                    <times type="array"/>
                    <ci>Svpm2</ci>
                    <ci>e_flux</ci>
                  </apply>
                </apply>
                <ci>E</ci>
              </apply>
              <xsl:call-template name="iterate-grad-adjoint"/>  <!-- contributions des observations  des marquages -->
            </apply>
          </apply>
        </xsl:if>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>grad</ci>
          <apply>
            <transpose/>
            <ci>grad</ci>
          </apply>
        </apply>
      </body>
    </function>
  
    <!-- Fonction de calcul de A et B pour les contraintes sur les fluxs Av=b (dans common_gen_mathml.xsl)-->
  
    <xsl:call-template name="fluxSubspace"/>
  
    <!-- Construction des matrices d'observation (dans common_gen_mathml.xsl)-->
  
    <xsl:call-template name="construct_matrix_C"/>
    <xsl:call-template name="construct_matrix_E"/>
  
    <!-- Construction des matrices d'entr�e, permettant d'obtenir la contribution sur les cumom�res des isotopom�res en entr�e. -->
  
    <xsl:call-template name="construct_matrix_D"/>
  
    <!-- Matrice des noms des fluxs, Matrice des ids des fluxs -->
  
    <xsl:call-template name="names_and_ids_fluxes"/>

    <!-- Script pour le calcul direct des cumom�res. Ce script est ex�cut� dans l'application XMLlab, d�s qu'un flux est chang� ou d�s que l'on 
  sort de l'optimisation avec des nouvelles valeurs de fluxs. -->

    <script href="{$file}_direct.sce" xmlns="http://www.utc.fr/sysmetab">

    <!-- En fonction des valeurs des fluxs, les free fluxes peuvent changer, en g�n�ral c'est le cas quand le statut de certaines contraintes change 
  (satur�es ou inactives). La fonction adjustFluxesTab (d�finie dans SYSMETAB/SCI/sysmetab4.sci) a pour but de rendre actives les cases du tableau
  des fluxs correspondant � des free fluxes. On peut alors les faire varier avec   <ctrl>+fl�che droite ou gauche. -->

      <comment>Ajustement du controle des free fluxes</comment>
    
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>nothing</ci>
        <apply>
          <fn>
            <ci>adjustFluxesTab</ci>
          </fn>
          <ci>fluxes</ci>
        </apply>
      </apply>
  
      <comment>Affectation des flux</comment>
  
      <xsl:for-each select="sbml:listOfReactions/sbml:reaction">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>  <xsl:value-of select="@id"/></ci>
          <apply>
            <selector/>
            <ci type="vector">fluxes</ci>
            <cn>  <xsl:value-of select="position()"/></cn>
          </apply>
        </apply>
      </xsl:for-each>
  
      <comment>Ajustement des flux des r�actions bidirectionnelles</comment>
  
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>fluxesnetxch</ci>
        <list separator=";">
          <xsl:for-each select="sbml:listOfReactions/smtb:listOfBidirectionalReactions/smtb:bidirectionalReaction">
            <list separator=",">
              <xsl:copy-of select="smtb:netFlux/m:apply"/>
              <xsl:copy-of select="smtb:exchangeFlux/m:apply"/>
            </list>
          </xsl:for-each>
        </list>
      </apply>
  
      <comment>Cumom�res en entr�e, calcul�s � partir des isotopom�res des m�tabolites d'entr�e.</comment>
  
      <xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>
            <xsl:value-of select="concat('x',@weight,'_input')"/>
          </ci>
          <apply>
            <times/>
            <ci>
              <xsl:value-of select="concat('D',@weight)"/>
            </ci>
            <list separator=";">
              <xsl:for-each select="../../sbml:listOfSpecies/sbml:species[smtb:input]">
                <ci>
                  <xsl:value-of select="concat(@id,'_input')"/>
                </ci>
              </xsl:for-each>
            </list>
          </apply>
        </apply>
      </xsl:for-each>
   
      <comment>Calcul direct des cumom�res</comment>
  
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>v</ci>
        <apply>
          <selector/>
          <ci type="matrix">fluxes</ci>
          <cn>:</cn>
          <cn>1</cn>
        </apply>
      </apply>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <list separator=",">
          <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
            <ci>
              <xsl:value-of select="concat('x',position())"/>
            </ci>
          </xsl:for-each>
        </list>
        <apply>
          <fn>
            <ci>solveCumomers</ci>
          </fn>
          <ci>v</ci>
          <xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers[(@weight&lt;=$weight)]">
            <ci>
              <xsl:value-of select="concat('x',@weight,'_input')"/>
            </ci>
          </xsl:for-each>
        </apply>
      </apply>
  
      <comment>Calcul des observations en fonction des cumom�res</comment>
  
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>y</ci>
        <apply>
          <plus/>
          <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
            <apply>
              <times/>
              <ci>
                <xsl:value-of select="concat('C',@weight)"/>
              </ci>
              <ci>
                <xsl:value-of select="concat('x',@weight)"/>
              </ci>
            </apply>
          </xsl:for-each>
        </apply>
      </apply>
  
      <comment>Remplissage des tableaux des cumom�res dans l'interface</comment>
   
      <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>
            <xsl:value-of select="concat('cumomers_weight_',@weight)"/>
          </ci>
          <ci>
            <xsl:value-of select="concat('x',@weight)"/>
          </ci>
        </apply>
      </xsl:for-each>
  
      <comment>Remplissage des tableaux des observations dans l'interface</comment>
  
      <xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
        <xsl:variable name="n" select="count(smtb:measurement)"/>
        <xsl:variable name="start" select="count(preceding::smtb:measurement)"/>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <apply>
            <selector/>
            <ci type="matrix">
              <xsl:value-of select="concat(@id,'_obs')"/>
            </ci>
            <cn>
              <xsl:value-of select="concat('1:',$n)"/>
            </cn>
            <cn>
              <xsl:value-of select="concat(3,':3:',3*$experiences)"/>
            </cn>
          </apply>
          <apply>
            <selector/>
            <ci type="matrix">y</ci>
            <cn>
              <xsl:value-of select="concat(1+$start,':',$n+$start)"/>
            </cn>
            <cn>
              <xsl:value-of select="concat('1:',$experiences)"/>
            </cn>
          </apply>
        </apply>
      </xsl:for-each>
  
      <comment>R�cup�rations des donn�es pour le calcul de la fonction cout</comment>
      <comment>Flux mesur�s</comment>
  
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>vobs</ci>
        <apply>
          <selector/>
          <ci type="matrix">flux_obs</ci>
          <cn>:</cn>
          <cn>2</cn>
        </apply>
      </apply>
  
      <comment>Matrice de pond�ration de l'erreur sur les flux observ�s</comment>
  
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>Svpm2</ci>
        <apply>
          <selector/>
          <ci type="matrix">flux_obs</ci>
          <cn>:</cn>
          <cn>1</cn>
        </apply>
      </apply>
   
      <comment>Marquages mesur�s</comment>
    
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>ymeas</ci>
        <list separator=";">
          <xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
            <apply>
              <selector/>
              <ci type="matrix">
                <xsl:value-of select="concat(@id,'_obs')"/>
              </ci>
              <cn>:</cn>
              <cn>
                <xsl:value-of select="concat('2:3:',(3*$experiences)-1)"/>
              </cn>
            </apply>
          </xsl:for-each>	
        </list>
      </apply>

      <comment>Matrice de pond�ration de l'erreur sur les marquages observ�s</comment>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>Sypm2</ci>
        <list separator=";">
          <xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
            <apply>
              <selector/>
              <ci type="matrix">
                <xsl:value-of select="concat(@id,'_obs')"/>
              </ci>
              <cn>:</cn>
              <cn>
                <xsl:value-of select="concat('1:3:',(3*$experiences)-2)"/>
              </cn>
            </apply>
          </xsl:for-each>	
        </list>
      </apply>
 
      <comment>Calcul de la fonction cout</comment>
  
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <list separator=",">
          <ci>cost</ci>
          <ci>grad</ci>
          <ci>label_error</ci>
        </list>
        <apply>
          <fn>
            <ci>costAndGrad</ci>
          </fn>
          <apply>
            <selector/>
            <ci type="matrix">fluxes</ci>
            <cn>:</cn>
            <cn>1</cn>
          </apply>
        </apply>
      </apply>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>flux_obs(:,3)</ci>
        <apply>
          <times type="matrix"/>
          <ci>E</ci>
          <ci>fluxes(:,1)</ci>
        </apply>
      </apply>
    </script>
  </xsl:template>

  <xsl:template name="iterate-cost">
    <xsl:param name="i" select="'1'"/>
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>sum</ci>
      </fn>
      <apply>
        <times type="array"/>
        <apply>
          <selector/>
          <ci type="matrix">Sypm2</ci>
          <cn>:</cn>
          <cn>
            <xsl:value-of select="$i"/>
          </cn>
        </apply>
        <apply>
          <power type="array"/>
          <apply>
            <selector/>
            <ci type="matrix">e_label</ci>
            <cn>:</cn>
            <cn><xsl:value-of select="$i"/></cn>
          </apply>
          <cn>2</cn>
        </apply>
      </apply>
    </apply>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="iterate-cost">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="iterate-grad">
    <xsl:param name="i" select="'1'"/>
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <times/>
      <apply>
        <transpose/>
        <apply>
          <times type="array"/>
          <apply>
            <selector/>
            <ci type="matrix">Sypm2</ci>
            <cn>:</cn>
            <cn>
              <xsl:value-of select="$i"/>
            </cn>
          </apply>
          <apply>
            <selector/>
            <ci type="matrix">e_label</ci>
            <cn>:</cn>
            <cn><xsl:value-of select="$i"/></cn>
          </apply>
        </apply>
      </apply>
      <apply>
        <plus/>
        <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
          <apply>
            <times/>
            <ci>
              <xsl:value-of select="concat('C',@weight)"/>
            </ci>
            <apply>
              <selector/>
              <ci type="hypermatrix">
                <xsl:value-of select="concat('dx',@weight,'_dv')"/>
              </ci>
              <cn>:</cn>
              <cn>:</cn>
              <cn><xsl:value-of select="$i"/></cn>
            </apply>
          </apply>
        </xsl:for-each>
      </apply>
    </apply>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="iterate-grad">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="iterate-grad-adjoint">
    <xsl:param name="i" select="'1'"/>
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <times type="array"/>
      <apply>
        <selector/>
        <ci type="matrix">Sypm2</ci>
        <cn>:</cn>
        <cn>
          <xsl:value-of select="$i"/>
        </cn>        
      </apply>
      <apply>
        <plus/>
        <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
          <apply>
            <times/>
            <apply>
              <transpose/>
              <apply>
                <selector/>
                <ci type="hypermatrix">
                  <xsl:value-of select="concat('p',@weight)"/>
                </ci>
                <cn>:</cn>
                <cn>:</cn>
                <cn><xsl:value-of select="$i"/></cn>
              </apply>
            </apply>
            <apply>
              <ci>
                <xsl:value-of select="concat('dg',@weight,'_dv_',$i)"/>
              </ci>
            </apply>
          </apply>
        </xsl:for-each>
      </apply>
    </apply>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="iterate-grad-adjoint">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="smtb:listOfIntermediateCumomers">
    <xsl:for-each select="smtb:listOfCumomers[@weight&lt;=$weight]">
      <xsl:sort select="@weight" order="ascending" type="number"/>
    <!-- D�claration et initialisation matrice et vecteur -->
    
      <xsl:variable name="current_weight" select="@weight"/>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>
          <xsl:value-of select="concat('n',@weight)"/>
        </ci>
        <cn>
          <xsl:value-of select="count(smtb:cumomer)"/>
        </cn>
      </apply>
      <optimize xmlns="http://www.utc.fr/sysmetab">
        <matrix-open type="sparse" id="{concat('M',@weight)}" rows="{concat('n',@weight)}" cols="{concat('n',@weight)}"/>
        <matrix-open type="s_full" id="{concat('b',@weight)}" rows="{concat('n',@weight)}" cols="1" versions="{$experiences}"/>
        <matrix-open type="s_full" id="{concat('dg',@weight,'_dv')}" rows="{concat('n',@weight)}" cols="{count(../../sbml:listOfReactions/sbml:reaction)}" 
          versions="{$experiences}" />
        <xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
          <matrix-open type="sparse" id="{concat('db',$current_weight,'_dx',@weight)}" rows="{concat('n',$current_weight)}" cols="{concat('n',@weight)}"
            versions="{$experiences}"/>
        </xsl:for-each>
        
        <comment>Weight <xsl:value-of select="@weight"/> cumomers</comment>
        
        <!-- Pour chaque cumom�re on appelle la template g�n�rant les diverses affectations, � la matrice ou au second membre du syst�me ayant 
        pour solution le vecteur des cumom�res du poids courant -->
        
        <xsl:for-each select="smtb:cumomer">
          <xsl:call-template name="iteration"/>            
        </xsl:for-each>
        <matrix-close id="{concat('M',@weight)}"/>
        <matrix-close id="{concat('b',@weight)}"/>
        
        <!-- R�solution des syst�mes -->
        <!-- R�solution du syst�me pour x_weight -->

        <solve matrix="{concat('M',@weight)}">
          <lhs>
            <xsl:value-of select="concat('x',@weight)"/>
          </lhs>
          <rhs>
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <minus/>
              <list separator=",">
                <xsl:call-template name="concatenate-rhs">
                  <xsl:with-param name="id" select="concat('b',@weight)"/>
                </xsl:call-template>
              </list>
            </apply>
          </rhs>
        </solve>
        <matrix-close id="{concat('dg',@weight,'_dv')}"/>
        <xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
          <matrix-close id="{concat('db',$current_weight,'_dx',@weight)}" />
        </xsl:for-each>
        
        <!-- R�solution du syst�me pour dx_weight/dv -->
        <xsl:if test="$method='direct'">        
          <hypermatrix id="{concat('dx',@weight,'_dv')}" rows="{concat('n',@weight)}" cols="{count(../../sbml:listOfReactions/sbml:reaction)}" versions="{$experiences}"/>
          <xsl:call-template name="solve-dx-dv"/>
        </xsl:if>
      </optimize>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template name="concatenate-rhs">
    <xsl:param name="i">1</xsl:param>
    <xsl:param name="id"/>
    <ci type="vector" xmlns="http://www.w3.org/1998/Math/MathML">
      <xsl:value-of select="concat($id,'_',$i)"/>
    </ci>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="concatenate-rhs">
        <xsl:with-param name="i" select="($i)+1"/>
        <xsl:with-param name="id" select="$id"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="solve-dx-dv">
    <xsl:param name="i">1</xsl:param>
    <xsl:variable name="current_weight" select="@weight"/>
    <solve matrix="{concat('M',@weight)}" xmlns="http://www.utc.fr/sysmetab">
      <lhs>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <selector/>
          <ci type="hypermatrix">
            <xsl:value-of select="concat('dx',@weight,'_dv')"/>
          </ci>
          <cn>:</cn>
          <cn>:</cn>
          <cn><xsl:value-of select="$i"/></cn>
        </apply>
      </lhs>
      <rhs>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <minus/>
          <apply>
            <plus/>
            <ci>
              <xsl:value-of select="concat('dg',@weight,'_dv_',$i)"/>
            </ci>
            <xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
              <apply>
                <times/>
                <ci>
                  <xsl:value-of select="concat('db',$current_weight,'_dx',@weight,'_',$i)"/>
                </ci>
                <apply>
                  <selector/>
                  <ci type="hypermatrix">
                    <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                  </ci>
                  <cn>:</cn>
                  <cn>:</cn>
                  <cn><xsl:value-of select="$i"/></cn>
                </apply>
              </apply>
            </xsl:for-each>
          </apply>
        </apply>
      </rhs>
    </solve>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="solve-dx-dv">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="solve-p">
    <xsl:param name="i">1</xsl:param>
    <xsl:variable name="current_weight" select="@weight"/>
    <solve matrix="{concat('M',@weight)}" transpose="yes" xmlns="http://www.utc.fr/sysmetab">
      <lhs>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <selector/>
          <ci type="hypermatrix">
            <xsl:value-of select="concat('p',@weight)"/>
          </ci>
          <cn>:</cn>
          <cn>:</cn>
          <cn><xsl:value-of select="$i"/></cn>
        </apply>
      </lhs>
      <rhs>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <apply>
            <times/>
            <apply>
              <transpose/>
              <ci>
                <xsl:value-of select="concat('C',@weight)"/>
              </ci>
            </apply>
            <apply>
              <selector/>
              <ci type="matrix">e_label</ci>
              <cn>:</cn>
              <cn><xsl:value-of select="$i"/></cn>
            </apply>
          </apply>
          <xsl:for-each select="following-sibling::smtb:listOfCumomers">
            <apply>
              <minus/>
              <apply>
                <times/>
                <ci>
                  <xsl:value-of select="concat('db',@weight,'_dx',$current_weight,'_',$i,$quote)"/>
                </ci>
                <apply>
                  <selector/>
                  <ci type="hypermatrix">
                    <xsl:value-of select="concat('p',@weight)"/>
                  </ci>
                  <cn>:</cn>
                  <cn>:</cn>
                  <cn><xsl:value-of select="$i"/></cn>
                </apply>
              </apply>
            </apply>
          </xsl:for-each>
        </apply>
      </rhs>
    </solve>
    <xsl:if test="$i&lt;$experiences">
      <xsl:call-template name="solve-p">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>