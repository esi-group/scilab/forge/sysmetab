<?xml version="1.0" encoding="UTF-8"?>
<!-- 
    Auteur  :   Stéphane Mottelet
    Date    :   Tue Mar 27 09:27:49 CEST 2007

    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but :

    1 - ajouter après l'élément <listOfSpecies> deux éléments
            
    <smtb:listOfIntermediateCumomers>
        <smtb:listOfCumomers weight="1">
        ...
        </smtb:listOfCumomers>
        <smtb:listOfCumomers weight="2">
        ...
        </smtb:listOfCumomers>
    </smtb:listOfIntermediateCumomers>
    
    <smtb:listOfInputCumomers>
        <smtb:listOfCumomers weight="1">
        ...
        </smtb:listOfCumomers>
    </smtb:listOfInputCumomers>
    
    contenant les cumomères classés par poids (pour les intermédiaires)
    ou pas classés du tout (pour les entrées).

    2 - d'ajouter explicitement, pour chaque
    réaction (sauf si elle est déclarée comme irreversible), la réaction
    inverse correspondante. Du point de vue des identificateurs, si par exemple
    une réaction a pour id "re1", cela donne deux nouvelles réactions avec les
    id "re1_f" et "re1_b" (f pour forward et b pour backward).    

    3 - De traduire les observations sous forme de combinaisons linéaires des cumomères.

    REMARQUE : il semble que pour ce dernier point il y ait un problème de version
    de libxslt ou libxml sous MacOSX/Intel 10.4.9 qui fait que les position ne sont
    pas calculées comme il faut.
       
-->

<xsl:stylesheet  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:exslt="http://exslt.org/common" 
  xmlns:str="http://exslt.org/strings"   
  xmlns:math="http://exslt.org/math"   
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  exclude-result-prefixes="math sbml celldesigner m str exslt xhtml smtb">

  <xsl:param name="experiences">1</xsl:param>
    
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <xsl:strip-space elements="*"/>
	
   
	<!-- Poids maximal des cumomères présents dans le réseau. -->
    
    <xsl:variable name="maxweight" select="math:max(//sbml:species/smtb:cumomer/@weight)"/>
    
    <xsl:key name="species" match="sbml:species" use="@id"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>
    <xsl:key name="cumomer" match="smtb:cumomer" use="@id"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="sbml:listOfSpecies">
        <listOfSpecies xmlns="http://www.sbml.org/sbml/level2/version4">
            <xsl:apply-templates/>
        </listOfSpecies>
        <listOfIntermediateCumomers xmlns="http://www.utc.fr/sysmetab">
            <xsl:call-template name="make-cumomer-list"/>
        </listOfIntermediateCumomers>
        <listOfInputCumomers xmlns="http://www.utc.fr/sysmetab">
            <xsl:call-template name="make-cumomer-list-input"/>
        </listOfInputCumomers>
        <listOfExperiments xmlns="http://www.utc.fr/sysmetab">
            <xsl:call-template name="make-list-of-experiments"/>
        </listOfExperiments>
    </xsl:template>
    
    <xsl:template match="sbml:listOfReactions">
      <listOfReactions xmlns="http://www.sbml.org/sbml/level2/version4">
        <xsl:apply-templates/>
        <listOfBidirectionalReactions xmlns="http://www.utc.fr/sysmetab">
          <xsl:call-template name="make-list-of-bidirectional-reactions"/>
        </listOfBidirectionalReactions>
      </listOfReactions>
    </xsl:template>
    
    
  <xsl:template name="make-list-of-experiments">
    <xsl:param name="i">1</xsl:param>
    <experiment number="{$i}" xmlns="http://www.utc.fr/sysmetab"/>
    <xsl:if test="$i &lt; $experiences">
      <xsl:call-template name="make-list-of-experiments">
        <xsl:with-param name="i" select="1+$i"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
    
  <xsl:template name="make-cumomer-list">
    <xsl:param name="w">1</xsl:param>
    <xsl:if test="$w&lt;=$maxweight">
      <listOfCumomers weight="{$w}" xmlns="http://www.utc.fr/sysmetab">
        <xsl:for-each select="//sbml:species[key('products',@id) and key('reactants',@id)]/smtb:cumomer[@weight=$w]">
          <cumomer>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="position">
              <xsl:value-of select="position()"/>
            </xsl:attribute>
            <xsl:copy-of select="*"/>
          </cumomer>
        </xsl:for-each>
      </listOfCumomers>
      <xsl:call-template name="make-cumomer-list">
        <xsl:with-param name="w" select="($w)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="make-cumomer-list-input">
    <xsl:param name="w">1</xsl:param>
    <xsl:if test="$w&lt;=$maxweight and //sbml:species[smtb:input]/smtb:cumomer[@weight=$w]">
      <listOfCumomers weight="{$w}" xmlns="http://www.utc.fr/sysmetab">
        <xsl:for-each select="//sbml:species[smtb:input]/smtb:cumomer[@weight=$w]">
          <cumomer>
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="position">
              <xsl:value-of select="position()"/>
            </xsl:attribute>
            <xsl:copy-of select="*"/>
          </cumomer>
        </xsl:for-each>
      </listOfCumomers>
      <xsl:call-template name="make-cumomer-list-input">
        <xsl:with-param name="w" select="($w)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="make-list-of-bidirectional-reactions">
    <xsl:for-each select="sbml:reaction[(@reversible!='false') or not(@reversible)]">
      <bidirectionalReaction id="{@id}" name="{@name}" xmlns="http://www.utc.fr/sysmetab">
        <netFlux>
          <m:apply>
            <m:minus/>
            <xsl:call-template name="forward"/>  
            <xsl:call-template name="backward"/>  
          </m:apply>
        </netFlux>
        <exchangeFlux>
          <m:apply>
            <m:min/>
            <xsl:call-template name="forward"/>  
            <xsl:call-template name="backward"/>  
          </m:apply>
        </exchangeFlux>
      </bidirectionalReaction>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="forward">
    <xsl:choose>
      <xsl:when test="key('species',sbml:listOfReactants/sbml:speciesReference/@species)/@symmetric='yes'">
        <m:apply>
          <m:plus/>  
          <m:ci>
            <xsl:value-of select="concat(@id,'_f_1')"/>
          </m:ci>
          <m:ci>
            <xsl:value-of select="concat(@id,'_f_2')"/>
          </m:ci>
        </m:apply>                                          
      </xsl:when>
      <xsl:otherwise>
       <m:ci>
          <xsl:value-of select="concat(@id,'_f')"/>
        </m:ci>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="backward">
    <xsl:choose>
      <xsl:when test="key('species',sbml:listOfProducts/sbml:speciesReference/@species)/@symmetric='yes'">
        <m:apply>
          <m:plus/>  
          <m:ci>
            <xsl:value-of select="concat(@id,'_b_1')"/>
          </m:ci>
          <m:ci>
            <xsl:value-of select="concat(@id,'_b_2')"/>
          </m:ci>
        </m:apply>                                          
      </xsl:when>
      <xsl:otherwise>
       <m:ci>
          <xsl:value-of select="concat(@id,'_b')"/>
        </m:ci>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

    <xsl:template match="*|@*|text()|comment()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>
    
  <xsl:template match="sbml:reaction[(@reversible!='false') or not(@reversible)]">
        <reaction id="{@id}_f" name="{@name} (forward)" xmlns="http://www.sbml.org/sbml/level2/version4">
            <xsl:copy-of select="*"/>
        </reaction>
        <reaction id="{@id}_b" name="{@name} (backward)" xmlns="http://www.sbml.org/sbml/level2/version4">
            <listOfReactants>
                <xsl:copy-of select="sbml:listOfProducts/*"/>
            </listOfReactants>
            <listOfProducts>
                <xsl:copy-of select="sbml:listOfReactants/*"/>
            </listOfProducts>
        </reaction>
    </xsl:template>

	<xsl:template match="smtb:measurement">
	    <measurement xmlns="http://www.utc.fr/sysmetab">
			<xsl:copy-of select="@*"/>
			<xsl:for-each select="smtb:token">
				<xsl:call-template name="disassemble-cumomer"/>
			</xsl:for-each>
		</measurement>
	</xsl:template>
	
	<!-- Template récursive de détermination des contributions des cumomères dans une observation donnée. Cela
	     sert plus tard pour la génération des matrices d'observation C1, C2, etc. -->

  <xsl:template name="disassemble-cumomer">
    <xsl:param name="i" select="string-length(.)"/>
    <xsl:param name="string" select="."/>
    <xsl:param name="id" select="'0'"/>
    <xsl:param name="sign" select="'1'"/>
    <xsl:choose>
      <xsl:when test="$i=0">
        <cumomer-contribution id="{concat(../../@id,'_',$id)}" string="{$string}" weight="{string-length(translate($string,'x',''))}" sign="{$sign}" xmlns="http://www.utc.fr/sysmetab"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:choose>
          <xsl:when test="substring($string,$i,1)='x'">
            <xsl:call-template name="disassemble-cumomer">
              <xsl:with-param name="i" select="($i)-1"/>
              <xsl:with-param name="string" select="$string"/>
              <xsl:with-param name="id" select="$id"/>
              <xsl:with-param name="sign" select="$sign"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="substring($string,$i,1)='1'">
            <xsl:call-template name="disassemble-cumomer">
              <xsl:with-param name="i" select="($i)-1"/>
              <xsl:with-param name="string" select="$string"/>
              <xsl:with-param name="id" select="$id + math:power(2,string-length(.)-$i)"/>
              <xsl:with-param name="sign" select="$sign"/>
            </xsl:call-template>
          </xsl:when>
          <xsl:when test="substring($string,$i,1)='0'">
            <xsl:call-template name="disassemble-cumomer">
              <xsl:with-param name="i" select="$i"/>
              <xsl:with-param name="string" select="concat(substring($string,1,($i)-1),'x',substring($string,($i)+1))"/>
              <xsl:with-param name="id" select="$id"/>
              <xsl:with-param name="sign" select="$sign"/>
            </xsl:call-template>
            <xsl:call-template name="disassemble-cumomer">
              <xsl:with-param name="i" select="$i"/>
              <xsl:with-param name="string" select="concat(substring($string,1,($i)-1),'1',substring($string,($i)+1))"/>
              <xsl:with-param name="id" select="$id"/>
              <xsl:with-param name="sign" select="-($sign)"/>
            </xsl:call-template>
          </xsl:when>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="sbml:annotation"/>

</xsl:stylesheet>
