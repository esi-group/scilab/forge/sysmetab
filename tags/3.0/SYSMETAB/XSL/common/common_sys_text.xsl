<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   Stéphane Mottelet
    Date    :   Thu Feb  8 09:50:47 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de générer les équations du système
    sous forme de texte lisible par un humain. La sortie n'est pas destinée
    à un logiciel particulier, mais on pourra s'inspirer du code ci-dessous
    pour écrire une feuille de style destinée par exemple à Maple ou autre.
-->


<xsl:stylesheet   version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"  
    xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="smtb sbml celldesigner m str exslt xhtml">

    <xsl:param name="weight">10000</xsl:param>
    <xsl:param name="lang">scilab</xsl:param>

    <xsl:output method="text"  encoding="UTF-8"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:key name="matrix-assignments-by-id" match="smtb:matrix-assignment" use="concat(generate-id(ancestor::smtb:optimize),' ',@id)"/>
    <xsl:key name="matrix-assignments-by-id-row-col" match="smtb:matrix-assignment" use="concat(generate-id(ancestor::smtb:optimize),' ',@id,' ',@row,' ',@col)"/>
    <xsl:key name="matrix-open" match="smtb:matrix-open" use="concat(generate-id(ancestor::smtb:optimize),' ',@id)"/>
	    
    <xsl:variable name="quote">'</xsl:variable>
    
	<xsl:variable name="double_quote">"</xsl:variable>
	
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="carbon-labeling-system">
		<xsl:message>Generating <xsl:value-of select="$lang"/> code, please wait...</xsl:message>
        <xsl:apply-templates/>
    </xsl:template>    
    
    <xsl:template match="listOfReactions"/>

    <xsl:template match="listOfSpecies | metabolitepools">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="species | pool">
        <xsl:value-of select="concat('// ',@id,' , ',@type,'&#xA;')"/>
<!--        <xsl:value-of select="concat('// ',@id,' : ',@name,', ',@type,'&#xA;')"/>-->
        <xsl:apply-templates select="equations"/>
    </xsl:template>
    
    <xsl:template match="equations">
        <xsl:apply-templates select="equation[@weight&lt;=$weight]"/>
    </xsl:template>
    
    <xsl:template match="equation/m:apply">
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>=</xsl:text>
        <xsl:apply-templates select="*[3]"/>   
        <xsl:text>&#xA;</xsl:text>     
    </xsl:template>

    
    <xsl:template match="m:apply[m:plus]">
        <xsl:for-each select="*[position()&gt;1]">
            <xsl:apply-templates select="."/>
            <xsl:choose>
                <xsl:when test="following-sibling::m:apply[m:minus and (count(*)=2)]">
                </xsl:when>
                <xsl:otherwise>
                    <xsl:if test="position()&lt;last()">
                        <xsl:text>+</xsl:text>
                    </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each> 
    </xsl:template>

    <xsl:template match="m:apply[count(m:apply)=count(*)]">
      <xsl:text>(</xsl:text>
        <xsl:apply-templates/>
      <xsl:text>)</xsl:text>
    </xsl:template>
    
    <xsl:template match="m:apply[m:plus and ((preceding-sibling::m:power) or (preceding-sibling::m:times) or (preceding-sibling::m:minus) or (preceding-sibling::m:backslash)) and (count(*)&gt;2)]">
        <xsl:text>(</xsl:text>
        <xsl:for-each select="*[position()&gt;1]">
            <xsl:apply-templates select="."/>
            <xsl:if test="position()&lt;last()">
                <xsl:text>+</xsl:text>
            </xsl:if>
        </xsl:for-each> 
        <xsl:text>)</xsl:text>
    </xsl:template>



    <xsl:template match="m:apply[m:minus and (count(*)=3)]">
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>-</xsl:text>
        <xsl:apply-templates select="*[3]"/>
    </xsl:template>

    <xsl:template match="m:apply[m:minus and (count(*)=2)]">
        <xsl:text>-</xsl:text>
        <xsl:apply-templates select="*[2]"/>
    </xsl:template>


    <xsl:template match="m:apply[m:minus and (count(*)=3) and ((preceding-sibling::m:power) or (preceding-sibling::m:times) or (preceding-sibling::m:transpose))]">
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>-</xsl:text>
        <xsl:apply-templates select="*[3]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>
   
   <xsl:template match="m:apply[m:times]">
        <xsl:for-each select="*[position()&gt;1]">
            <xsl:apply-templates select="."/>
            <xsl:if test="position()&lt;last()">
				<xsl:choose>
					<xsl:when test="../m:times/@type='array'">
		                <xsl:text>.*</xsl:text>
					</xsl:when>
					<xsl:otherwise>
		                <xsl:text>*</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
            </xsl:if>
        </xsl:for-each> 
    </xsl:template>

   <xsl:template match="m:apply[m:divide]">
        <xsl:for-each select="*[position()&gt;1]">
            <xsl:apply-templates select="."/>
            <xsl:if test="position()&lt;last()">
				<xsl:choose>
					<xsl:when test="../m:divide/@type='array'">
		                <xsl:text>./</xsl:text>
					</xsl:when>
					<xsl:otherwise>
		                <xsl:text>/</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
            </xsl:if>
        </xsl:for-each> 
    </xsl:template>

   <xsl:template match="m:apply[m:times and ((preceding-sibling::m:power) or (preceding-sibling::m:transpose))]">
		<xsl:text>(</xsl:text>
        <xsl:for-each select="*[position()&gt;1]">
            <xsl:apply-templates select="."/>
            <xsl:if test="position()&lt;last()">
				<xsl:choose>
					<xsl:when test="../m:times/@type='array'">
		                <xsl:text>.*</xsl:text>
					</xsl:when>
					<xsl:otherwise>
		                <xsl:text>*</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
            </xsl:if>
        </xsl:for-each>
		<xsl:text>)</xsl:text> 
    </xsl:template>
    

    <xsl:template match="m:apply[m:diff]">
        <xsl:text>d(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>)/dt</xsl:text>
    </xsl:template>

    <xsl:template match="m:apply[m:transpose]">
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>'</xsl:text>
    </xsl:template>

    <xsl:template match="m:apply[m:power]">
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>^</xsl:text>
        <xsl:apply-templates select="*[3]"/>
    </xsl:template>

    <xsl:template match="m:apply[m:power[@type='array']]">
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>.^</xsl:text>
        <xsl:apply-templates select="*[3]"/>
    </xsl:template>

    <xsl:template match="m:apply[m:eq]">
       <!-- <xsl:if test="ancestor::smtb:body">
    		<xsl:text>    </xsl:text>
        </xsl:if>-->
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>=</xsl:text>
        <xsl:apply-templates select="*[3]"/>
		<xsl:text>;&#xA;</xsl:text>
    </xsl:template>

    <xsl:template match="m:apply[m:eqplus]">
        <xsl:if test="ancestor::smtb:body">
    		<xsl:text>    </xsl:text>
        </xsl:if>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>+=</xsl:text>
        <xsl:apply-templates select="*[3]"/>
		<xsl:text>;&#xA;</xsl:text>
    </xsl:template>


    <xsl:template match="m:apply[m:selector and m:ci[@type='vector']]">
        <xsl:apply-templates select="m:ci[1]"/>
        <xsl:choose>
            <xsl:when test="$lang='scilab'">
                <xsl:text>(</xsl:text>
                <xsl:apply-templates select="*[3]"/>
                <xsl:text>)</xsl:text>
            </xsl:when>
            <xsl:when test="$lang='C'">
                <xsl:text>[</xsl:text>
                <xsl:choose>
                    <xsl:when test="*[3][self::m:cn]">
                        <xsl:value-of select="(*[3])-1"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="*[3]"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text>]</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="m:apply[m:selector and m:ci[@type='matrix']]">
        <xsl:apply-templates select="m:ci[1]"/>
        <xsl:choose>
            <xsl:when test="$lang='scilab'">
                <xsl:text>(</xsl:text>
                <xsl:apply-templates select="*[3]"/>
                <xsl:text>,</xsl:text>
                <xsl:apply-templates select="*[4]"/>
                <xsl:text>)</xsl:text>
            </xsl:when>
            <xsl:when test="$lang='C'">
                <xsl:text>[</xsl:text>
                <xsl:apply-templates select="m:cn[1]"/>
                <xsl:text>][</xsl:text>
                <xsl:apply-templates select="m:cn[2]"/>
                <xsl:text>]</xsl:text>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="m:apply[m:selector and m:ci[@type='hypermatrix']]">
        <xsl:apply-templates select="m:ci[1]"/>
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="m:cn[1]"/>
        <xsl:text>,</xsl:text>
        <xsl:apply-templates select="m:cn[2]"/>
        <xsl:text>,</xsl:text>
        <xsl:apply-templates select="m:cn[3]"/>
        <xsl:text>)</xsl:text>
    </xsl:template>


    <xsl:template match="m:apply[m:backslash]">
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>\</xsl:text>
        <xsl:apply-templates select="*[3]"/>
    </xsl:template>

    <xsl:template match="m:apply[m:min]">
		<xsl:text>min(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>,</xsl:text>
        <xsl:apply-templates select="*[3]"/>
		<xsl:text>)</xsl:text>
    </xsl:template>

    <xsl:template match="m:apply[m:max]">
		<xsl:text>max(</xsl:text>
        <xsl:apply-templates select="*[2]"/>
        <xsl:text>,</xsl:text>
        <xsl:apply-templates select="*[3]"/>
		<xsl:text>)</xsl:text>
    </xsl:template>


    <xsl:template match="m:apply[m:fn]">
        <xsl:apply-templates select="m:fn/m:ci"/>
		<xsl:text>(</xsl:text>
		<xsl:for-each select="*[not(self::m:fn)]">
			<xsl:apply-templates select="."/>
			<xsl:if test="position()&lt;last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>)</xsl:text>
    </xsl:template>

	<xsl:template match="smtb:input/m:list">
		<xsl:text>(</xsl:text>
		<xsl:for-each select="*">
			<xsl:apply-templates select="."/>
			<xsl:if test="position()&lt;last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>)</xsl:text>
	</xsl:template>


	<xsl:template match="smtb:output/m:list">
		<xsl:text>[</xsl:text>
		<xsl:for-each select="*">
			<xsl:apply-templates select="."/>
			<xsl:if test="position()&lt;last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>]</xsl:text>
	</xsl:template>


	<xsl:template match="m:list">
		<xsl:text>[</xsl:text>
		<xsl:for-each select="*">
			<xsl:apply-templates select="."/>
			<xsl:if test="position()&lt;last()">
				<xsl:choose>
					<xsl:when test="../@separator">
						<xsl:value-of select="../@separator"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>;</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>]</xsl:text>
	</xsl:template>

	<xsl:template match="m:vector">
		<xsl:text>[</xsl:text>
		<xsl:for-each select="*">
			<xsl:apply-templates select="."/>
			<xsl:if test="position()&lt;last()">
				<xsl:choose>
					<xsl:when test="../@separator">
						<xsl:value-of select="../@separator"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>;</xsl:text>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
		</xsl:for-each>
		<xsl:text>]</xsl:text>
	</xsl:template>

	<xsl:template match="m:matrix">
		<xsl:text>[</xsl:text>
			<xsl:apply-templates/>
		<xsl:text>]</xsl:text>
	</xsl:template>

	<xsl:template match="m:matrixrow">
    <xsl:for-each select="*">
			<xsl:apply-templates select="."/>
			<xsl:if test="position()&lt;last()">
			  <xsl:text>,</xsl:text>
			</xsl:if>
    </xsl:for-each>
		<xsl:if test="position()&lt;last()">
		  <xsl:text>;</xsl:text>
		</xsl:if>
	</xsl:template>


	<xsl:template match="smtb:function">
		<xsl:text>function </xsl:text>
		<xsl:apply-templates select="smtb:output"/>
		<xsl:text>=</xsl:text>
		<xsl:apply-templates select="m:ci"/>
		<xsl:apply-templates select="smtb:input"/>
		<xsl:text>&#xA;</xsl:text>
		<!--><xsl:text>//&#10;</xsl:text>
		<xsl:text>//Initialize all time computing&#10;</xsl:text>
		<xsl:text>//&#10;</xsl:text>		
		<xsl:text>tcmv=0;// time computing for building matrix and vector&#10;</xsl:text>
		<xsl:text>tclu=0;// time computing for lu factorisation of matrices&#10;</xsl:text>
		<xsl:text>tcss=0;// time computing for solving system&#10;</xsl:text>
		<xsl:text>tcludel=0;// time computing for ludel of matrices&#10;</xsl:text>-->
		<xsl:apply-templates select="smtb:body"/>
		<xsl:text>endfunction&#xA;</xsl:text>
	</xsl:template>
	
	<xsl:template match="smtb:output">
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="smtb:input">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="smtb:body">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="smtb:comment">
<!--        <xsl:if test="ancestor::smtb:body">
    		<xsl:text>    </xsl:text>
        </xsl:if>	-->	
        <xsl:choose>     
            <xsl:when test="$lang='scilab'">      
                <xsl:text>//&#xA;</xsl:text>		
                <xsl:value-of select="concat('// ',normalize-space(.),'&#xA;//')"/>
            </xsl:when>
            <xsl:when test="$lang='C'">
                <xsl:text>/* </xsl:text>
                <xsl:apply-templates/>
                <xsl:text> */</xsl:text> 
            </xsl:when>
        </xsl:choose>  
        <xsl:text>&#xA;</xsl:text>
	</xsl:template>

	<xsl:template match="smtb:optimize">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="smtb:matrix-open"/>
	<xsl:template match="smtb:hypermatrix-open"/>
	
	<xsl:template match="smtb:matrix-assignment"/>

	<xsl:template match="smtb:hypermatrix">
		<xsl:value-of select="concat(@id,'=zeros(',@rows,',',@cols,',',@versions,');&#xA;')"/>
	</xsl:template>

	<xsl:template match="smtb:matrix-close">
    <xsl:message><xsl:value-of select="concat('Assembling matrix ',@id)"/></xsl:message>
        <!-- On collecte les affectations aux éléments d'une matrice donnée, en regroupant
             les affectations multiples à un même terme (i,j), qui sont par convention 
             interprétées comme une affectation d'une somme. Pour cela il faut identifier les
             affectations "uniques" et boucler sur les éventuels multiples. Cela se fait avec
             un système de clés (méthode Muenchienne), qui est beaucoup plus rapide (mais
             gourmand en mémoire) que la méthode "naïve" commentée ci-dessous. -->
        
<!--        <xsl:variable name="assignments">
		    <xsl:for-each select="preceding-sibling::smtb:matrix-assignment[@id=current()/@id]">
                <xsl:if test="not(following-sibling::smtb:matrix-assignment[(@id=current()/@id) and (@row=current()/@row) and (@col=current()/@col)])">
                    <smtb:matrix-assignment id="{@id}" row="{@row}" col="{@col}">
                        <m:apply>
                            <m:plus/>
                            <xsl:copy-of select="*"/>
                            <xsl:for-each select="preceding-sibling::smtb:matrix-assignment[(@id=current()/@id) and (@row=current()/@row) and (@col=current()/@col)]">
                                <xsl:copy-of select="*"/>
                            </xsl:for-each>
                        </m:apply>
                    </smtb:matrix-assignment>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
		<xsl:for-each select="exslt:node-set($assignments)/smtb:matrix-assignment">
			<xsl:value-of select="concat(@row,',',@col,',')"/>
			<xsl:apply-templates/>
			<xsl:if test="position()&lt;last()">
				<xsl:text>&#xA;</xsl:text>
			</xsl:if>
		</xsl:for-each>-->
        
        <!-- Méthode Muenchienne http://www.jenitennison.com/xslt/grouping/muenchian.html -->
	    
		<xsl:variable name="optimize_id" select="generate-id(ancestor::smtb:optimize)"/>

		<xsl:value-of select="concat(@id,'_ijv=[')"/>
		
    <xsl:choose>
      <xsl:when test="key('matrix-open',concat($optimize_id,' ',@id))/@assignments='unique'">
        <xsl:for-each select="preceding::smtb:matrix-assignment[@id=current()/@id][key('matrix-assignments-by-id',concat($optimize_id,' ',@id))]">
    			<xsl:value-of select="concat(@row,',',@col,',')"/>
	    		<xsl:apply-templates/>
		      <xsl:if test="position()&lt;last()">
			      <xsl:text>&#xA;</xsl:text>
			    </xsl:if>
        </xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
        <xsl:for-each select="preceding::smtb:matrix-assignment[@id=current()/@id][count(. | key('matrix-assignments-by-id-row-col',concat($optimize_id,' ',@id,' ',@row,' ',@col))[1])=1]">     
          <xsl:variable name="assignments">
            <m:apply>
                <m:plus/>
                <xsl:for-each select="key('matrix-assignments-by-id-row-col',concat($optimize_id,' ',@id,' ',@row,' ',@col))">
                    <xsl:copy-of select="*"/>
                </xsl:for-each>
            </m:apply>
          </xsl:variable>
    			<xsl:value-of select="concat(@row,',',@col,',')"/>
	    		<xsl:apply-templates select="exslt:node-set($assignments)/*"/>
		        <xsl:if test="position()&lt;last()">
			   	    <xsl:text>&#xA;</xsl:text>
			    </xsl:if>
        </xsl:for-each>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:text>];&#xA;</xsl:text>

		<xsl:for-each select="key('matrix-open',concat($optimize_id,' ',@id))">
			<xsl:choose>
				<xsl:when test="@versions">
					<xsl:call-template name="loop-versions"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat(@id,'=',@type,'(',@id,'_ijv(:,1:2),',@id,'_ijv(:,3),[',@rows,',',@cols,']);&#xA;')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		
	</xsl:template>
	
  
  
	<xsl:template match="smtb:hypermatrix-close">
		<xsl:value-of select="concat(@id,'_ijv=[')"/>

        <xsl:for-each select="preceding-sibling::smtb:matrix-assignment[@id=current()/@id][count(. | key('matrix-assignments',concat(@id,' ',@row,' ',@col))[1])=1]">
           
                <xsl:variable name="assignments">
                    <m:apply>
                        <m:plus/>
                        <xsl:for-each select="key('matrix-assignments',concat(@id,' ',@row,' ',@col))">
                            <xsl:copy-of select="*"/>
                        </xsl:for-each>
                    </m:apply>
                </xsl:variable>
           
    			<xsl:value-of select="concat('[',@row,',',@col,',')"/>
	    		<xsl:apply-templates select="exslt:node-set($assignments)/*"/>
			<xsl:value-of select="concat(']',$quote)"/>
		        <xsl:if test="position()&lt;last()">
				    
			   	    <xsl:text> ...&#xA;</xsl:text>
			    </xsl:if>

		</xsl:for-each>

		<xsl:text>]';&#xA;</xsl:text>
		<xsl:for-each select="preceding-sibling::smtb:hypermatrix-open[@id=current()/@id]">
			<xsl:choose>
				<xsl:when test="@versions2">
					<xsl:call-template name="loop-versions2"/>
				</xsl:when>
				<xsl:when test="@nb_step and @exp">
					<xsl:choose>
						<xsl:when test="@transpose">
							<xsl:value-of select="concat(@id,'=',@type,'(',@id,'_ijv(:,1:2),',@id,'_ijv(:,3:$),[',@rows,',',@cols,',',@exp,',',@nb_step,'],%t);&#xA;')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="concat(@id,'=',@type,'(',@id,'_ijv(:,1:2),',@id,'_ijv(:,3:$),[',@rows,',',@cols,',',@exp,',',@nb_step,'],%f);&#xA;')"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="concat(@id,'=',@type,'(',@id,'_ijv(:,1:2),',@id,'_ijv(:,3:$),[',@rows,',',@versions,',',@nb_step,']);&#xA;')"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		
	</xsl:template>
    
	<xsl:template match="smtb:timer">
		
		<!--<xsl:text>timer();&#10;</xsl:text>-->
		
		<xsl:apply-templates/>
		
		<xsl:choose>
			<xsl:when test="(@name='t_grad') or (@name='t_cost')">
				<xsl:value-of select="concat(@name,'=timer();&#10;')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat(@name,'=',@name,'+timer();&#10;')"/>
			</xsl:otherwise>
		</xsl:choose>		
	</xsl:template>
	
	<xsl:template match="smtb:solve">
		<xsl:variable name="id" select="generate-id(ancestor::smtb:function)"/>
		<xsl:if test="not(preceding::smtb:solve[(@matrix=current()/@matrix) and (generate-id(ancestor::smtb:function)=$id)])">
			<xsl:value-of select="concat(@matrix,'_handle=umf_lufact(',@matrix,');&#xA;')"/>
		</xsl:if>
		<xsl:apply-templates select="smtb:lhs"/>
		
		<xsl:value-of select="concat('=umf_lusolve(',@matrix,'_handle,')"/>

		<xsl:apply-templates select="smtb:rhs"/>

		<xsl:if test="@transpose='yes'">
			<xsl:value-of select="concat(',',$quote,'A',$quote,$quote,'x=b',$quote)"/>
		</xsl:if>

		<xsl:value-of select="');&#xA;'"/>
		
		<xsl:if test="not(following::smtb:solve[(@matrix=current()/@matrix) and (generate-id(ancestor::smtb:function)=$id)])">
			<xsl:value-of select="concat('umf_ludel(',@matrix,'_handle);&#xA;')"/>
		</xsl:if>

	</xsl:template>
	
	<xsl:template match="smtb:rhs">
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="smtb:lhs">
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="smtb:loop">
		<xsl:value-of select="concat('for ',@id,'=',@start,':',@end,'&#xA;')"/>
			<xsl:apply-templates/>
		<xsl:text>end&#xA;</xsl:text>
	</xsl:template>
	
	
	<xsl:template name="loop-versions">
		<xsl:param name="i">1</xsl:param>
		<xsl:value-of select="concat(@id,'_',$i,'=',@type,'(',@id,'_ijv(:,1:2),',@id,'_ijv(:,',($i)+2,'),[',@rows,',',@cols,']);&#xA;')"/>
		<xsl:if test="$i&lt;@versions">
			<xsl:call-template name="loop-versions">
				<xsl:with-param name="i" select="($i)+1"/>
			</xsl:call-template>	
		</xsl:if>	
	</xsl:template>
	
	<xsl:template name="loop-versions2">
		<xsl:param name="i">1</xsl:param>
		
		<xsl:choose>
			<xsl:when test="@matrix_type">
				<xsl:value-of select="concat(@id,'_',$i,'=',@type,'(',@id,'_ijv(:,1:2),',@id,'_ijv(:,2+',$i,':',@versions2,':$),[',@rows,',',@cols,',',@nb_step,'],',$double_quote,'full',$double_quote,',%t);&#xA;')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat(@id,'_',$i,'=',@type,'(',@id,'_ijv(:,1:2),',@id,'_ijv(:,2+',$i,':',@versions2,':$),[',@rows,',',@cols,',',@nb_step,'],',$double_quote,'sparse',$double_quote,',%t);&#xA;')"/>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:if test="$i&lt;@versions2">
			<xsl:call-template name="loop-versions2">
				<xsl:with-param name="i" select="($i)+1"/>
			</xsl:call-template>	
		</xsl:if>	
	</xsl:template>
	
    <xsl:template match="m:ci">
        <xsl:value-of select="."/>
    </xsl:template>

    <xsl:template match="m:cn">
        <xsl:value-of select="."/>
    </xsl:template>

	<xsl:template match="smtb:script">
		<xsl:document href="{@href}" method="text" encoding="ISO-8859-15">
			<xsl:apply-templates/>
		</xsl:document>
	</xsl:template>

    <xsl:template match="smtb:string">
        <xsl:value-of select="concat('&quot;',normalize-space(.),'&quot;')"/>
    </xsl:template>


</xsl:stylesheet>
