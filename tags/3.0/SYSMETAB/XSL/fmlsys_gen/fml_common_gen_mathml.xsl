<?xml version="1.0" encoding="UTF-8" ?>

<!-- 
    Auteur  :   Georges Sadaka, Stéphane Mottelet
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Projet  :   PIVERT/Metalippro-PL1
-->

<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
  xmlns:m="http://www.w3.org/1998/Math/MathML" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:exslt="http://exslt.org/common" 
  xmlns:str="http://exslt.org/strings" 
  xmlns:math="http://exslt.org/math" 
  xmlns:smtb="http://www.utc.fr/sysmetab" 
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb f">
 
  <xsl:key name="rproduct" match="f:reaction/f:rproduct" use="@id"/>
  <xsl:key name="reduct" match="f:reaction/f:reduct" use="@id"/>
  <xsl:key name="rproductf" match="f:reaction[contains(@id,'_f')]/f:rproduct" use="@id"/>
  <xsl:key name="reductf" match="f:reaction[contains(@id,'_f')]/f:reduct" use="@id"/>
  <xsl:key name="reaction" match="f:reaction" use="@id"/>
  <xsl:key name="reactionf" match="f:reaction[contains(@id,'_f')]" use="@id"/>  
  <xsl:key name="pool" match="f:pool" use="@id"/>
  <xsl:key name="input_cumomer" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>
  <xsl:key name="cumomer" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>
  <xsl:key name="cumomers" match="f:pool/smtb:cumomer" use="@id"/>
  <xsl:key name="datummeasurment" match="f:datum" use="@id"/>

 
  <xsl:template name="iteration">

  <!-- Attention le noeud contextuel est un élement <smtb:cumomer>.
  La variable "carbons" contient les carbones 13 de l'espèce courante -->

    <xsl:variable name="carbons">
      <xsl:copy-of select="key('cumomers',@id)/smtb:carbon"/>
    </xsl:variable>
    <xsl:variable name="weight" select="../@weight"/>
    <xsl:variable name="position" select="@number"/>

    <!-- influx rule : le plus chiant, mais aussi le plus intéressant. C'est là que se crée véritablement
l'information 
    supplémentaire par rapport à la stoechiométrie simple. 
    On boucle sur tous les éléments rproduct qui ont l'espèce du cumomère courant comme produit,
donc dans le 
    for-each le noeud contextuel est de type reaction/rproduct. -->
    
    <xsl:for-each select="key('rproduct',@pool)">
    
    <!-- Maintenant, on essaye de trouver des occurences des carbones marqués du reactant de la réaction courante : c'est du boulot... -->
    
      <xsl:variable name="id" select="@id"/>
      <xsl:variable name="reaction" select="../@position"/>
      <xsl:variable name="occurence" select="count(preceding-sibling::f:rproduct)+1"/>
      <xsl:variable name="influx">
      
        <!-- C'est ici que les choses sérieuses commencent. On boucle sur tous les réactants qui ont des atomes de carbones
qui "pointent" sur l'espèce -->

        <xsl:for-each select="../f:reduct[smtb:carbon[(@id=$id) and (@occurence=$occurence)]]">
          <xsl:variable name="somme">
        
            <!-- On boucle donc maintenant sur les carbones du reactant. Si un carbone du reactant pointe vers un carbone 13 du cumomère du produit, 
            on note son numéro dans un élément <token/>-->
        
            <xsl:for-each select="smtb:carbon[(@id=$id) and (@occurence=$occurence)]">
              <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                <token>
                  <xsl:value-of select="@position"/>
                </token>
              </xsl:if>
            </xsl:for-each>
        
            <!-- A la fin de cette boucle la variable "somme" contient un certain nombre, éventuellement nul, d'éléments <token> qui identifient sans 
            ambiguité le cumomère concerné. -->
        
          </xsl:variable>
          <xsl:if test="sum(exslt:node-set($somme)/token)&gt;0">
        
          <!-- On fait la somme des <token>, ce qui est une façon de savoir s'il y en a au moins un... -->

          <!-- On génére un nouvel élément <token> avec un attribut weight précisant le poids du cumomère concerné (le nombre de <token>dans $somme), le
          type de l'espèce concernée (intermédiaire ou entrée) et son identificateur, que l'on obtient en faisant la somme des <token>. -->

            <token weight="{count(exslt:node-set($somme)/token)}" type="{key('pool',@id)/@type}">
              <xsl:value-of select="concat(@id,'_',sum(exslt:node-set($somme)/token))"/>
            </token>
          </xsl:if>
        </xsl:for-each>
      
        <!-- A la fin de cette boucle la variable "influx" contient un certain nombre de <token>. Suivant le type et le poids des cumomères concernés, 
        il s'agit soit d'inconnues, si le poids est le poids courant $weight, soit de quantités intervenant au second membre
sous forme de produit, 
        dont la somme des poids est égale au poids courant $weight. -->
        
      </xsl:variable>
      
      <!-- Ici on génère le code qui forme la matrice et le second membre du système qui permet d'obtenir les
cumomères de poids $weight -->

      <xsl:choose>
        <xsl:when test="(count(exslt:node-set($influx)/token)=1) and (exslt:node-set($influx)/token/@weight=$weight) and (exslt:node-set($influx)/token/@type='intermediate')">
        
        <!-- On n'a qu'un seul terme du poids courant, on assemble donc la matrice ainsi que sa dérivée -->
        
          <matrix-assignment id="{concat('M',$weight)}" row="{$position}" col="{key('cumomer',$influx)/@number}" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <selector/>
              <ci type="vector">v</ci>
              <cn><!-- id de la réaction -->
                <xsl:value-of select="$reaction"/>
              </cn>
            </apply>
          </matrix-assignment>
          <matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{$reaction}" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <selector/>
              <ci type="matrix">
                <xsl:value-of select="concat('x',$weight)"/>
              </ci>
              <cn>
                <xsl:value-of select="key('cumomer',$influx)/@number"/>
              </cn>
              <cn>:</cn>
            </apply>
          </matrix-assignment>
        </xsl:when>
        <xsl:when test="count(exslt:node-set($influx)/token)&gt;=1">
        <!-- On a un produit de plusieurs termes, donc de poids inférieur, on assemble donc le second membre
ainsi que la 
        jacobienne du second membre par rapport aux cumomères de poids inférieur  -->
        
          <xsl:variable name="influx-factors">
            <xsl:call-template name="iterate-influx">
              <xsl:with-param name="tokens">
                <xsl:copy-of select="$influx"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:variable>
          <matrix-assignment id="{concat('b',$weight)}" row="{$position}" col="1" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <times type="array"/>
              <apply>
                <selector/>
                <ci type="vector">v</ci>
                <cn> <!-- id de la réaction -->
                  <xsl:value-of select="$reaction"/>
                </cn>
              </apply>
              <xsl:copy-of select="$influx-factors"/>
            </apply>
          </matrix-assignment>
          <matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{$reaction}" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <times type="array"/>
              <xsl:copy-of select="$influx-factors"/>
            </apply>
          </matrix-assignment>  
          <xsl:call-template name="iterate-influx-jacobian-x">
            <xsl:with-param name="reaction" select="$reaction"/>
            <xsl:with-param name="weight" select="$weight"/>
            <xsl:with-param name="tokens">
              <xsl:copy-of select="$influx"/>
            </xsl:with-param>
            <xsl:with-param name="position" select="$position"/>
            <xsl:with-param name="i">1</xsl:with-param>
            <xsl:with-param name="n" select="count(exslt:node-set($influx)/token)"/>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
    </xsl:for-each>

    <!-- outflux rule : partie la plus simple à générer (voir papier Wiechert) -->
    
    <matrix-assignment id="{concat('M',$weight)}" row="{$position}" col="{$position}" xmlns="http://www.utc.fr/sysmetab">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <minus/>
        <xsl:call-template name="bilan-outflux">
          <xsl:with-param name="id" select="@pool"/>
        </xsl:call-template>
      </apply>
    </matrix-assignment>
    <xsl:for-each select="key('reduct',@pool)">
      <matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{../@position}" xmlns="http://www.utc.fr/sysmetab">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <minus/>
          <apply>
            <selector/>
            <ci type="matrix">
              <xsl:value-of select="concat('x',$weight)"/>
            </ci>
            <cn>
              <xsl:value-of select="$position"/>
            </cn>
            <cn>:</cn>
          </apply>
        </apply>
      </matrix-assignment>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="bilan-outflux">
    <xsl:param name="id"/>
    
    <!-- outflux rule -->

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <plus/>
      <xsl:for-each select="key('reduct',$id)">
        <apply>
          <selector/>
          <ci type="vector">v</ci>
          <cn> <!-- id de la réaction -->
            <xsl:value-of select="../@position"/>
          </cn>
        </apply>
      </xsl:for-each>
    </apply>
  </xsl:template>

  <xsl:template name="iterate-influx">
    <xsl:param name="tokens"/>
    <xsl:variable name="id" select="exslt:node-set($tokens)/token[1]"/>
    <xsl:choose>
      <xsl:when test="key('cumomer',$id)">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <selector/>
          <ci type="matrix">
            <xsl:value-of select="concat('x',key('cumomer',$id)/@weight)"/>
          </ci>
          <cn>
            <xsl:value-of select="key('cumomer',$id)/@number"/>
          </cn>
          <cn>:</cn>
        </apply>
      </xsl:when>
      <xsl:when test="key('input_cumomer',$id)">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <selector/>
          <ci type="matrix">
            <xsl:value-of select="concat('x',key('input_cumomer',$id)/@weight,'_input')"/>
          </ci>
          <cn>
            <xsl:value-of select="key('input_cumomer',$id)/@number"/>
          </cn>
          <cn>:</cn>
        </apply>
      </xsl:when>
    </xsl:choose>
    <xsl:if test="count(exslt:node-set($tokens)/token)&gt;1">
      <xsl:call-template name="iterate-influx">
        <xsl:with-param name="tokens">
          <xsl:copy-of select="exslt:node-set($tokens)/token[position()&gt;1]"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="iterate-influx-jacobian-x">

  <!-- Generation des jacobiennes (dérivées) dbi/dxj -->

    <xsl:param name="reaction"/>
    <xsl:param name="weight"/>
    <xsl:param name="tokens"/>
    <xsl:param name="position"/>
    <xsl:param name="i"/>
    <xsl:param name="n"/>
    <xsl:variable name="id" select="exslt:node-set($tokens)/token[position()=$i]"/>
    <xsl:if test="key('cumomer',$id)">
      <matrix-assignment id="{concat('db',$weight,'_dx',key('cumomer',$id)/@weight)}" row="{$position}" col="{key('cumomer',$id)/@number}" xmlns="http://www.utc.fr/sysmetab">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <times type="array"/>
          <xsl:call-template name="iterate-influx">
          
          <!-- La dérivée est égale à tout ce qui est en facteur de la variable -->
          
            <xsl:with-param name="tokens">
              <xsl:copy-of select="exslt:node-set($tokens)/token[position()!=$i]"/>
            </xsl:with-param>
          </xsl:call-template>
          <apply>
            <selector/>
            <ci type="vector">v</ci>
            <cn>
              <xsl:value-of select="$reaction"/>
            </cn>
          </apply>
        </apply>
      </matrix-assignment>
    </xsl:if>
    <xsl:if test="$i&lt;$n">
      <xsl:call-template name="iterate-influx-jacobian-x">
        <xsl:with-param name="reaction" select="$reaction"/>
        <xsl:with-param name="weight" select="$weight"/>
        <xsl:with-param name="tokens">
          <xsl:copy-of select="$tokens"/>
        </xsl:with-param>
        <xsl:with-param name="position" select="$position"/>
        <xsl:with-param name="i" select="($i)+1"/>
        <xsl:with-param name="n" select="$n"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!--Construction des matrices C-->

  <xsl:template name="observation-matrices-open">
    <xsl:param name="number-of-observations"/>
    <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
      <matrix-open id="C{@weight}" 
                   rows="{$number-of-observations}" 
                   cols="{count(smtb:cumomer)}" 
                   type="sparse" 
                   assignments="unique"
                   xmlns="http://www.utc.fr/sysmetab"/>
    </xsl:for-each>
    <matrix-open id="C0"
                 rows="{$number-of-observations}" 
                 cols="1" 
                 type="sparse" 
                 assignments="unique"
                 xmlns="http://www.utc.fr/sysmetab"/>
  </xsl:template>

  <xsl:template name="observation-matrices-close">
    <xsl:param name="number-of-observations"/>
    <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
      <matrix-close id="C{@weight}" xmlns="http://www.utc.fr/sysmetab"/>
    </xsl:for-each>
    <matrix-close id="C0" xmlns="http://www.utc.fr/sysmetab"/>
  </xsl:template>

  <xsl:template name="construct_matrix_C">

    <comment xmlns="http://www.utc.fr/sysmetab">Matrices d'observation (cumomères)</comment>

    <optimize xmlns="http://www.utc.fr/sysmetab"><!-- On ouvre un élément "optimize" dans lequel on va construire petit à petit ces matrices -->
      <xsl:call-template name="observation-matrices-open">
        <xsl:with-param name="number-of-observations" select="count(f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']/smtb:measurement)"/>
      </xsl:call-template>
      <xsl:for-each select="f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']/smtb:measurement">
        <xsl:variable name="position" select="position()"/>
        <xsl:for-each select="smtb:cumomer-contribution">
          <xsl:choose>
            <xsl:when test="@weight=0">
              <matrix-assignment id="C0" row="{$position}" col="1">
                <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
              </matrix-assignment>
            </xsl:when>
            <xsl:otherwise>  
              <matrix-assignment id="C{@weight}" row="{$position}" col="{key('cumomer',concat(../../@id,'_',@subscript))/@number}">
                <cn xmlns="http://www.w3.org/1998/Math/MathML">
                  <xsl:value-of select="@sign"/>
                </cn>
              </matrix-assignment>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:call-template name="observation-matrices-close"/>
     </optimize>

      <comment xmlns="http://www.utc.fr/sysmetab"> (Sy)^-2 array</comment>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>Sypm2</ci>
      <apply>
        <power/>
        <vector>
          <xsl:for-each select="f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']/smtb:measurement">
            <cn>
              <xsl:choose>
                <xsl:when test="count(key('datummeasurment',@id))=1">
                  <xsl:value-of select="key('datummeasurment',@id)/@stddev"/>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="key('datummeasurment',@id)[@weight=current()/@weight]/@stddev"/>
                </xsl:otherwise>
              </xsl:choose>  
            </cn>
          </xsl:for-each>
          </vector>
        <apply>
          <minus/>
          <cn>2</cn>
        </apply>
        </apply>
    </apply>

      <comment xmlns="http://www.utc.fr/sysmetab"> ymeas array</comment>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>ymeas</ci>
      <vector>
        <xsl:for-each select="f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate']/smtb:measurement">
          <cn>
            <xsl:choose>
              <xsl:when test="count(key('datummeasurment',@id))=1">
                <xsl:value-of select="key('datummeasurment',@id)"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="key('datummeasurment',@id)[@weight=current()/@weight]"/>
              </xsl:otherwise>
            </xsl:choose>  
          </cn>
        </xsl:for-each>
        </vector>
    </apply>
  </xsl:template>

  <!--Construction de la matrice E"-->

  <xsl:template name="construct_matrix_E">

    <comment xmlns="http://www.utc.fr/sysmetab">Matrice d'observation (flux)</comment>

    <optimize xmlns="http://www.utc.fr/sysmetab"><!-- On ouvre un élément "optimize" dans lequel on va construire petit à petit la matrice E -->

    <!--Construction de la matrice E"-->

      <matrix-open id="E" 
                   cols="{count(f:reactionnetwork/f:reaction)}" 
                   rows="{count(f:configuration/f:measurement/f:model/f:fluxmeasurement/f:netflux)}"
                   assignments="unique"
                   type="sparse"/>
      <xsl:for-each select="f:configuration/f:measurement/f:model/f:fluxmeasurement/f:netflux">
        <xsl:variable name="pool" select="@pool"/>
        <matrix-assignment id="E" row="{position()}" col="{key('reactionf',concat(f:textual,'_f'))/@pos}">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
        </matrix-assignment>
      </xsl:for-each>  
      <matrix-close id="E"/>
    
      <!-- Lorsque l'on va rencontrer cet élément dans la transformation qui a pour but de produire le code Scilab, on rassemble tous les ordres d'affectation
      à la matrice "E" et on crée cette matrice -->
    </optimize>

    <comment xmlns="http://www.utc.fr/sysmetab"> (Sv)^-2 array</comment>
  
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>Svpm2</ci>
      <apply>
        <power/>
        <vector>
          <xsl:for-each select="f:configuration/f:measurement/f:model/f:fluxmeasurement/f:netflux">
            <cn>
              <xsl:value-of select="key('datummeasurment',@id)/@stddev"/>
            </cn>
          </xsl:for-each>
        </vector>
        <apply>
          <minus/>
          <cn>2</cn>
        </apply>
      </apply>
    </apply>

    <comment  xmlns="http://www.utc.fr/sysmetab"> vmeas array</comment>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>vmeas</ci>
      <vector>
        <xsl:for-each select="f:configuration/f:measurement/f:model/f:fluxmeasurement/f:netflux">
          <cn>
            <xsl:value-of select="key('datummeasurment',@id)"/>
          </cn>
        </xsl:for-each>
      </vector>
    </apply>
  </xsl:template>
  
  <!-- Construction des matrices d'entrée, permettant d'obtenir la contribution sur les cumomères des isotopomères en entrée. -->
  
  <xsl:template name="construct_matrix_D">
    <comment xmlns="http://www.utc.fr/sysmetab">Matrices d'entrée</comment>
    <optimize xmlns="http://www.utc.fr/sysmetab">

    <!-- On ouvre un élément "optimize" dans lequel on va construire petit à petit les matrices D1,D2,...,Dn où "n" est le poids maximum, déterminé 
    lors des transformations préalables en fonction des observations choisies -->

      <xsl:variable name="number-of-inputs" select="count(f:reactionnetwork/f:metabolitepools/f:pool[smtb:input]/smtb:input)"/>
      <xsl:for-each select="f:reactionnetwork/smtb:listOfInputCumomers/smtb:listOfCumomers">
        <matrix-open id="D{@weight}" 
                     cols="{$number-of-inputs}" 
                     rows="{count(smtb:cumomer)}" 
                     assignments="unique"
                     type="sparse"/>  
        <xsl:variable name="current-weight" select="@weight"/>
        
        <!-- Pour chaque cumomère d'un métabolité d'entrée, on détermine quelles sont les contributions des différents isotopomères en entrée. -->
        
        <xsl:for-each select="smtb:cumomer">
        
        <!-- Attention ici l'attribut position de l'élément smtb:cumomer ne représente que le numéro du cumomère dans le  vecteur des cumomères 
        du poids courant -->
        
          <xsl:variable name="position" select="@number"/>
          <xsl:variable name="cumomer_id" select="@id"/>
          <!-- La variable $carbons contient les éléments <smtb:carbon/>correspondant aux carbones marqués du cumomère courant. -->
          <xsl:variable name="pool" select="@pool"/>
          <xsl:for-each select="../../../f:metabolitepools/f:pool[smtb:input]/smtb:input">
            <xsl:if test="../@id=$pool">
              <xsl:variable name="pos" select="position()"/>
              <xsl:for-each select="smtb:cumomer-contribution[(@weight=$current-weight) and (concat($pool,'_',@subscript)=$cumomer_id)]">
                <matrix-assignment id="D{@weight}" row="{$position}" col="{$pos}">
                  <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
                </matrix-assignment>
              </xsl:for-each>
            </xsl:if>
          </xsl:for-each>
        </xsl:for-each>
        <matrix-close id="D{@weight}"/>
      </xsl:for-each>
    </optimize>
  </xsl:template>
  
  <!--Contruction des matrice de noms et ids des fluxs -->

  <xsl:template name="names_and_ids_fluxes">

  <!-- Matrice des noms des fluxs -->

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>flux_names</ci>
      <list separator=";">
        <xsl:for-each select="f:reactionnetwork/f:reaction">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select='@id'/>
          </string>
        </xsl:for-each>
      </list>
    </apply>
    
    <!-- Matrice des ids des fluxs -->
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>flux_ids</ci>
      <list separator=";">
        <xsl:for-each select="f:reactionnetwork/f:reaction">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select='@id'/>
          </string>
        </xsl:for-each>
      </list>
    </apply>

    <!-- Matrice des ids des measurement -->
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>measurement_ids</ci>
      <list separator=";">
        <xsl:for-each select="f:reactionnetwork/f:metabolitepools/f:pool/smtb:measurement">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(../@id,@string)"/>
          </string>
        </xsl:for-each>
      </list>
    </apply>


  </xsl:template>

  
  <!--Cumomères en entrée, calculés à partir des isotopomères des métabolites d'entrée.-->

  <xsl:template name="input_cumomeres">
    
    <comment xmlns="http://www.utc.fr/sysmetab"><xsl:value-of select="name(.)"/>Cumomères en entrée, calculés à partir des isotopomères des métabolites d'entrée.</comment>

    <xsl:for-each select="f:configuration/f:input">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci><xsl:value-of select="concat(@pool,'_input')"/></ci>
        <list separator=";">
          <xsl:for-each select="f:label">
            <cn>
              <xsl:value-of select="."/>
            </cn>
          </xsl:for-each>
        </list>
      </apply>    
    </xsl:for-each>
    
    <xsl:for-each select="f:reactionnetwork/smtb:listOfInputCumomers/smtb:listOfCumomers">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci><xsl:value-of select="concat('x',@weight,'_input')"/></ci>
        <apply>
          <times/>
          <ci><xsl:value-of select="concat('D',@weight)"/></ci>
          <list separator=";">
            <xsl:for-each select="../../f:metabolitepools/f:pool[smtb:input]">
              <ci><xsl:value-of select="concat(@id,'_input')"/></ci>
            </xsl:for-each>
          </list>
        </apply>
      </apply>
    </xsl:for-each>
  </xsl:template>

  <xsl:template name="fluxSubspace">
  
    <comment xmlns="http://www.utc.fr/sysmetab">Construction des matrices (dont la matrice de stoechiométrie) permettant de calculer le changement de variable affine des flux.</comment>
    
    <xsl:apply-templates select="f:constraints"/>
    
    <xsl:apply-templates select="f:reactionnetwork/f:metabolitepools" mode="stoichiometric-matrix"/>        
    
    <xsl:apply-templates select="f:configuration/f:simulation"/>

    <comment xmlns="http://www.utc.fr/sysmetab">Contraintes d'égalité et d'inégalité</comment>
  
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>C</ci>
      <matrix>
        <xsl:if test="f:constraints/f:net/m:math/m:apply[m:eq]">
          <matrixrow>
          <ci>A_eq_net</ci>
          <apply>
            <fn><ci>zeros</ci></fn>
            <cn>
              <xsl:value-of select="count(f:constraints/f:net/m:math/m:apply[m:eq])"/>
            </cn>
            <cn>
              <xsl:value-of select="$dimdiv2"/>
            </cn>
          </apply>
         </matrixrow>
        </xsl:if>
        <xsl:if test="f:constraints/f:xch/m:math/m:apply[m:eq]">
          <matrixrow>
            <apply>
              <fn><ci>zeros</ci></fn>
              <cn>
                <xsl:value-of select="count(f:constraints/f:xch/m:math/m:apply[m:eq])"/>
              </cn>
              <cn>
                <xsl:value-of select="$dimdiv2"/>
              </cn>
            </apply>
            <ci>A_eq_xch</ci>            
          </matrixrow>
        </xsl:if>
        <matrixrow>
          <ci>N</ci>
          <apply>
            <fn><ci>zeros</ci></fn>
            <cn>
              <xsl:value-of select="count(f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate'])"/>
            </cn>
            <cn>
              <xsl:value-of select="$dimdiv2"/>
            </cn>
          </apply>
        </matrixrow>
        <xsl:if test="f:constraints/f:net/m:math/m:apply[m:leq]">
          <matrixrow>
            <apply>
              <minus/>
              <ci>A_leq_net</ci>
            </apply>
            <apply>
              <fn><ci>zeros</ci></fn>
              <cn>
                <xsl:value-of select="count(f:constraints/f:net/m:math/m:apply[m:leq])"/>
              </cn>
              <cn>
                <xsl:value-of select="$dimdiv2"/>
              </cn>
            </apply>
          </matrixrow>
        </xsl:if>
        <xsl:if test="f:constraints/f:xch/m:math/m:apply[m:leq]">
          <matrixrow>
            <apply>
              <fn><ci>zeros</ci></fn>
              <cn>
                <xsl:value-of select="count(f:constraints/f:xch/m:math/m:apply[m:leq])"/>
              </cn>
              <cn>
                <xsl:value-of select="$dimdiv2"/>
              </cn>
            </apply>
            <apply>
              <minus/>
              <ci>A_leq_xch</ci>
            </apply>            
          </matrixrow>
        </xsl:if>
      </matrix>
    </apply>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>b</ci>
      <vector>
        <xsl:if test="f:constraints/f:net/m:math/m:apply[m:eq]">
          <ci>b_eq_net</ci>
        </xsl:if>
        <xsl:if test="f:constraints/f:xch/m:math/m:apply[m:eq]">
          <ci>b_eq_xch</ci>            
        </xsl:if>
        <apply>
          <fn><ci>zeros</ci></fn>
          <cn>
            <xsl:value-of select="count(f:reactionnetwork/f:metabolitepools/f:pool[@type='intermediate'])"/>
          </cn>
          <cn>1</cn>
        </apply>
        <xsl:if test="f:constraints/f:net/m:math/m:apply[m:leq]">
          <apply>
            <minus/>
            <ci>b_leq_net</ci>
          </apply>
        </xsl:if>
        <xsl:if test="f:constraints/f:xch/m:math/m:apply[m:leq]">
          <apply>
            <minus/>
            <ci>b_leq_xch</ci>
          </apply>            
        </xsl:if>
      </vector>
    </apply>
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>Flux_M</ci>
      <matrix>
        <xsl:if test="f:configuration/f:simulation/f:variables/f:fluxvalue[@type='net']">
          <matrixrow>
          <ci>Flux_net</ci>
          <apply>
            <fn><ci>zeros</ci></fn>
            <cn>
              <xsl:value-of select="count(f:configuration/f:simulation/f:variables/f:fluxvalue[@type='net'])"/>
            </cn>
            <cn>
              <xsl:value-of select="$dimdiv2"/>
            </cn>
          </apply>
         </matrixrow>
        </xsl:if>
        <xsl:if test="f:configuration/f:simulation/f:variables/f:fluxvalue[@type='xch']">
          <matrixrow>
            <apply>
              <fn><ci>zeros</ci></fn>
              <cn>
                <xsl:value-of select="count(f:configuration/f:simulation/f:variables/f:fluxvalue[@type='xch'])"/>
              </cn>
              <cn>
                <xsl:value-of select="$dimdiv2"/>
              </cn>
            </apply>
            <ci>Flux_xch</ci>            
          </matrixrow>
        </xsl:if>
      </matrix>
    </apply>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>b_Flux</ci>
      <vector>
        <xsl:if test="f:configuration/f:simulation/f:variables/f:fluxvalue[@type='net']">
          <ci>b_Flux_net</ci>
        </xsl:if>
        <xsl:if test="f:configuration/f:simulation/f:variables/f:fluxvalue[@type='xch']">
          <ci>b_Flux_xch</ci>
        </xsl:if>
      </vector>
    </apply>
    
  </xsl:template>
  
  <xsl:template match="f:constraints">
    <optimize xmlns="http://www.utc.fr/sysmetab">
        <xsl:if test="f:net/m:math/m:apply[m:leq]">
          <matrix-open type="s_full" id="A_leq_net" rows="{count(f:net/m:math/m:apply[m:leq])}" cols="{$dimdiv2}"/>
          <matrix-open type="s_full" id="b_leq_net" rows="{count(f:net/m:math/m:apply[m:leq])}" cols="1"/>
        </xsl:if>
        <xsl:if test="f:net/m:math/m:apply[m:eq]">
          <matrix-open type="s_full" id="A_eq_net" rows="{count(f:net/m:math/m:apply[m:eq])}" cols="{$dimdiv2}"/>
          <matrix-open type="s_full" id="b_eq_net" rows="{count(f:net/m:math/m:apply[m:eq])}" cols="1"/>
        </xsl:if>
        <xsl:if test="f:xch/m:math/m:apply[m:leq]">
          <matrix-open type="s_full" id="A_leq_xch" rows="{count(f:xch/m:math/m:apply[m:leq])}" cols="{$dimdiv2}"/>
          <matrix-open type="s_full" id="b_leq_xch" rows="{count(f:xch/m:math/m:apply[m:leq])}" cols="1"/>
        </xsl:if>  
        <xsl:if test="f:xch/m:math/m:apply[m:eq]">
          <matrix-open type="s_full" id="A_eq_xch" rows="{count(f:xch/m:math/m:apply[m:eq])}" cols="{$dimdiv2}"/>
          <matrix-open type="s_full" id="b_eq_xch" rows="{count(f:xch/m:math/m:apply[m:eq])}" cols="1"/>
        </xsl:if>
      
      <xsl:apply-templates select="f:xch/m:math/m:apply[m:eq]"/>
      <xsl:apply-templates select="f:xch/m:math/m:apply[m:leq]"/>
      <xsl:apply-templates select="f:net/m:math/m:apply[m:eq]"/>
      <xsl:apply-templates select="f:net/m:math/m:apply[m:leq]"/>
      
        <xsl:if test="f:net/m:math/m:apply[m:leq]">
          <matrix-close id="A_leq_net"/>
          <matrix-close id="b_leq_net"/>
        </xsl:if>
        <xsl:if test="f:net/m:math/m:apply[m:eq]">
          <matrix-close id="A_eq_net"/>      
          <matrix-close id="b_eq_net"/>
        </xsl:if>
        <xsl:if test="f:xch/m:math/m:apply[m:leq]">
          <matrix-close id="A_leq_xch"/>
          <matrix-close id="b_leq_xch"/>
        </xsl:if>  
        <xsl:if test="f:xch/m:math/m:apply[m:eq]">
          <matrix-close id="b_eq_xch"/>
          <matrix-close id="A_eq_xch"/>      
        </xsl:if>
    </optimize>
  </xsl:template>
  
  <xsl:template match="f:constraints/*/m:math/m:apply">
    <xsl:if test="m:cn!='0'">
      <matrix-assignment id="{concat('b_',name(*[1]),'_',name(../..))}" row="{position()}" col="1" xmlns="http://www.utc.fr/sysmetab">
        <cn xmlns="http://www.w3.org/1998/Math/MathML"><xsl:value-of select="m:cn"/></cn>
      </matrix-assignment>
    </xsl:if>
    <xsl:variable name="id" select="concat('A_',name(*[1]),'_',name(../..))"/>
      <xsl:choose>
        <xsl:when test="m:ci and not(m:apply)">
          <matrix-assignment id="{$id}" row="{position()}" col="{key('reactionf',concat(m:ci,'_f'))/@pos}" xmlns="http://www.utc.fr/sysmetab">
            <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
          </matrix-assignment>
        </xsl:when>
        <xsl:when test="m:apply">
          <xsl:call-template name="iteration_eq_leq">
            <xsl:with-param name="id" select="$id"/>
            <xsl:with-param name="apply" select="m:apply"/>
            <xsl:with-param name="minus">
              <xsl:choose>
                <xsl:when test="m:apply[m:plus]">1</xsl:when>
                <xsl:when test="m:apply[m:minus]">-1</xsl:when>
              </xsl:choose>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:when>
      </xsl:choose>
  </xsl:template>
  
  <xsl:template name="iteration_eq_leq">
    <xsl:param name="id"/>
    <xsl:param name="apply"/>
    <xsl:param name="minus" select="'1'"/>
    <xsl:choose>
      <xsl:when test="$apply[m:times]">
        <matrix-assignment id="{$id}" row="{position()}" col="{key('reaction',concat($apply/m:ci,'_f'))/@pos}" xmlns="http://www.utc.fr/sysmetab">
          <xsl:choose>
            <xsl:when test="$minus='-1'">
              <apply xmlns="http://www.w3.org/1998/Math/MathML">
                <minus/>
                <cn><xsl:value-of select="$apply/m:cn"/></cn>
              </apply>
            </xsl:when>
            <xsl:otherwise>
              <cn><xsl:value-of select="$apply/m:cn"/></cn>
            </xsl:otherwise>
          </xsl:choose>
        </matrix-assignment>
      </xsl:when>
      <xsl:when test="(count($apply/m:ci)='2') and not($apply/m:apply)">
        <matrix-assignment id="{$id}" row="{position()}" col="{key('reaction',concat($apply/m:ci[1],'_f'))/@pos}" xmlns="http://www.utc.fr/sysmetab">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
        </matrix-assignment>
        <matrix-assignment id="{$id}" row="{position()}" col="{key('reaction',concat($apply/m:ci[2],'_f'))/@pos}" xmlns="http://www.utc.fr/sysmetab">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:value-of select="$minus"/>
          </cn>
        </matrix-assignment>
      </xsl:when>
      <xsl:when test="count($apply/m:apply)='1'">
        <matrix-assignment id="{$id}" row="{position()}" col="{key('reaction',concat($apply/m:ci,'_f'))/@pos}" xmlns="http://www.utc.fr/sysmetab">
          <cn xmlns="http://www.w3.org/1998/Math/MathML">
            <xsl:choose>
              <xsl:when test="$apply[m:plus]">1</xsl:when>
              <xsl:when test="$apply[m:minus]">-1</xsl:when>
            </xsl:choose>
          </cn>  
        </matrix-assignment>
        <xsl:call-template name="iteration_eq_leq">
          <xsl:with-param name="id" select="$id"/>
          <xsl:with-param name="apply" select="$apply/m:apply"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:when test="count($apply/m:apply)='2'">
        <xsl:call-template name="iteration_eq_leq">
          <xsl:with-param name="id" select="$id"/>
          <xsl:with-param name="apply" select="$apply/m:apply[1]"/>
        </xsl:call-template>
        <xsl:call-template name="iteration_eq_leq">
          <xsl:with-param name="id" select="$id"/>
          <xsl:with-param name="apply" select="$apply/m:apply[2]"/>
          <xsl:with-param name="minus">
            <xsl:choose>
              <xsl:when test="$apply[m:plus]">1</xsl:when>
              <xsl:when test="$apply[m:minus]">-1</xsl:when>
            </xsl:choose>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
    </xsl:choose>
  </xsl:template> 
  
  <xsl:template match="f:metabolitepools" mode="stoichiometric-matrix">
    <optimize xmlns="http://www.utc.fr/sysmetab">
      <matrix-open type="s_full" id="N" rows="{count(f:pool[@type='intermediate'])}" cols="{count(../f:reaction) div 2}"/>
      <xsl:apply-templates select="f:pool[@type='intermediate']" mode="stoichiometric-matrix"/>
      <matrix-close id="N"/>
    </optimize>
  </xsl:template>

  <xsl:template match="f:pool" mode="stoichiometric-matrix">
    <xsl:variable name="position" select="position()"/>
    <xsl:for-each select="key('rproductf',@id) | key('reductf',@id)">
      <matrix-assignment id="N" row="{$position}" col="{../@pos}" xmlns="http://www.utc.fr/sysmetab">
        <xsl:choose>
          <xsl:when test="self::f:rproduct">
            <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
          </xsl:when>
          <xsl:when test="self::f:reduct">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <minus/>
              <cn>1</cn>
            </apply>
          </xsl:when>
        </xsl:choose>
      </matrix-assignment>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="f:simulation">
    <optimize xmlns="http://www.utc.fr/sysmetab">
      <xsl:if test="f:variables/f:fluxvalue[@type='net']">
        <matrix-open type="s_full" id="Flux_net" rows="{count(f:variables/f:fluxvalue[@type='net'])}" cols="{$dimdiv2}"/>
        <matrix-open type="s_full" id="b_Flux_net" rows="{count(f:variables/f:fluxvalue[@type='net'])}" cols="1"/>
      </xsl:if>
      <xsl:if test="f:variables/f:fluxvalue[@type='xch']">
        <matrix-open type="s_full" id="Flux_xch" rows="{count(f:variables/f:fluxvalue[@type='xch'])}" cols="{$dimdiv2}"/>
        <matrix-open type="s_full" id="b_Flux_xch" rows="{count(f:variables/f:fluxvalue[@type='xch'])}" cols="1"/>
      </xsl:if>
      
      <xsl:apply-templates select="f:variables/f:fluxvalue[@type='net']"/>
      <xsl:apply-templates select="f:variables/f:fluxvalue[@type='xch']"/>
            
      <xsl:if test="f:variables/f:fluxvalue[@type='net']">
        <matrix-close id="Flux_net"/>
        <matrix-close id="b_Flux_net"/>
      </xsl:if>
      <xsl:if test="f:variables/f:fluxvalue[@type='xch']">
        <matrix-close id="Flux_xch"/>
        <matrix-close id="b_Flux_xch"/>
      </xsl:if>
    </optimize>
  </xsl:template>

  <xsl:template match="f:variables/f:fluxvalue">
    <matrix-assignment id="{concat('b_Flux_',@type)}" row="{position()}" col="1" xmlns="http://www.utc.fr/sysmetab">
      <cn xmlns="http://www.w3.org/1998/Math/MathML"><xsl:value-of select="text()"/></cn>
    </matrix-assignment>
    <matrix-assignment id="{concat('Flux_',@type)}" row="{position()}" col="{key('reactionf',concat(@flux,'_f'))/@pos}" xmlns="http://www.utc.fr/sysmetab">
      <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
    </matrix-assignment>
  </xsl:template>

  
  
</xsl:stylesheet>