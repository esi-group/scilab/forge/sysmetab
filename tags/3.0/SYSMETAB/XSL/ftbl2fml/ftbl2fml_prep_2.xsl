<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
  xmlns:ftbl="http://ftbl.org"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:str="http://exslt.org/strings"   
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:math="http://exslt.org/math"
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:Scr="http://Scr.com"
  xmlns:func="http://exslt.org/functions"
  exclude-result-prefixes="xsl xhtml str sbml celldesigner f m Scr func ftbl math">
  
  <xsl:output method="xml" indent="yes" encoding="utf-8"/>

  <xsl:strip-space elements="*"/> <!-- preserve the white space-->

<!--  <xsl:key name="cfg" match="contains(.,'#')[1]" use="."/>-->

  <xsl:key name="REDUCTPRODUCT" match="f:reduct|f:rproduct" use="@id"/>  

  <xsl:key name="REDUCT" match="f:reduct" use="@id"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="fluxml">
    <fluxml xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates/>
    </fluxml>
  </xsl:template>

  <xsl:template match="f:pool[key('REDUCT',@id)]">
    <pool atoms="{string-length(key('REDUCTPRODUCT',@id)[1]/@cfg)}"  id="{@id}" xmlns="http://www.13cflux.net/fluxml"/>
  </xsl:template>
  
  <xsl:template match="f:pool[not(key('REDUCT',@id))]"/>
  
  <xsl:template match="f:reaction">
    <reaction id="{@id}" xmlns="http://www.13cflux.net/fluxml">
    <xsl:apply-templates select="f:reduct"/>
    <xsl:for-each select="f:rproduct[key('REDUCT',@id)]">
      <rproduct>
        <xsl:copy-of select="@*"/>
      </rproduct>
    </xsl:for-each>
    </reaction>
  </xsl:template>    

<!--
        <xsl:if test="not(@id=../preceding-sibling::f:reduct/@id) and not(@id=../../preceding-sibling::f:reaction/f:reduct/@id)">
  <xsl:template match="f:reactionnetwork">
    <reactionnetwork xmlns="http://www.13cflux.net/fluxml">
    <metabolitepools>
      <xsl:for-each select="f:reaction/f:reduct">

        <xsl:if test="not(.=../preceding-sibling::f:reduct/@id)">

        <pool atoms="{string-length(key('REDUCTPRODUCT',@id)[1]/@cfg)}"  id="{@id}" xmlns="http://www.13cflux.net/fluxml"/>
        </xsl:if>
      </xsl:for-each>
    </metabolitepools>
    <xsl:apply-templates/>
    </reactionnetwork>
  </xsl:template>    
-->
    
<!--    
      <xsl:for-each select="row[position() mod 2 = 0]/value[(@position &lt;= 6) and (@position &gt;=3)]">
      	<xsl:if test="not(.=../preceding-sibling::row[position() mod 2 = 0]/value[(@position &lt;= 6) and (@position &gt;=3)]) and not(.=preceding-sibling::value[(@position &lt;= 6) and (@position &gt;=3)])">
        <pool id="{.}"/>
        </xsl:if>
      </xsl:for-each>
    </metabolitepools>
    
  <xsl:template match="f:reduct">
    <pool atoms="{string-length(key('REDUCTPRODUCT',@id)[1]/@cfg)}"  id="{@id}" xmlns="http://www.13cflux.net/fluxml"/>  
  </xsl:template>

-->  

  <xsl:template match="*|@*|text()">    
      <xsl:copy>
          <xsl:apply-templates select="*|@*|text()"/>
      </xsl:copy>
  </xsl:template>


</xsl:stylesheet>  
