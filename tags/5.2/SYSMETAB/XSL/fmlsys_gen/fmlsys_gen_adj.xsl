<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb f exslt">

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <xsl:param name="output"/>
  <xsl:param name="verb_level"/>

  <xsl:variable name="params" select="document(concat($output,'.params.xml'),/)/smtb:params/smtb:param"/>

  <!-- find all f:rproduct from f:reaction -->
  <xsl:key name="RPRODUCT" match="f:rproduct" use="@id"/>
  <xsl:key name="REDUCT" match="f:reduct" use="@id"/>

  <xsl:key name="SOURCES" match="f:group/smtb:measurement/smtb:cumomer-contribution" use="concat(@pool,'_',@subscript)"/>
  <!--<xsl:key name="SINKS" match="f:pool/smtb:input/smtb:cumomer-contribution" use="concat(../../@id,'_',@subscript)"/>-->
  <xsl:key name="CUMOMER" match="smtb:cumomer" use="@id"/>

  <xsl:strip-space elements="*"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="f:fluxml">
    <carbon-labeling-system>
      <xsl:apply-templates/>
    </carbon-labeling-system>
  </xsl:template>

  <xsl:template match="f:reactionnetwork">
    <xsl:variable name="nX" select="count(//smtb:cumomer)"/>
		<xsl:if test="$verb_level&gt;0">
      <xsl:message>
        <xsl:value-of select="concat('Network simplification, initial number of cumomers : ',count(//smtb:cumomer[../@type='intermediate']))"/>
      </xsl:message>
    </xsl:if>
    <optimize xmlns="http://www.utc.fr/sysmetab">
      <!--<single-matrix-open id="sinks" cols="3"/>-->
      <single-matrix-open id="sources" cols="3"/>
      <single-matrix-open id="adj" cols="3"/>
      <xsl:apply-templates select="//smtb:cumomer"/>
  		<single-matrix-close id="sources" where="disk"/>
  		<single-matrix-close id="adj" where="disk"/>
      <!--<single-matrix-close id="sinks" where="disk"/>-->
    </optimize>
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>exec</ci>
      </fn>
      <string xmlns="http://www.utc.fr/sysmetab">
        <xsl:value-of select="concat(exslt:node-set($params)[@name='sysmetab'],'/SCI/macros/s_sparse.sci')"/>
      </string>
      <cn>-1</cn>
    </apply>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>exec</ci>
      </fn>
      <string xmlns="http://www.utc.fr/sysmetab">
        <xsl:value-of select="concat(exslt:node-set($params)[@name='sysmetab'],'/SCI/macros/tracing.sci')"/>
      </string>
      <cn>-1</cn>
    </apply>
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>path</ci>
      <apply>
        <fn>
          <ci>get_absolute_file_path</ci>
        </fn>
        <string xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="concat($output,'.trace.sci')"/>
        </string>
      </apply>
    </apply>
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <fn>
        <ci>chdir</ci>
      </fn>
      <ci xmlns="http://www.utc.fr/sysmetab">path</ci>
    </apply>
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>err</ci>
      <apply>
        <fn>
          <ci>tracing</ci>
        </fn>
        <string xmlns="http://www.utc.fr/sysmetab">
          <xsl:value-of select="$output"/>
        </string>
        <cn>
          <xsl:value-of select="$nX"/>
        </cn>
      </apply>
    </apply>
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>err</ci>
      <apply>
        <fn><ci>execstr</ci></fn>
        <string xmlns="http://www.utc.fr/sysmetab">quit</string>
        <string xmlns="http://www.utc.fr/sysmetab">errcatch</string>
      </apply>
    </apply>
    
  </xsl:template>

  <xsl:template match="smtb:cumomer">
    <xsl:variable name="number" select="position()"/>
    <xsl:choose>
      <xsl:when test="key('SOURCES',@id)">
        <single-matrix-row id="sources" xmlns="http://www.utc.fr/sysmetab">
          <cn><xsl:value-of select="$number"/></cn>
          <cn>1</cn>
          <cn>1</cn>
        </single-matrix-row>
      </xsl:when>
        <!--<xsl:when test="key('SINKS',@id)">
          <single-matrix-row id="sinks" xmlns="http://www.utc.fr/sysmetab">
            <cn><xsl:value-of select="$number"/></cn>
            <cn>1</cn>
            <cn>1</cn>
          </single-matrix-row>
        </xsl:when>-->
      </xsl:choose>
      <xsl:if test="../@type='intermediate'">
      <xsl:variable name="name" select="../@id"/>
      <xsl:variable name="carbons">
        <xsl:copy-of select="smtb:carbon"/>
      </xsl:variable>
      <!-- Find cumomers (other than itself) in the right hand side of balance equation by looping on all reactions
           where the cumomer pool is a product -->
      <xsl:for-each select="key('RPRODUCT',../@id) | key('REDUCT',../@id)[not(../@bidirectional)]"> <!-- context node is a reduct or a rproduct -->
        <xsl:variable name="id" select="@id"/>
        <xsl:variable name="occurrence" select="count(preceding-sibling::*[name()=name(current())])+1"/>
        <!-- Find all occurrences (in educts) of product marked carbons by making a loop on all educts 
             of the current reaction : -->
        <xsl:for-each select="../*[(name() != name(current())) and smtb:carbon[(@id=$id) and (@occurrence=$occurrence)]]">
          <xsl:variable name="matchedCarbons">
            <xsl:for-each select="smtb:carbon[(@id=$id) and (@occurrence=$occurrence)]">
              <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                <token>
                  <xsl:value-of select="@position"/>
                </token>
              </xsl:if>
            </xsl:for-each>
          </xsl:variable>
          <xsl:if test="string-length($matchedCarbons)&gt;0">
            <single-matrix-row id="adj" xmlns="http://www.utc.fr/sysmetab">
              <cn>
                <xsl:value-of select="key('CUMOMER',concat(@id,'_',sum(exslt:node-set($matchedCarbons)/token)))/@number"/>
              </cn>
              <cn><xsl:value-of select="$number"/></cn>
              <cn>1</cn>
            </single-matrix-row>
          </xsl:if>
        </xsl:for-each>
      </xsl:for-each>
    </xsl:if>
  </xsl:template>

  <xsl:template match="node()"/>

</xsl:stylesheet>