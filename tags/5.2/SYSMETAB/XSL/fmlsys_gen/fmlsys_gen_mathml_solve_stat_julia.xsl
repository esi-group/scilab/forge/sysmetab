<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
-->

<!-- Cette feuille de style a pour but de générer différentes fonctions spécifiques
  d'un modèle donné sous forme FML -->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl smtb math m f exslt">
     
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <!-- Output file name -->
  <xsl:param name="output"/>
  <xsl:param name="language">scilab</xsl:param>

  <xsl:variable name="params" select="document(concat($output,'.params.xml'),/)/smtb:params/smtb:param"/>

  <!-- Mathml generation of the code, this templates is common between stationnary case or dynamical one -->
  <xsl:include href="fml_common_gen_mathml.xsl"/>

  <!-- find all f:datum -->
  <xsl:key name="DATUM" match="f:datum" use="@id"/>

  <xsl:strip-space elements="*"/>
  
  <xsl:template match="/">
    <carbon-labeling-system>
      <xsl:apply-templates/>
    </carbon-labeling-system>
  </xsl:template>

  <xsl:template match="f:fluxml">
    <!-- Building input matrices, allowing to obtain the contribution over the input cumomers and isotopomers 
         (in fml_common_gen_mathml.xsl) -->
    <xsl:call-template name="construct_matrix_D"/>
    <!-- Computation function of C and b for the contraints on the fluxes Cv=b (in fml_common_gen_mathml.xsl) -->
    <xsl:call-template name="fluxSubspace"/>
    <xsl:call-template name="construct_matrix_E"/>
    <!-- Fluxes name matrices, Fluxes ids matrices (in fml_common_gen_mathml.xsl) -->
    <xsl:call-template name="names_and_ids_fluxes"/>
    <!-- Script for Direct computation of cumomers. This script is running in XMLlab application, once a flux 
         is changed or once we exit the optimization with new values of fluxes.
         path=get_absolute_file_path("exslt:node-set($params)[@name='file']_direct.sce"); 
         err=chdir(path);
         err=exec("exslt:node-set($params)[@name='file'].sci");
    -->
    <script href="{exslt:node-set($params)[@name='output']}_direct.jl" xmlns="http://www.utc.fr/sysmetab">

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <fn>
            <ci>include</ci>
          </fn>
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(exslt:node-set($params)[@name='sysmetab'],'/JL/sysmetab_lib.jl')"/>
          </string>
      </apply>

      <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <fn>
            <ci>include</ci>
          </fn>
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(exslt:node-set($params)[@name='file'],'_solve_state_equation.jl')"/>
          </string>
      </apply>
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <fn>
            <ci>include</ci>
          </fn>
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select="concat(exslt:node-set($params)[@name='output'],'.jl')"/>
          </string>
      </apply>
  
      <comment> Input cumomers, computed from isotopomers of input metabolites.</comment>

      <xsl:for-each select="f:configuration/f:input">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci><xsl:value-of select="concat(@pool,'_input')"/></ci>
          <vector>
            <xsl:for-each select="f:label">
              <cn>
                <xsl:value-of select="."/>
              </cn>
            </xsl:for-each>
          </vector>
        </apply>
      </xsl:for-each>
      <xsl:for-each select="f:reactionnetwork/smtb:listOfInputCumomers/smtb:listOfCumomers">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>
            <xsl:value-of select="concat('x',@weight,'_input')"/>
          </ci>
          <apply>
            <times/>
            <ci>
              <xsl:value-of select="concat('D',@weight)"/>
            </ci>
            <vector>
              <xsl:for-each select="../../f:metabolitepools/f:pool[smtb:input]">
                <ci>
                  <xsl:value-of select="concat(@id,'_input')"/>
                </ci>
              </xsl:for-each>
            </vector>
          </apply>
        </apply>
      </xsl:for-each>
      
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci>v</ci>
        <apply>
          <fn><ci>rand</ci></fn>
          <cn>
            <xsl:value-of select="count(f:reactionnetwork/f:reaction)"/>
          </cn>
          <cn>1</cn>
      </apply>
    </apply>

          <apply xmlns="http://www.w3.org/1998/Math/MathML">
            <eq/>
            <ci>omega</ci>
            <apply>
              <vector>
                <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group">
                      <cn>1</cn>
                </xsl:for-each>
              </vector>
            </apply>
          </apply>

         <comment>Gradient computation</comment>  

         <apply xmlns="http://www.w3.org/1998/Math/MathML">
           <eq/>
           <ci>solveCumomersAndGrad</ci>
             <xsl:choose>
               <xsl:when test="exslt:node-set($params)[@name='gradient']='adjoint'">
                 <ci>solveCumomersAndGradAdjoint</ci>
               </xsl:when>
               <xsl:otherwise>
                 <ci>solveCumomersAndGradDirect</ci>
               </xsl:otherwise>
             </xsl:choose>
         </apply>

      
    </script>
  </xsl:template>

</xsl:stylesheet>