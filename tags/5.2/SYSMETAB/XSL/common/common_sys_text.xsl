<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Thu Feb  8 09:50:47 CET 2007
    Project :   PIVERT/Metalippro-PL1
-->

<!-- This style sheet have a goal to generate the system equations as a text readable by a human. The output is 
     not designed to a particular software, but we can inspire from the code below in order to write a 
     style sheet designed for example to Maple or other.
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:exslt="http://exslt.org/common"
  xmlns:str="http://exslt.org/strings"
  xmlns:func="http://exslt.org/functions"
extension-element-prefixes="func"
  exclude-result-prefixes="xsl smtb m exslt">

  <xsl:output method="text"  encoding="UTF-8"/>

  <xsl:param name="output"/>

  <xsl:variable name="verb_level">0</xsl:variable>

  <!-- Maximum weight -->

  <xsl:param name="weight">10000</xsl:param>

  <!-- Output language  -->

  <xsl:param name="lang">scilab</xsl:param>

  <xsl:strip-space elements="*"/>

  <!-- Find all <smtb:matrix-assignment>-->
  <xsl:key name="MATRIX-ASSIGNMENTS-BY-ID-ROW-COL" match="smtb:matrix-assignment" use="concat(@id,' ',@row,' ',@col)"/>

  <!-- Quote ' character -->
  <xsl:variable name="quote">'</xsl:variable>

  <!-- Double quote " character -->
  <xsl:variable name="double_quote">"</xsl:variable>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="carbon-labeling-system">
    <xsl:if test="$verb_level&gt;0">
      <xsl:message>Generating <xsl:value-of select="$lang"/>code, please wait...</xsl:message>
    </xsl:if>
    <xsl:apply-templates/>
		<xsl:apply-templates select="//smtb:matrix-close[@prepost='yes']" mode="post"/>
  </xsl:template>

  <xsl:template match="listOfReactions"/>

  <xsl:template match="listOfSpecies | metabolitepools">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="species | pool">
    <xsl:value-of select="concat('// ',@id,' , ',@type,'&#xA;')"/>
    <!-- <xsl:value-of select="concat('// ',@id,' : ',@name,', ',@type,'&#xA;')"/>-->
    <xsl:apply-templates select="equations"/>
  </xsl:template>

  <xsl:template match="equations">
    <xsl:apply-templates select="equation[@weight&lt;=$weight]"/>
  </xsl:template>

  <xsl:template match="equation/m:apply">
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>=</xsl:text>
    <xsl:apply-templates select="*[3]"/>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:plus]">
    <xsl:for-each select="*[position()&gt;1]">
      <xsl:apply-templates select="."/>
      <xsl:choose>
        <xsl:when test="following-sibling::m:apply[m:minus and (count(*)=2)]">
        </xsl:when>
        <xsl:otherwise>
          <xsl:if test="position()&lt;last()">
            <xsl:text>+</xsl:text>
          </xsl:if>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="m:apply[count(m:apply)=count(*)]">
    <xsl:text>(</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:plus and ((preceding-sibling::m:power) or (preceding-sibling::m:times) or (preceding-sibling::m:minus) or (preceding-sibling::m:backslash)) and (count(*)&gt;2)]">
    <xsl:text>(</xsl:text>
    <xsl:for-each select="*[position()&gt;1]">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>+</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:minus and (count(*)=3)]">
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>-</xsl:text>
    <xsl:apply-templates select="*[3]"/>
  </xsl:template>

  <xsl:template match="m:apply[m:minus and (count(*)=2)]">
    <xsl:text>-</xsl:text>
    <xsl:apply-templates select="*[2]"/>
  </xsl:template>

  <xsl:template match="m:apply[m:minus and (count(*)=3) and ((preceding-sibling::m:power) or 
                       (preceding-sibling::m:times) or (preceding-sibling::m:transpose))]">
    <xsl:text>(</xsl:text>
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>-</xsl:text>
    <xsl:apply-templates select="*[3]"/>
    <xsl:text>)</xsl:text>
  </xsl:template>
   
  <xsl:template match="m:apply[m:times]">
    <xsl:for-each select="*[position()&gt;1]">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:choose>
          <xsl:when test="../m:times/@type='array'">
            <xsl:text>.*</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>*</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="m:apply[m:divide]">
    <xsl:for-each select="*[position()&gt;1]">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:choose>
          <xsl:when test="../m:divide/@type='array'">
            <xsl:text>./</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>/</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="m:apply[m:times and ((preceding-sibling::m:power) or (preceding-sibling::m:transpose))]">
    <xsl:text>(</xsl:text>
    <xsl:for-each select="*[position()&gt;1]">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:choose>
          <xsl:when test="../m:times/@type='array'">
            <xsl:text>.*</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>*</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:diff]">
    <xsl:text>d(</xsl:text>
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>)/dt</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:transpose]">
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>'</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:transpose and m:apply]">
    <xsl:text>(</xsl:text>
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>)</xsl:text>
    <xsl:text>'</xsl:text>
  </xsl:template>


  <xsl:template match="m:apply[m:power]">
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>^</xsl:text>
    <xsl:apply-templates select="*[3]"/>
  </xsl:template>

  <xsl:template match="m:apply[m:power[@type='array']]">
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>.^</xsl:text>
    <xsl:apply-templates select="*[3]"/>
  </xsl:template>

  <xsl:template match="m:apply[m:eq]">
    <!-- <xsl:if test="ancestor::smtb:body">
    <xsl:text>  </xsl:text>
    </xsl:if>-->
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>=</xsl:text>
    <xsl:apply-templates select="*[3]"/>
    <xsl:text>;&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:eqplus]">
    <xsl:if test="ancestor::smtb:body">
      <xsl:text></xsl:text>
    </xsl:if>
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>+=</xsl:text>
    <xsl:apply-templates select="*[3]"/>
    <xsl:text>;&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:selector and m:ci[@type='vector']]">
    <xsl:apply-templates select="m:ci[1]"/>
    <xsl:choose>
      <xsl:when test="$lang='scilab'">
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="*[3]"/>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:when test="$lang='C'">
        <xsl:text>[</xsl:text>
        <xsl:choose>
          <xsl:when test="*[3][self::m:cn]">
            <xsl:value-of select="(*[3])-1"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="*[3]"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:text>]</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="m:apply[m:selector and m:ci[@type='matrix']]">
    <xsl:apply-templates select="m:ci[1]"/>
    <xsl:choose>
      <xsl:when test="$lang='scilab'">
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="*[3]"/>
        <xsl:text>,</xsl:text>
        <xsl:apply-templates select="*[4]"/>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:when test="$lang='C'">
        <xsl:text>[</xsl:text>
        <xsl:apply-templates select="m:cn[1]"/>
        <xsl:text>][</xsl:text>
        <xsl:apply-templates select="m:cn[2]"/>
        <xsl:text>]</xsl:text>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="m:apply[m:backslash]">
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>\</xsl:text>
    <xsl:apply-templates select="*[3]"/>
  </xsl:template>

  <xsl:template match="m:apply[m:min]">
    <xsl:text>min(</xsl:text>
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>,</xsl:text>
    <xsl:apply-templates select="*[3]"/>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:max]">
    <xsl:text>max(</xsl:text>
    <xsl:apply-templates select="*[2]"/>
    <xsl:text>,</xsl:text>
    <xsl:apply-templates select="*[3]"/>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="m:apply[m:fn]">
    <xsl:apply-templates select="m:fn/m:ci"/>
    <xsl:text>(</xsl:text>
    <xsl:for-each select="*[not(self::m:fn)]">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>)</xsl:text>
		<xsl:if test="not(ancestor::m:apply or ancestor::smtb:struct)">
	    <xsl:text>&#xA;</xsl:text>
		</xsl:if>
  </xsl:template>

  <xsl:template match="smtb:input/m:list">
    <xsl:text>(</xsl:text>
    <xsl:for-each select="*">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>)</xsl:text>
  </xsl:template>

  <xsl:template match="smtb:output/m:list">
    <xsl:text>[</xsl:text>
    <xsl:for-each select="*">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>]</xsl:text>
  </xsl:template>

  <xsl:template match="m:list">
    <xsl:text>[</xsl:text>
    <xsl:for-each select="*">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:choose>
          <xsl:when test="../@separator">
            <xsl:value-of select="../@separator"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>;</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>]</xsl:text>
  </xsl:template>

  <xsl:template match="m:vector">
    <xsl:text>[</xsl:text>
    <xsl:for-each select="*">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:choose>
          <xsl:when test="../@separator">
            <xsl:value-of select="../@separator"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>;</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>]</xsl:text>
  </xsl:template>

  <xsl:template match="m:matrix">
    <xsl:text>[</xsl:text>
    <xsl:apply-templates/>
    <xsl:text>]</xsl:text>
  </xsl:template>

  <xsl:template match="m:matrixrow">
    <xsl:for-each select="*">
      <xsl:apply-templates select="."/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:if test="position()&lt;last()">
      <xsl:text>&#xA;</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template match="smtb:function">
    <xsl:text>//&#xA;</xsl:text>
    <xsl:text>function </xsl:text>
    <xsl:apply-templates select="smtb:output"/>
    <xsl:text>=</xsl:text>
    <xsl:apply-templates select="m:ci"/>
    <xsl:apply-templates select="smtb:input"/>
    <xsl:text>&#xA;</xsl:text>
    <xsl:apply-templates select="smtb:body"/>
    <xsl:text>//&#xA;</xsl:text>
    <xsl:text>endfunction&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="smtb:output">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="smtb:input">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="smtb:body">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="smtb:comment">
    <!-- <xsl:if test="ancestor::smtb:body">
    <xsl:text>  </xsl:text>
    </xsl:if>-->
    <xsl:choose>
      <xsl:when test="$lang='scilab'">
        <xsl:text>//&#xA;</xsl:text>
        <xsl:value-of select="concat('// ',normalize-space(.),'&#xA;//')"/>
      </xsl:when>
      <xsl:when test="$lang='C'">
        <xsl:text>/* </xsl:text>
        <xsl:apply-templates/>
        <xsl:text>*/</xsl:text>
      </xsl:when>
    </xsl:choose>
    <xsl:text>&#xA;</xsl:text>
  </xsl:template>

  <xsl:template match="smtb:optimize">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="smtb:matrix-open"/>
  <xsl:template match="smtb:single-matrix-open"/>
 
  <xsl:template match="smtb:matrix-assignment"/>
  <xsl:template match="smtb:single-matrix-row"/>

  <xsl:template match="smtb:single-matrix-close">
    <xsl:choose>
      <xsl:when test="@where='disk'">
        <xsl:variable name="file" select="concat($output,'.network/',@id,'.txt')"/>

        <xsl:if test="$verb_level&gt;0">
          <xsl:message><xsl:value-of select="$file"/></xsl:message>
        </xsl:if>
        
        <xsl:document href="{$file}" method="text" encoding="ISO-8859-15">
          <xsl:text> </xsl:text>
          <xsl:for-each select="preceding-sibling::smtb:single-matrix-row[@id=current()/@id]">
            <!--<xsl:sort select="smtb:ci[1]" data-type="number" direction="ascending"/>
            <xsl:sort select="smtb:ci[2]" data-type="number" direction="ascending"/>-->
      	    <xsl:for-each select="*">
      	      <xsl:apply-templates select="."/>
      	      <xsl:if test="position()&lt;last()">
      	        <xsl:text> </xsl:text>
      	      </xsl:if>
      	    </xsl:for-each>
      	    <xsl:if test="position()&lt;last()">
      	      <xsl:text>&#xA;</xsl:text>
      	    </xsl:if>    	
      		</xsl:for-each>		
        </xsl:document>
      </xsl:when>
      <xsl:otherwise>
      	<xsl:value-of select="concat(@id,'=[')"/>		
        <xsl:for-each select="preceding-sibling::smtb:single-matrix-row[@id=current()/@id]">
          <!--<xsl:sort select="smtb:ci[1]" data-type="number" direction="ascending"/>
          <xsl:sort select="smtb:ci[2]" data-type="number" direction="ascending"/>-->
    	    <xsl:for-each select="*">
    	      <xsl:apply-templates select="."/>
    	      <xsl:if test="position()&lt;last()">
    	        <xsl:text> </xsl:text>
    	      </xsl:if>
    	    </xsl:for-each>
    	    <xsl:if test="position()&lt;last()">
    	      <xsl:text>&#xA;</xsl:text>
    	    </xsl:if>    	
    		</xsl:for-each>		
        <xsl:value-of select="concat(']','&#xA;')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="smtb:matrix-close">
    <xsl:if test="$verb_level&gt;0">
      <xsl:message><xsl:value-of select="concat('Assembling matrix ',@id)"/></xsl:message>
    </xsl:if>
    <!-- On collecte les affectations aux éléments d'une matrice donnée, en regroupant
       les affectations multiples à un même terme (i,j), qui sont par convention 
       interprétées comme une affectation d'une somme. Pour cela il faut identifier les
       affectations "uniques" et boucler sur les éventuels multiples. Cela se fait avec
       un système de clés (méthode Muenchienne), qui est beaucoup plus rapide (mais
       gourmand en mémoire) que la méthode "naïve" commentée ci-dessous. -->
    <!-- 
    <xsl:variable name="assignments">
      <xsl:for-each select="preceding-sibling::smtb:matrix-assignment[@id=current()/@id]">
        <xsl:if test="not(following-sibling::smtb:matrix-assignment[(@id=current()/@id) and (@row=current()/@row) and (@col=current()/@col)])">
          <smtb:matrix-assignment id="{@id}" row="{@row}" col="{@col}">
            <m:apply>
              <m:plus/>
              <xsl:copy-of select="*"/>
              <xsl:for-each select="preceding-sibling::smtb:matrix-assignment[(@id=current()/@id) and (@row=current()/@row) and (@col=current()/@col)]">
                <xsl:copy-of select="*"/>
              </xsl:for-each>
            </m:apply>
          </smtb:matrix-assignment>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:for-each select="exslt:node-set($assignments)/smtb:matrix-assignment">
      <xsl:value-of select="concat(@row,',',@col,',')"/>
      <xsl:apply-templates/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>&#xA;</xsl:text>
      </xsl:if>
    </xsl:for-each>
    -->
    <!-- Muenchienne method http://www.jenitennison.com/xslt/grouping/muenchian.html -->
    <xsl:value-of select="concat(@id,'_ijv=[')"/>
    <xsl:for-each select="preceding-sibling::smtb:matrix-assignment[@id=current()/@id]">
      <xsl:value-of select="concat(@row,',',@col,',')"/>
      <xsl:apply-templates/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>&#xA;</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>];&#xA;</xsl:text>
    <xsl:for-each select="preceding-sibling::smtb:matrix-open[@id=current()/@id][1]">
    	<xsl:value-of select="concat(@id,'=s_',@type,'(',@id,'_ijv,',@rows,',',@cols,');&#xA;')"/>
    </xsl:for-each>
  </xsl:template>

  <xsl:template match="smtb:rhs">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="smtb:lhs">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="m:ci">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="m:cn">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="m:infinity">
    <xsl:value-of select="'%inf'"/>
  </xsl:template>
  
  <xsl:template match="smtb:script">
    <xsl:document href="{@href}" method="text" encoding="ISO-8859-15">
      <xsl:apply-templates/>
    </xsl:document>
  </xsl:template>

  <xsl:template match="smtb:string">
    <xsl:value-of select="concat('&quot;',normalize-space(.),'&quot;')"/>
  </xsl:template>

  <xsl:template match="smtb:for">
		<xsl:value-of select="concat('for ',@var,'=',@from,':',@to,'&#xA;')"/>
		<xsl:apply-templates/>
		<xsl:text>end&#xA;</xsl:text>			
  </xsl:template>
  
  <xsl:template match="smtb:struct">
    <xsl:apply-templates select="*[not(self::smtb:field)]"/>
    <xsl:value-of select="concat(smtb:name-or-id(),'=struct(')"/>
    <xsl:for-each select="smtb:field">
      <xsl:value-of select="concat('&quot;',@name,'&quot;',',','[]')"/>
      <xsl:if test="position()&lt;last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>)&#xA;</xsl:text>
    <xsl:apply-templates select="smtb:field"/>
  </xsl:template>

  <xsl:template match="smtb:field[count(*)&gt;1]">
    <xsl:apply-templates select="smtb:struct"/>
    <xsl:value-of select="concat(smtb:name-or-id-parent(),'.',@name,'=list(')"/>
    <xsl:for-each select="*">
      <xsl:choose>
        <xsl:when test="self::smtb:struct">
          <xsl:value-of select="smtb:name-or-id()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="."/>
        </xsl:otherwise>
      </xsl:choose>      
      <xsl:if test="position()&lt;last()">
        <xsl:text>,</xsl:text>
      </xsl:if>
    </xsl:for-each>
    <xsl:text>)&#xA;</xsl:text>
  </xsl:template>

  
  <xsl:template match="smtb:field">
    <xsl:choose>
      <xsl:when test="not(smtb:struct)">
        <xsl:value-of select="concat(smtb:name-or-id-parent(),'.',@name,'=')"/>
        <xsl:apply-templates/>
        <xsl:text>&#xA;</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates/>
        <xsl:value-of select="concat(smtb:name-or-id-parent(),'.',@name,'=',generate-id(*),'&#xA;')"/>
        <xsl:value-of select="concat('clear ',generate-id(*),'&#xA;')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <func:function name="smtb:name-or-id">
    <func:result>
      <xsl:choose>
        <xsl:when test="not(@name)">
          <xsl:value-of select="generate-id()"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@name"/>          
        </xsl:otherwise>
      </xsl:choose>
    </func:result>
  </func:function>

  <func:function name="smtb:name-or-id-parent">
    <func:result>
      <xsl:choose>
        <xsl:when test="not(../@name)">
          <xsl:value-of select="generate-id(..)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="../@name"/>          
        </xsl:otherwise>
      </xsl:choose>
    </func:result>
  </func:function>



</xsl:stylesheet>