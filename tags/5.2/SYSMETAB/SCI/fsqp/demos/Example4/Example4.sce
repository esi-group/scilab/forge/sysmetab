mode(-1)
//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//

demopath = get_absolute_file_path("Example4.sce");
methods=['Scilab Code','C Code','C+Scilab code',..
	 'C +constraints in  Scilab','Using lists']
sz=[450 380]

fsqp_demo_gui(demopath,'example4',sz,methods)
