mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 1 of the fsqp documentation

exdir=get_absolute_file_path("example1.2.sce");
// load the libray of C routines
exec(exdir+'/example1-src/loader.sce');
//state boundary constraint
bigbnd=1.e10; //no bound value
bl=[0;0;0];
bu=[bigbnd;bigbnd;bigbnd];

//fsqp parameters 
nf=1; nineqn=1; nineq=1; neqn=0; neq=1; 
modefsqp=100; miter=500; iprint=0;
ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

 eps=1.e-8; epsneq=0.e0; udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];


x0=[0.1;0.7;0.2];  
mprintf('%s\n',['---------------------------Quadratic problem with fsqp(C code)-----------------------------------------'
		'Initial values'
		'   Value of the starting point:     '+sci2exp(x0,0)])
timer();
[x,info,f,g]=fsqp(x0,ipar,rpar,[bl,bu],'obj32','cntr32','grob32','grcn32')
t=timer();
mprintf('%s\n',['Final values'
		'   Value of the  solution:          '+sci2exp(x,0)
		'   Value of the objective function: '+sci2exp(f,0)
                '   Time:                            '+sci2exp(t,0)
	       ])


