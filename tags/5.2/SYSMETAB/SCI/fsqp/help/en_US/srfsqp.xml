<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="srfsqp" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns4="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>June 1997</pubdate>
  </info>

  <refnamediv>
    <refname>srfsqp</refname>

    <refpurpose>sequentially related FSQP solver</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)
x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,'grob','grcn')
x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],'obj','cntr','grob','grcn', [cd])
x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],'obj','cntr','grobfd','grcnfd', [cd])
[x,inform]=srfsqp(...)
[x,inform,f,g,lambda]=srfsqp(...)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>x0</term>

        <listitem>
          <para>real column vector (initial guess)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>ipar</term>

        <listitem>
          <para>integer vector of size 8.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>srpar</term>

        <listitem>
          <para>integer vector of size 3.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>mesh_pts</term>

        <listitem>
          <para>integer vector of size nfsr+ncsrn+ncsrl</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>rpar</term>

        <listitem>
          <para>real vector of size 4.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>[bl,bu]</term>

        <listitem>
          <para>real matrix with 2 columns and same row dimension as x.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>obj, cntr</term>

        <listitem>
          <para>name of scilab functions or character strings (name of C
          functions) defining the objective function and the constraints
          function.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>grob,grcn</term>

        <listitem>
          <para>name of scilab functions or character strings (name of C
          functions) defining the gradient of objective function and the
          gradient of constraints function.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>cd</term>

        <listitem>
          <para>optional real vector (argument passed to C functions).</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>fsqp interface for SR problems. This interface uses the notations of
    the cfsqp user's guide (see c_manual.ps). The four functions (objective,
    constraints, gradient-of-objective, gradient-of-constraints) can be given
    either as C-functions dynamically linked with Scilab or as Scilab
    functions (mixtures are allowed). If a Scilab function is given as
    argument of fsqp, just use its name (without quotes). If a C function is
    to be called, use a character string to define it. For instance
    fsqp(...,obj,...) invokes fsqp with the Scilab function
    <literal>obj</literal> and <literal>fsqp(...,"obj",...)</literal> invokes
    fsqp with the C function <literal>obj</literal>, linked with Scilab by
    e.g. <literal> link("obj.o", "obj") </literal>.</para>

    <para>The chains "grobfd" and "grcnfd" can be used as C functions to
    compute the gradients of the functions grob and cntr by finite difference.
    Notations:</para>

    <programlisting>
ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
srpar=[nfsr,ncsrl,ncsrn];
rpar=[bigbnd,eps,epsneq,udelta];
   </programlisting>

    <programlisting>
function fj=obj(j,x)
function  gj=cntr(j,x)
function gradf=grob(j,x)
function gradgj=grcn(j,x)
   </programlisting>

    <para>obj(j,x) returns the value of the jth objective, given x. cntr(j,x)
    returns the value of the jth constraint, given x. grob(j,x) (resp.
    grcn(j,x)) is the value at x of the gradient of the function
    x-&gt;obj(j,x) (resp. x-&gt;cntr(j,x)).</para>

    <para>It may be useful to make use of the fsqp C variable x_is_new and to
    evaluate these functions in a vector way. For instance the function cntr
    can be replaced by the following cntr2 function:</para>

    <programlisting>
function cj=cntr2(j,x)
if x_is_new() then
 all_objectives=allobj(x);
 all_constraints=allcntr(x);
 all_gradobjectives=allgrob(x);
 all_gradconstraints=allgrcn(x);
 cj=all_constraints(j);
 set_x_is_new(0);  //Use resume to define global Scilab variables
 [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
 resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
else
  cj=all_constraints(j);
end
   </programlisting>

    <para>Here, the function allcntr(x) returns a vector all_constraints whose
    jth component all_constraints(j) is cntr(j). A typical example comes from
    discretized semi-infinite program. See example5.sci.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">
// see demo 2 second part
 </programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="fsqp">fsqp</link></member>

      <member><link linkend="x_is_new">x_is_new</link></member>

      <member><link linkend="set_x_is_new">set_x_is_new</link></member>
    </simplelist>
  </refsection>
</refentry>
