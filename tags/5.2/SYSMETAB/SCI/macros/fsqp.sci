function oj=_obj(j,x)
    // Computation of the cost function 
    // Note : we compute here all that is necessary to fsqp, namely the function
    // giving the satisfaction of constraints and their gradient respectively.
    // Since in practice we compute simultaneously the cost function and its 
    // gradient, this allows to not make unnecessary computations, using the 
    // function x_is_new() of the fsqp toolbox, which allows to know if the 
    // current x have changed or not.
    if x_is_new()
        [all_obj,all_grobj]=all_stuff(x,'fsqp');
        oj=all_obj;
        set_x_is_new(0);
        [all_obj,all_grobj]=resume(all_obj,all_grobj);
    else
        oj=all_obj;
    end
endfunction

function goj=_grobj(j,x)
    // gradient computation of the cost function
    if x_is_new()
        [all_obj,all_grobj]=all_stuff(x,'fsqp');
        goj=all_grobj;
        set_x_is_new(0);
        [all_obj,all_grobj]=resume(all_obj,all_grobj);
    else
        goj=all_grobj;
    end
endfunction

function cj=_cntr(j,x)
    // computation of the function giving satisfaction to the constraints.
    cj=_C(j,:)*x-_d(j);
endfunction

function gcj=_grcntr(j,x)
    // gradient computation of the function giving satisfaction to the constraints.
    gcj=_C(j,:)';
endfunction




