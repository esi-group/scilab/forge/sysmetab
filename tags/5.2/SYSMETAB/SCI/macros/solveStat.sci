function [X,y,omega,residual,Sypm2_e_label]=solveStat(v,MList,NETWORK,CONF)
    //
    // Solve the state equation cascade
    //
    //
    X=CONF.Xinp;
    cum=NETWORK.cum
    b=NETWORK.b
    for n=1:length(cum)
        X(cum(n))=solveStatWeight(v,X,MList(n).LU_handle,b(n))
    end
    labeling=CONF.measurement.labeling
    //
    // Compute the simulated measurements
    //
    y_unscaled=labeling.Cx*X;
    //
    // Compute optimal scaling factors (simple least squares problem)
    //
    stddev=labeling.stddev
    meas_ind_group=labeling.meas_ind_group
    meas=labeling.meas
    omega=ones(labeling.nb_group,1)
    for i=labeling.auto_scales_ind // for groups with scale="auto"
      ind=meas_ind_group(i)
      yui=y_unscaled(ind)./stddev(ind)
      ymi=meas(ind)./stddev(ind)
      omega(i)=yui\ymi;
    end
    y=omega(labeling.omega_ind_meas).*y_unscaled;
    //
    // Compute the residual
    //
    e_label=y-meas;
    Sypm2_e_label=labeling.stddevpm2.*e_label;
    residual=Sypm2_e_label.*e_label;
endfunction

function [residual,X,y,dy_dq,omega]=solveStatAndDerivative(v,basis,dv_dw,MList,NETWORK,CONF,PARAMS)
    //
    [X,y,omega,residual,Sypm2_e_label]=solveStat(v,MList,NETWORK,CONF)
    labeling=CONF.measurement.labeling
    meas_ind_group=labeling.meas_ind_group
    stddev=labeling.stddev
    stddevpm2=labeling.stddev
    meas=labeling.meas
    y_unscaled=labeling.Cx*X;
    // Compute the state derivatives
    //
    db_dx=NETWORK.db_dx
    dgdv=NETWORK.dgdv
    cum=NETWORK.cum
    [dgdv_nz,db_dx_nz]=forwardStatDerivatives(v,X,dgdv,db_dx)
    dX_dq=zeros(size(X,1),size(basis,2));
    dgdq=full(spset(dgdv.value,dgdv_nz))*dv_dw*basis;
    //
    dX_dq(cum(1),:)=-umf_lusolve(MList(1).LU_handle,dgdq(cum(1),:));
    for i=2:length(cum)
        dX_dq(cum(i),:)=-umf_lusolve(MList(i).LU_handle,dgdq(cum(i),:)+spset(db_dx(i).value,db_dx_nz(i))*dX_dq);
    end
    //
    // Compute the derivative (forward method)
    //
    dy_unscaled_dq=labeling.Cx*dX_dq;
    dy_dq=spdiag(omega(labeling.omega_ind_meas))*dy_unscaled_dq;
    //
    // Finish the computation of dy_dq for observations belonging to groups with scale="auto"
    for i=labeling.auto_scales_ind
      indg=meas_ind_group(i);
      yui=y_unscaled(indg)./stddev(indg);
      domega_i_dq=-((2*y(indg)-meas(indg)).*stddevpm2(indg))'*dy_unscaled_dq(indg,:)/norm(yui)^2;
      dy_dq(indg,:)=dy_dq(indg,:)+y_unscaled(indg)*domega_i_dq
    end
    //
endfunction 

function [residual,gradq]=solveStatAndGradAdjoint(v,basis,dv_dw,MList,NETWORK,CONF,PARAMS)
    //
    [X,y,omega,residual,Sypm2_e_label]=solveStat(v,MList,NETWORK,CONF)
    //
    // Compute the state derivatives
    //
    db_dx_t=NETWORK.db_dx_t
    dgdvt=NETWORK.dgdvt
    [dgdvt_nz,db_dxt_nz]=adjointStatDerivatives(v,X,dgdvt,db_dx_t)
    //
    Ct_Omega_Sypm2_e_label=CONF.measurement.labeling.Cxt*(omega(CONF.measurement.labeling.omega_ind_meas).*Sypm2_e_label)
    //
    p=zeros(X)
    cum=NETWORK.cum
    ncum=length(cum)
    p(cum(ncum))=umf_lusolve(MList(ncum).LU_handle,Ct_Omega_Sypm2_e_label(cum(ncum)),"A''x=b")
    for i=ncum-1:-1:1
        p(cum(i))=umf_lusolve(MList(i).LU_handle,Ct_Omega_Sypm2_e_label(cum(i))-spset(db_dx_t(i).value,db_dxt_nz(i))*p,"A''x=b")
    end
    //
    // Compute the gradient (adjoint method)
    //
    gradq=-2*(dv_dw*basis)'*spset(dgdvt.value,dgdvt_nz)*p
    //
endfunction

function [dgdv_nz,db_dx_nz]=forwardStatDerivatives(v,X,dgdv,db_dx)
    //
    // Derivatives needed for the forward computation of gradient
    //
    dgdv_nz=dgdv.t*(X(dgdv.m1).*X(dgdv.m2));
    db_dx_nz=list();
    for i=2:length(db_dx)
        db_dx_nz(i)=db_dx(i).t*(v(db_dx(i).m2).*X(db_dx(i).m1));
    end
    //
endfunction

function [dgdvt_nz,db_dxt_nz]=adjointStatDerivatives(v,X,dgdvt,db_dxt)
    //
    // Derivatives needed for the adjoint computation of gradient
    //
    dgdvt_nz=dgdvt.t*(X(dgdvt.m1).*X(dgdvt.m2));
    db_dxt_nz=list();
    for i=1:length(db_dxt)
        db_dxt_nz(i)=db_dxt(i).t*(v(db_dxt(i).m2).*X(db_dxt(i).m1));
    end
    //
endfunction

function Xn=solveStatWeight(v,X,LU_handle,bn)
  // Weight n cumomers
  Xn=umf_lusolve(LU_handle,-full(spset(bn.value,bn.t*(X(bn.m1).*X(bn.m2).*v(bn.m3)))));
endfunction

function MList=factorizeStateMatrices(v,M)
  MList=list()
  for n=1:length(M)
      MList(n)=struct('LU_handle',umf_lufact(sparse(M(n).ij,M(n).t*v(M(n).m1),M(n).size))) 
  end
endfunction

function freeStateMatrices(MList)
  for n=1:length(MList)
      umf_ludel(MList(n).LU_handle)
  end
endfunction

