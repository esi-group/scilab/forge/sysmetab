function spm=spdiag(v)
    // return the components of a column array in a diagonal sparse matrix
    spm=diag(sparse(v));
endfunction
