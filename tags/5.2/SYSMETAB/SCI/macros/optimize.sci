function [q,f,info,mess]=optimize(MODEL,PARAMS)

    // Monte-Carlo method 

    function conf=resample(CONF,RES,itask)
      rand(itask,1) // otherwise all samples will be the same for each task
      conf=CONF
      if itask>1 // leave first sample untouched
        conf.ymeas=max(RES.y+rand(CONF.ymeas,'normal')./sqrt(CONF.Sypm2),0)
        if ~isempty(conf.wmeas)
          conf.wmeas=max(RES.flux_meas+rand(CONF.wmeas,'normal')./sqrt(CONF.Svpm2),0)
        end
      end
    endfunction

    function [_q,_f,_info,_iter,_time]=optimize_mc(itask)
        CONFMC=list()
        for iconf=1:length(CONF)
          CONFMC(iconf)=resample(CONF(iconf),RES(iconf),itask)
        end
        [_q,_f,_info,_iter,_time]=optimize_single(CONFMC,algorithm,qstart,qmin,qmax,...
        C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,0);
        printf("task=%4d, info=%2d, iterations=%5d, residual=%8g\n",itask,_info,_iter,_f)         
        unix(':') // to circumvent OSX parallel_run bug
    endfunction

    // multistart

    function [_q,_f,_info,_iter,_time]=optimize_multi(i)
        [_q,_f,_info,_iter,_time]=optimize_single(CONF,algorithm,qstarttab(:,i),qmin,qmax,...
            C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,0);
        printf("task=%4d, info=%2d, iterations=%5d, residual=%8g\n",i,_info,_iter,_f)
        unix(':') // to circumvent OSX parallel_run bug
    endfunction

    time=0;
    eps_reg=max(1e-14,eps_reg) // safety reg (helps sometimes)

    if exists('task','local')
    
      params=init_param('dynamic_scheduling',1,'nb_workers',nb_cores)

      if task=='mc'    

        w=W*qstart+w0;
        v=Phi(w,eps_phi);
      
        MList=factorizeStateMatrices(v,M,cum);
      
        for iconf=1:length(CONF)
          [RES(iconf).label_error,RES(iconf).X,RES(iconf).y]=solveStat(v,MList,b,cum,CONF(iconf));
          RES(iconf).flux_meas=CONF(iconf).E*w;
        end
      
        freeStateMatrices(MList);

        printf("\n%d tasks started, please wait.\n\n",ntasks); 

        tic
        [q,f,info,iter,time]=parallel_run(1:ntasks+1,optimize_mc,[length(qstart) 1 1 1 1]',params);	
        time=toc()
      
        k=find(info==0 | info==3 | info==8); // keep semi-successfull optimization runs: 0=OK, 3=max. iter. reached, 8=should not be present (because of specific tolX handling)

        q=q(:,k);f=f(k);info=info(k);
        qt=quart(f);
        printf("\nResidual min=%f, q1=%f, median=%f, mean=%f, q3=%f, max=%f, std=%f\n",min(f),qt(1),qt(2),mean(f),qt(3),max(f),stdev(f))
    
      elseif task=="multi"
          w=convexSampler(C,d,me,ntasks,wadm);
          qstarttab=w(ff,:);

          printf("\n%d tasks started, please wait\n\n",ntasks)

          [q,f,info,iter,time]=parallel_run(1:ntasks,optimize_multi,[length(qstart) 1 1 1 1]',params);
          
          k=find(info==0 | info==3 | info==8); // keep semi-successfull optimization runs: 0=OK, 3=max. iter. reached, 8=should not be present (because of specific tolX handling)

          if ~isempty(k)
            q=q(:,k);f=f(k);info=info(k);iter=iter(k);time=time(k);
            printf("\nResidual best=%f, mean=%f, median=%f, std=%f\n",min(f),mean(f),median(f),stdev(f))
            [bc,bk]=min(f);
            q=q(:,bk);
            f=f(bk);
            info=info(bk);
            iter=iter(bk);
            time=time(bk);
          else // in order to have a more detailed error message, since string output is complicated with parallel_run... 
            [q,f,info,iter,time,mess]=optimize_single(algorithm,qstart,qmin,qmax,C,d,me,eps_x,eps_grad,eps_reg,eps_phi,miter,0);
          end
          // write best free fluxes set in original file ?
      end   
  else
    [q,f,info,iter,time,mess]=optimize_single(MODEL,PARAMS);
  end

  mess.iter=iter(1);
  mess.time=time(1);
  infoStrings=['Normal termination'
  'Cannot find a feasible initial guess'
  'Cannot find a feasible initial guess'
  'Maximum number of iteration has been reached'
  'Step size smaller than machine precision'
  'Failure in constructing d0'
  'Failure in constructing d1'
  ''
  'New iterate is equal to previous iterate to machine precision'];
  if info($)==-1
    mess.info="Error during optimization"
  else
      mess.info=infoStrings(info($)+1);
  end
endfunction

function [freeFlux,f,info,iter,time,mess]=optimize_single(_MODEL,_PARAMS)
    // w0=flux.values verify the equality constrains (CS) and the inequality
    // CS that why we reduce our problem by neglecting equality CS and working
    // with only inequality CS. In the other hand, we remove all evident 
    // inequality and zeros line of C(me+1:$,:)*W from inequality CS. 
    
    global lastobj iteration lastFreeFlux message userstop

    if _PARAMS.optimize_method=='fsqp'
    
        _C=_MODEL.subspace.flux.free.C
        _d=_MODEL.subspace.flux.free.d
        nf=1; // is the number of cost function
        nineq=size(_C,1) // is the number of nonlinear inequalities
        nineqn=0; // is the number of nonlinear inequalities
        neqn=0; // is the number of nonlinear equalities
        neq=0; // is the number of linear equalities
        modefsqp=110; // see documentation of fsqp (110 is max4 option)
        ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,4*_PARAMS.max_iter,0];
        bigbnd=1.e10; // play the role of infinity
        epsneq=0.e0; // Maximum violation of nonlinear equality constraints allowed 
                     // by the user at an optimal point
        udelta=0.e0; // The perturbation size the user suggests to use in 
                     // approximating gradients by finite difference
        rpar=[bigbnd,max(_PARAMS.eps_grad,1e-15),epsneq,udelta];
        // the input _obj,_cntr,_grobj and _grcntr must be defined as a function
        // inform is a parameter indicating the status of the execution of fsqp
        // f is a value of the cost function at q at the end of the execution
        // lambda are the values of the Lagrange multipliers at q 
        // at the end of execution
        // g are the values of all constraints at q at the end of execution
    
        f=0;
        lastobj=%inf
        iteration=0
        userstop=-1
        lastq=[]
        tic();
        [freeFlux,info,f,g]=fsqp(_MODEL.subspace.flux.free.value,ipar,rpar,_MODEL.subspace.flux.free.bounds,_obj,_cntr,_grobj,_grcntr);
        if userstop>=0
          info=userstop
        end
        time=toc();
    end

    iter=iteration
    mess=message
    
    if ~isempty(message)
      info=-1
      f=%nan
    else
      f=f-_PARAMS.eps_reg*norm(freeFlux)^2;
    end

endfunction

function [value,gradient]=all_stuff(freeFlux,_algorithm)

    global lastobj iteration lastFreeFlux message userstop
        
    // We build in the function all cost function and its gradient
    
    // Computation of a net/xch flux vector w=W*q+w0 using free fluxes.

    basis=_MODEL.subspace.flux.basis
    netXchFlux=basis*freeFlux+_MODEL.subspace.flux.origin
    [fwdRevFlux,dv_dw]=Phi(netXchFlux,_PARAMS.eps_phi);
    
    MList=factorizeStateMatrices(fwdRevFlux,_MODEL.network.M);
    gradient=2*_PARAMS.eps_reg*freeFlux;
    value=_PARAMS.eps_reg*norm(freeFlux)^2;
    
    for i=1:prod(size(_MODEL.configuration)) // loop on (stationary) configurations
      _CONF=_MODEL.configuration(i)
      measurement=_CONF.measurement
      // Computation of flux observation residual
      e_flux=measurement.flux.E*netXchFlux-measurement.flux.meas;
      flux_error=measurement.flux.stddevpm2.*e_flux.^2;
		  // Solve the state equation and compute the label observation residual and gradient
     try
       [residual,gradq]=solveStatAndGradAdjoint(fwdRevFlux,_MODEL.subspace.flux.basis,dv_dw,MList,_MODEL.network,_CONF,_PARAMS)
    		// Overall cost and gradient final computation
        value=value+sum(residual)+sum(flux_error);
        gradient=gradient+gradq+2*(basis'*measurement.flux.E'*(measurement.flux.stddevpm2.*e_flux));  
      catch // only way of properly stopping fsqp...
         value=%nan;
         gradient=%nan+zeros(freeFlux)
         [str,n,line,func]=lasterror();
         errclear(0);
         message=struct('str',str,'n',n,'line',line,'func',func);
         break
       end
      
    end // loop on configurations

    freeStateMatrices(MList);

		// Iteration report and stopping criterion

    if value<=lastobj
      lastobj=value;
      if 1>0 & _algorithm=='fsqp' & lastFreeFlux<>[]
          printf("iter=%5d, residual=%14.8e, errx=%14.8e\n",iteration,value-_PARAMS.eps_reg*norm(freeFlux)^2,norm(freeFlux-lastFreeFlux));
      end
      if iteration>2
        if norm(freeFlux-lastFreeFlux)<_PARAMS.eps_x
          gradient=%nan+zeros(freeFlux)
          userstop=0  
        elseif iteration>=_PARAMS.max_iter
          gradient=%nan+zeros(freeFlux)
          userstop=3          
        end
      end
      iteration=iteration+1;
    end

    lastFreeFlux=freeFlux     
      
endfunction
