function fluxSubspace=computeFluxSubSpace(model,configuration)
  model=model
  C=model.constraints.flux.C  
  d=model.constraints.flux.d
  me=model.constraints.flux.me
  Ceq=C(1:me,:)
  deq=d(1:me)
  // consolidate free variables across configurations
  free=[]
  freeValues=[]
  for i=prod(size(configuration))
    conf=configuration(i)
    if isfield(conf,"simulation")
      free=[free;conf.simulation.variables.flux.index]
      freeValues=[freeValues;conf.simulation.variables.flux.value]
    end
  end
  // constraints should also be consolidated across configurations
  n=size(Ceq,2)
  n1=rank(Ceq)
  if ~isempty(free)
    [free,k]=unique(free) // keep only unique freefluxes (across mutiple configurations)
    freeValues=freeValues(k)
    others=(1:n)
    others(free)=[]
    p=[others(:);free(:)]
    if rank(Ceq(:,others))<>n1
      disp('The choice of free fluxes is incoherent, falling back to automatic freefluxes determination')
      free=[] // freeValues are kept, if it can help...
    end        
  end
  if isempty(free)        
    // We use QR factorisation of A : A*P=QR with P of size (n,n), R of size (n1,n)
    // and Q of size (n1,n1). We use here the fact that the permutation P ensures
    // that the principal submatrix (n1,n1) of A*P has full rank if rank(A)=n1.
    [Q,R,P]=qr(Ceq)
    [p,j]=find(P)
    free=p(n1+1:$)
    others=p(1:n1)
  end
  // free/dependent/constrained tag
  // should be (for better conditionning): basis=kernel(Ceq), origin=Ceq\deq
  basis=zeros(n,n-n1)
  basis(p,:)=[-Ceq(:,others)\Ceq(:,free);eye(n-n1,n-n1)]  
  origin=zeros(n-n1,1)
  origin(p)=[Ceq(:,others)\deq;zeros(n-n1,1)]
  
  ftype=string(zeros(n,1))
  ftype(:)='d'
  ftype(free)='f'
  ftype(find(clean(sum(abs(basis),'c'))==0))='c'
  // test overall admissibility (+inequality constraints) with quadratic programs
  fluxMat=[]
  fluxMeas=[]
  for i=prod(size(configuration)) // consolidate flux measurment across configurations
    conf=configuration(i)
    if isfield(conf,"measurement.flux")
      fluxMat=[fluxMat;conf.measurement.flux.E]
      fluxMeas=[fluxMeas;conf.measurement.flux.meas]
    end
  end
  try // admissibility + closeness to flux measurements
    Q=full(fluxMat'*fluxMat)+1e-6*eye(n,n)
    p=-full(fluxMat'*fluxMeas)+1e-6*rand(n,1,'normal')
    admissibleFlux=qld(Q,p,C,d,[],[],me)
    if ~isempty(freeValues) // projection of freefluxes values (if necessary)
      fluxMat=zeros(n-n1,n)
      fluxMat(:,free)=eye(n-n1,n-n1)
      admissibleFlux=qld(fluxMat'*fluxMat,-fluxMat'*freeValues,C,d,[],[],me)
      residual=norm(fluxMat*admissibleFlux-freeValues)
      if residual>1e-6
        printf("User free fluxes have been projected on admissible domain (residual=%f)\n",residual) 
      end
    end
  catch
     error('The constraints are inconsistent');
  end
  // constraint matrices and bounds in parameter subspace
  Cp=C(me+1:$,:)*basis
  dp=d(me+1:$)-C(me+1:$,:)*origin
  // find non-trival constraints
  ineqConstraints=find(clean(sum(abs(Cp),'col'))~=0)
  Cp=Cp(ineqConstraints,:)
  dp=dp(ineqConstraints)
  inf=-%inf*ones(free(:))
  sup=%inf*ones(free(:))
  // detection of pure bound constraints to be treated as such
  boundConstraints=[]
  for i=1:size(Cp,1)
      j=find(clean(Cp(i,:)))
      if length(j)==1
          boundConstraints=[boundConstraints i]
          if Cp(i,j)<0 then
              inf(j)=max(inf(j),dp(i)/Cp(i,j))
          else
              sup(j)=min(sup(j),dp(i)/Cp(i,j))
          end
      end
  end
  Cp(boundConstraints,:)=[]
  dp(boundConstraints)=[]    
  // final structure
  fluxSubspace.type=ftype
  fluxSubspace.basis=basis
  fluxSubspace.origin=origin    
  fluxSubspace.start=admissibleFlux
  fluxSubspace.free.index=free
  fluxSubspace.free.C=Cp
  fluxSubspace.free.d=dp  
  fluxSubspace.free.bounds=[inf,sup]  
  fluxSubspace.free.value=fluxSubspace.basis\(fluxSubspace.start-fluxSubspace.origin)
endfunction