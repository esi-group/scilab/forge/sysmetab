Auteur  :   St�phane Mottelet
Date    :   Tue Mar 25 10:01:05 CET 2008
Projet  :   SYSMETAB/Carnot


R�pertoire SYSMETAB/CUMOMER

Dossiers :
----------

Ces repertoires contiennent des fichiers CellDesigner d�crivant les 
r�seaux :

Wiechert1 : le r�seau du premier papier de Wiechert
Branching : "le branching network" du 2e papier de Wiechert

Sysmetab_CT_complet : 

le r�seau de Sinorhizobium Meliloti, sur lequel on bosse.

Sysmetab_CT_complet_weight3 : 

le meme, mais aves des observations de cumom�res de poids
sup�rieur, histoire de voire ce que l'on pourrait r�cup�rer en identifiabilit�.

Sysmetab_CT_light : 

le r�seau de Sinorhizobium Meliloti simplifi�, sur lequel il faudrait
voir ce que l'on peut obtenir (il y a moins de flux inconnus).
