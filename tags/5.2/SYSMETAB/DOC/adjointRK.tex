\documentclass[onecolumn,11pt]{article}

% Use the option doublespacing or reviewcopy to obtain double line spacing
% \documentclass[doublespacing]{elsart}

% if you use PostScript Figures in your article
% use the graphics package for simple commands
% \usepackage{graphics}
% or use the graphicx package for more complicated commands
% \usepackage{graphicx}
% or use the epsfig package if you prefer to use the old commands
% \usepackage{epsfig}

% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}
\usepackage[utf8]{inputenc}
\usepackage[french,english]{babel}
\usepackage{amsmath,amsfonts,graphicx,scalefnt,fancyvrb}
\usepackage{color,listings} 
\lstset{language=xml,basicstyle=\small,numberstyle=\tiny,numbers=left,frame=single}
   \voffset=-1in
   \hoffset=-1in
   \topmargin=1.5cm
   \headheight=0cm
   \headsep=0cm
   \setlength{\paperheight}{29.7cm}%
   \setlength{\paperwidth}{21cm}%
   \setlength{\oddsidemargin}{2.5cm}%
   \setlength{\evensidemargin}{2.5cm}%
   \setlength{\marginparsep}{0cm}%
   \setlength{\marginparwidth}{0cm}%
   \setlength{\footskip}{1cm}% 
   \setlength{\textheight}{24cm}%
   \setlength{\textwidth}{16cm}%
   \setlength{\parskip}{0.82ex}%

\selectlanguage{english}

\newcommand{\vect}[1]{\mathbf{#1}}


\def\xmllab{XMLlab}
% The lineno packages adds line numbers. Start line numbering with
% \begin{linenumbers}, end it with \end{linenumbers}. Or switch it on
% for the whole article with \linenumbers.
% \usepackage{lineno}

% \linenumbers
\begin{document}

% Title, authors and addresses
\title{Adjoint RK schemes}

% use the thanksref command within \title, \author or \address for footnotes;
% use the corauthref command within \author for corresponding author footnotes;
% use the ead command for the email address,
% and the form \ead[url] for the home page:
% \title{Title\thanksref{label1}}
% \thanks[label1]{}
\author{Stéphane Mottelet}
% \thanks[label2]{}
% \corauth[cor1]{}
% \address{Address\thanksref{label3}}
% \thanks[label3]{}


% use optional labels to link authors explicitly to addresses:
% \author[label1,label2]{}
% \address[label1]{}
% \address[label2]{}

\maketitle
\section{A first formulation}
\subsection{Adjoint equation}
The simplest and the (numerically) best formulation of a Runge-Kutta scheme is the following:
\begin{equation}
X_{n+1}=X_n+\sum_{i=1}^s d_iZ_{ni},\quad 0\leq n < N-1,
\end{equation}
where the intermediate values $Z_{ni}$ verify 
%
\begin{equation}
MZ_{ni}=h \sum_{i=1}^s a_{ij}f(X_n+Z_{nj}),\quad 1\leq i\leq s,\quad 0\leq n < N-1,
\end{equation}
%
and $X_n+Z_{nj}=Y_{nj}$ in the following. We have originally $d=bA^{-1}$, where $A$, $b$ and $c$ (not used here) constitue the Butcher tableau data. If we consider the particular case where the scheme is stiffly accurate, then $b=\underline{A}_s$ and $d=(0,\dots,0,1)$. 

In the particular case of a SDIRK scheme, we have $a_{ij}=0$ for $j>i$ and $a_{ii}=\gamma$ for all $i$. If we define
$$
G_{ni}= \sum_{i=1}^{i-1} \frac{a_{ij}}{\gamma}(MZ_{nj}-G_{nj}),
$$
the we have the simpler form
$$
MZ_{ni}=h \gamma f(X_n+Z_{ni})+G_{ni},\quad 1\leq i\leq s,\quad 0\leq n < N-1,
$$


We consider the following lagrangian
\begin{equation}
\begin{split}
L(X,\lambda,Z,\phi)=J(X,Z)+\sum_{n=0}^{N-1}\left[\lambda_n^\top\left(X_{n+1}-X_n-\sum_{i=1}^s d_iZ_{ni}\right)+\right.\\
\left.\sum_{i=1}^s \phi_{ni}^\top\left(
MZ_{ni}-h \sum_{j=1}^s a_{ij}f(X_n+Z_{nj}).
\right)\right].
\end{split}
\end{equation}
where the cost function $J$ depends not only on $X$ but also $Z$ because of the eventual use of dense output to approximate the solution at arbitrary values of $t$. We simplify the Lagrangian expression with the notation $S_{ni}=h \sum_{j=1}^s a_{ji}\phi_{nj}$, which gives
$$
L(X,\lambda,Z,\phi)=\dots+\sum_{i=1}^s \phi_{ni}^\top MZ_{ni}-S_{ni}^\top f(X_n+Z_{ni})
$$


On the one hand, we have, for $0<n<N$, 
\begin{align*}
\frac{\partial L}{\partial X_n} &= \frac{\partial J}{\partial X_n} + (\lambda_{n-1}-\lambda_n)^\top-\sum_{i=1}^s S_{ni}^\top\frac{\partial f}{\partial X}(Y_{nj}),\end{align*}
and 
$$
\frac{\partial L}{\partial X_n}=\frac{\partial J}{\partial X_n}+\lambda_{N-1}^\top.
$$
On the other hand, 
%
\begin{align*}
\frac{\partial L}{\partial Z_{ni}}=\frac{\partial J}{\partial Z_{ni}}-\lambda_n^\top d_i+\phi_{ni}^\top M- \left(S_{ni}^\top \right)\frac{\partial f}{\partial X}(Y_{ni}).
\end{align*}
%
Setting $\frac{\partial L}{\partial Z_{ni}}=0$ and $\frac{\partial L}{\partial X_{n}}=0$ we obtain:
%
\begin{align*}
\lambda_{n-1}&=\lambda_{n}-{\cfrac{\partial J}{\partial X_n}}^\top+ \sum_{i=1}^s \cfrac{\partial f}{\partial X}(Y_{ni})^\top S_{ni},\\
&=\lambda_{n}-{\cfrac{\partial J}{\partial X_n}}^\top+\sum_{i=1}^s (M\phi_{ni}-d_i\lambda_n+\frac{\partial J}{\partial Z_{ni}}).
\end{align*}
%
Since the scheme is stiffly accurate we have $\sum_{i}d_i=1$ and we obtain the final adjoint scheme as:
%
\begin{equation}
\lambda_{n-1}=\sum_{i=1}^s \left(M\phi_{ni}+\frac{\partial J}{\partial Z_{ni}}\right)-{\cfrac{\partial J}{\partial X_n}}^\top,
\end{equation}
where
$$
M\phi_{ni}=d_i\lambda_n+\frac{\partial f}{\partial X}(Y_{ni})^\top S_{ni}-\frac{\partial J}{\partial Z_{ni}}\cdot
$$
\subsection{Gradient}
The original ode is given by
$$
M(m)y'=f(t,y;v),
$$
and the derivative of the Lagrangian w.r.t. $v,m$ are given by
$$
\frac{\partial L}{\partial v}=\frac{\partial J}{\partial v}(X,v)-\sum_{n=0}^{N-1} \sum_{i=1}^s S_{ni}^\top \frac{\partial f}{\partial v}(Y_{ni},v)
$$
$$
\frac{\partial L}{\partial m}=\sum_{n=0}^{N-1} \sum_{i=1}^s \frac{\partial}{\partial m}\left(M(m)Z_{ni}\right)^\top \phi_{ni}
$$


\section{alternative formulation}
\subsection{Adjoint equation}
In fact, in the case of a SDIRK scheme, the state formulation can be further improved as
\begin{equation}
X_{n+1}=X_n+\sum_{i=1}^s d_iZ_{ni},\quad 0\leq n < N-1,
\end{equation}
with
%
\begin{equation}
MZ_{ni}=h\gamma f(X_n+Z_{ni})+M\sum_{j=1}^{i-1} \alpha_{ij}Z_{nj},\quad 1\leq i\leq s,\quad 0\leq n < N-1,
\end{equation}
%
where $\alpha=(A-\gamma I)A^{-1}$. 

We consider the following lagrangian
\begin{equation}
\begin{split}
L(X,\lambda,Z,\phi)=J(X)+\sum_{n=0}^{N-1}\left[\lambda_n^\top\left(X_{n+1}-X_n-\sum_{i=1}^s d_iZ_{ni}\right)+\right.\\
\left.\sum_{i=1}^s \phi_{ni}^\top\left(
MZ_{ni}-h\gamma f(X_n+Z_{ni})-M\sum_{j=1}^{i-1} \alpha_{ij}Z_{nj}).
\right)\right].
\end{split}
\end{equation}
We simplify the Lagrangian expression with the notation $R_{ni}=\sum_{j={i+1}}^s \alpha_{ji}\phi_{nj}$, which gives
$$
L(X,\lambda,Z,\phi)=\dots+\sum_{i=1}^s \phi_{ni}^\top \left(MZ_{ni}-h\gamma f(X_n+Z_{ni})\right)-R_{ni}^\top M Z_{ni}
$$

On the one hand, we have, for $0<n<N$, 
\begin{align*}
\frac{\partial L}{\partial X_n} &= \frac{\partial J}{\partial X_n} + (\lambda_{n-1}-\lambda_n)^\top-h\gamma\sum_{i=1}^s \phi_{ni}^\top \partial_y f (Y_{ni}),\end{align*}
and 
$$
\frac{\partial L}{\partial X_n}=\frac{\partial J}{\partial X_n}+\lambda_{N-1}^\top.
$$
On the other hand, 
%
\begin{align*}
\frac{\partial L}{\partial Z_{ni}}=-\lambda_n^\top d_i+\phi_{ni}^\top (M-h\gamma \partial_y f (Y_{ni}))- R_{ni}^\top M.
\end{align*}
%
Setting $\frac{\partial L}{\partial Z_{ni}}=0$ and $\frac{\partial L}{\partial X_{n}}=0$ we obtain:
%
\begin{align*}
\lambda_{n-1}&=\lambda_{n}-{\cfrac{\partial J}{\partial X_n}}^\top+ h\gamma \sum_{i=1}^s \partial X f (Y_{ni})^\top \phi_{ni},\\
&=\lambda_{n}-{\cfrac{\partial J}{\partial X_n}}^\top+\sum_{i=1}^s (M(\phi_{ni}-R_{ni})-d_i\lambda_n).
\end{align*}
%
Since the scheme is stiffly accurate we have $\sum_{i}d_i=1$ and we obtain the final adjoint scheme as:
%
\begin{equation}
\lambda_{n-1}=M\sum_{i=1}^s (\phi_{ni}-R_{ni})-{\cfrac{\partial J}{\partial X_n}}^\top.
\end{equation}
where
$$
(M-h\gamma \partial_y f(Y_{ni}))\phi_{ni}=d_i\lambda_n+M R_{ni}.
$$




\subsection{Gradient}
The derivative of the Lagrangian w.r.t. $v,m$ are given by
$$
\frac{\partial L}{\partial v}=\frac{\partial J}{\partial v}(X,v)-h\gamma\sum_{n=0}^{N-1} \sum_{i=1}^s \phi_{ni}^\top \partial_y f(Y_{ni},v)
$$


\section{Taking into account the model of $J(v)$}
In its continuous formulation the residual is computed (among other terms) from the values of $X$ at $t=\tau_j$, $j=1\dots n_{meas}$. Since a given $\tau_j$ does not necessary match with any value of $t_k$, dense output coefficients of the scheme are used, hence $J(v)$ has the following form
$$
J(v)=\sum_{j=1}^{n_{meas}} \left\Vert r_j(X_{n(j)} + \textstyle\sum_{i=1}^s \alpha_{i,j} Z_{n(j),i})\right\Vert^2,
$$
where the $\alpha_{i,j}$ are derived from the dense output coefficients of the RK scheme. Hence we have
$$
\frac{\partial J}{\partial X_n}=2 \sum_{\{j,\,n(j)=n\}} r_j(.)^\top \frac{\partial r_j}{\partial X},
$$
and 
$$
\frac{\partial J}{\partial Z_{ni}}=2 \sum_{\{j,\,n(j)=n\}}\alpha_{i,j} r_j(.)^\top \frac{\partial r_j}{\partial X},
$$
which modifies the equation for the intermediate values of the adjoint RK scheme :
$$
M\phi_{ni}=d_i\lambda_n+\frac{\partial f}{\partial X}(Y_{ni})^\top S_{ni}-\frac{\partial J}{\partial Z_{ni}}\cdot
$$












\end{document}

