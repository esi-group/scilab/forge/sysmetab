<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Authors :   Stéphane Mottelet, Georges Sadaka and Gil Gaullier
    Date    :   2013/2015
    Project :   PIVERT/Metalippro-PL1-PL2
-->
                                   
<!-- In this style sheet, each measurement is translated into a set of <cumomer-contribution> elements, 
     for example, we have <labelingmeasurement> for the pool F which will be translate to :

     <measurement xmlns="http://www.utc.fr/sysmetab" string="1x" id="lm_group_F_1">
       <cumomer-contribution subscript="2" string="1x" weight="1" sign="1"/>
     </measurement>
     <measurement xmlns="http://www.utc.fr/sysmetab" string="x1" id="lm_group_F_2">
       <cumomer-contribution subscript="1" string="x1" weight="1" sign="1"/>
     </measurement>
     <measurement xmlns="http://www.utc.fr/sysmetab" string="11" id="lm_group_F_3">
       <cumomer-contribution subscript="3" string="11" weight="2" sign="1"/>
     </measurement>
      
     also for each set of measurement of "mass spectrometry" type, the corresponding <cumomer-contribution> 
     are computed. We take into account the mapping of the carbon atoms into xml elements in order to allow a 
     straightforward matching of educt cumomers when assembling the equations, so we obtain for example 
     to the reaction "v1" :

     <reaction id="v1">
       <reduct cfg="IJ" id="A">
         <carbon xmlns="http://www.utc.fr/sysmetab" position="2" destination="2" occurrence="1" id="F"/>
         <carbon xmlns="http://www.utc.fr/sysmetab" position="1" destination="1" occurrence="1" id="F"/>
       </reduct>
       <rproduct cfg="IJ" id="F">
         <carbon xmlns="http://www.utc.fr/sysmetab" position="2" destination="2" occurrence="1" id="A"/>
         <carbon xmlns="http://www.utc.fr/sysmetab" position="1" destination="1" occurrence="1" id="A"/>
       </rproduct>
     </reaction>
    
     At the end, we generate the isotopomer and cumomer list for each pool, where all the definition of the 
     functions used here are called from the style sheet gen_isotopomer_cumomer_list.xsl, thus for example 
     for the pool A, we obtain :

     <pool atoms="2" id="A" type="intermediate">
       <cumomer xmlns="http://www.utc.fr/sysmetab" id="A_1" pool="A" subscript="1" weight="1" pattern="x1">
         <carbon position="1" index="0"/>
       </cumomer>
       <cumomer xmlns="http://www.utc.fr/sysmetab" id="A_2" pool="A" subscript="2" weight="1" pattern="1x">
         <carbon position="2" index="1"/>
       </cumomer>
       <cumomer xmlns="http://www.utc.fr/sysmetab" id="A_3" pool="A" subscript="3" weight="2" pattern="11">
         <carbon position="1" index="0"/>
         <carbon position="2" index="1"/>
       </cumomer>
     </pool>
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:str="http://exslt.org/strings"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:math="http://exslt.org/math"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl str smtb m math f exslt">

  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
  
  <xsl:param name="verb_level">1</xsl:param>
  
  <xsl:strip-space elements="*"/>
  
  <!-- Functions for the generation of the isotopomer and cumomer list for each pool -->
  <xsl:include href="gen_isotopomer_cumomer_list.xsl"/>

  <!-- find all pool -->
  <xsl:key name="POOL" match="f:pool" use="@id"/>


  <!-- find all label input -->
  <xsl:key name="INPUT" match="f:input" use="@pool"/>

  <!-- find all Mass Spectrometry (MS) -->
  <xsl:key name="MS" match="f:labelingmeasurement/f:group/f:textual" use="substring-before(.,'[')"/>

  <!-- find all Label Measurement (LM) from f:configuration -->
  <xsl:key name="LM" match="f:labelingmeasurement/f:group/f:textual" use="substring-before(.,'#')"/>

  <!-- find all f:reduct from f:reaction -->
  <xsl:key name="REDUCT" match="f:reaction/f:reduct" use="@id"/>

  <!-- find all f:rproduct from f:reaction -->
  <xsl:key name="RPRODUCT" match="f:reaction/f:rproduct" use="@id"/>
  
  <!-- find all f:datum with a given group -->
  <xsl:key name="DATUM" match="f:datum" use="@id"/>

  <xsl:template match="/">
    <xsl:if test="$verb_level&gt;0">
      <xsl:message>Network parsing</xsl:message>
    </xsl:if>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="f:labelingmeasurement/f:group">
    <group xmlns="http://www.13cflux.net/fluxml">
      <xsl:copy-of select="@*"/>
      <xsl:attribute name="pos">
        <xsl:value-of select="position()"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="contains(f:textual,'#M')">
          <xsl:attribute name="type">
            <xsl:value-of select="'MS'"/>
          </xsl:attribute>
          <xsl:copy-of select="*"/>
          <!-- For each set of measurement of "mass spectrometry" type, the corresponding <cumomer-contribution>
               are computed. For each mass isotopomer, say [1,..,n]M#p, where n >= p >=0,
               all cumomers of weight p,p+1,..,n have contributions. The coefficient are given by the binomial
               coefficients, up to an alternating sign.
               Here is an example for [1,2,3]M#0,1,2,3 :

               M#0 = 000 = 1*xxx - 1*(xx1 + x1x + 1xx) +1*(x11+1x1+11x) -1*(111)
               M#1 = 001+010+100 = 1*(xx1 + x1x + 1xx) -2*(x11+1x1+11x) +3*(111)
               M#2 = 011+101+110 =                      1*(x11+1x1+11x) -3*(111)
               M#3 = 111 =                                               1*(111) 

               Another example for [1,2,3,4]M#0,1,2,3,4 (the sum of isotopomers is omitted for clarity)

               M#0 = 1*xxxx - 1*(xxx1+xx1x+x1xx+1xxx) +1*(xx11+x1x1+x11x+1xx1+1x1x+11xx) -1*(x111+1x11+11x1+111x) +1*(1111)             
               M#1 =          1*(xxx1+xx1x+x1xx+1xxx) -2*(xx11+x1x1+x11x+1xx1+1x1x+11xx) +3*(x111+1x11+11x1+111x) -4*(1111)
               M#2 =                                   1*(xx11+x1x1+x11x+1xx1+1x1x+11xx) -3*(x111+1x11+11x1+111x)  6*(1111)
               M#3 =                                                                      1*(x111+1x11+11x1+111x) -4*(1111)
               M#4 =                                                                                               1*(1111)

               i.e. for the mass isotopomer M#p the coefficient of cumomers of weight k=p,p+1,...,n is equal to (-1)^(k-p)*C(k,p)

               Remark  : this fact should be proved, moreover, the coefficients remain the same even for incomplete fragments.
          -->
          <xsl:variable name="pool" select="substring-before(f:textual,'[')"/>
          <xsl:variable name="atoms" select="key('POOL',$pool)/@atoms"/>
          <xsl:variable name="fragment" select="substring-before(substring-after(f:textual,'['),']')"/>
          <xsl:variable name="splittedFragment" select="str:split($fragment,',')"/>
          <!-- Warning: fragments are supposed to be composed of consecutive atoms -->
          <xsl:variable name="before" select="str:padding($splittedFragment[1]-1,'x')"/>
          <xsl:variable name="after" select="str:padding(($atoms)-$splittedFragment[last()],'x')"/>
          <!-- for each mass isotopomer -->
          <xsl:for-each select="str:split(substring-after(f:textual,'#M'),',')"> 
            <xsl:variable name="weight" select="."/>
            <measurement string="{concat('[',$fragment,']','#M',.)}" weight="{.}" xmlns="http://www.utc.fr/sysmetab">
              <!-- for each cumomer of n atoms (where n is the length of the fragment) 
                   and weight >=  weight of the mass isotopomer -->
              <xsl:for-each select="exslt:node-set($list)/listOfCumomers[@atoms=count($splittedFragment)]/cumomer[@weight&gt;=current()]">
                <xsl:sort select="@weight" data-type="number" direction="ascending"/>
                  <!-- dump the cumomer contribution. The binomial coefficients are in the "Cnp" global 
                       variable, which is created in the style sheet "gen_isotopomer_cumomer.xsl" -->
                <cumomer-contribution pool="{$pool}" subscript="{math:power(2,string-length($after))*@number}" 
                        string="{concat($before,@pattern,$after)}" weight="{@weight}" 
                        sign="{(math:power((-1),(@weight)-$weight))*(exslt:node-set($Cnp)/c[(@p=$weight) and (@n=current()/@weight)])}" 
                        xmlns="http://www.utc.fr/sysmetab"/>
              </xsl:for-each>
            </measurement>
          </xsl:for-each>

        </xsl:when>

        <xsl:otherwise>
          <xsl:attribute name="type">
            <xsl:value-of select="'LM'"/>
          </xsl:attribute>
          <xsl:copy-of select="*"/>
          <xsl:for-each select="str:split(f:textual,';')">
            <measurement row="{position()}" xmlns="http://www.utc.fr/sysmetab">
              <xsl:for-each select="str:split(.,'+')">
                <xsl:variable name="pattern" select="substring-after(normalize-space(translate(.,'&#10;&#13;','')),'#')"/>
                <xsl:call-template name="disassemble-cumomer">
                  <xsl:with-param name="string" select="$pattern"/>
                  <xsl:with-param name="pool" select="substring-before(normalize-space(translate(.,'&#10;&#13;','')),'#')"/>
                </xsl:call-template>
              </xsl:for-each>
            </measurement>
          </xsl:for-each>
        </xsl:otherwise>

      </xsl:choose>
    </group>
  </xsl:template>
  
  <xsl:template match="f:configuration[@stationary='false']/f:measurement/f:model/f:labelingmeasurement">

    <!-- Determine the union of all measurement times in the non-starionary case -->

  <xsl:variable name="times">      
    <xsl:for-each select="f:group/@times">
      <xsl:copy-of select="str:split(.)"/>          
    </xsl:for-each>              
  </xsl:variable>

  <labelingmeasurement xmlns="http://www.13cflux.net/fluxml">
    <times xmlns="http://www.utc.fr/sysmetab">    
      <xsl:for-each select="exslt:node-set($times)/*[not(.=preceding-sibling::*)]">
        <xsl:sort select="." data-type="number"/>
        <time>
          <xsl:value-of select="."/>          
        </time>
      </xsl:for-each>
    </times>
    <xsl:apply-templates/>
  </labelingmeasurement>

  </xsl:template>


  <xsl:template match="f:pool">
    <pool atoms="{@atoms}" id="{@id}" number="{position()}" xmlns="http://www.13cflux.net/fluxml">
      <!-- add an attribute type for each pool -->
      <xsl:choose>
        <xsl:when test="key('RPRODUCT',@id) and key('REDUCT',@id)">
          <xsl:attribute name="type">intermediate</xsl:attribute>
        </xsl:when>
        <xsl:when test="not (key('RPRODUCT',@id)) and key('REDUCT',@id)">
          <xsl:attribute name="type">input</xsl:attribute>
        </xsl:when>
        <xsl:when test="not(key('REDUCT',@id)) and key('RPRODUCT',@id)"> <!-- useless with fluxml convention -->
          <xsl:attribute name="type">output</xsl:attribute>
        </xsl:when>
      </xsl:choose>
      <xsl:if test="key('INPUT',@id)">
        <xsl:for-each select="key('INPUT',@id)/f:label/@cfg">
          <input string="{.}" xmlns="http://www.utc.fr/sysmetab"/>
        </xsl:for-each>
      </xsl:if>
        <!-- Generation of the pool cumomer list -->
      <xsl:call-template name="iteration-cumomers">
        <xsl:with-param name="nbCumomer" select="(math:power(2,@atoms))-1"/>
        <xsl:with-param name="i" select="'1'"/>
        <xsl:with-param name="carbons">
          <carbon position="1" index="0" xmlns="http://www.utc.fr/sysmetab"/>
        </xsl:with-param>
      </xsl:call-template>
    </pool>
  </xsl:template>

  <!-- These two templates translate the carbon atoms mapping to xml elements in order to 
       allow a straightforward matching of educt cumomers when assembling the equations -->
  <xsl:template match="f:reduct|f:rproduct">
    <xsl:element name="{name()}" namespace="http://www.13cflux.net/fluxml">
      <xsl:apply-templates select="@*"/>
      <xsl:call-template name="iteration-atoms-map"/>
    </xsl:element>
  </xsl:template>
 
  <xsl:template name="iteration-atoms-map">
    <xsl:param name="i" select="'1'"/>
    <xsl:if test="$i&lt;=string-length(@cfg)">
      <xsl:variable name="c" select="substring(@cfg,$i,1)"/>
      <!-- Attention, we shall number the carbons from right to left if we want to have a direct correspondence
           between the numbers of marked pool and the binary number representing the marking. -->
      <carbon position="{math:power(2,string-length(@cfg)-($i))}" xmlns="http://www.utc.fr/sysmetab">
      <!-- Here, we scan the f:rproduct if the calling context was a f:reduct and vice-versa.
           This is done thanks to the following XPATH expression (a little bit tricky ...) : -->
        <xsl:for-each select="../*[name()!=name(current())]">
          <xsl:if test="contains(@cfg,$c)">
            <xsl:attribute name="destination">
              <xsl:value-of select="math:power(2,string-length(substring-after(@cfg,$c)))"/>
            </xsl:attribute>
            <!-- The "occurrence" attribute is used to differentiate the two versions of a pool when the
                 stoichiometry is grater than 1. -->
            <xsl:attribute name="occurrence">
              <xsl:value-of select="count(preceding-sibling::*[name()=name(current())])+1"/>
            </xsl:attribute>
            <xsl:copy-of select="@id"/>
          </xsl:if>
        </xsl:for-each>
      </carbon>
      <xsl:call-template name="iteration-atoms-map">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <!-- To hide the f:annotation section -->
  
  <xsl:template match="f:annotation"/>

</xsl:stylesheet>