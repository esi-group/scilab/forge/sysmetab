// 
// plot the time course of measurements vs. simuations
//

clf
drawlater
id=[1:9]
names=['Suc';'ICit';'PEP';'PGA';'FruBP';'Glc6P';'Fru6P';'Rib5P';'Gnt6P']

for i=1:9
  subplot(3,3,i)
  if i==1
    plot(tmeas,[ymeas(1,:)*0;ymeas(meas_ind_group(id(i)),:)],'o')
    plot(t,[X(1,:)*0;Cx(meas_ind_group(id(i)),:)*X])  
  else
    plot(tmeas,ymeas(meas_ind_group(id(i)),:),'o')
    plot(t,[Cx(meas_ind_group(id(i)),:)*X])  
  end

  a=gca();
  a.data_bounds=[0 10.5 0 1];
  a.tight_limits(1)='on';
//  ti=a.x_ticks
//  ti.locations=0:2:10
//  ti.labels=string(ti.locations)
//  a.x_ticks=ti
  title(names(i),'fontsize',2)

end
drawnow
set(gcf(),'axes_size',800*[1 2/3])
