function %meas_p(m)
    // User friendly display of a measurement vector
    maxlength=max(length(m.names))+2;
    nmeas=size(m.values,'*')
    printf("%"+string(maxlength)+"s : %7s\n\n","isotopomer","value");
    for i=1:nmeas
        printf("%"+string(maxlength)+"s : %7.5f\n",m.names(i),m.values(i));
    end
endfunction
