function [t,ind_meas]=time_discr(tmeas,stepsize)
//
// Compute index of time measurements
//
if stepsize<=0
  stepsize=max(tmeas)/100
end
ind_meas=floor(tmeas./stepsize)+1
t=0:stepsize:(max(tmeas)+stepsize)
//
endfunction