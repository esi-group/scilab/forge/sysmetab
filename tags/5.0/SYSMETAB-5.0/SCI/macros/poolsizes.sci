function [m,minf,msup,mtype]=poolsizes(mmat,nb_pool)
	val=0.8;
	m=rand(nb_pool,1);	
  minf=1e-5*ones(nb_pool,1)
  msup=1e5*ones(nb_pool,1)
  mtype=string(zeros(nb_pool,1))
  mtype(:)="f"
  if ~isempty(mmat)
  	m(mmat(:,1))=mmat(:,2);
    minf(mmat(:,1))=max(1e-5,mmat(:,3))
    msup(mmat(:,1))=min(1e5,mmat(:,4))
    mtype(find(minf==msup))="c"
  end
endfunction