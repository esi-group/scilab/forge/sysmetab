#include <math.h>


void objmad(nparam,j,x,fj,cd)
     int nparam,j;
     double *x,*fj;
     double *cd;
{
  double pi,theta;
  int i;
  pi=cd[0];
  /* pi=3.1415e0; */
  theta=pi*(8.5e0+j*0.5e0)/180.e0;
  *fj=0.e0;
  for (i=0; i<=5; i++)
    *fj=*fj+cos(2.e0*pi*x[i]*sin(theta));
  *fj=2.e0*(*fj+cos(2.e0*pi*3.5e0*sin(theta)))/15.e0+1.e0/15.e0;
  return; 
}

void cnmad(nparam,j,x,gj,cd)
     int nparam,j;
     double *x,*gj;
     double *cd;
{
  double ss;
  ss=cd[1];
  /*   ss=0.425e0; */
  switch (j) 
    {
    case 1:
      *gj=ss-x[0];
      break;
    case 2:
      *gj=ss+x[0]-x[1];
      break;
    case 3:
      *gj=ss+x[1]-x[2];
      break;
    case 4:
      *gj=ss+x[2]-x[3];
      break;
    case 5:
      *gj=ss+x[3]-x[4];
      break;
    case 6:
      *gj=ss+x[4]-x[5];
      break;
    case 7:
      *gj=ss+x[5]-3.5e0;
      break;
    }
  return;
}



