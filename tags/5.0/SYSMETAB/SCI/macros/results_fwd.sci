function [out]=results_fwd(flux,pool,scale,label_meas,label_error,flux_meas,flux_error,pool_meas,pool_error,X,message,filename,outputname,SYSMETAB)

    // This script modify step3.xml file in order to save results obtain 
    // with scilab after optimization and transfer them to a fwd file wich
    // contains all details.
    
    ftypetext.f='free';
    ftypetext.d='dependent';
    ftypetext.c='constraint';
    //
    doc=xmlRead(filename+'.step3.xml');
    // add stoichiometry paragraph which will contain the type and the value of
    // fluxes id.
    nfluxes=size(flux.names,'*')/2;
    fluxml=xmlXPath(doc,'//f:fluxml',['f','http://www.13cflux.net/fluxml']);
    stoich=xmlElement(doc,"stoichiometry");
    doc1 = xmlReadStr("<root xmlns=""http://www.13cflux.net/fwdsim""/>");
    NS=xmlGetNsByHref(doc1.root, "http://www.13cflux.net/fwdsim")
    for i=1:nfluxes
        f=xmlElement(doc,"flux");
        f.attributes.id=part(flux.names(i),1:$-2);
        //
        flux_net= xmlElement(doc,"net");
        xmlSetAttributes(flux_net,['type' ftypetext(ftype(i))
                                   'value' sprintfnumber(flux.values(i))
                                   'stddev' sprintfnumber(flux.sigma(i))] )
        if ftype(i)=='d'
            vect=find(clean(W(i,:)));
            form='';
            if clean(w0(i))<>0
                form=string(w0(i));
            end
            for j=1:length(vect)
                coef=W(i,vect(j));
                coefs='';
                if coef>0 & form<>''
                    coefs='+'
                elseif coef<0
                    coefs='-'
                end
                if clean(abs(coef)-1)
                    coefs=coefs+string(abs(coef))+'*';
                end
                form=form+coefs+part(flux.names(ff(vect(j))),1:$-2)+'.n';
            end
        else
          form=part(flux.names(i),1:$-2)+'.n';;
        end
        flux_net.content=form;
        xmlAppend(f,flux_net);
        flux_xch= xmlElement(doc,"xch");
        xmlSetAttributes(flux_xch,['type' ftypetext(ftype(nfluxes+i))
                                   'value' sprintfnumber(flux.values(nfluxes+i))
                                   'stddev' sprintfnumber(flux.sigma(nfluxes+i))] )

       if ftype(nfluxes+i)=='d'
           vect=find(clean(W(nfluxes+i,:)));
           form='';
           if clean(w0(nfluxes+i))<>0
               form=string(w0(nfluxes+i));
           end
           for j=1:length(vect)
               coef=W(nfluxes+i,vect(j));
               coefs='';
               if coef>0 & form<>''
                   coefs='+'
               elseif coef<0
                   coefs='-'
               end
               if clean(abs(coef)-1)
                   coefs=coefs+string(abs(coef))+'*';
               end
               form=form+coefs+part(flux.names(ff(vect(j))),1:$-2)+'.x';
           end
       else
         form=part(flux.names(nfluxes+i),1:$-2)+'.x';;
       end
       flux_xch.content=form;
        xmlAppend(f,flux_xch);
        xmlAppend(stoich,f);
    end
    xmlAddNs(stoich,NS);
    xmlAppend(fluxml(1),stoich);
    
    if info>=0
    
       if isempty(pool) // dump cumomers only for non-stationary case
      
        // adding value for each smtb:cumomer in smtb:listOfIntermediateCumomers 
      
        listOfIntermediateCumomers=xmlXPath(doc,'//smtb:listOfIntermediateCumomers/smtb:listOfCumomers',...
                                            ['smtb','http://www.utc.fr/sysmetab']);
        number_listOfCumomers=size(listOfIntermediateCumomers,'*');
        for w=1:number_listOfCumomers
            number_cumomer=size(listOfIntermediateCumomers(w).children,'*');
            for i=1:number_cumomer
                value=listOfIntermediateCumomers(w).children(i);
                number=evstr(listOfIntermediateCumomers(w).children(i).attributes.number);
                value.content=sprintfnumber(X(number));
            end
        end
    else // non-stationary case
          listOfPools=xmlXPath(doc,'//f:pool',['f','http://www.13cflux.net/fluxml']);
          number_pools=size(listOfPools,'*');
          for i=1:number_pools
            if listOfPools(i).attributes.type=="intermediate"
              xmlSetAttributes(listOfPools(i),['value',sprintfnumber(pool.values(i))
                                               'stddev' sprintfnumber(pool.sigma(i))] )
           end
          end
        end
      
      // adding scaling value, residual and measurement values for each f:group
    
      labelingmeasurement=xmlXPath(doc,'//f:labelingmeasurement',['f','http://www.13cflux.net/fluxml']);
    
      xmlSetAttributes(labelingmeasurement(1),['residual' sprintfnumber(sum(label_error))]);
            
      groups=xmlXPath(doc,'//f:labelingmeasurement/f:group',['f','http://www.13cflux.net/fluxml']);
      
      for i=1:size(groups,'*') // for each group
          mgroup=groups(i)
          meas_ind=meas_ind_group(i)
          xmlSetAttributes(mgroup,...
          ['scale' sprintfnumber(scale.values(i))
           'norm'  sprintfnumber(sum(label_error(meas_ind,:)))]);
          for i=1:length(meas_ind)
            xmlRemove(mgroup.children(2))
          end

          if isempty(pool) // stationary measurements
            time=xmlElement(doc,"time")
            xmlSetAttributes(time,["value" sprintfnumber(%inf)])
            for j=1:length(meas_ind) // for each value in the group
                value=xmlElement(doc,"value")
                value.content=sprintfnumber(label_meas.values(meas_ind(j)));
                xmlAppend(time,value)
            end            
            xmlAppend(mgroup,time)
          else // non-stationary measurements
          
            times=evstr("["+mgroup.attributes.times+"]") // only for measurement times of group
            for it=1:length(times)
              l=find(tmeas==times(it))
              time=xmlElement(doc,"time")
              xmlSetAttributes(time,["value" sprintfnumber(tmeas(l))])
              for j=1:length(meas_ind) // for each value in the group
                  value=xmlElement(doc,"value")
                  value.content=sprintfnumber(label_meas.values(meas_ind(j),l));
                  xmlAppend(time,value)
              end
              xmlAppend(mgroup,time)
            end
          end
      end
      
    end
      
    fluxmeasurement=xmlXPath(doc,'//f:fluxmeasurement',['f','http://www.13cflux.net/fluxml']);
    xmlSetAttributes(fluxmeasurement(1),['residual' sprintfnumber(sum(flux_error))]);
    netflux=xmlXPath(doc,'//f:fluxmeasurement/f:netflux',['f','http://www.13cflux.net/fluxml']);
    
    for i=1:size(flux_meas,'*')
        xmlSetAttributes(netflux(i),...
        ['norm'  sprintfnumber(flux_error(i))
         'value' sprintfnumber(flux_meas(i))]);
    end
    
    // poolsizes measurements
        
    if ~isempty(pool)
      poolmeasurement=xmlXPath(doc,'//f:poolmeasurement',['f','http://www.13cflux.net/fluxml']);
      xmlSetAttributes(poolmeasurement(1),['residual' sprintfnumber(sum(pool_error))]);
      poolsize=xmlXPath(doc,'//f:poolmeasurement/f:poolsize',['f','http://www.13cflux.net/fluxml']);

      for i=1:size(pool_meas,'*')
          xmlSetAttributes(poolsize(i),...
          ['norm'  sprintfnumber(pool_error(i))
           'value' sprintfnumber(pool_meas(i))]);
      end
    end
      
    xmlWrite(doc,outputname+'.step5.xml')
    xmlDelete(doc)
    unix_g('xsltproc --stringparam output '+outputname+' '+SYSMETAB+'/XSL/fmlsys_gen/fmltofwd.xsl '+outputname+'.step5.xml'+' > '+outputname+'.fwd')
    out=1;
endfunction


function y=sprintfnumber(number)
    if floor(number)==number then
        y=sprintf('%g',number);
    else
        y=sprintf('%.16g',clean(number));
    end
endfunction
