function [flux,pool,scale,label_meas,label_error,flux_meas,flux_error,pool_meas,pool_error,X]=measurements_and_stats(q,m,eps_phi,info)
    
	// This function return the standard deviation of w=W*q+w0
	// The actual number of "free" parameters depend on the saturation of constraints
	// at the optimum.
  
  nff=size(q,1)
  nsim=size(q,2)-1;
  w=W*q(:,1)+w0; 	// get the net/xch flux vector
  flux=tlist(['fluxes','names','values','sigma','quantiles'],flux_ids,w,zeros(w),zeros(w));
  flux_meas=E*w;
  e_flux=flux_meas-wmeas;
  flux_error=Svpm2.*e_flux.^2;
  pool=[];pool_meas=[];pool_error=[];
  label_meas=[];label_error=[];
  scale=[];
  X=[];

  if info>=0

  	// Computation of the fwd/rev flux vector

  	[v,dv_dw]=Phi(w,eps_phi);
    
    if isempty(m) // stationnary configuration
  	// Solve the state equation, then compute the label observation residual and gradient

    	[label_error,X,y,dy_dv,omega]=solveStatAndDerivative(v,Xinp,M,b,cum);
  
      if nsim>0 // in case of Monte Carlo stats
    		flux.sigma=stdev(W*q(:,2:$),'c')       
        values=gsort(W*q(:,2:$)+w0(:,ones(1,nsim)),'c')
        if nsim>40
          flux.quantiles=[values(:,nsim*0.975) median(values,'c') values(:,nsim*0.025)]
        end
    	else

        // Here, we denote by Y the full observation (labeling and fluxes) [y;E*v]
        // The derivative below is made with respect to free parameter q
        
        dYdq=[dy_dv*dv_dw*W;E*dv_dw*W];
        [U,S,V]=svd(dYdq);
        r=rank(dYdq);
    
        // find (locally) non-identifiable subspace
    
        [i1,i2]=find(abs(V(:,r+1:$))>1e-6)
        nonId=unique(i1);
        V=V(:,1:r);
        dYdq=dYdq*V;
        try // inversion can fail because of missing data
          F=dYdq'*diag([Sypm2;Svpm2])*dYdq;
          // Covariance matrix is the inverse of Fisher matrix
          C=inv(F); // Invertible because of the rank checking stuff above
          flux.sigma=sqrt(diag(W*V*C*V'*W'));            
        catch
        end
        flux.sigma(ff(nonId))=%inf; // not exhaustive !
      end
    else // non-stationnary configuration      
      
      [label_error,X,y,dy_dv,dy_dm,omega]=solveNonStatAndDerivative(v,m,Xinp,M,b,cum);
      pool=tlist(['pools','names','values','sigma','quantiles'],[],m(:,1),zeros(m(:,1)));

      if nsim>0 // in case of Monte Carlo stats
        flux.sigma=stdev(W*q(:,2:$),'c')       
        values=gsort(W*q(:,2:$)+w0(:,ones(1,nsim)),'c')
        if nsim>40
          flux.quantiles=[values(:,nsim*0.975) median(values,'c') values(:,nsim*0.025)]
          pool.sigma=stdev(m,'c')
          values=gsort(m,'c')
          pool.quantiles=[values(:,nsim*0.975) median(values,'c') values(:,nsim*0.025)]
        end     
    	else
      
        dy_dq=dy_dv*dv_dw*W;
        
        pool_meas=Em*m
        e_pool=pool_meas-mmeas;
        pool_error=Smpm2.*e_pool.^2;

        dYdqm=[dy_dq dy_dm
               E*dv_dw*W zeros(size(wmeas,1),size(m,1))
               zeros(size(mmeas,1),size(q,1)) full(Em)]
             
             
        [U,S,V]=svd(dYdqm);
        r=rank(dYdqm)
      
        // find (locally) non-identifiable subspace
  
        [i1,i2]=find(abs(V(:,r+1:$))>1e-6)
        nonId=unique(i1);
      
      
        V=V(:,1:r);
        dYdqm=dYdqm*V;
      
        try 
          F=dYdqm'*spdiag([Sypm2(:);Svpm2;Smpm2])*dYdqm; // Sypm2 is also "vectorized" (see yq() and ym() above)
          C=inv(F);
          flux.sigma=sqrt(diag(W*V(1:nff,:)*C*V(1:nff,:)'*W'));            
          pool.sigma=sqrt(diag(V(nff+1:$,:)*C*V(nff+1:$,:)'))
        catch
        end

        flux.sigma(ff(nonId(nonId<=nff)))=%inf
        pool.sigma(nonId(nonId>nff)-nff)=%inf
        pool.sigma(m_type=="c")=0
      
      end 
    end
    
    label_meas=tlist(['meas' 'names' 'values'],measurement_ids,y);
    scale=tlist(['scale','names','values'],scaling_ids,omega,zeros(omega)); 
         
  else
    flux=tlist(['fluxes','names','values','sigma'],flux_ids,w,zeros(w));
	end
  	
endfunction
