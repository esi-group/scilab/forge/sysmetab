function %scale_p(scale)
    // User friendly display of a scale set
    maxlength=max(length(scale.names))+2;
    printf("%"+string(maxlength)+"s : %6s\n\n","group","scale");    
    for i=1:length(scale.values)
        printf("%"+string(maxlength)+"s : %7.4f\n",scale.names(i),scale.values(i));
    end
endfunction
