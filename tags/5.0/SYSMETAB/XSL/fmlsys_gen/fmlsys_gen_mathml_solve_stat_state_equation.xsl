<?xml version="1.0" encoding="ISO-8859-1" ?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   Tue Nov 14 15:00:00 CET 2013
    Project :   PIVERT/Metalippro-PL1
-->

<!-- 
    System resolution for x_weight
-->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:m="http://www.w3.org/1998/Math/MathML"
  xmlns:f="http://www.13cflux.net/fluxml"
  xmlns:exslt="http://exslt.org/common"
  xmlns:str="http://exslt.org/strings"
  exclude-result-prefixes="xsl smtb math m f exslt str">
  
  <!-- Output file name -->
  <xsl:param name="output"/>

  <xsl:param name="verb_level">1</xsl:param>

  <xsl:variable name="params" select="document(concat($output,'.params.xml'),/)/smtb:params/smtb:param"/>

  <!-- find all f:rproduct from f:reaction -->
  <xsl:key name="RPRODUCT" match="f:reaction/f:rproduct" use="@id"/>

  <!-- find all f:reduct from f:reaction -->
  <xsl:key name="REDUCT" match="f:reaction/f:reduct" use="@id"/>

  <!-- find all f:pool -->
  <xsl:key name="POOL" match="f:pool" use="@id"/>

  <!-- find all smtb:cumomer from smtb:listOfInputCumomers -->
  <xsl:key name="ALL_CUMOMER" match="smtb:listOfInputCumomers//smtb:cumomer | smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>

  <!-- find all smtb:cumomer from smtb:listOfInputCumomers -->
  <xsl:key name="INPUT_CUMOMER" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>

  <!-- find all smtb:cumomer from smtb:listOfIntermediateCumomers -->
  <xsl:key name="INTERMEDIATE_CUMOMER" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>

  <!-- find all smtb:cumomer from f:pool -->
  <xsl:key name="CUMOMERS" match="f:pool/smtb:cumomer" use="@id"/>

  <!-- find all f:datum -->
  <xsl:key name="DATUM" match="f:datum" use="@id"/>

  <!-- find all f:group -->
  <xsl:key name="GROUP" match="f:group" use="@id"/>

	<xsl:variable name="maxweight" select="math:max(//smtb:listOfIntermediateCumomers/smtb:listOfCumomers/@weight)"/>
	<xsl:variable name="nX" select="1+count(//smtb:listOfIntermediateCumomers//smtb:cumomer | //smtb:listOfInputCumomers//smtb:cumomer)"/>
	<xsl:variable name="npool" select="count(//f:reactionnetwork/f:metabolitepools/f:pool)"/>
  <xsl:variable name="nY" select="count(//f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group/smtb:measurement)"/>
  <xsl:variable name="nv" select="count(//f:reactionnetwork/f:reaction)"/>

	<!-- sparse storage : 1 (csr) for Scilab and 2 (csc) for Julia -->

  <xsl:param name="storage">1</xsl:param>

  <xsl:template match="/">
    <xsl:if test="$verb_level&gt;0">
      <xsl:message>Generating state equation code</xsl:message>
    </xsl:if>
    <carbon-labeling-system>
      <xsl:apply-templates/>
    </carbon-labeling-system>
  </xsl:template>

  <xsl:template match="f:fluxml">
    
    <xsl:for-each select="str:split('M Mt b cum db_dx db_dxt pool_ind_weight')">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci><xsl:value-of select="."/></ci>
        <apply>
          <fn><ci>s_list</ci></fn>
          <cn>
            <xsl:value-of select="$maxweight"/>
          </cn>
        </apply>
      </apply>
    </xsl:for-each>
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>meas_ind_group</ci>
      <apply>
        <fn><ci>s_list</ci></fn>
        <cn>
          <xsl:value-of select="count(f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group)"/>
        </cn>
      </apply>
    </apply>
  
    <!-- Generate all index tables needed to construct M_i, b_i, dbi_dx, db_dxj^t, dgdv and dgdv^t matrices -->

    <xsl:call-template name="generate_index_tables"/>

    <xsl:call-template name="construct_matrix_C"/>

    <!-- Building index matrix for dy_domega -->

    <xsl:call-template name="groups"/>
    
  </xsl:template>
  
  
  <xsl:template name="generate_index_tables">       
	
    <optimize xmlns="http://www.utc.fr/sysmetab">
    
      <single-matrix-open id="dP_dpool_m" cols="4"/>
      <single-matrix-open id="dP_dpool_t_m" cols="4"/>
       
			<xsl:for-each select="f:reactionnetwork/*/smtb:listOfCumomers/smtb:cumomer">
        <xsl:sort select="key('POOL',@pool)/@number" data-type="number"/>
      	<single-matrix-row id="dP_dpool_m">
					<cn>
						<xsl:value-of select="@number"/>
					</cn>
					<cn>
            <xsl:value-of select="key('POOL',@pool)/@number"/>
					</cn>
          <cn>1</cn>
					<cn>
						<xsl:value-of select="@number"/>
					</cn>
				</single-matrix-row>
      	<single-matrix-row id="dP_dpool_t_m">
					<cn>
            <xsl:value-of select="key('POOL',@pool)/@number"/>
					</cn>
					<cn>
						<xsl:value-of select="@number"/>
					</cn>
          <cn>1</cn>
					<cn>
						<xsl:value-of select="@number"/>
					</cn>
				</single-matrix-row>
      </xsl:for-each>

      <single-matrix-close id="dP_dpool_m" cols="4" where="disk"/>
      <single-matrix-close id="dP_dpool_t_m" cols="4" where="disk"/>
    
    </optimize>

    <optimize xmlns="http://www.utc.fr/sysmetab">

  		<single-matrix-open  id="dgdv_m" cols="5"/>
  		<single-matrix-open  id="dgdvt_m" cols="5"/>

      <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
  			<single-matrix-open id="{concat('db',@weight,'_dx_m')}" cols="4"/>
      	<single-matrix-open id="{concat('db_dx',@weight,'t_m')}" cols="4"/>
  		</xsl:for-each>
	
      <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
        <xsl:variable name="current_weight" select="@weight"/>

  			<single-matrix-open  id="{concat('M',@weight,'_m')}" cols="4"/>				
  			<single-matrix-open  id="{concat('Mt',@weight,'_m')}" cols="4"/>				
  			<single-matrix-open  id="{concat('b',@weight,'_m')}" cols="5"/>

  		  <!-- For each cumomer, we call the template generating various assignments, to the matrix in the 
  		  right hand side of the system whose solution is the cumomer vector of the current weight.
  		  The template "iteration" is in the style sheet fml_common_gen_mathml.xsl -->

  			<xsl:for-each select="smtb:cumomer">			
  				<xsl:call-template name="iteration"/>
  			</xsl:for-each>

  			<single-matrix-close id="{concat('M',@weight,'_m')}" where="disk"/>
  			<single-matrix-close id="{concat('Mt',@weight,'_m')}" where="disk"/>
  			<single-matrix-close id="{concat('b',@weight,'_m')}" where="disk"/>

      </xsl:for-each>
      
  		<single-matrix-close id="dgdv_m" where="disk"/>
  		<single-matrix-close id="dgdvt_m" where="disk"/>

      <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&gt;1]">
        <xsl:variable name="id" select="concat('db',@weight,'_dx')"/>
  			<single-matrix-close id="{concat($id,'_m')}" cols="4" where="disk"/>
  		</xsl:for-each>

      <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;$maxweight]">
        <xsl:variable name="id" select="concat('db_dx',@weight,'t')"/>
  			<single-matrix-close id="{concat($id,'_m')}" cols="4" where="disk"/>
  		</xsl:for-each>

    </optimize>
        
    <!-- index matrix allowing to form (d(P(m)*Z)/dm) as spset(dP_dpool.value,Z(dP_dpool.m1)) -->
	
  	<comment xmlns="http://www.utc.fr/sysmetab"> index matrix allowing to form (d(P(m)*Z)/dm) as spset(dP_dpool.value,Z(dP_dpool.m1))</comment>
        
		<apply xmlns="http://www.w3.org/1998/Math/MathML">
			<eq/>
				<ci>dP_dpool</ci>
        <apply>
				<fn><ci>gather</ci></fn>
        <smtb:string>
          <xsl:value-of select="concat($output,'.network/dP_dpool_m.txt')"/>
        </smtb:string>
        <cn><xsl:value-of select="$nX"/></cn>
        <cn><xsl:value-of select="$npool"/></cn>
				<cn><xsl:value-of select="$storage"/></cn>
			</apply>
		</apply>

    <!-- index matrix allowing to form (d(P(m)*Z)/dm)^t as spset(dP_dpool_t.value,Z(dP_dpool_t.m1)) -->
	
  	<comment xmlns="http://www.utc.fr/sysmetab"> index matrix allowing to form (d(P(m)*Z)/dm)^t as spset(dP_dpool_t.value,Z(dP_dpool_t.m1))</comment>
        
		<apply xmlns="http://www.w3.org/1998/Math/MathML">
			<eq/>
				<ci>dP_dpool_t</ci>
        <apply>
				<fn><ci>gather</ci></fn>
        <smtb:string>
          <xsl:value-of select="concat($output,'.network/dP_dpool_t_m.txt')"/>
        </smtb:string>
        <cn><xsl:value-of select="$npool"/></cn>
        <cn><xsl:value-of select="$nX"/></cn>
				<cn><xsl:value-of select="$storage"/></cn>
			</apply>
		</apply>

 
    <xsl:for-each select="f:reactionnetwork/smtb:listOfIntermediateCumomers/smtb:listOfCumomers">    
      <xsl:variable name="nc" select="number(@last)-number(@first)+1"/>
	
      <comment xmlns="http://www.utc.fr/sysmetab">Weight <xsl:value-of select="@weight"/> cumomers</comment>

    	<comment xmlns="http://www.utc.fr/sysmetab">pool_ind_weight(<xsl:value-of select="@weight"/>) contains the pool numbers of weight <xsl:value-of select="@weight"/> cumomers</comment>
    
  		<apply xmlns="http://www.w3.org/1998/Math/MathML">
  			<eq/>
        <apply>
          <selector/>
					<ci type="vector">pool_ind_weight</ci>
					<cn><xsl:value-of select="@weight"/></cn>
        </apply>
        <vector>
          <xsl:for-each select="smtb:cumomer">
            <cn>
              <xsl:value-of select="key('POOL',@pool)/@number"/>
            </cn>
          </xsl:for-each>
        </vector>
      </apply>

			<apply xmlns="http://www.w3.org/1998/Math/MathML">
				<eq/>
        <apply>
          <selector/>
					<ci type="vector">cum</ci>
					<cn><xsl:value-of select="@weight"/></cn>
        </apply>
				<cn><xsl:value-of select="concat(@first,':',@last)"/></cn>
			</apply>
      
  		<apply xmlns="http://www.w3.org/1998/Math/MathML">
  			<eq/>
        <apply>
          <selector/>
  				<ci type="vector">M</ci>
  				<cn><xsl:value-of select="@weight"/></cn>
        </apply>
  			<apply>
  				<fn><ci>gather</ci></fn>
          <smtb:string>
            <xsl:value-of select="concat($output,'.network/M',@weight,'_m.txt')"/>
          </smtb:string>
          <cn><xsl:value-of select="$nc"/></cn>
          <cn><xsl:value-of select="$nc"/></cn>
  				<cn><xsl:value-of select="$storage"/></cn>
  			</apply>
  		</apply>
    
  		<apply xmlns="http://www.w3.org/1998/Math/MathML">
  			<eq/>
        <apply>
          <selector/>
  				<ci type="vector">Mt</ci>
  				<cn><xsl:value-of select="@weight"/></cn>
        </apply>
  			<apply>
  				<fn><ci>gather</ci></fn>
          <smtb:string>
            <xsl:value-of select="concat($output,'.network/Mt',@weight,'_m.txt')"/>
          </smtb:string>
          <cn><xsl:value-of select="$nc"/></cn>
          <cn><xsl:value-of select="$nc"/></cn>
  				<cn><xsl:value-of select="$storage"/></cn>
  			</apply>
  		</apply>
    
  		<apply xmlns="http://www.w3.org/1998/Math/MathML">
  			<eq/>
        <apply>
          <selector/>
  				<ci type="vector">b</ci>
  				<cn><xsl:value-of select="@weight"/></cn>
        </apply>
          <apply>
  				<fn><ci>gather</ci></fn>
          <smtb:string>
            <xsl:value-of select="concat($output,'.network/b',@weight,'_m.txt')"/>
          </smtb:string>
          <cn><xsl:value-of select="$nc"/></cn>
          <cn>1</cn>
  				<cn><xsl:value-of select="$storage"/></cn>
  			</apply>
  		</apply>
      
      <xsl:if test="@weight&gt;1">
        <xsl:variable name="id" select="concat('db',@weight,'_dx')"/>
    		<apply xmlns="http://www.w3.org/1998/Math/MathML">
    			<eq/>
          <apply>
            <selector/>
  					<ci type="vector">db_dx</ci>
  					<cn><xsl:value-of select="@weight"/></cn>
          </apply>
    			<apply>
    				<fn><ci>gather</ci></fn>
            <smtb:string>
              <xsl:value-of select="concat($output,'.network/',$id,'_m.txt')"/>
            </smtb:string>
            <cn><xsl:value-of select="number(@last)-number(@first)+1"/></cn>
            <cn><xsl:value-of select="$nX"/></cn>
    				<cn><xsl:value-of select="$storage"/></cn>
    			</apply>
        </apply>
      </xsl:if>
      
      <xsl:if test="@weight&lt;$maxweight">
        <xsl:variable name="id" select="concat('db_dx',@weight,'t')"/>
    		<apply xmlns="http://www.w3.org/1998/Math/MathML">
    			<eq/>
          <apply>
            <selector/>
  					<ci type="vector">db_dxt</ci>
  					<cn><xsl:value-of select="@weight"/></cn>
          </apply>
    			<apply>
    				<fn><ci>gather</ci></fn>
            <smtb:string>
              <xsl:value-of select="concat($output,'.network/',$id,'_m.txt')"/>
            </smtb:string>
            <cn><xsl:value-of select="number(@last)-number(@first)+1"/></cn>
            <cn><xsl:value-of select="$nX"/></cn>
    				<cn><xsl:value-of select="$storage"/></cn>
    			</apply>    
        </apply>    
      </xsl:if>
      
    </xsl:for-each>
    
		<comment xmlns="http://www.utc.fr/sysmetab">Derivatives</comment>
    
		<apply xmlns="http://www.w3.org/1998/Math/MathML">
			<eq/>
				<ci>dgdv</ci>
        <apply>
				<fn><ci>gather</ci></fn>
        <smtb:string>
          <xsl:value-of select="concat($output,'.network/dgdv_m.txt')"/>
        </smtb:string>
        <cn><xsl:value-of select="$nX"/></cn>
        <cn><xsl:value-of select="$nv"/></cn>
				<cn><xsl:value-of select="$storage"/></cn>
			</apply>
		</apply>

		<apply xmlns="http://www.w3.org/1998/Math/MathML">
			<eq/>
				<ci>dgdvt</ci>
        <apply>
				<fn><ci>gather</ci></fn>
        <smtb:string>
          <xsl:value-of select="concat($output,'.network/dgdvt_m.txt')"/>
        </smtb:string>
        <cn><xsl:value-of select="$nv"/></cn>
        <cn><xsl:value-of select="$nX"/></cn>
				<cn><xsl:value-of select="$storage"/></cn>
			</apply>
		</apply>
    		
  </xsl:template>


  <xsl:template name="iteration">
    <!-- Beware the context node is an element <smtb:cumomer>.
    The variable "carbons" contains the carbons 13 of the current pool -->
    <xsl:variable name="carbons">
      <xsl:copy-of select="key('CUMOMERS',@id)/smtb:carbon"/>
    </xsl:variable>
    <xsl:variable name="weight" select="../@weight"/>
    <xsl:variable name="position" select="@local-number"/>
    <xsl:variable name="global-number" select="@number"/>
    <!-- influx rule : the most difficult, but also the most interesting, it is here that truly creates 
    supplementary information compared to the simple stoichiometry.
    We loop all the "rproduct" elements that have the current cumomer pool as a product, so in the for-each the
    contextual node is of reaction/rproduct type. -->
    
    <xsl:for-each select="key('RPRODUCT',@pool)">
    
      <!-- Now, We try to find occurrences marked carbons of reactant of the current reaction: it needs work -->
    
      <xsl:variable name="id" select="@id"/>
      <xsl:variable name="reaction" select="../@position"/>
      <xsl:variable name="occurrence" select="count(preceding-sibling::f:rproduct)+1"/>
      
      <xsl:variable name="influx">
        <!-- This is where the serious stuff begins. We loop all the reactants which have carbons atoms which 
        "point" to the pool. -->
        <xsl:for-each select="../f:reduct[smtb:carbon[(@id=$id) and (@occurrence=$occurrence)]]">
          
          <xsl:variable name="somme">
            <!-- So now, we loop all the carbons of reactant. If a carbon of reactant point to a carbon 13 of 
            cumomer of product, we note its number in an element <token/> -->
            <xsl:for-each select="smtb:carbon[(@id=$id) and (@occurrence=$occurrence)]">
              <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                <token>
                  <xsl:value-of select="@position"/>
                </token>
              </xsl:if>
            </xsl:for-each>
            <!-- At the end of this loop, the "somme" variable contains a number, possibly zero, of elements
            <token> which identify without ambiguity the concerned cumomer. -->
          </xsl:variable>
          
          <xsl:if test="sum(exslt:node-set($somme)/token)&gt;0">
            <!-- We sum up the <token>, this is a way to know if there is at least one ... -->
            <!-- We generate a new element <token> with a weight attribut specifying the weight of concerned 
            cumomer (the number of <token> in $somme), the type of the concerned pool (intermediate or input) 
            and its identifier, which is obtained by the sum of <token>. -->
            <token weight="{count(exslt:node-set($somme)/token)}" type="{key('POOL',@id)/@type}">
              <xsl:value-of select="concat(@id,'_',sum(exslt:node-set($somme)/token))"/>
            </token>
          </xsl:if>
        </xsl:for-each>
        <!-- At the end of this loop, the "influx" variable conteins a number of <token>. According to the type
        and the weight of concerned cumomers. It is either unknown, if the weight is the current weight $weight,
        or involved quantities in the right hand side as product form, whose the sum of weight is equal to the 
        current weight $weight. -->
      </xsl:variable>
      
      <!-- Here, we generate the code which forms the matrix and the right hand side of the system which allows 
      to obtain the cumomers of weight $weight -->
      <xsl:choose>
        <xsl:when test="(count(exslt:node-set($influx)/token)=1) and (exslt:node-set($influx)/token/@weight=$weight) and (exslt:node-set($influx)/token/@type='intermediate')">

          <!-- We only have one term of the current weight, so the matrix is assembled so that its derivative -->

					<single-matrix-row id="{concat('M',$weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
						<ci>
							<xsl:value-of select="$position"/>
						</ci>
						<ci>
							<xsl:value-of select="key('INTERMEDIATE_CUMOMER',$influx)/@local-number"/>
						</ci>
						<ci>1</ci>
						<ci><xsl:value-of select="$reaction"/></ci>
					</single-matrix-row>

					<single-matrix-row id="{concat('Mt',$weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
						<ci>
							<xsl:value-of select="key('INTERMEDIATE_CUMOMER',$influx)/@local-number"/>
						</ci>
						<ci>
							<xsl:value-of select="$position"/>
						</ci>
						<ci>1</ci>
						<ci><xsl:value-of select="$reaction"/></ci>
					</single-matrix-row>

          <!-- for global matrix dgdv -->

					<single-matrix-row id="dgdv_m" xmlns="http://www.utc.fr/sysmetab">
						<ci><xsl:value-of select="$global-number"/></ci>
						<ci><xsl:value-of select="$reaction"/></ci>
						<ci>1</ci>
						<ci><xsl:value-of select="key('INTERMEDIATE_CUMOMER',$influx)/@number"/></ci>
						<ci>1</ci>
					</single-matrix-row>

					<single-matrix-row id="dgdvt_m" xmlns="http://www.utc.fr/sysmetab">
						<ci><xsl:value-of select="$reaction"/></ci>
						<ci><xsl:value-of select="$global-number"/></ci>
						<ci>1</ci>
						<ci><xsl:value-of select="key('INTERMEDIATE_CUMOMER',$influx)/@number"/></ci>
						<ci>1</ci>
					</single-matrix-row>

        </xsl:when>
				
        <xsl:when test="count(exslt:node-set($influx)/token)&gt;=1">
          <!-- We have one input term or a product of two terms of lower weight, thus we assembles the right hand 
          side and the Jacobien of the right hand side in comparison of cumomers of lower weight. -->
					
					<single-matrix-row id="{concat('b',$weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
						<ci>
							<xsl:value-of select="$position"/>
						</ci>
						<ci>1</ci>
						<ci>1</ci>
						<ci>
							<xsl:value-of select="key('ALL_CUMOMER',exslt:node-set($influx)/token[1])/@number"/>
						</ci>
						<xsl:choose>
							<xsl:when test="exslt:node-set($influx)/token[2]">
								<ci>
									<xsl:value-of select="key('ALL_CUMOMER',exslt:node-set($influx)/token[2])/@number"/>
								</ci>
							</xsl:when>
							<xsl:otherwise>
								<ci>1</ci>
							</xsl:otherwise>
						</xsl:choose>
						<ci>
							<xsl:value-of select="$reaction"/>
						</ci>
					</single-matrix-row>
          
          <!-- for global matrix dgdv -->

					<single-matrix-row id="dgdv_m" xmlns="http://www.utc.fr/sysmetab">
						<ci><xsl:value-of select="$global-number"/></ci>
						<ci><xsl:value-of select="$reaction"/></ci>
						<ci>1</ci>
						<ci>
							<xsl:value-of select="key('ALL_CUMOMER',exslt:node-set($influx)/token[1])/@number"/>
						</ci>
						<xsl:choose>
							<xsl:when test="exslt:node-set($influx)/token[2]">
								<ci>
									<xsl:value-of select="key('ALL_CUMOMER',exslt:node-set($influx)/token[2])/@number"/>
								</ci>
							</xsl:when>
							<xsl:otherwise>
								<ci>1</ci>
							</xsl:otherwise>
						</xsl:choose>
					</single-matrix-row>        

          <!-- for global matrix dgdv^t -->

					<single-matrix-row id="dgdvt_m" xmlns="http://www.utc.fr/sysmetab">
						<ci><xsl:value-of select="$reaction"/></ci>
						<ci><xsl:value-of select="$global-number"/></ci>
						<ci>1</ci>
						<ci>
							<xsl:value-of select="key('ALL_CUMOMER',exslt:node-set($influx)/token[1])/@number"/>
						</ci>
						<xsl:choose>
							<xsl:when test="exslt:node-set($influx)/token[2]">
								<ci>
									<xsl:value-of select="key('ALL_CUMOMER',exslt:node-set($influx)/token[2])/@number"/>
								</ci>
							</xsl:when>
							<xsl:otherwise>
								<ci>1</ci>
							</xsl:otherwise>
						</xsl:choose>
					</single-matrix-row>
         
	        <xsl:if test="count(exslt:node-set($influx)/token)&gt;1">
						<xsl:variable name="id1" select="exslt:node-set($influx)/token[1]"/>
						<xsl:variable name="id2" select="exslt:node-set($influx)/token[2]"/>
						<xsl:if test="key('INTERMEDIATE_CUMOMER',$id1)">
							
		          <!-- for global matrix db_$weight_dx -->
							
							<single-matrix-row id="{concat('db',$weight,'_dx_m')}" xmlns="http://www.utc.fr/sysmetab">
								<ci><xsl:value-of select="$position"/></ci>
								<ci><xsl:value-of select="key('INTERMEDIATE_CUMOMER',$id1)/@number"/></ci>
    						<ci>1</ci>
								<ci><xsl:value-of select="key('ALL_CUMOMER',$id2)/@number"/></ci>
								<ci><xsl:value-of select="$reaction"/></ci>									
							</single-matrix-row>
              
		          <!-- for global matrix db_dx_$id1/@weight^t -->

							<single-matrix-row id="{concat('db_dx',key('INTERMEDIATE_CUMOMER',$id1)/@weight,'t_m')}" xmlns="http://www.utc.fr/sysmetab">
								<ci><xsl:value-of select="key('INTERMEDIATE_CUMOMER',$id1)/@local-number"/></ci>
								<ci><xsl:value-of select="$global-number"/></ci>
    						<ci>1</ci>
								<ci><xsl:value-of select="key('ALL_CUMOMER',$id2)/@number"/></ci>
								<ci><xsl:value-of select="$reaction"/></ci>									
							</single-matrix-row>
							
						</xsl:if>

						<xsl:if test="key('INTERMEDIATE_CUMOMER',$id2)">

		          <!-- for global matrix db_$weight_dx -->
							
							<single-matrix-row id="{concat('db',$weight,'_dx_m')}" xmlns="http://www.utc.fr/sysmetab">
								<ci><xsl:value-of select="$position"/></ci>
								<ci><xsl:value-of select="key('INTERMEDIATE_CUMOMER',$id2)/@number"/></ci>
    						<ci>1</ci>
								<ci><xsl:value-of select="key('ALL_CUMOMER',$id1)/@number"/></ci>
								<ci><xsl:value-of select="$reaction"/></ci>									
							</single-matrix-row>

		          <!-- for global matrix db_dx_$id2/@weight^t -->

							<single-matrix-row id="{concat('db_dx',key('INTERMEDIATE_CUMOMER',$id2)/@weight,'t_m')}" xmlns="http://www.utc.fr/sysmetab">
								<ci><xsl:value-of select="key('INTERMEDIATE_CUMOMER',$id2)/@local-number"/></ci>
								<ci><xsl:value-of select="$global-number"/></ci>
    						<ci>1</ci>
								<ci><xsl:value-of select="key('ALL_CUMOMER',$id1)/@number"/></ci>
								<ci><xsl:value-of select="$reaction"/></ci>									
							</single-matrix-row>


						</xsl:if>
					</xsl:if>
					
        </xsl:when>
      </xsl:choose>
    </xsl:for-each>
		
    <!-- outflux rule : the easiest part to generate (cf Wiechert paper) -->

    <xsl:for-each select="key('REDUCT',@pool)">
			<single-matrix-row id="{concat('M',$weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
				<ci>
					<xsl:value-of select="$position"/>
				</ci>
				<ci>
					<xsl:value-of select="$position"/>
				</ci>
				<ci>-1</ci>
	      <cn><xsl:value-of select="../@position"/></cn>
			</single-matrix-row>
      
			<single-matrix-row id="{concat('Mt',$weight,'_m')}" xmlns="http://www.utc.fr/sysmetab">
				<ci>
					<xsl:value-of select="$position"/>
				</ci>
				<ci>
					<xsl:value-of select="$position"/>
				</ci>
				<ci>-1</ci>
	      <cn><xsl:value-of select="../@position"/></cn>
			</single-matrix-row>
      
      <!-- for global matrix dgdv -->

			<single-matrix-row id="dgdv_m" xmlns="http://www.utc.fr/sysmetab">
				<ci><xsl:value-of select="$global-number"/></ci>
				<ci><xsl:value-of select="../@position"/></ci>
				<ci>-1</ci>
				<ci><xsl:value-of select="$global-number"/></ci>
				<ci>1</ci>
			</single-matrix-row>

      <!-- for global matrix dgdv^t -->

			<single-matrix-row id="dgdvt_m" xmlns="http://www.utc.fr/sysmetab">
				<ci><xsl:value-of select="../@position"/></ci>
				<ci><xsl:value-of select="$global-number"/></ci>
				<ci>-1</ci>
				<ci><xsl:value-of select="$global-number"/></ci>
				<ci>1</ci>
			</single-matrix-row>
          
    </xsl:for-each>

  </xsl:template>

  <xsl:template name="construct_matrix_C">

    <comment xmlns="http://www.utc.fr/sysmetab">Observation matrix for labelling measurements</comment>

    <optimize xmlns="http://www.utc.fr/sysmetab">
	    <single-matrix-open id="Cx_ijv" cols="{$nX}" />
      <!-- We open an item "optimize" in which we will gradually build this matrix -->
      <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group/smtb:measurement">
        <xsl:variable name="position" select="position()"/>
        <xsl:for-each select="smtb:cumomer-contribution">
          <xsl:choose>
            <xsl:when test="@weight=0">
              <single-matrix-row id="Cx_ijv">
                <cn>
                  <xsl:value-of select="$position"/>
                </cn>
                <cn>1</cn>
                <cn>1</cn>
              </single-matrix-row>
            </xsl:when>
            <xsl:otherwise>  
              <single-matrix-row id="Cx_ijv">
                <cn>
                  <xsl:value-of select="$position"/>
                </cn>
                <cn>
                  <xsl:value-of select="key('INTERMEDIATE_CUMOMER',concat(@pool,'_',@subscript))/@number"/>
                </cn>
                <cn>
                  <xsl:value-of select="@sign"/>
                </cn>
              </single-matrix-row>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </xsl:for-each>
	    <single-matrix-close id="Cx_ijv" where="disk"/>
    </optimize>

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
			<eq/>
			<ci>Cx</ci>    
  		<apply>
  			<fn><ci>s_sparse</ci></fn>
        <smtb:string>
          <xsl:value-of select="concat($output,'.network/Cx_ijv.txt')"/>
        </smtb:string>
        <cn><xsl:value-of select="$nY"/></cn>
        <cn><xsl:value-of select="$nX"/></cn>
  		</apply>     
    </apply>
		<apply xmlns="http://www.w3.org/1998/Math/MathML">
			<eq/>
      <ci>Cxt</ci>
			<apply>
				<transpose/>
        <ci>Cx</ci>
      </apply>
    </apply>
    
  </xsl:template>
	
  
	<xsl:template name="groups">
    
  	<comment xmlns="http://www.utc.fr/sysmetab">omega_ind_meas contains the group number for each measurement</comment>
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
			<eq/>
			<ci>omega_ind_meas</ci>
			<matrix>
      	<matrixrow>
				  <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group/smtb:measurement">
						<cn>
							<xsl:value-of select="key('GROUP',../@id)/@pos"/>
						</cn>
          </xsl:for-each>
			  </matrixrow>
      </matrix>
	  </apply>
    
  	<comment xmlns="http://www.utc.fr/sysmetab">meas_in_group(i) contains measurement numers of group i</comment>
    
    <xsl:for-each select="f:configuration/f:measurement/f:model/f:labelingmeasurement/f:group">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
    		<eq/>
        <apply>
          <selector/>
          <ci type="vector">meas_ind_group</ci>
          <cn><xsl:value-of select="position()"/></cn>
        </apply>
    		<vector>
    			<xsl:for-each select="smtb:measurement">
    			  <cn>
    				  <xsl:value-of select="@number"/>
    				</cn>
          </xsl:for-each>
        </vector>
      </apply>
    </xsl:for-each>
	
  </xsl:template>


</xsl:stylesheet>
