function s_sparse(ijv,n,m)
  if isa(ijv,String)
    try
      ijv=readdlm(ijv)
    catch
    	ijv=[]
    end
  end
  if isempty(ijv)
    spzeros(n,m)
  else
  	sparse(int(ijv[:,1]),int(ijv[:,2]),float64(ijv[:,3]),n,m)
  end
end