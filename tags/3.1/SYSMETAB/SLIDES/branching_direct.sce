//
// Ajustement du controle des free fluxes
//
nothing=adjustFluxesTab(fluxes);
//
// Cumom�res en entr�e, calcul�s � partir des isotopom�res des m�tabolites d'entr�e.
//
x1_input=D1*[A_out_input];
x2_input=D2*[A_out_input];
//
// Calcul direct des cumom�res
//
v=fluxes(:,1);
[x1,x2]=solveCumomers(v,x1_input,x2_input);
//
// Calcul des observations en fonction des cumom�res
//
y=C1*x1+C2*x2;
//
// Remplissage des tableaux des cumom�res dans l'interface
//
cumomers_weight_1=x1;
cumomers_weight_2=x2;
//
// Remplissage des tableaux des observations dans l'interface
//
F_obs(1:3,3:3:3)=y(1:3,1:1);
//
// R�cup�rations des donn�es pour le calcul de la fonction cout
//
//
// Flux observ�s
//
vobs=flux_obs(:,2);
//
// Matrice de pond�ration de l'erreur sur les flux observ�s
//
delta=flux_obs(:,1);
//
// Marquages observ�s
//
yobs=[F_obs(:,2:3:2)];
//
// Matrice de pond�ration de l'erreur sur les marquages observ�s
//
alpha=[F_obs(:,1:3:1)];
//
// Calcul de la fonction cout
//
[label_error,grad]=costAndGrad(fluxes(:,1));
