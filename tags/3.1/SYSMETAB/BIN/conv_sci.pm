#!/usr/bin/perl

print "<xml>";

while (<>) {
    s!<!&lt;!;
    s!>!&gt;!;
    s!^!<row>!;
    s!$!</row>!;
    print;
};
print "</xml>";
