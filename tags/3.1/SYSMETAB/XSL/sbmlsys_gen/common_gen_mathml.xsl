<?xml version="1.0" encoding="UTF-8" ?> 

<!--
Auteur: Florent Tixier
Date: lun mai 26 12:18 CEST 2008
Projet: SYSMETAB/Carnot
-->

<!--
Templates communes à la génération des différentes fonctions spécifiques d'un modèle donné sous forme SBML,
édité dans CellDesigner pour les cas stationnaire et non-stationnaire. Extraites telles quelles du fichier
initial sbmlsys_gen_mathml_solve_stat_4.xsl
-->

<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  
  xmlns:m="http://www.w3.org/1998/Math/MathML" 
  xmlns:xhtml="http://www.w3.org/1999/xhtml"
  xmlns:exslt="http://exslt.org/common" 
  xmlns:str="http://exslt.org/strings" 
  xmlns:math="http://exslt.org/math" 
  xmlns:smtb="http://www.utc.fr/sysmetab" 
  xmlns:sbml="http://www.sbml.org/sbml/level2/version4"
  xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
  exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
 
  <xsl:template match="sbml:listOfSpecies" mode="stoichiometric-matrix">
    <optimize xmlns="http://www.utc.fr/sysmetab">
      <matrix-open type="s_full" id="N" rows="{count(sbml:species[@type='intermediate'])}" cols="{count(../sbml:listOfReactions/sbml:reaction)}"/>
      <xsl:apply-templates select="sbml:species[@type='intermediate']" mode="stoichiometric-matrix"/>
      <matrix-close id="N"/>
    </optimize>
  </xsl:template>
	
  <xsl:template match="sbml:species" mode="stoichiometric-matrix">
    <xsl:variable name="position" select="position()"/>
    <xsl:for-each select="key('products',@id) | key('reactants',@id)">
      <matrix-assignment id="N" row="{$position}" col="{../../@position}" xmlns="http://www.utc.fr/sysmetab">
        <xsl:choose>
          <xsl:when test="parent::sbml:listOfProducts">
            <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
          </xsl:when>
          <xsl:when test="parent::sbml:listOfReactants">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <minus/>
              <cn>1</cn>
            </apply>
          </xsl:when>
        </xsl:choose>
      </matrix-assignment>
    </xsl:for-each>
  </xsl:template> 

  <xsl:template match="sbml:listOfRules" mode="stoichiometric-matrix">
    <optimize xmlns="http://www.utc.fr/sysmetab">
      <matrix-open type="s_full" id="R" rows="{count(sbml:algebraicRule)}" cols="{count(../sbml:listOfReactions/sbml:reaction)}"/>
      <xsl:apply-templates select="sbml:algebraicRule" mode="stoichiometric-matrix"/>
      <matrix-close id="R"/>
    </optimize>
  </xsl:template>

  <xsl:template match="sbml:algebraicRule" mode="stoichiometric-matrix">
    <matrix-assignment id="R" row="{position()}" col="{key('reaction',m:math/m:apply[m:minus]/m:ci[1])/@position}" xmlns="http://www.utc.fr/sysmetab">
      <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
    </matrix-assignment>
    <matrix-assignment id="R" row="{position()}" col="{key('reaction',m:math/m:apply[m:minus]/m:ci[2])/@position}" xmlns="http://www.utc.fr/sysmetab">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <minus/>
        <cn>1</cn>
      </apply>
    </matrix-assignment>
  </xsl:template>

  <xsl:template name="iteration">

  <!-- Attention le noeud contextuel est un élement <smtb:cumomer> -->

    <!-- La variable "carbons" contient les carbones 13 de l'espèce courante -->
  
    <xsl:variable name="carbons">
      <xsl:copy-of select="smtb:carbon"/>
    </xsl:variable>
    <xsl:variable name="weight" select="@weight"/>
    <xsl:variable name="position" select="@position"/>
  
    <!-- influx rule : le plus chiant, mais aussi le plus intéressant. C'est là que se crée véritablement
l'information supplémentaire 
    par rapport à la stoechiométrie simple. On boucle sur tous les éléments <speciesReference> qui ont l'espèce du cumomère courant comme produit,
    donc dans le for-each le noeud contextuel est de type reaction/listOfProducts/speciesReference. -->

    <xsl:for-each select="key('products',@species)"> 
  
    <!-- Maintenant, on essaye de trouver des occurences des carbones marqués
du reactant de la réaction 
    courante : c'est du boulot... -->

      <xsl:variable name="species" select="@species"/>
      <xsl:variable name="reaction" select="../../@position"/>
      <xsl:variable name="occurence" select="count(preceding-sibling::sbml:speciesReference)+1"/>
      <xsl:variable name="influx">

      <!-- C'est ici que les choses sérieuses commencent. On boucle sur tous les réactants qui ont des atomes de carbones
      qui "pointent" sur l'espèce -->

        <xsl:for-each select="../../sbml:listOfReactants/sbml:speciesReference[smtb:carbon[(@species=$species) and (@occurence=$occurence)]]">
          <xsl:variable name="somme">
        
          <!-- On boucle donc maintenant sur les carbones du reactant. Si un carbone du reactant pointe vers
vers un carbone 13 
          du cumomère du produit, on note son numéro dans un élément <token/> -->
        
            <xsl:for-each select="smtb:carbon[(@species=$species) and (@occurence=$occurence)]">
              <xsl:if test="exslt:node-set($carbons)/smtb:carbon[@position=current()/@destination]">
                <token>
                  <xsl:value-of select="@position"/>
                </token>
              </xsl:if>
            </xsl:for-each>

          
            <!-- A la fin de cette boucle la variable "somme" contient un certain nombre, éventuellement nul, d'éléments <token> qui identifient 
            sans abiguité le cumomère concerné. -->
          </xsl:variable>    
          <xsl:if test="sum(exslt:node-set($somme)/token)&gt;0"> 
        
          <!-- On fait la somme des <token>, ce qui est une façon de savoir s'il y en a au moins un...
          On génère un nouvel élément <token> avec un attribut weight précisant le poids du cumomère concerné (le nombre de <token> dans $somme), 
          le type de l'espèce concernée (intermédiaire ou entrée) et son identificateur, que l'on obtient en faisant la somme des <token>. --> 
          
            <token weight="{count(exslt:node-set($somme)/token)}" type="{key('species',@species)/@type}">
              <xsl:value-of select="concat(@species,'_',sum(exslt:node-set($somme)/token))"/>
            </token>
          </xsl:if>
        </xsl:for-each>

        <!-- A la fin de cette boucle la variable "influx" contient un certain nombre de <token>. Suivant le type et le poids des cumomères concernés, 
        il s'agit soit d'inconnues, si le poids est le poids courant $weight, soit de quantités intervenant au second membre sous forme de produit, 
        dont la somme des poids est égale au poids courant $weight. -->
      </xsl:variable>
    
      <!-- Ici on génère le code qui forme la matrice et le second membre du système qui permet d'obtenir les
cumomères de poids $weight -->
    
      <xsl:choose>
        <xsl:when test="(count(exslt:node-set($influx)/token)=1) and (exslt:node-set($influx)/token/@weight=$weight) and (exslt:node-set($influx)/token/@type='intermediate')">
      
        <!-- On n'a qu'un seul terme du poids courant, on assemble donc la matrice ainsi que sa dérivée -->
				
      
          <matrix-assignment id="{concat('M',$weight)}" row="{$position}" col="{key('cumomer',$influx)/@position}" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <selector/>
              <ci type="vector">v</ci>
              <cn>  <!-- id de la réaction -->
                <xsl:value-of select="$reaction"/>
              </cn>
            </apply>
          </matrix-assignment>
          <matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{$reaction}" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <selector/>
              <ci type="matrix">
                <xsl:value-of select="concat('x',$weight)"/>
              </ci>
              <cn>
                <xsl:value-of select="key('cumomer',$influx)/@position"/>
              </cn>
              <cn>:</cn>
            </apply>
          </matrix-assignment>
        </xsl:when>
        <xsl:otherwise>
      
        <!-- On a un produit de plusieurs termes, donc de poids inférieur, on assemble donc le second membre
ainsi que la jacobienne 
        du second membre par rapport aux cumomères de poids inférieur  -->
      
          <xsl:variable name="influx-factors">
            <xsl:call-template name="iterate-influx">
              <xsl:with-param name="tokens">
                <xsl:copy-of select="$influx"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:variable>
          <matrix-assignment id="{concat('b',$weight)}" row="{$position}" col="1" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <times type="array"/>
              <apply>
                <selector/>
                <ci type="vector">v</ci>
                <cn>  <!-- id de la réaction -->
                  <xsl:value-of select="$reaction"/>
                </cn>
              </apply>
              <xsl:copy-of select="$influx-factors"/>
            </apply>
          </matrix-assignment>
          <matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{$reaction}" xmlns="http://www.utc.fr/sysmetab">
            <apply xmlns="http://www.w3.org/1998/Math/MathML">
              <times type="array"/>
              <xsl:copy-of select="$influx-factors"/>
            </apply>
          </matrix-assignment>
          <xsl:call-template name="iterate-influx-jacobian-x">
            <xsl:with-param name="reaction" select="$reaction"/>
            <xsl:with-param name="weight" select="$weight"/>
            <xsl:with-param name="tokens">
              <xsl:copy-of select="$influx"/>
            </xsl:with-param>
            <xsl:with-param name="position" select="$position"/>
            <xsl:with-param name="i">1</xsl:with-param>
            <xsl:with-param name="n" select="count(exslt:node-set($influx)/token)"/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    
    <!-- outflux rule : partie la plus simple à générer (voir papier Wiechert) -->
    
    <matrix-assignment id="{concat('M',$weight)}" row="{$position}" col="{$position}" xmlns="http://www.utc.fr/sysmetab">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <minus/>
        <xsl:call-template name="bilan-outflux">
          <xsl:with-param name="id" select="@species"/>
        </xsl:call-template>
      </apply>
    </matrix-assignment>
    <xsl:for-each select="key('reactants',@species)">
      <matrix-assignment id="{concat('dg',$weight,'_dv')}" row="{$position}" col="{../../@position}" xmlns="http://www.utc.fr/sysmetab">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <minus/>
          <apply>
            <selector/>
            <ci type="matrix">
              <xsl:value-of select="concat('x',$weight)"/>
            </ci>
            <cn>
              <xsl:value-of select="$position"/>
            </cn>
            <cn>:</cn>
          </apply>
        </apply>
      </matrix-assignment>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template name="bilan-outflux">
    <xsl:param name="id"/>

    <!-- outflux rule -->

    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <plus/>
      <xsl:for-each select="key('reactants',$id)">
        <apply>
          <selector/>
          <ci type="vector">v</ci>
          <cn>  <!-- id de la réaction -->
            <xsl:value-of select="../../@position"/>
          </cn>
        </apply>
      </xsl:for-each>
    </apply>
  </xsl:template>

  <xsl:template name="iterate-influx">
    <xsl:param name="tokens"/>
    <xsl:variable name="id" select="exslt:node-set($tokens)/token[1]"/>
    <xsl:choose>
      <xsl:when test="key('cumomer',$id)">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <selector/>
          <ci type="matrix">
            <xsl:value-of select="concat('x',key('cumomer',$id)/@weight)"/>
          </ci>
          <cn>
            <xsl:value-of select="key('cumomer',$id)/@position"/>
          </cn>
          <cn>:</cn>
        </apply>
      </xsl:when>
      <xsl:when test="key('input-cumomer',$id)">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <selector/>
          <ci type="matrix">
            <xsl:value-of select="concat('x',key('input-cumomer',$id)/@weight,'_input')"/>
          </ci>
          <cn>
            <xsl:value-of select="key('input-cumomer',$id)/@position"/>
          </cn>
          <cn>:</cn>
        </apply>
      </xsl:when>
    </xsl:choose>
    <xsl:if test="count(exslt:node-set($tokens)/token)&gt;1">
      <xsl:call-template name="iterate-influx">
        <xsl:with-param name="tokens">
          <xsl:copy-of select="exslt:node-set($tokens)/token[position()&gt;1]"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <xsl:template name="iterate-influx-jacobian-x">

  <!-- Generation des jacobiennes (dérivées) dbi/dxj -->

    <xsl:param name="reaction"/>
    <xsl:param name="weight"/>
    <xsl:param name="tokens"/>
    <xsl:param name="position"/>
    <xsl:param name="i"/>
    <xsl:param name="n"/>
    <xsl:variable name="id" select="exslt:node-set($tokens)/token[position()=$i]"/>
    <xsl:if test="key('cumomer',$id)">
      <matrix-assignment id="{concat('db',$weight,'_dx',key('cumomer',$id)/@weight)}" row="{$position}" col="{key('cumomer',$id)/@position}" xmlns="http://www.utc.fr/sysmetab">
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <times type="array"/>
          <xsl:call-template name="iterate-influx">
          
          <!-- La dérivée est égale à tout ce qui est en facteur de la variable -->
          
            <xsl:with-param name="tokens">
              <xsl:copy-of select="exslt:node-set($tokens)/token[position()!=$i]"/>
            </xsl:with-param>
          </xsl:call-template>
          <apply>
            <selector/>
            <ci type="vector">v</ci>
            <cn>
              <xsl:value-of select="$reaction"/>
            </cn>
          </apply>
        </apply>
      </matrix-assignment>
    </xsl:if>
    <xsl:if test="$i&lt;$n">
      <xsl:call-template name="iterate-influx-jacobian-x">
        <xsl:with-param name="reaction" select="$reaction"/>
        <xsl:with-param name="weight" select="$weight"/>
        <xsl:with-param name="tokens">
          <xsl:copy-of select="$tokens"/>
        </xsl:with-param>
        <xsl:with-param name="position" select="$position"/>
        <xsl:with-param name="i" select="($i)+1"/>
        <xsl:with-param name="n" select="$n"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <!--Construction des matrices C-->

  <xsl:template name="observation-matrices-open">
    <xsl:param name="number-of-observations"/>
    <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
      <matrix-open id="C{@weight}" rows="{$number-of-observations}" cols="{count(smtb:cumomer)}" type="sparse" xmlns="http://www.utc.fr/sysmetab"/>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template name="observation-matrices-close">
    <xsl:param name="number-of-observations"/>
    <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers">
      <matrix-close id="C{@weight}" rows="{$number-of-observations}" cols="{count(smtb:cumomer)}" type="sparse" xmlns="http://www.utc.fr/sysmetab"/>
    </xsl:for-each>
  </xsl:template>
  
  <xsl:template name="construct_matrix_C">
    
    <comment xmlns="http://www.utc.fr/sysmetab">Matrices d'observation (cumomères)</comment>
    
    <optimize xmlns="http://www.utc.fr/sysmetab"> <!-- On ouvre un élément "optimize" dans lequel on va construire petit à petit ces matrices -->
      <xsl:call-template name="observation-matrices-open">
        <xsl:with-param name="number-of-observations" select="count(sbml:listOfSpecies/sbml:species[@type='intermediate']/smtb:measurement)"/>
      </xsl:call-template>
      <xsl:for-each select="sbml:listOfSpecies/sbml:species[@type='intermediate']/smtb:measurement">
        <xsl:variable name="position" select="position()"/>
        <xsl:for-each select="smtb:cumomer-contribution">
          <matrix-assignment id="C{@weight}" row="{$position}" col="{key('cumomer',@id)/@position}">
            <xsl:choose>
              <xsl:when test="@sign='1'">
                <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
              </xsl:when>
              <xsl:otherwise> <!--@sign="'-1'-->
                <apply xmlns="http://www.w3.org/1998/Math/MathML">
                  <minus/>
                  <cn>1</cn>
                </apply>
              </xsl:otherwise>
            </xsl:choose>
          </matrix-assignment>
        </xsl:for-each>
      </xsl:for-each>
      <xsl:call-template name="observation-matrices-close"/>
    </optimize>
  </xsl:template>
  
  <!--Construction de la matrice E"-->
  
  <xsl:template name="construct_matrix_E">
    
    <comment xmlns="http://www.utc.fr/sysmetab">Matrice d'observation (flux)</comment>
    
    <optimize xmlns="http://www.utc.fr/sysmetab"> 
    
    <!-- On ouvre un élément "optimize" dans lequel on va construire petit à petit la matrice E -->
    
    <!--Construction de la matrice E"-->
    
      <matrix-open id="E" cols="{count(sbml:listOfReactions/sbml:reaction)}" rows="{count(sbml:listOfReactions/sbml:reaction[(@known='yes') or (@observation='yes')])}" type="sparse"/>
      <xsl:for-each select="sbml:listOfReactions/sbml:reaction[(@known='yes') or (@observation='yes')]">
        <matrix-assignment id="E" row="{position()}" col="{@position}"><cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn></matrix-assignment>
      </xsl:for-each>
      <matrix-close id="E"/> 
      <!-- Lorsque l'on va rencontrer cet élément dans la transformation qui a pour but de produire le code Scilab, on rassemble tous les ordres d'affectation
      à la matrice "E" et on crée cette matrice -->
    </optimize>
  </xsl:template>
  
  <!-- Construction des matrices d'entrée, permettant d'obtenir la contribution sur les cumomères des isotopomères en entrée. -->

  <xsl:template name="construct_matrix_D">
    
    <comment xmlns="http://www.utc.fr/sysmetab">Matrices d'entrée</comment>
    
    <optimize xmlns="http://www.utc.fr/sysmetab"> 
    
    <!-- On ouvre un élément "optimize" dans lequel on va construire petit à petit les matrices D1,D2,...,Dn où "n" est le poids maximum, 
    déterminé lors des transformations préalables en fonction des observations choisies -->
    
      <xsl:variable name="number-of-inputs" select="count(sbml:listOfSpecies/sbml:species[smtb:input]/smtb:input)"/>
      <xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers">
        <matrix-open id="D{@weight}" cols="{$number-of-inputs}" rows="{count(smtb:cumomer)}" type="sparse"/>
        <xsl:variable name="current-weight" select="@weight"/>

        <!-- Pour chaque cumomère d'un métabolité d'entrée, on détermine quelles sont les contributions des différents
isotopomères en entrée. -->
        
        <xsl:for-each select="smtb:cumomer">

        <!-- Attention ici l'attribut position de l'élément smtb:cumomer ne représente que le numéro du cumomère dans le vecteur des cumomères du poids courant -->
        
          <xsl:variable name="position" select="@position"/>
          
          <!-- La variable $carbons contient les éléments <smtb:carbon/> correspondant aux carbones marqués du cumomère courant. -->
          <xsl:variable name="carbons" select="smtb:carbon"/>

          <!-- On boucle sur tous les isotopomères des métabolites en entrée. -->
          
          <xsl:for-each select="../../../sbml:listOfSpecies/sbml:species[smtb:input]/smtb:input">
          
          <!-- Element contextuel : un isotopomère d'entrée. Ici la variable $string contient le pattern avec des 0 et des 1 suivant que le carbone
          correspondant est marqué ou non. -->
          
            <xsl:variable name="string" select="@string"/>
            
            <!-- Attention on se concentre : si toutes les positions marquées d'un cumomères sont occupées par un "1" dans l'isotopomère, 
            alors l'isotopomère contribue au cumomère. On met donc un "1" dans la matrice D1 (par exemple, pour les poids 1) à la position 
            ligne=$position (du cumomère), colonne=position() (de l'isotopomère), de manière à pouvoir utiliser ensuite cette matrice pour écrire 
            x1_input=D1*[s1_input;s2_input;...]; -->
            
            <xsl:if test="count(exslt:node-set($carbons)[substring($string,string-length($string)-@index,1)='1'])=count(exslt:node-set($carbons))">
              <matrix-assignment id="D{$current-weight}" row="{$position}" col="{position()}">
                <cn xmlns="http://www.w3.org/1998/Math/MathML">1</cn>
              </matrix-assignment>
            </xsl:if>
          </xsl:for-each>
        </xsl:for-each>
        <matrix-close id="D{@weight}"/>
      </xsl:for-each>
    </optimize>
  </xsl:template>
  
  <!--Contruction des matrice de noms et ids des fluxs -->

  <xsl:template name="names_and_ids_fluxes">
  
  <!-- Matrice des noms des fluxs -->
  
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>flux_names</ci>
      <list separator=";">
        <xsl:for-each select="sbml:listOfReactions/sbml:reaction">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select='@name'/>
          </string>
        </xsl:for-each>
      </list>
    </apply>
    
    <!-- Matrice des ids des fluxs -->
    
    <apply xmlns="http://www.w3.org/1998/Math/MathML">
      <eq/>
      <ci>flux_ids</ci>
      <list separator=";">
        <xsl:for-each select="sbml:listOfReactions/sbml:reaction">
          <string xmlns="http://www.utc.fr/sysmetab">
            <xsl:value-of select='@id'/>
          </string>
        </xsl:for-each>
      </list>
    </apply>
  </xsl:template>

  <!--Cumomères en entrée, calculés à partir des isotopomères des métabolites d'entrée.-->

  <xsl:template name="input_cumomeres">
    
    <comment xmlns="http://www.utc.fr/sysmetab">Cumomères en entrée, calculés à partir des isotopomères des métabolites d'entrée.</comment>
    
    <xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers">
      <apply xmlns="http://www.w3.org/1998/Math/MathML">
        <eq/>
        <ci><xsl:value-of select="concat('x',@weight,'_input')"/></ci>
        <apply>
          <times/>
          <ci><xsl:value-of select="concat('D',@weight)"/></ci>
          <list separator=";">
            <xsl:for-each select="../../sbml:listOfSpecies/sbml:species[smtb:input]">
              <ci><xsl:value-of select="concat(@id,'_input')"/></ci>
            </xsl:for-each>
          </list>
        </apply>
      </apply>
    </xsl:for-each>
  </xsl:template>
 
  <xsl:template name="fluxSubspace">
    <function xmlns="http://www.utc.fr/sysmetab">
      <ci xmlns="http://www.w3.org/1998/Math/MathML">fluxSubspace</ci>
      <input>
        <list xmlns="http://www.w3.org/1998/Math/MathML">
          <ci>_fluxes</ci>
        </list>
      </input>
      <output>
        <list xmlns="http://www.w3.org/1998/Math/MathML">
          <ci>A</ci>
          <ci>b</ci>
          <ci>N</ci>
          <ci>H</ci>
          <ci>w</ci>
        </list>
      </output>
      <body>
        
        <comment>Construction des matrices (dont la matrice de stoechiométrie) permettant de calculer le changement de variable affine des flux.</comment>
        
        <xsl:apply-templates select="sbml:listOfSpecies" mode="stoichiometric-matrix"/>
        
        <!-- Prise en compte d'égalités imposées par exmple pour les éspèces symétriques -->
        
        <xsl:if test="sbml:listOfRules/sbml:algebraicRule">
          <xsl:apply-templates select="sbml:listOfRules" mode="stoichiometric-matrix"/>
        </xsl:if>
        
        <comment>Flux imposés dans l'interface</comment>
        
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <list separator=",">
            <ci>H</ci>
            <ci>w</ci>
          </list>
          <apply>
            <fn><ci>fluxConstraints</ci></fn>
            <ci>_fluxes</ci>
          </apply>
        </apply>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>b</ci>
          <list>
            <apply>
              <fn><ci>zeros</ci></fn>
              <cn>
                <xsl:value-of select="count(sbml:listOfSpecies/sbml:species[@type='intermediate'])+count(sbml:listOfRules/sbml:algebraicRule)"/>
              </cn>
              <cn>1</cn>
            </apply>
            <ci>w</ci>
          </list>
        </apply>
        <apply xmlns="http://www.w3.org/1998/Math/MathML">
          <eq/>
          <ci>A</ci>
          <list separator=";">
            <ci>N</ci>

            <!-- Prise en compte d'égalités imposées par exmple pour les éspèces symétriques -->
            
            <xsl:if test="sbml:listOfRules/sbml:algebraicRule">
              <ci>R</ci>
            </xsl:if>
            <ci>H</ci>
          </list>
        </apply>
      </body>
    </function>
  </xsl:template>
  
</xsl:stylesheet>