<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   2013/2014
    Project :   PIVERT/Metalippro-PL1
-->

<!-- This style sheet aims to minimize the graph generated with fmlsys_gen_graph.xsl, it gives as results :

    <graph>
      <node id="F_1" pool="F" subscript="1" weight="1" pattern="x1" type="intermediate" source="yes"/>
      <node id="A_1" pool="A" subscript="1" weight="1" pattern="x1" type="intermediate"/>
      <node id="A_out_1" pool="A_out" subscript="1" weight="1" pattern="x1" type="input"/>
      <node id="D_1" pool="D" subscript="1" weight="1" pattern="1" type="intermediate"/>
      <node id="A_2" pool="A" subscript="2" weight="1" pattern="1x" type="intermediate"/>
      <node id="A_out_2" pool="A_out" subscript="2" weight="1" pattern="1x" type="input"/>
      <node id="F_2" pool="F" subscript="2" weight="1" pattern="1x" type="intermediate" source="yes"/>
      <node id="F_3" pool="F" subscript="3" weight="2" pattern="11" type="intermediate" source="yes"/>
      <node id="A_3" pool="A" subscript="3" weight="2" pattern="11" type="intermediate"/>
      <node id="A_out_3" pool="A_out" subscript="3" weight="2" pattern="11" type="input"/>
    </graph>
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:exslt="http://exslt.org/common"
  exclude-result-prefixes="xsl exslt">

  <xsl:output  method="xml" indent="yes"/>

  <xsl:param name="verb_level">1</xsl:param>

  <!-- find all nodes in the graph -->
  <xsl:key name="NODES" match="node" use="@id"/> 

  <!-- Count the number of nodes in the graph -->
  <xsl:variable name="nnodes" select="count(/graph/node)"/>

  <!-- Count the number of child in each node -->
  <xsl:variable name="maxchild">
    <xsl:for-each select="/graph/node">
      <xsl:sort select="count(child)" data-type="number"/>
      <xsl:if test="position()=last()">
        <xsl:value-of select="count(child)"/>
      </xsl:if>
    </xsl:for-each>
  </xsl:variable>

  <xsl:template match="/">
    <xsl:if test="$verb_level&gt;0">
      <xsl:message>Graph simplification</xsl:message>
    </xsl:if>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="graph">
  <!-- <xsl:message>Graph statistics</xsl:message>
       <xsl:message>****************</xsl:message>
       <xsl:message>nodes : <xsl:value-of select="$nnodes"/></xsl:message>
       <xsl:call-template name="outdegree"/>
  -->
    <graph>
      <xsl:call-template name="search-alt">
        <xsl:with-param name="todo">
          <xsl:for-each select="node[@source='yes']">
            <link node="{@id}"/>
          </xsl:for-each>
        </xsl:with-param>
      </xsl:call-template>
    </graph>
  </xsl:template>

  <xsl:template name="outdegree">
    <xsl:param name="i">0</xsl:param>
    <xsl:variable name="out" select="count(node[count(child)=$i])"/>
    <xsl:if test="$i&lt;=$maxchild">
      <xsl:message>nodes with outdegree=<xsl:value-of select="$i"/> : <xsl:value-of select="concat($out,' (',100*($out div $nnodes),'%)')"/></xsl:message>
      <xsl:call-template name="outdegree">
        <xsl:with-param name="i" select="($i)+1"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <xsl:template name="search-alt">
    <xsl:param name="todo"/>
    <xsl:param name="visited" select="/.."/>
    <xsl:variable name="nodeid" select="exslt:node-set($todo)/*[1]/@node"/>
    <xsl:choose>
      <xsl:when test="exslt:node-set($visited)/*[@node=$nodeid]">
        <xsl:if test="count(exslt:node-set($todo)/*)&gt;1">
          <xsl:call-template name="search-alt">
            <xsl:with-param name="todo">
              <xsl:copy-of select="exslt:node-set($todo)/*[position()&gt;1]"/>
            </xsl:with-param>
            <xsl:with-param name="visited">
              <xsl:copy-of select="$visited"/>
            </xsl:with-param>
          </xsl:call-template>
        </xsl:if>
      </xsl:when>
      <xsl:when test="(key('NODES',$nodeid)/child) or (count(exslt:node-set($todo)/*)&gt;1)">
        <node>
          <xsl:copy-of select="key('NODES',$nodeid)/@*"/>
        </node>
        <xsl:call-template name="search-alt">
          <xsl:with-param name="todo">
            <xsl:copy-of select="key('NODES',$nodeid)/child"/>
            <xsl:copy-of select="exslt:node-set($todo)/*[position()&gt;1]"/>
          </xsl:with-param>
          <xsl:with-param name="visited">
            <xsl:copy-of select="$visited"/>
            <link node="{$nodeid}"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>