<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Authors :   Stéphane Mottelet and Georges Sadaka
    Date    :   2013/2014
    Project :   PIVERT/Metalippro-PL1
-->

<!-- This style sheet is intended to add the list of intermediate cumomers and the list of input cumomers as :

    <listOfIntermediateCumomers xmlns="http://www.utc.fr/sysmetab">
      <listOfCumomers weight="1">
        <cumomer id="A_1" pool="A" weight="1" number="1" pattern="x1"/>
        <cumomer id="A_2" pool="A" weight="1" number="2" pattern="1x"/>
        <cumomer id="D_1" pool="D" weight="1" number="3" pattern="1"/>
        <cumomer id="F_1" pool="F" weight="1" number="4" pattern="x1"/>
        <cumomer id="F_2" pool="F" weight="1" number="5" pattern="1x"/>
      </listOfCumomers>
      <listOfCumomers weight="2">
        <cumomer id="A_3" pool="A" weight="2" number="1" pattern="11"/>
        <cumomer id="F_3" pool="F" weight="2" number="2" pattern="11"/>
      </listOfCumomers>
    </listOfIntermediateCumomers>
    <listOfInputCumomers xmlns="http://www.utc.fr/sysmetab">
      <listOfCumomers weight="1">
        <cumomer id="A_out_1" pool="A_out" weight="1" number="1" pattern="x1"/>
        <cumomer id="A_out_2" pool="A_out" weight="1" number="2" pattern="1x"/>
      </listOfCumomers>
      <listOfCumomers weight="2">
        <cumomer id="A_out_3" pool="A_out" weight="2" number="1" pattern="11"/>
      </listOfCumomers>
    </listOfInputCumomers>

     At the end, we add a position number to each reaction.
-->

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:smtb="http://www.utc.fr/sysmetab"
  xmlns:math="http://exslt.org/math"
  xmlns:f="http://www.13cflux.net/fluxml"
  exclude-result-prefixes="xsl smtb math f">
    
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

  <xsl:param name="verb_level">1</xsl:param>

  <xsl:strip-space elements="*"/>

  <!-- Maximum weight of all cumomers inside the network -->
  <xsl:variable name="maxweight" select="math:max(//f:pool/@atoms)"/>

  <xsl:template match="/">
    <xsl:if test="$verb_level&gt;0">
      <xsl:message>>Generation without network simplification</xsl:message>
    </xsl:if>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="*|@*|text()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
    </xsl:copy>
  </xsl:template>

  <!-- The following two templates will add the list of intermediate cumomers and the list of input cumomers -->
  <xsl:template match="f:metabolitepools">
    <metabolitepools xmlns="http://www.13cflux.net/fluxml">
      <xsl:apply-templates/>
    </metabolitepools>
    <listOfIntermediateCumomers xmlns="http://www.utc.fr/sysmetab">
      <xsl:call-template name="make-cumomer-list">
        <xsl:with-param name="type" select="'intermediate'"/>
      </xsl:call-template>
    </listOfIntermediateCumomers>
    <listOfInputCumomers xmlns="http://www.utc.fr/sysmetab">
      <xsl:call-template name="make-cumomer-list">
        <xsl:with-param name="type" select="'input'"/>
      </xsl:call-template>
    </listOfInputCumomers>
  </xsl:template>

  <xsl:template name="make-cumomer-list">
    <xsl:param name="w">1</xsl:param>
    <xsl:param name="type"/>
    <xsl:if test="$w&lt;=$maxweight">
      <xsl:if test="f:pool[@type=$type]/smtb:cumomer[@weight=$w]">
        <listOfCumomers xmlns="http://www.utc.fr/sysmetab" weight="{$w}">
          <xsl:for-each select="f:pool[@type=$type]/smtb:cumomer[@weight=$w]">
            <xsl:sort select="@pool" data-type="text"/>
            <xsl:sort select="@subscript" data-type="number"/>
            <cumomer id="{@id}" pool="{@pool}" weight="{$w}" number="{position()}" pattern="{@pattern}"/>
          </xsl:for-each>
        </listOfCumomers>
      </xsl:if>
      <xsl:call-template name="make-cumomer-list">
        <xsl:with-param name="w" select="($w)+1"/>
        <xsl:with-param name="type" select="$type"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

  <!-- Add position number to each reaction -->
  <xsl:template match="f:reaction">
    <xsl:variable name="pos" select="count(preceding-sibling::f:reaction)+1"/>
    <reaction id="{@id}" position="{$pos}" xmlns="http://www.13cflux.net/fluxml">
      <xsl:copy-of select="@*"/>
      <xsl:copy-of select="*"/>
    </reaction>
  </xsl:template>

</xsl:stylesheet>