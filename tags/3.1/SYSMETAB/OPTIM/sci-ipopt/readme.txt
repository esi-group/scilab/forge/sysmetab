Ipopt toolbox

Purpose

The goal of this toolbox is to provide an interface to IPOPT,
an optimization solver based on an Interior Point algorithm.

This module is mainly based on compiled source code.

In order to compile this module, you must compile its 
dependencies first. To do this, go in the src/Ipopt-3.8.1
directory. The remaining depends on your plateform.
 * On Linux, type 
./configure
make
make install
Be sure that the libraries are correctly installed, so that the 
command line argument '-L -lipopt -llapack -lblas -lpthread' can be 
used to link with the IPOPT libraries and dependencies.

 * On Windows, open the Ipopt/MSVisualStudio/v8-ifort/IpOpt-ifort.sln
solution. Build it. On output, you must get the following libraries:
   Ipopt/MSVisualStudio/v8-ifort/CoinBlas/Debug/CoinBlas.lib
   Ipopt/MSVisualStudio/v8-ifort/CoinLapack/Debug/CoinLapack.lib
   Ipopt/MSVisualStudio/v8-ifort/CoinMumps/Debug/CoinMumpsF90.lib
   Ipopt/MSVisualStudio/v8-ifort/Debug/CoinMumpsC.lib
   Ipopt/MSVisualStudio/v8-ifort/Debug/CoinMetis.lib
   Ipopt/MSVisualStudio/v8-ifort/Debug/IpOpt.lib
   Ipopt/MSVisualStudio/v8-ifort/lib/win32/debug/Ipopt.lib
   Ipopt/MSVisualStudio/v8/libIpopt/Release/libIpopt.lib
Then, make sure that the "LIB" environment variable contains a directory
leading to the "ifconsol.lib" library.

Once compiling the pre-requirements is done, execute the builder
and pray.

Author

2008 - Yann Collette
2009-2010 - Consortium Scilab - Digiteo - Yann Collette
2010 - Consortium Scilab - Digiteo - Michael Baudin

Licence

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

