// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function sparse_dh = ipopt_sparsity_h ( nv )
  // Computes the sparsity pattern of the Hessian matrix. 
  //
  // Calling Sequence
  //   sparse_dh = ipopt_sparsity_h ( nv )
  //
  // Parameters
  //   nv : a 1 x 1 matrix of floating point integers, the number of variables
  //   sparse_dh : a (nv^2) x 2 matrix of floating point integers, the sparsity pattern
  //
  // Description
  //   This function should be used to fill the sparse_dh argument of the 
  //   ipopt solver.
  //
  // Examples
  //   // 4 variables
  //   nv = 4
  //   sparse_dh = ipopt_sparsity_h ( nv )
  //   expected = [
  //     1 1
  //     1 2
  //     1 3
  //     1 4
  //     2 1
  //     2 2
  //     2 3
  //     2 4
  //     3 1
  //     3 2
  //     3 3
  //     3 4
  //     4 1
  //     4 2
  //     4 3
  //     4 4
  //   ]
  //
  // Authors
  // Michael Baudin, DIGITEO, 2010

  for i = 1 : nv
    for j = 1 : nv
      k = j + (i-1)*nv
      sparse_dh(k,1:2) = [i j]
    end
  end
endfunction

