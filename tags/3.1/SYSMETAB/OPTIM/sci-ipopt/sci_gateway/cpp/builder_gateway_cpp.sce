// ====================================================================
//  Copyright (C) 2010 - Digiteo - Yann Collette.
//  Copyright (C) 2010 - Digiteo - Michael Baudin
//  Copyright (C) 2012 - Yann Collette.
// This file is released into the public domain
// ====================================================================

list_add_inter      = [];
files_to_compile    = [];
cflags              = [];
ldflags             = [];

path_builder     = get_absolute_file_path('builder_gateway_cpp.sce');

list_add_inter   = ['sciipopt', 'sciipopt'];

files_to_compile = ['scilabjournal.cpp','sciipopt.cpp','call_function.cpp', ...
		    'manage_ipopt_params.cpp','scilabexception.cpp'];


if getos()=='Windows' then
  // rebuild parameters.lib
  exec(path_builder + 'rebuild_lib_windows.sci');
  // We need to use Visual studio 10.0
  if win64() then
    machine = 'X64';
  else
    machine = 'X86';
  end

  sci_vers = getversion('scilab');
  if sci_vers(2)>=4 then
    status = rebuild_lib_windows(SCI + filesep() + 'bin', path_builder, 'parameters', machine, '10.0');
  else
    status = rebuild_lib_windows(SCI + filesep() + 'bin', path_builder, 'parameters', machine, '9.0');
  end

  if ~status then
    printf('Error: problem while rebuilding parameters.lib\n');
    abort();
  end
  parameters_lib = 'parameters.lib';
end

if getos()=='Windows' then
  ////////////////////////////////////////////////////
  // Definition of base paths //
  // This part need to be modified if you work with //
  // other version of tools //
  ////////////////////////////////////////////////////

  base_dir = path_builder+".."+filesep()+".."+filesep()+"thirdparty";
  inc_base_dir = base_dir + filesep() + "Ipopt-" + VERSION + "-win" + filesep() + "include/coin";
  if win64() then
    lib_base_dir = base_dir + filesep() + "Ipopt-" + VERSION + "-win" + filesep() + "Win64" + filesep();
  else
    lib_base_dir = base_dir + filesep() + "Ipopt-" + VERSION + "-win" + filesep() + "Win32" + filesep();
  end

  /////////////////////////////
  // Definition of libraries //
  /////////////////////////////
  
  ipopt_lib = lib_base_dir + filesep() + 'IpOpt-vc10.lib';

  /////////////////////////////
  // Definitions of includes //
  /////////////////////////////
  
  ipopt_inc = ' -I ' + inc_base_dir;
  
  ////////////////////////////////////////
  // Definition of compilator variables //
  ////////////////////////////////////////

  cflags = '';
  cflags = cflags + ' -I ' + path_builder + ' -D__USE_DEPRECATED_STACK_FUNCTIONS__ ';
  cflags = cflags + ipopt_inc + ' ';
  cflags = cflags + ' -I' + SCI + '/modules/parameters/includes';

  ldflags = ipopt_lib + ' ' + parameters_lib;

  libs = '';
else
  /////////////////////////////////////////////////
  // Adapt the paths wrt your installation paths //
  /////////////////////////////////////////////////

  tools_path  = path_builder + '../../thirdparty/';
  ipopt_dir  = 'ipopt/';

  include_ipopt = tools_path + ipopt_dir + 'include/coin';
  
  ipopt_lib  = '-L' + tools_path + ipopt_dir + 'lib -lipopt -lcoinlapack -lcoinblas -lcoinmumps -lcoinmetis -lpthread';

  cflags     = '-ggdb -w -I. -I' + path_builder + ' -I' + include_ipopt + ' -D__USE_DEPRECATED_STACK_FUNCTIONS__ ';
  ldflags    = ipopt_lib;

  libs = '';
end

tbx_build_gateway('sci_ipopt', list_add_inter, files_to_compile, path_builder, libs, ldflags, cflags);

clear tbx_build_gateway;
