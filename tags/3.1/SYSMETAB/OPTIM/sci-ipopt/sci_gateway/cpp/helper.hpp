#ifndef HELPER_H
#define HELPER_H

#define IPOPT_ERROR if(_SciErr.iErr)	\
    {						\
      printError(&_SciErr, 0);			\
      return _SciErr.iErr;			\
    }
#endif
