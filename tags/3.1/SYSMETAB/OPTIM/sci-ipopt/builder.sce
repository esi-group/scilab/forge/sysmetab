// ====================================================================
// Copyright Yann COLLETTE 2009-2012
// This file is released into the public domain
// ====================================================================
mode(-1);
lines(0);
try
 getversion('scilab');
catch
 error(gettext('Scilab 5.0 or more is required.'));  
end;
// Uncomment to make a Debug version
//setenv("DEBUG_SCILAB_DYNAMIC_LINK","YES")
// ====================================================================
if ~with_module('development_tools') then
  error(msprintf(gettext('%s module not installed."),'development_tools'));
end
// ====================================================================
TOOLBOX_NAME = 'sci_ipopt';
TOOLBOX_TITLE = 'SciIPOpt';
// ====================================================================
toolbox_dir = get_absolute_file_path('builder.sce');
// ====================================================================
// Build thirdparty first

Build_64Bits = %f;
if getos()=='Windows' then
  Build_64Bits = win64();
else
  [v, opts] = getversion();
  Build_64Bits = ~(find(opts == 'x86') <> []);
end

Build    = %f;
VERSION  = '3.11.7';
UseProxy = %t;
proxy    = "on"; // For some old wget, you must set this to on and add a http_proxy= line into .wgetrc
//proxy    = "10.10.10.10:8080";
MSV_PATH = '""c:\Program Files (x86)\Microsoft Visual Studio 10.0\Common7\IDE\""';

if getos()~='Windows' then
  cd('thirdparty');
  printf('Downloading Ipopt ... please wait\n\n');
  if (UseProxy) then
    unix_w('wget --proxy=" + proxy + " http://www.coin-or.org/download/source/Ipopt/Ipopt-' + VERSION + '.tgz');
  else
    unix_w('wget http://www.coin-or.org/download/source/Ipopt/Ipopt-' + VERSION + '.tgz');
  end

  unix_w('tar xvfz Ipopt-' + VERSION + '.tgz');
  cd('Ipopt-' + VERSION + '/ThirdParty');
  cd('Blas');
  unix_w('./get.Blas');
  cd('../Lapack');
  unix_w('./get.Lapack');
  cd('../Metis');
  unix_w('./get.Metis');
  cd('../Mumps');
  unix_w('./get.Mumps');
  cd('../../');

  printf('\n\nConfiguring Ipopt ... please wait\n\n');

  if Build_64Bits then
    FLAGS='--with-pic';
  else
    FLAGS='';
  end

  unix_w('./configure --prefix=' + toolbox_dir + '/thirdparty/ipopt ' + FLAGS + ' --enable-static --disable-shared --enable-debug');

  printf('\n\nBuilding Ipopt ... please wait\n\n');
  unix_w('make');

  printf('\n\nInstalling Ipopt ... please wait\n\n');
  unix_w('make install');
  cd('../');
  unix_w('rm Ipopt-' + VERSION + '.tgz');
  cd('../');
else
  //cd('thirdparty/');
  //
  //printf('\n\nDownloading Ipopt ... please wait\n\n');
  //if (UseProxy) then
  //  unix_w(SCI + '\tools\curl\curl.exe -x ' + proxy + ' http://www.coin-or.org/download/source/Ipopt/Ipopt-' + VERSION + '.zip -o Ipopt-' + VERSION + '.zip');
  //else
  //  unix_w(SCI + '\tools\curl\curl.exe -o Ipopt-' + VERSION + '.zip http://www.coin-or.org/download/source/Ipopt/Ipopt-' + VERSION + '.zip');
  //end
  //  
  //printf('\n\nUncompressing Ipopt ... please wait\n\n');
  //unix_w(SCI + '\tools\zip\unzip.exe -o Ipopt-' + VERSION + '.zip');
  //unix_w('cd Ipopt-' + VERSION);
  //unix_w(SCI + '\tools\zip\unzip.exe -o ../Thirdparty.zip');
  //unix_w('cd ..');
  //
  //printf('\n\nBuilding the solution ... please wait\n\n')
  //cd('Ipopt-' + VERSION + '\Ipopt\MSVisualStudio\v8-ifort');
  //dos(MSV_PATH + '..\..\VC\vcvarsall.bat');
  //dos(MSV_PATH + 'devenv.exe IpOpt-vc10.sln /build Release');
end

// ====================================================================

tbx_builder_macros(toolbox_dir);
tbx_builder_gateway(toolbox_dir);
tbx_builder_help(toolbox_dir);
tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE Build_64Bits;

// ====================================================================
// Remove the ipopt build tree + install tree
if getos()~='Windows' then
  printf('\n\nCleaning Ipopt ... please wait\n\n');
  cd('thirdparty');
  unix_w('rm -rf ipopt');
  unix_w('rm -rf Ipopt-' + VERSION);
  cd('../');
end
