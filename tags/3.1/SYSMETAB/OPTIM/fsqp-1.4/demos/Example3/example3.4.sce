mode(-1)
//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 3 of the fsqp documentation

//[2] fsqp with  lists 
//=======================================================
//f_1 returns the objective (R-valued)
function f=f_1(x)
  f=x(1)*x(4)*sum(x(1:3))+x(3)
endfunction
function g=grf_1(x)
  s=sum(x(1:3));p=x(1)*x(4);
  g=[x(4)*s+p,p,1+p,x(1)*s];
endfunction

//g_1 returns non linear regular inequality constaint (R-valued)
function g=g_1(x)
//nonlinear ineq
  g=25-prod(x);
endfunction
function gr=grg_1(x)
  gr=-(prod(x)./x)'
endfunction

//h_1 returns non linear regular equality constaint (R-valued)
function h=h_1(x)
//nonlinear equal.
  h=norm(x)^2-40;
endfunction
function gr=grh_1(x)
  gr=2*x';
endfunction










// List of objectives: 1 reg, 0 SR
list_obj=list(list(f_1),list()) 
// List of constraints: 1 nonlin regular inequ and 1 non lin regular equality
list_cntr=list(list(g_1),list(),list(),list(),list(h_1),list())

//lists of gradients functions
list_grobj=list(list(grf_1),list())
list_grcn=list(list(grg_1),list(),list(),list(),list(grh_1),list())

x0=[1;5;5;1];
//fsqp parameters obtained by findparam
[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0);

modefsqp=100;iprint=1;miter=500;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

srpar=[nfsr,ncsrl,ncsrn];

bigbnd=1.e10;eps=1.e-7;epsneq=7.e-6;udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=ones(x0);
bu=5*bl;

x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)
