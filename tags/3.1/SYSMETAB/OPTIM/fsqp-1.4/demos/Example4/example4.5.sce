mode(-1)

//
// Copyright (C) ????-2008 - INRIA Francois Delebecque, Serge Steer
//
// This file is distributed under the same license as the Scilab package.
//
//Example 4 of the fsqp documentation
//fsqp with  lists 
//=======================================================
function ztx=z(t,x)
  k=1:9;
  tk=t*k;
  ztx=(cos(tk)*x(k))^2+(sin(tk)*x(k))^2
endfunction
function grad=grz(t,x)
  k=1:9;
  tk=t*k;
  ctk=cos(tk);
  stk=sin(t*k);
  grad=[diag(2*ctk*x(k))*ctk+diag(2*stk*x(k))*stk]
endfunction
//f_1 regular objective R-valued
//G_1 first SR constraint R^100 valued non linear inequality 
//G_2 2nd   SR constraint R^100 valued non linear inequality
//G_3 3rd   SR constraint R^150 valued non linear inequality
function f=f_1(x),f=x(10);endfunction
function g=grf_1(x), g=[0,0,0,0,0,0,0,0,0,1];endfunction
function g=G_1(x); g=(1-x(10))^2-z(t1,x);endfunction
function g=G_2(x); g=z(t2,x)-(1+x(10))^2;endfunction
function g=G_3(x); g=z(t3,x)-(x(10))^2;endfunction
function w=grG_1(x); w=[-grz(t1,x),-2*(1-x(10))*ones(t1)];endfunction
function w=grG_2(x); w=[grz(t2,x),-2*(1+x(10))*ones(t2)];endfunction
function w=grG_3(x); w=[grz(t3,x),-2*x(10)*ones(t3)];endfunction

r=100;
t1=%pi*0.025*((1:r)-1)';
t2=%pi*0.025*((1:r)-1)';
t3=%pi*0.25*(1.2+0.2*((1:1.5*r)-1))';
t=[t1;t2;t3];


list_obj=list(list(f_1),list()); 

list_cntr=list(list(),list(G_1,G_2,G_3),list(),list(),list(),list());

list_grobj=list(list(grf_1),list());

list_grcn=list(list(),list(grG_1,grG_2,grG_3),list(),list(),list(),list());

x0=[0.1*ones(9,1);1];

[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0);

modefsqp=100;iprint=1;miter=500;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

srpar=[nfsr,ncsrl,ncsrn];

bigbnd=1.e10;eps=1.e-7;epsneq=0.e0;udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);

x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)

