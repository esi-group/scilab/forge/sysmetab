// builder for a shared library for example3.c functions 
//=======================================================
// functions to be added to the call table 
link_name =["obj71","cntr71","grob71","grcn71"];
flag  = "c";		// ext1c is a C function 
files = ['example3.o' ];   // objects files for ext1c 
libs  = [];		// other libs needed for linking 

// the next call generates files (Makelib,loader.sce) used
// for compiling and loading ext1c and performs the compilation

ilib_for_link(link_name,files,libs,flag);

