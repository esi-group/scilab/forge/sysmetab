#include "cfsqpusr.h"

void
obj(nparam,j,x,fj,cd)
     int nparam,j;
     double *x,*fj;
     void *cd;
{
  *fj=(1.e0/3.e0)*pow(x[0],2.e0)+pow(x[1],2.e0)+0.5e0*x[0];
  return;
}

void
grob(nparam,j,x,gradfj,dummy,cd)
     int nparam,j;
     double *x,*gradfj;
     void (* dummy)(),*cd;
{
  gradfj[0]=(2.e0/3.e0)*x[0]+0.5e0;
  gradfj[1]=2.e0*x[1];
  return;
}

void  
cntr(nparam,j,x,gj,cd)
     int nparam,j;
     double *x,*gj;
     void *cd;
{
  double y;

  y=(j-1)/100.e0;
  *gj=pow((1.e0-pow(y*x[0],2.e0)),2.e0)-x[0]*y*y-pow(x[1],2.e0)
    +x[1];
  return;
}

void
grcn(nparam,j,x,gradgj,dummy,cd)
     int nparam,j;
     double *x,*gradgj;
     void (* dummy)(),*cd;
{
  double y;

  y=(j-1)/100.e0;
  gradgj[0]=-4.e0*(1.e0-pow(y*x[0],2.e0))*y*y*x[0]-y*y;
  gradgj[1]=-2.e0*x[1]+1.e0;
  return;
}
