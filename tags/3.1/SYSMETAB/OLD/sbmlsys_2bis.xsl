<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Thu Feb  8 09:50:47 CET 2007
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de mettre les informations de transition
    entre les atomes de carbones sous une forme exploitable par la suite, en les
    d�crivant � l'aide d'�l�ments <smtb:carbons> que l'on inclut � l'int�rieur
    de chaque �l�ment <speciesReference> dans les �lements <listOfReactants>
    et <listOfProducts>. Par exemple pour la r�action 
    
                              s1+s2->s3 
    
    avec des transitions des atomes de carbones �crites "� la Wiechert" 
    
                             AB + DC -> ADBC 
    cela donne :
    
    <reaction>
        <listOfReactants>
            <speciesReference species="s1">
                <smtb:carbon position="1" destination="1" species="s3"/>
                <smtb:carbon position="2" destination="3" species="s3"/>
            </speciesReference>
            <speciesReference species="s2">
                <smtb:carbon position="1" destination="2" species="s3"/>
                <smtb:carbon position="2" destination="4" species="s3"/>
            </speciesReference>
        </listOfReactants>
        <listOfProducts>
            <speciesReference species="s3">
                <smtb:carbon position="1" destination="1" species="s1"/>
                <smtb:carbon position="2" destination="1" species="s2"/>
                <smtb:carbon position="3" destination="2" species="s1"/>
                <smtb:carbon position="4" destination="2" species="s2"/>
            </speciesReference>
        </listOfProducts>
    </reaction>
-->

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"    
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml">
    
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="*|@*|text()|comment()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="sbml:speciesReference">
        <xsl:element name="sbml:speciesReference">
            <xsl:apply-templates select="@*"/>
                <xsl:call-template name="iteration">
                    <xsl:with-param name="i" select="'1'"/>
                </xsl:call-template>
        </xsl:element>
    </xsl:template>
    
    <xsl:template name="iteration">
        <xsl:param name="i"/>
        
        <xsl:if test="$i&lt;=string-length(@carbons)">
            
            <xsl:variable name="c" select="substring(@carbons,$i,1)"/>
            
            <!-- Attention, on doit num�roter les carbones de droite � gauche si on veut avoir
                une correspondance directe entre les num�ros des esp�ces marqu�es et le nombre
                binaire repr�sentant le marquage. -->
            
            <smtb:carbon position="{string-length(@carbons)-($i)+1}">
            
                <!-- Ici on balaye les sbml:listOfProducts/sbml:speciesReference si 
                     le contexte appelant �tait un sbml:listOfReactants/sbml:speciesReference
                     et vice-versa. Cela se fait grace � l'expression XPath (un peu compliqu�e...)
                     ci dessous : -->
                     
                <xsl:for-each select="../../*[name()!=name(current()/..)]/sbml:speciesReference">
                    <xsl:if test="contains(@carbons,$c)">
                        <xsl:attribute name="destination">
                            <xsl:value-of select="string-length(substring-after(@carbons,$c))+1"/>
                        </xsl:attribute>
                        
                        <!-- L'attribut "occurence" sert � diff�rencier les deux versions d'une esp�ce quand
                            la stoechiom�trie est sup�rieure � 1. -->
                        
                        <xsl:attribute name="occurence">
                            <xsl:value-of select="count(preceding-sibling::sbml:speciesReference)+1"/>
                        </xsl:attribute>
                        <xsl:copy-of select="@species"/>
                    </xsl:if>
                </xsl:for-each>
                
            </smtb:carbon>    
            
            <xsl:call-template name="iteration">
                <xsl:with-param name="i" select="($i)+1"/>
            </xsl:call-template>
            
        </xsl:if>
  
    </xsl:template>

</xsl:stylesheet>
