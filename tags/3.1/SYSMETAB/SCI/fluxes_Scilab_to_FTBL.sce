nfluxes=size(flux.names,'*')/2;
printf("FLUXES\n");
printf("\tNET\n");
printf("\t\tNAME\tFCD\tVALUE(F/C)\tED_WEIGHT\tLOW(F)\tINC(F)\tUP(F)\n");
for i=1:nfluxes
    if ftype(i)=='f' then
        printf("\t\t%s\t%s\t%f\n",strsubst(flux.names(i),'_f',''),'F',flux.values(i));
    end
    if ftype(i)=='c' then
        printf("\t\t%s\t%s\t%f\n",strsubst(flux.names(i),'_f',''),'C',flux.values(i));
    end
    if ftype(i)=='d' then
        printf("\t\t%s\t%s\n",strsubst(flux.names(i),'_f',''),'D');
    end
end

printf("\tXCH\n");
printf("\t\tNAME\tFCD\tVALUE(F/C)\tED_WEIGHT\tLOW(F)\tINC(F)\tUP(F)\n");

for i=nfluxes+1:2*nfluxes
    if ftype(i)=='f' then
        printf("\t\t%s\t%s\t%f\n",strsubst(flux.names(i),'_b',''),'F',flux.values(i));
    end
    if ftype(i)=='c' then
        printf("\t\t%s\t%s\t%f\n",strsubst(flux.names(i),'_b',''),'C',flux.values(i));
    end
    if ftype(i)=='d' then
        printf("\t\t%s\t%s\n",strsubst(flux.names(i),'_b',''),'D');
    end
end
