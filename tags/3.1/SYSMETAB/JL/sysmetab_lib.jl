function s_full(ij,v,n,m)
  full(sparse(int(ij[:,1]),int(ij[:,2]),float64(vec(v)),n,m))
end

function s_sparse(ij,v,n,m)
	sparse(int(ij[:,1]),int(ij[:,2]),float64(vec(v)),n,m)
end

function umf_lufact(a)
  lufact(a)
end

function umf_ludel(a)
end

function umf_lusolve(a,b,dir="Ax=b")
  if dir=="Ax=b"
    a\full(b)
  else
    a'\full(b)
  end
end

function spdiag(a)
  spdiagm(a)
end
