// Cumom�res en entr�e
x1_input=D1*[s1_input];
// Flux observ�s
vobs=flux_obs(:,2:2);
// Matrice de pond�ration de l'erreur sur les flux observ�s
sigma=sparse(diag(flux_obs(:,1)));
// Marquages observ�s
yobs=[s2_obs(:,2:2);s48_obs(:,2:2)];
// Matrice de pond�ration de l'erreur sur les marquages observ�s
alpha=sparse(diag([s2_obs(:,1);s48_obs(:,1)]));
// Calcul du sous-espace "stoechiom�trique"
[A,b]=fluxSubspace(flux_obs([1;3;4;5;6;7;8;9;10;11;12;13;14;15;16],2));
v=optimize();
re1=v(1);
re2=v(2);
re3=v(3);
re4=v(4);
re5=v(5);
re6=v(6);
re7=v(7);
re8=v(8);
re9_f=v(9);
re9_b=v(10);
re16=v(11);
re18=v(12);
re19=v(13);
re26=v(14);
re30=v(15);
re31=v(16);
re34=v(17);
re38_f=v(18);
re38_b=v(19);
re39=v(20);
re40_f=v(21);
re40_b=v(22);
re41_f=v(23);
re41_b=v(24);
re44=v(25);
re45=v(26);
re47=v(27);
re48=v(28);
re49=v(29);
re50=v(30);
re51=v(31);
re52=v(32);
re53=v(33);
re54=v(34);
re56=v(35);
re57=v(36);
re58=v(37);
re59=v(38);
re60_f=v(39);
re60_b=v(40);
re61_f=v(41);
re61_b=v(42);
re62_f=v(43);
re62_b=v(44);
re63_f=v(45);
re63_b=v(46);
re64_f=v(47);
re64_b=v(48);
re65_f=v(49);
re65_b=v(50);
re66=v(51);
re67_f=v(52);
re67_b=v(53);
re68_f=v(54);
re68_b=v(55);
re69=v(56);
re70=v(57);
// Calcul des cumom�res apr�s optimisation
[x1]=solveCumomers(v,x1_input);
y=C1*x1;
s2_obs(1:6,3:3)=y(1:6,1:1);
s48_obs(1:4,3:3)=y(7:10,1:1);
