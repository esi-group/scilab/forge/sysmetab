global LANGUAGE
// Default language is the one used at installation time
mode(-1)
lines(0)
path=get_absolute_file_path("builder.sce");
chdir(path)
disp('Installing SYSMETAB')
if LANGUAGE=="eng"
	TCL_SetVar('lang','english')
  install_message="Installation of SYSMETAB was successful.\n"+...
                  "The next time you launch Scilab, the SYSMETAB menu "+...
                  "will be loaded automatically.\n";
elseif LANGUAGE=="fr"
	TCL_SetVar('lang','french')
  install_message="L''installation de SYSMETAB est r�ussie.\n"+...
                  "La prochaine fois que Scilab sera lanc�, le menu SYSMETAB "+...
                  "sera charg� automatiquement.\n"
end
TCL_SetVar('SCI',SCI)
TCL_SetVar('SCIHOME',SCIHOME)
TCL_SetVar('SYSMETAB',path)
TCL_EvalFile('TCL/build.tcl')

exec('OPTIM/fsqp-1.3/builder.sce',-1)

if havewindow()
	TCL_EvalStr('mess ""'+install_message+'""');
	exec('loader.sce',-1)
else
	printf("\n\n"+install_message+"\n\n")	
    quit
end

