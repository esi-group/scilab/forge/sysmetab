# Installation of the loading part in the user's startup file (.scilab)

insert  [file join $SCIHOME .scilab] "*SYSMETAB*loader.sce*" "exec(\"[file join $SYSMETAB loader.sce]\",-1) // Load SYSMETAB"
insert  [file join $SCIHOME .scilab] "*OPTIM*loader.sce*" "exec(\"[file join $SYSMETAB OPTIM fsqp-1.3 loader.sce]\",-1) // Load FSQP"
