<?xml version="1.0" encoding="ISO-8859-1"?>

<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   hu Mar 27 14:52:34 CET 2008
    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer diff�rentes fonctions sp�cifiques
	d'un mod�le donn� sous forme SBML, �dit� dans CellDesigner. On prend ici l'exemple
	du "Branching Network" de l'article de N. Isermann, W. Wiechert :
	
	Metabolic isotopomer labeling systems. Part II: structural flux identifiability
	analysis.

            A_out
	         |
			 |v6
			 |
			 v
	_________A _______
	|        |       |
	|        |v2     |
	|v3      |       |
	|        v       |
	|        D       |v1
	|        |       |
	|        |v4     |
	|        |       |
	|        v       |
	_______> F <______
	         |
			 |v5
			 |
			 v
			 G

    Ici on fait abstraction des identificateurs originaux des cumom�res et des flux. On a dans cet
	exemple (les cumom�res des m�tabolites d'entr�e et de sortie ne font pas partie de l'�tat) :
	
	x1=[A$01;A$10;D$1;F$01;F$10];
	x2=[A$11;F$11];
	
	x1_input=[A_out$01;A_out$10];
	x2_input=[A_out$11];
	
	v=[v1;v2;v3;v4;v5;v6];
    
    Ici on g�n�re en fait des fonctions d�crites en MathML, il faut enchainer avec
    une derni�re transformation pour avoir le code Scilab (sbml_sys_text.xsl) :


*** 1 - une fonction Scilab permettant
    calculer les diff�rentes fractions des cumom�res en fonction des flux
    et des fractions des cumom�res des entr�es. 
	On g�n�re aussi le code des blocs non nuls de la jacobienne des �tats par rapport � v
	
	
	Exemple pour le "branching network"
    du papier "part II" de Wiechert :
	
	function [x1,x2,dx1_dv,dx2_dv]=solveCumomers(v,x1_input,x2_input)

		n1=5;

		// Cumom�res de poids 1

		M1_ijv=[1,1,-(v(1)+v(2)+v(3))
		2,2,-(v(1)+v(2)+v(3))
		3,2,v(2)
		3,1,v(2)
		3,3,-(v(4)+v(4))
		4,1,v(1)
		4,2,v(3)
		4,3,v(4)
		4,4,-v(5)
		5,2,v(1)
		5,1,v(3)
		5,3,v(4)
		5,5,-v(5)];
		M1=sparse(M1_ijv(:,1:2),M1_ijv(:,3),[n1,n1]);

		b1_ijv=[1,1,v(6).*x1_input(1,:)
		2,1,v(6).*x1_input(2,:)];
		b1_1=s_full(b1_ijv(:,1:2),b1_ijv(:,3),[n1,1]);

		[M1_handle,M1_rank]=lufact(M1);
		x1=lusolve(M1_handle,-[b1_1]);

		dg1_dv_ijv=[1,6,x1_input(1,:)
		1,1,-x1(1,:)
		1,2,-x1(1,:)
		1,3,-x1(1,:)
		2,6,x1_input(2,:)
		2,1,-x1(2,:)
		2,2,-x1(2,:)
		2,3,-x1(2,:)
		3,2,x1(2,:)
		3,2,x1(1,:)
		3,4,-x1(3,:)
		3,4,-x1(3,:)
		4,1,x1(1,:)
		4,3,x1(2,:)
		4,4,x1(3,:)
		4,5,-x1(4,:)
		5,1,x1(2,:)
		5,3,x1(1,:)
		5,4,x1(3,:)
		5,5,-x1(5,:)];
		dg1_dv_1=s_full(dg1_dv_ijv(:,1:2),dg1_dv_ijv(:,3),[n1,6]);
		dx1_dv(:,:,1)=lusolve(M1_handle,-dg1_dv_1);

		ludel(M1_handle);

		n2=2;

		// Cumom�res de poids 2

		M2_ijv=[1,1,-(v(1)+v(2)+v(3))
		2,1,v(1)
		2,1,v(3)
		2,2,-v(5)];
		M2=sparse(M2_ijv(:,1:2),M2_ijv(:,3),[n2,n2]);

		b2_ijv=[1,1,v(6).*x2_input(1,:)
		2,1,v(4).*x1(3,:).*x1(3,:)];
		b2_1=s_full(b2_ijv(:,1:2),b2_ijv(:,3),[n2,1]);

		[M2_handle,M2_rank]=lufact(M2);
		x2=lusolve(M2_handle,-[b2_1]);

		dg2_dv_ijv=[1,6,x2_input(1,:)
		1,1,-x2(1,:)
		1,2,-x2(1,:)
		1,3,-x2(1,:)
		2,1,x2(1,:)
		2,3,x2(1,:)
		2,4,x1(3,:).*x1(3,:)
		2,5,-x2(2,:)];
		dg2_dv_1=s_full(dg2_dv_ijv(:,1:2),dg2_dv_ijv(:,3),[n2,6]);
		db2_dx1_ijv=[2,3,x1(3,:).*v(4)
		2,3,x1(3,:).*v(4)];
		db2_dx1_1=sparse(db2_dx1_ijv(:,1:2),db2_dx1_ijv(:,3),[n2,n1]);
		dx2_dv=zeros(n2,6,1);
		dx2_dv(:,:,1)=lusolve(M2_handle,-(dg2_dv_1+db2_dx1_1*dx1_dv(:,:,1)));
		ludel(M2_handle);
	endfunction

2 *** - Une fonction permettant de calculer la fonction cout du probl�me d'identification
	ainsi que son gradient (sans utiliser d'�tat adjoint; on utilise directement les jacobiennes
	des �tats par rapport � v)


	function [cost,grad]=costAndGrad(v)
		[x1,x2,dx1_dv,dx2_dv]=solveCumomers(v,x1_input,x2_input);
		e_label=(C1*x1+C2*x2)-yobs;
		e_flux=E*v-vobs;
		cost=0.5*(sum(delta.*e_flux.^2)+sum(alpha(:,1).*e_label(:,1).^2));
		grad=(delta.*e_flux)'*E+(alpha(:,1).*e_label(:,1))'*(C1*dx1_dv(:,:,1)+C2*dx2_dv(:,:,1));
	endfunction


3 *** - Les "matrices d'observation" correspondant aux cumom�res et aux fluxs observ�s

	1) isotopom�res en entr�e -> contributions aux cumom�res en entr�e, matrices D1,D2, etc.

	Exemple ici pour A_out (seul m�tabolite d'entr�e), on pr�cise dans les notes (dans CellDesigner)
	
	LABEL_INPUT 01,10,11

	ce qui signifie qu'on donne en entr�e les trois isotopom�res dans un vecteur A_out_input
	qui contient en fait [A_out#01;A_out#10;A_out#11]. Pour le calcul des cumom�res, on a besoin
	de connaitre les contributions aux vecteurs d'entr�es x1_input (cumom�res de poids 1) et
	x2_input (cumom�res de poids 2). On g�n�re donc les matrices D1 et D2 telles que
	
	x1_input=D1*A_out_input;
	x2_input=D2*A_out_input;


	// Matrices d'entr�e
	D1_ijv=[1,1,1
	1,3,1
	2,2,1
	2,3,1];
	D1=sparse(D1_ijv(:,1:2),D1_ijv(:,3),[2,3]);
	D2_ijv=[1,3,1];
	D2=sparse(D2_ijv(:,1:2),D2_ijv(:,3),[1,3]);
	
	2) cumom�res calcul�s -> isotopom�res observ�s

	Exemple ici pour F, dont on observe les cumom�res F$10,F$01,F$11, on a dans les notes

	LABEL_MEASUREMENT 1x,x1,11
	
	avec la convention du logiciel 13CFlux (1x=cumom�re 10, x1=cumom�re 01), on a besoin de
	pouvoir calculer ces observations en fonction des vecteurs des cumom�res calcul�s x1,x2,
	on g�n�re donc les matrices C1 et C2 telles que
	
	y=C1*x1+C2*x2

	et ici y contient finalement [F$10;F$01;F$11] car il n'y a pas d'autre observation dans 
	le mod�le.

	// Matrices d'observation (cumom�res)
	C1_ijv=[1,5,1
	2,4,1];
	C1=sparse(C1_ijv(:,1:2),C1_ijv(:,3),[3,5]);
	C2_ijv=[3,2,1];
	C2=sparse(C2_ijv(:,1:2),C2_ijv(:,3),[3,2]);


	3) flux -> flux observ�s
	
	Dans le Branching Network, l'exemple utilis�, on observe v5,v6. Dans le mod�le CellDesigner
	ces deux r�actions/flux sont identifi�s comme TRANSPORT ou KNOWN_TRANSITION_OMMITED


	// Matrice d'observation (flux)
	E_ijv=[1,5,1
	2,6,1];
	E=sparse(E_ijv(:,1:2),E_ijv(:,3),[2,6]);

    
    3 - une fonction Scilab [A,b]=fluxSubspace(_fluxes) calculant le param�trage du
    sous-espace des flux v v�riant
    
        Nv=0 
        Hv=w

    N est la matrice stoechiom�trique et Hv=w exprime des contraintes d'�galit�
    sur les flux, typiquement utilis�e lorsque des flux ont des valeurs connues.

	La partie Hv=w peut etre changee dans l'interface. La matrice _fluxes a deux colonnes
	dont la deuxi�me pr�cise si on impose un flux ou pas. La valeur est par d�faut -1 sinon
	on met une valeur positive ou nulle impos�e. La matrice H et le vecteur w sont ainsi r�cup�r�s
	apr�s appel � la fonction fluxConstraints (faisant partie de la biblioth�que de macros
	SYSMETAB).
        

	Ici on a finalement 
	
		A=[N;H] et b=[0;w]

	function [A,b,N,H,w]=fluxSubspace(_fluxes)
		// Construction des matrices (dont la matrice de stoechiom�trie) permettant
		// de calculer le changement de variable affine des flux.
		N=zeros(3,6);
		// A
        N_ijv=[1,1,-1
        1,2,-1
        1,3,-1
        1,6,1
        2,2,1
        2,2,1
        2,4,-1
        2,4,-1
        3,1,1
        3,3,1
        3,4,1
        3,5,-1];
		// Flux impos�s dans l'interface
		[H,w]=fluxConstraints(_fluxes);
		b=[zeros(3,1);w];
		A=[N;H];
	endfunction
    
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"
    xmlns:math="http://exslt.org/math"
        version="1.0"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
       
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>
 
    <xsl:param name="file"/>
    <xsl:param name="verbose">no</xsl:param>
    <xsl:param name="type">stationnaire</xsl:param>
    <xsl:param name="experiences">1</xsl:param>

    <xsl:include href="common_gen_mathml.xsl"/> <!-- templates communes stationnaire/dynamique -->

	<xsl:key name="reaction" match="sbml:reaction" use="@id"/>
    <xsl:key name="species" match="sbml:species" use="@id"/>
    <xsl:key name="input-cumomer" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="cumomer" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>

    <xsl:strip-space elements="*"/>


	<!-- D�termination du poids maximum n�cessaire : on regarde le poids le plus grand dans les
		cumom�res contribuant aux observations -->

	<xsl:variable name="weight" select="math:max(//sbml:species[@type='intermediate']/smtb:measurement/smtb:cumomer-contribution/@weight)"/>
    
    <xsl:template match="/">
        <carbon-labeling-system>
          <xsl:apply-templates/>
        </carbon-labeling-system>
    </xsl:template>

     <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
     </xsl:template>

     <xsl:template match="sbml:model">

        <!-- Construction de la fonction solveCumomers (calcul des cumom�res connaissant les cumom�res 
		     en entr�e et les flux) -->

        <smtb:function>
	        <m:ci>solveCumomers</m:ci>
	        <smtb:input>
		        <m:list>
			        <m:ci>v</m:ci>
	                <xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    			        <m:ci>
                            <xsl:value-of select="concat('x',@weight,'_input')"/>
                        </m:ci>
    	            </xsl:for-each>
		        </m:list>
	        </smtb:input>
	        <smtb:output>
                <m:list>
                    <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    			        <m:ci>
                            <xsl:value-of select="concat('x',@weight)"/>
                        </m:ci>
                    </xsl:for-each>
                    <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    			        <m:ci>
                            <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                        </m:ci>
                    </xsl:for-each>
	            </m:list>
            </smtb:output>
	        <smtb:body>
	            <xsl:apply-templates select="smtb:listOfIntermediateCumomers"/>
	        </smtb:body>
        </smtb:function>

        <!-- Construction de la fonction calculant le cout et son gradient -->

        <smtb:function>
	        <m:ci>costAndGrad</m:ci>
	        <smtb:input>
		        <m:list>
			        <m:ci>v</m:ci>
		        </m:list>
	        </smtb:input>`
	        <smtb:output>
		        <m:list>
			        <m:ci>cost</m:ci>
			        <m:ci>grad</m:ci>
			        <m:ci>label_error</m:ci>
		        </m:list>
	        </smtb:output>
	        <smtb:body>
		        <m:apply>
			        <m:eq/>
                    <m:list separator=",">
                        <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    				        <m:ci>
                                <xsl:value-of select="concat('x',@weight)"/>
                            </m:ci>
                        </xsl:for-each>
                        <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    				        <m:ci>
                                <xsl:value-of select="concat('dx',@weight,'_dv')"/>
                            </m:ci>
                        </xsl:for-each>
	                </m:list>
			        <m:apply>
				        <m:fn>
					        <m:ci>solveCumomers</m:ci>
				        </m:fn>
				        <m:ci>v</m:ci>
                        <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
    				        <m:ci>
                                <xsl:value-of select="concat('x',@weight,'_input')"/>
                            </m:ci>
                        </xsl:for-each>
			        </m:apply>
		        </m:apply>
		        <m:apply>
			        <m:eq/>
			        <m:ci>e_label</m:ci>
			        <m:apply>
				        <m:minus/>
				        <m:apply>
					        <m:plus/>
					        <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
						        <m:apply>
							        <m:times/>
 		   				            <m:ci>
	                                    <xsl:value-of select="concat('C',@weight)"/>
	                                </m:ci>
 		   				            <m:ci>
	                                    <xsl:value-of select="concat('x',@weight)"/>
	                                </m:ci>
						        </m:apply>
                            </xsl:for-each>						
				        </m:apply>
				        <m:ci>yobs</m:ci>
			        </m:apply>
		        </m:apply>
		        <m:apply>
			        <m:eq/>
			        <m:ci>e_flux</m:ci>
			        <m:apply>
				        <m:minus/>
				        <m:apply>
					        <m:times/>
					        <m:ci>E</m:ci>
					        <m:ci>v</m:ci>
				        </m:apply>
				        <m:ci type="matrix">vobs</m:ci>
			        </m:apply>
		        </m:apply>
				
		        <m:apply>
			        <m:eq/>
			        <m:ci>flux_error</m:ci>
				    <m:apply>
					    <m:plus/>
					    <m:apply> <!-- contribution des observations des flux -->
						    <m:times/>
						    <m:apply>
							    <m:fn>
								    <m:ci>sum</m:ci>
							    </m:fn>
							    <m:apply>
								    <m:times type="array"/>
								    <m:ci>delta</m:ci>
								    <m:apply>
									    <m:power type="array"/>
									    <m:ci type="matrix">e_flux</m:ci>
									    <m:cn>2</m:cn>
								    </m:apply>
							    </m:apply>
						    </m:apply>
					    </m:apply>
					</m:apply>
				</m:apply>
				
				<m:apply>
			        <m:eq/>
			        <m:ci>label_error</m:ci>
				    <m:apply>
					    <m:plus/>
					    <xsl:call-template name="iterate-cost"/> <!-- contributions des observations  des marquages -->
			        </m:apply>
		        </m:apply>

		        <m:apply>
			        <m:eq/>
			        <m:ci>cost</m:ci>
			        <m:apply>
				        <m:times/>
				        <m:cn>0.5</m:cn>
				        <m:apply>
					        <m:plus/>
							<m:ci>flux_error</m:ci>
							<m:ci>label_error</m:ci>
						</m:apply>
			        </m:apply>
		        </m:apply>
				
		        <m:apply>
			        <m:eq/>
			        <m:ci>grad</m:ci>
			        <m:apply>
				        <m:plus/>
				        <m:apply> <!-- contribution des observations des flux -->
					        <m:times/>
					        <m:apply>
						        <m:transpose/>
						        <m:apply>
							        <m:times type="array"/>
							        <m:ci>delta</m:ci>
							        <m:ci>e_flux</m:ci>
						        </m:apply>
					        </m:apply>
					        <m:ci>E</m:ci>
				        </m:apply>
				        <xsl:call-template name="iterate-grad"/> <!-- contributions des observations  des marquages -->
			        </m:apply>
		        </m:apply>
				
				<m:apply>
			        <m:eq/>
			        <m:ci>grad</m:ci>
					<m:apply>
						<m:transpose/>
						<m:ci>grad</m:ci>
					</m:apply>
		        </m:apply>
				
	        </smtb:body>
        </smtb:function>

        <!-- Fonction de calcul de A et B pour les contraintes sur les fluxs Av=b (dans common_gen_mathml.xsl)-->

 		<xsl:call-template name="fluxSubspace"/>

        <!-- Construction des matrices d'observation (dans common_gen_mathml.xsl)-->

        <smtb:comment>Matrices d'observation (cumom�res)</smtb:comment>

	    <xsl:call-template name="construct_matrix_C"/>

        <smtb:comment>Matrice d'observation (flux)</smtb:comment>

        <xsl:call-template name="construct_matrix_E"/> 

        <!-- Construction des matrices d'entr�e, permettant d'obtenir la contribution sur les cumom�res des isotopom�res en entr�e. -->

        <smtb:comment>Matrices d'entr�e</smtb:comment>

        <xsl:call-template name="construct_matrix_D"/> 

        <!-- Matrice des noms des fluxs, Matrice des ids des fluxs -->

        <xsl:call-template name="names_and_ids_fluxes"/>

        <!-- Script pour le calcul direct des cumom�res. Ce script est ex�cut� dans l'application XMLlab, d�s qu'un flux est chang� ou
	         d�s que l'on sort de l'optimisation avec des nouvelles valeurs de fluxs. -->

        <smtb:script href="{$file}_direct.sce">

	        <!-- En fonction des valeurs des fluxs, les free fluxes peuvent changer, en g�n�ral c'est le cas quand
			     le statut de certaines contraintes change (satur�es ou inactives). La fonction adjustFluxesTab
			     (d�finie dans SYSMETAB/SCI/sysmetab4.sci) a pour but de rendre actives les cases du tableau des
			     fluxs correspondant � des free fluxes. On peut alors les faire varier avec <ctrl>+fl�che droite
			     ou gauche. -->

	        <smtb:comment>Ajustement du controle des free fluxes</smtb:comment>

	        <m:apply>
		        <m:eq/>
		        <m:ci>nothing</m:ci>
		        <m:apply>
			        <m:fn>
				        <m:ci>adjustFluxesTab</m:ci>
			        </m:fn>
			        <m:ci>fluxes</m:ci>
		        </m:apply>
	        </m:apply>

	        <smtb:comment>Cumom�res en entr�e, calcul�s � partir des isotopom�res des m�tabolites d'entr�e.</smtb:comment>

	        <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
		        <m:apply>
			        <m:eq/>
			        <m:ci>
				        <xsl:value-of select="concat('x',@weight,'_input')"/>
			        </m:ci>
			        <m:apply>
				        <m:times/>
				        <m:ci>
					        <xsl:value-of select="concat('D',@weight)"/>
				        </m:ci>
				        <m:list separator=";">
					        <xsl:for-each select="../../sbml:listOfSpecies/sbml:species[smtb:input]">
						        <m:ci>
							        <xsl:value-of select="concat(@id,'_input')"/>
						        </m:ci>
					        </xsl:for-each>
				        </m:list>
			        </m:apply>
		        </m:apply>
	        </xsl:for-each>

	        <smtb:comment>Calcul direct des cumom�res</smtb:comment>

	        <m:apply>
		        <m:eq/>
		        <m:ci>v</m:ci>
		        <m:apply>
			        <m:selector/>
			        <m:ci type="matrix">fluxes</m:ci>
			        <m:cn>:</m:cn>
			        <m:cn>1</m:cn>
		        </m:apply>
	        </m:apply>
		
	        <m:apply>
		        <m:eq/>
		        <m:list separator=",">
                    <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
                        <m:ci>
					        <xsl:value-of select="concat('x',position())"/>
    			        </m:ci>
                    </xsl:for-each>
		        </m:list>
		        <m:apply>
			        <m:fn>
				        <m:ci>solveCumomers</m:ci>
			        </m:fn>
			        <m:ci>v</m:ci>				
        	        <xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers[(@weight&lt;=$weight) and (smtb:cumomer)]">
				        <m:ci>
					        <xsl:value-of select="concat('x',@weight,'_input')"/>
				        </m:ci>
			        </xsl:for-each>										
		        </m:apply>
	        </m:apply>

	        <smtb:comment>Calcul des observations en fonction des cumom�res</smtb:comment>

	        <m:apply>
		        <m:eq/>
		        <m:ci>y</m:ci>
		        <m:apply>
			        <m:plus/>
			        <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
				        <m:apply>
					        <m:times/>
					        <m:ci>
						        <xsl:value-of select="concat('C',@weight)"/>
					        </m:ci>
					        <m:ci>
						        <xsl:value-of select="concat('x',@weight)"/>
					        </m:ci>
				        </m:apply>
			        </xsl:for-each>
		        </m:apply>
	        </m:apply>

	        <smtb:comment>Remplissage des tableaux des cumom�res dans l'interface</smtb:comment>

	        <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
		        <m:apply>
			        <m:eq/>
			        <m:ci>
                        <xsl:value-of select="concat('cumomers_weight_',@weight)"/>
			        </m:ci>
			        <m:ci>
				        <xsl:value-of select="concat('x',@weight)"/>
			        </m:ci>
		        </m:apply>
	        </xsl:for-each>

	        <smtb:comment>Remplissage des tableaux des observations dans l'interface</smtb:comment>

	        <xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
		        <xsl:variable name="n" select="count(smtb:measurement)"/>
		        <xsl:variable name="start" select="count(preceding::smtb:measurement)"/>
		        <m:apply>
			        <m:eq/>
			        <m:apply>
				        <m:selector/>
				        <m:ci type="matrix">
                	        <xsl:value-of select="concat(@id,'_obs')"/>
				        </m:ci>
				        <m:cn>
					        <xsl:value-of select="concat('1:',$n)"/>
				        </m:cn>
				        <m:cn>
					        <xsl:value-of select="concat(3,':3:',3*$experiences)"/>
				        </m:cn>
			        </m:apply>
			        <m:apply>
				        <m:selector/>
				        <m:ci type="matrix">y</m:ci>
				        <m:cn>
					        <xsl:value-of select="concat(1+$start,':',$n+$start)"/>
				        </m:cn>
				        <m:cn>
					        <xsl:value-of select="concat('1:',$experiences)"/>
            	        </m:cn>
			        </m:apply>
		        </m:apply>
	        </xsl:for-each>

	        <smtb:comment>R�cup�rations des donn�es pour le calcul de la fonction cout</smtb:comment>

	        <smtb:comment>Flux observ�s</smtb:comment>
	        <m:apply>	
		        <m:eq/>
		        <m:ci>vobs</m:ci>
		        <m:apply>
			        <m:selector/>
			        <m:ci type="matrix">flux_obs</m:ci>
			        <m:cn>:</m:cn>
			        <m:cn>2</m:cn>
		        </m:apply>						
	        </m:apply>

	        <smtb:comment>Matrice de pond�ration de l'erreur sur les flux observ�s</smtb:comment>

	        <m:apply>	
		        <m:eq/>
		        <m:ci>delta</m:ci>
		        <m:apply>
			        <m:selector/>
			        <m:ci type="matrix">flux_obs</m:ci>
			        <m:cn>:</m:cn>
			        <m:cn>1</m:cn>
		        </m:apply>
	        </m:apply>

	        <smtb:comment>Marquages observ�s</smtb:comment>

	        <m:apply>	
		        <m:eq/>
		        <m:ci>yobs</m:ci>
		        <m:list separator=";">
			        <xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
				        <m:apply>
					        <m:selector/>
					        <m:ci type="matrix">
						        <xsl:value-of select="concat(@id,'_obs')"/>
					        </m:ci>
					        <m:cn>:</m:cn>
					        <m:cn>
						        <xsl:value-of select="concat('2:3:',(3*$experiences)-1)"/>
					        </m:cn>	
				        </m:apply>	
			        </xsl:for-each>					
		        </m:list>
	        </m:apply>

	        <smtb:comment>Matrice de pond�ration de l'erreur sur les marquages observ�s</smtb:comment>

	        <m:apply>	
		        <m:eq/>
		        <m:ci>alpha</m:ci>
		        <m:list separator=";">
			        <xsl:for-each select="sbml:listOfSpecies/sbml:species[smtb:measurement]">
				        <m:apply>
					        <m:selector/>
					        <m:ci type="matrix">
						        <xsl:value-of select="concat(@id,'_obs')"/>
					        </m:ci>
					        <m:cn>:</m:cn>
					        <m:cn>
						        <xsl:value-of select="concat('1:3:',(3*$experiences)-2)"/>
					        </m:cn>	
				        </m:apply>	
			        </xsl:for-each>					
		        </m:list>
	        </m:apply>

	        <smtb:comment>Calcul de la fonction cout</smtb:comment>

	        <m:apply>
		        <m:eq/>
		        <m:list separator=",">
			        <m:ci>cost</m:ci>
			        <m:ci>grad</m:ci>
			        <m:ci>label_error</m:ci>
		        </m:list>
		        <m:apply>
			        <m:fn>
				        <m:ci>costAndGrad</m:ci>
			        </m:fn>
			        <m:apply>
				        <m:selector/>
				        <m:ci type="matrix">fluxes</m:ci>
				        <m:cn>:</m:cn>
				        <m:cn>1</m:cn>
			        </m:apply>
		        </m:apply>
	        </m:apply>

		<m:apply>
		<m:eq/>
			<m:ci>flux_obs(:,3)</m:ci>
			<m:apply>
				<m:times type="matrix"/>	
				<m:ci>E</m:ci>
				<m:ci>fluxes(:,1)</m:ci>
			</m:apply>
		</m:apply>

        </smtb:script>

     </xsl:template>

     <xsl:template name="iterate-cost">
	     <xsl:param name="i" select="'1'"/>
	     <m:apply>
		     <m:fn>
			     <m:ci>sum</m:ci>
		     </m:fn>
		     <m:apply>			
			     <m:times type="array"/>

			     <m:apply>
				     <m:selector/>
				     <m:ci type="matrix">alpha</m:ci>
				     <m:cn>:</m:cn>
				     <m:cn>
					     <xsl:value-of select="$i"/>
				     </m:cn>
			     </m:apply>

			     <m:apply>
				     <m:power type="array"/>
				     <m:apply>
					     <m:selector/>
					     <m:ci type="matrix">e_label</m:ci>
					     <m:cn>:</m:cn>
					     <m:cn><xsl:value-of select="$i"/></m:cn>
				     </m:apply>
				     <m:cn>2</m:cn>
			     </m:apply>
		     </m:apply>
	     </m:apply>
	     <xsl:if test="$i&lt;$experiences">
		     <xsl:call-template name="iterate-cost">
			     <xsl:with-param name="i" select="($i)+1"/>
		     </xsl:call-template>
	     </xsl:if>
     </xsl:template>


     <xsl:template name="iterate-grad">
	     <xsl:param name="i" select="'1'"/>
	     <m:apply>
		     <m:times/>
		     <m:apply>
			     <m:transpose/>
			     <m:apply>
				     <m:times type="array"/>
				     <m:apply>
					     <m:selector/>
					     <m:ci type="matrix">alpha</m:ci>
					     <m:cn>:</m:cn>
					     <m:cn>
						     <xsl:value-of select="$i"/>
					     </m:cn>
				     </m:apply>
				     <m:apply>
					     <m:selector/>
					     <m:ci type="matrix">e_label</m:ci>
					     <m:cn>:</m:cn>
					     <m:cn><xsl:value-of select="$i"/></m:cn>
				     </m:apply>
			     </m:apply>
		     </m:apply>
		     <m:apply>
			     <m:plus/>
			     <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
				     <m:apply>
					     <m:times/>
 		   			     <m:ci>
	                         <xsl:value-of select="concat('C',@weight)"/>
	                     </m:ci>
					     <m:apply>
						     <m:selector/>
 		   				     <m:ci type="hypermatrix">
	                    	     <xsl:value-of select="concat('dx',@weight,'_dv')"/>
	                	     </m:ci>
						     <m:cn>:</m:cn>
						     <m:cn>:</m:cn>
						     <m:cn><xsl:value-of select="$i"/></m:cn>
					     </m:apply>
				     </m:apply>
                 </xsl:for-each>
		     </m:apply>			
	     </m:apply>

	     <xsl:if test="$i&lt;$experiences">
		     <xsl:call-template name="iterate-grad">
			     <xsl:with-param name="i" select="($i)+1"/>
		     </xsl:call-template>
	     </xsl:if>
         
     </xsl:template>


     <xsl:template match="smtb:listOfIntermediateCumomers">

        <xsl:for-each select="smtb:listOfCumomers[@weight&lt;=$weight]">    

	        <!-- D�claration et initialisation matrice et vecteur -->

	        <xsl:variable name="current_weight" select="@weight"/>


	        <m:apply>
		        <m:eq/>
		        <m:ci>
                    <xsl:value-of select="concat('n',@weight)"/>
		        </m:ci>
		        <m:cn>
			        <xsl:value-of select="count(smtb:cumomer)"/>
		        </m:cn>
	        </m:apply>

	        <smtb:optimize>

		        <smtb:matrix-open type="sparse" id="{concat('M',@weight)}" 
                                  rows="{concat('n',@weight)}" cols="{concat('n',@weight)}"/>

		        <smtb:matrix-open type="s_full" id="{concat('b',@weight)}" 
                                  rows="{concat('n',@weight)}" cols="1" 
				                  versions="{$experiences}"/>

		        <smtb:matrix-open type="s_full" id="{concat('dg',@weight,'_dv')}" 
                                  rows="{concat('n',@weight)}" 
                                  cols="{count(../../sbml:listOfReactions/sbml:reaction)}" 
				                  versions="{$experiences}" />

		        <xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
			        <smtb:matrix-open type="sparse" id="{concat('db',$current_weight,'_dx',@weight)}" 
                                      rows="{concat('n',$current_weight)}" 
                                      cols="{concat('n',@weight)}" 
					                  versions="{$experiences}"/>
		        </xsl:for-each>

                <smtb:comment>Cumom�res de poids <xsl:value-of select="@weight"/></smtb:comment>

                <!-- Pour chaque cumom�re on appelle la template g�n�rant les diverses affectations, � la
                matrice ou au second membre du syst�me ayant pour solution le vecteur des cumom�res du
                poids courant -->

                <xsl:for-each select="smtb:cumomer">
                        <xsl:call-template name="iteration"/>                                    
                </xsl:for-each>

		        <smtb:matrix-close id="{concat('M',@weight)}"/>
		        <smtb:matrix-close id="{concat('b',@weight)}"/>

		        <!-- R�solution des syst�mes -->

		        <!-- R�solution du syst�me pour x_weight -->

		        <smtb:solve matrix="{concat('M',@weight)}">
			        <smtb:lhs>
				        <xsl:value-of select="concat('x',@weight)"/>
			        </smtb:lhs>
			        <smtb:rhs>
				        <m:apply>	
					        <m:minus/>
					        <m:list separator=",">
						        <xsl:call-template name="concatenate-rhs">	
							        <xsl:with-param name="id" select="concat('b',@weight)"/>
						        </xsl:call-template>
					        </m:list>
				        </m:apply>
			        </smtb:rhs>
		        </smtb:solve> 

		        <smtb:matrix-close id="{concat('dg',@weight,'_dv')}"/>

		        <xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
			        <smtb:matrix-close id="{concat('db',$current_weight,'_dx',@weight)}" />
		        </xsl:for-each>

		        <!-- R�solution du syst�me pour dx_weight/dv -->

		        <smtb:hypermatrix id="{concat('dx',@weight,'_dv')}" rows="{concat('n',@weight)}" cols="{count(../../sbml:listOfReactions/sbml:reaction)}" 
				             versions="{$experiences}"/>

		        <xsl:call-template name="solve-dx-dv"/>

	        </smtb:optimize>

        </xsl:for-each>

     </xsl:template>

     <xsl:template name="concatenate-rhs">
        <xsl:param name="i">1</xsl:param>
        <xsl:param name="id"/>

        <m:ci type="vector">
	        <xsl:value-of select="concat($id,'_',$i)"/>
        </m:ci>		

        <xsl:if test="$i&lt;$experiences">
	        <xsl:call-template name="concatenate-rhs">
		        <xsl:with-param name="i" select="($i)+1"/>
		        <xsl:with-param name="id" select="$id"/>
	        </xsl:call-template>
        </xsl:if>
     </xsl:template> 

     <xsl:template name="solve-dx-dv">
        <xsl:param name="i">1</xsl:param>
        <xsl:variable name="current_weight" select="@weight"/>

        <smtb:solve matrix="{concat('M',@weight)}">
	        <smtb:lhs>
		        <m:apply>
			        <m:selector/>
			        <m:ci type="hypermatrix">
				        <xsl:value-of select="concat('dx',@weight,'_dv')"/>
			        </m:ci>
			        <m:cn>:</m:cn>
			        <m:cn>:</m:cn>
			        <m:cn><xsl:value-of select="$i"/></m:cn>
		        </m:apply>				
	        </smtb:lhs>
	        <smtb:rhs>
		        <m:apply>
			        <m:minus/>
			        <m:apply>
				        <m:plus/>
				        <m:ci>
					        <xsl:value-of select="concat('dg',@weight,'_dv_',$i)"/>
				        </m:ci>
				        <xsl:for-each select="preceding-sibling::smtb:listOfCumomers">
					        <m:apply>
						        <m:times/>
						        <m:ci>
							        <xsl:value-of select="concat('db',$current_weight,'_dx',@weight,'_',$i)"/>
						        </m:ci>
						        <m:apply>
							        <m:selector/>
							        <m:ci type="hypermatrix">
								        <xsl:value-of select="concat('dx',@weight,'_dv')"/>
							        </m:ci>
							        <m:cn>:</m:cn>
							        <m:cn>:</m:cn>
							        <m:cn><xsl:value-of select="$i"/></m:cn>
						        </m:apply>
					        </m:apply>
				        </xsl:for-each>
			        </m:apply>
		        </m:apply>
	        </smtb:rhs>
        </smtb:solve>

        <xsl:if test="$i&lt;$experiences">
	        <xsl:call-template name="solve-dx-dv">
		        <xsl:with-param name="i" select="($i)+1"/>
	        </xsl:call-template>
        </xsl:if>
     </xsl:template> 


</xsl:stylesheet>
