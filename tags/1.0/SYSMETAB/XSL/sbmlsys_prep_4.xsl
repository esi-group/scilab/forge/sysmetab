<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- 
    Auteur  :   St�phane Mottelet
    Date    :   Tue Mar 27 09:27:49 CEST 2007

    Projet  :   SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but :

    1 - pour chaque esp�ce, de lui ajouter un attribut "type" pouvant etre
    "intermediate", "input" ou "output" suivant qu'il s'agit d'un m�tabolite
    du type correspondant.

    2 - ajouter apr�s l'�l�ment <listOfSpecies> deux �l�ments
            
    <smtb:listOfIntermediateCumomers>
        <smtb:listOfCumomers weight="1">
        ...
        </smtb:listOfCumomers>
        <smtb:listOfCumomers weight="2">
        ...
        </smtb:listOfCumomers>
    </smtb:listOfIntermediateCumomers>
    
    <smtb:listOfInputCumomers>
        <smtb:listOfCumomers weight="1">
        ...
        </smtb:listOfCumomers>
    </smtb:listOfInputCumomers>
    
    contenant les cumom�res class�s par poids (pour les interm�diaires)
    ou pas class�s du tout (pour les entr�es).

    3 - d'ajouter explicitement, pour chaque
    r�action (sauf si elle est d�clar�e comme irreversible), la r�action
    inverse correspondante. Du point de vue des identificateurs, si par exemple
    une r�action a pour id "re1", cela donne deux nouvelles r�actions avec les
    id "re1_f" et "re1_b" (f pour forward et b pour backward).    

    4 - d'ajouter � chaque r�action un attribut "position" donnant l'indice
    de la r�action dans la liste.
	
	5 - De traduire les observations sous forme de combinaisons lin�aires des cumom�res.

    REMARQUE : il semble que pour ce dernier point il y ait un probl�me de version
    de libxslt ou libxml sous MacOSX/Intel 10.4.9 qui fait que les position ne sont
    pas calcul�es comme il faut.
       
-->

<xsl:stylesheet  version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"   
    xmlns:math="http://exslt.org/math"   
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="math sbml celldesigner m str exslt xhtml smtb">

    <xsl:param name="experiences">1</xsl:param>
    
    <xsl:output method="xml" indent="yes" encoding="ISO-8859-1"/>

    <xsl:strip-space elements="*"/>
	
   
	<!-- Poids maximal des cumom�res pr�sents dans le r�seau. -->
    
    <xsl:variable name="maxweight" select="math:max(//sbml:species/smtb:cumomer/@weight)"/>
    
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>
    <xsl:key name="cumomer" match="smtb:cumomer" use="@id"/>
    
    <xsl:template match="/">
      <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="sbml:sbml">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="sbml:listOfSpecies">
        <sbml:listOfSpecies>
            <xsl:apply-templates/>
        </sbml:listOfSpecies>
        <smtb:listOfIntermediateCumomers>
            <xsl:call-template name="make-cumomer-list"/>
        </smtb:listOfIntermediateCumomers>
        <smtb:listOfInputCumomers>
            <xsl:call-template name="make-cumomer-list-input"/>
        </smtb:listOfInputCumomers>
        <smtb:listOfExperiments>
            <xsl:call-template name="make-list-of-experiments"/>
        </smtb:listOfExperiments>
    </xsl:template>
    
    <xsl:template name="make-list-of-experiments">
        <xsl:param name="i">1</xsl:param>
        <smtb:experiment number="{$i}"/>
        <xsl:if test="$i &lt; $experiences">
            <xsl:call-template name="make-list-of-experiments">
                <xsl:with-param name="i" select="1+$i"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="make-cumomer-list">
        <xsl:param name="w">1</xsl:param>
        <xsl:if test="$w&lt;=$maxweight">
            <smtb:listOfCumomers weight="{$w}">
                <xsl:for-each select="//sbml:species[key('products',@id) and key('reactants',@id)]/smtb:cumomer[@weight=$w]">
					<smtb:cumomer>
						<xsl:copy-of select="@*"/>
						<xsl:attribute name="position">
							<xsl:value-of select="position()"/>
						</xsl:attribute>
						<xsl:copy-of select="*"/>
					</smtb:cumomer>
                </xsl:for-each>
            </smtb:listOfCumomers>
            <xsl:call-template name="make-cumomer-list">
                <xsl:with-param name="w" select="($w)+1"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>

    <xsl:template name="make-cumomer-list-input">
        <xsl:param name="w">1</xsl:param>
        <xsl:if test="$w&lt;=$maxweight">
            <smtb:listOfCumomers weight="{$w}">
	            <xsl:for-each select="//sbml:species[smtb:input]/smtb:cumomer[@weight=$w]">
					<smtb:cumomer>
						<xsl:copy-of select="@*"/>
						<xsl:attribute name="position">
							<xsl:value-of select="position()"/>
						</xsl:attribute>
						<xsl:copy-of select="*"/>
					</smtb:cumomer>
            	</xsl:for-each>
        	</smtb:listOfCumomers>
			<xsl:call-template name="make-cumomer-list-input">
            	<xsl:with-param name="w" select="($w)+1"/>
        	</xsl:call-template>
		</xsl:if>
    </xsl:template>

    <xsl:template match="*|@*|text()|comment()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()|comment()|processing-instruction()"/>
        </xsl:copy>
    </xsl:template>
    
  <xsl:template match="sbml:reaction[(@reversible!='false') or not(@reversible)]">
        <sbml:reaction id="{@id}_f" name="{@name} (forward)">
            <xsl:copy-of select="*"/>
        </sbml:reaction>
        <sbml:reaction id="{@id}_b" name="{@name} (backward)">
            <sbml:listOfReactants>
                <xsl:copy-of select="sbml:listOfProducts/*"/>
            </sbml:listOfReactants>
            <sbml:listOfProducts>
                <xsl:copy-of select="sbml:listOfReactants/*"/>
            </sbml:listOfProducts>
        </sbml:reaction>
    </xsl:template>

	<xsl:template match="sbml:species">
        <xsl:variable name="id" select="@id"/>
		<xsl:element name="sbml:species">
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates select="*"/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="smtb:measurement">
	    <smtb:measurement>
			<xsl:copy-of select="@*"/>
			<xsl:for-each select="smtb:token">
				<xsl:call-template name="disassemble-cumomer"/>
			</xsl:for-each>
		</smtb:measurement>
	</xsl:template>
	
	<!-- Template r�cursive de d�termination des contributions des cumom�res dans une observation donn�e. Cela
	     sert plus tard pour la g�n�ration des matrices d'observation C1, C2, etc. -->

	<xsl:template name="disassemble-cumomer">
		<xsl:param name="i" select="string-length(.)"/>
		<xsl:param name="string" select="."/>
		<xsl:param name="id" select="'0'"/>
		<xsl:param name="sign" select="'1'"/>
		
		<xsl:choose>
			<xsl:when test="$i=0">
				<smtb:cumomer-contribution id="{concat(../../@id,'_',$id)}" string="{$string}" 
				               weight="{key('cumomer',concat(../../@id,'_',$id))/@weight}" sign="{$sign}"/>
			</xsl:when>
			<xsl:otherwise>		
				<xsl:choose>
					<xsl:when test="substring($string,$i,1)='x'">
						<xsl:call-template name="disassemble-cumomer">
							<xsl:with-param name="i" select="($i)-1"/>
							<xsl:with-param name="string" select="$string"/>
							<xsl:with-param name="id" select="$id"/>
							<xsl:with-param name="sign" select="$sign"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="substring($string,$i,1)='1'">
						<xsl:call-template name="disassemble-cumomer">
							<xsl:with-param name="i" select="($i)-1"/>
							<xsl:with-param name="string" select="$string"/>
							<xsl:with-param name="id" select="$id + math:power(2,string-length(.)-$i)"/>
							<xsl:with-param name="sign" select="$sign"/>
						</xsl:call-template>
					</xsl:when>
					<xsl:when test="substring($string,$i,1)='0'">
						<xsl:call-template name="disassemble-cumomer">
							<xsl:with-param name="i" select="$i"/>
							<xsl:with-param name="string" select="concat(substring-before($string,'0'),'x',substring-after($string,'0'))"/>
							<xsl:with-param name="id" select="$id"/>
							<xsl:with-param name="sign" select="$sign"/>
						</xsl:call-template>
						<xsl:call-template name="disassemble-cumomer">
							<xsl:with-param name="i" select="$i"/>
							<xsl:with-param name="string" select="concat(substring-before($string,'0'),'1',substring-after($string,'0'))"/>
							<xsl:with-param name="id" select="$id"/>
							<xsl:with-param name="sign" select="-($sign)"/>
						</xsl:call-template>
					</xsl:when>
				</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

   <xsl:template match="sbml:annotation"/>

</xsl:stylesheet>
