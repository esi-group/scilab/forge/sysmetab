<?xml version="1.0" encoding="ISO-8859-1"?>

<!--
Auteur: Florent Tixier
Date: mar 19 aout 10:25 CEST 2008
Projet: SYSMETAB/Carnot
-->

<!-- Cette feuille de style a pour but de g�n�rer le fichier
    XMLlab d�crivant l'interface du logiciel de simulation/optimisation
    sp�cifique � un r�seau donn�.
-->

<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"
    xmlns:m="http://www.w3.org/1998/Math/MathML"
    xmlns:xhtml="http://www.w3.org/1999/xhtml"
    xmlns:exslt="http://exslt.org/common" 
    xmlns:str="http://exslt.org/strings"
	xmlns:date="http://exslt.org/dates-and-times"
	xmlns:math="http://exslt.org/math" version="1.0"
    xmlns:celldesigner="http://www.sbml.org/2001/ns/celldesigner"
    xmlns:smtb="http://www.utc.fr/sysmetab"
    exclude-result-prefixes="sbml celldesigner m str exslt xhtml smtb">
    
    <xsl:include href="common_xmllab.xsl"/>   
    <xsl:output method="xml" doctype-system="http://www.xmllab.org/dtd/1.6/fr/simulation.dtd" 
	            doctype-public="-//UTC//DTD XMLlab V1.6//FR" indent="yes" encoding="ISO-8859-1"/>
 
    <xsl:param name="file"/>
    <xsl:param name="verbose">no</xsl:param>
    <xsl:param name="type">dynamique</xsl:param>
    <xsl:param name="experiences">1</xsl:param>
    <xsl:param name="nb_t_obs">0</xsl:param>
    
    <xsl:key name="reaction" match="sbml:reaction" use="@id"/>
    <xsl:key name="species" match="sbml:species" use="@id"/>
    <xsl:key name="input-cumomer" match="smtb:listOfInputCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="cumomer" match="smtb:listOfIntermediateCumomers//smtb:cumomer" use="@id"/>
    <xsl:key name="reactionSpecies" match="sbml:speciesReference" use="@species"/>
    <xsl:key name="products" match="sbml:listOfProducts/sbml:speciesReference" use="@species"/>
    <xsl:key name="reactants" match="sbml:listOfReactants/sbml:speciesReference" use="@species"/>
    <xsl:key name="measurement" match="listOfSpecies/sbml:measurement" use="@id"/>

	<xsl:variable name="weight" select="math:max(//sbml:species[@type='intermediate']/smtb:measurement/smtb:cumomer-contribution/@weight)"/>
	
    <xsl:strip-space elements="*"/>
    
    <xsl:template match="/">
        <simulation>
          <xsl:apply-templates/>
        </simulation>
    </xsl:template>

    <xsl:template match="sbml:model">
        <xsl:variable name="all-fluxes">
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="unknown-fluxes">
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction[not(@known)]">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="known-fluxes">
            <xsl:for-each select="sbml:listOfReactions/sbml:reaction[@known='yes']">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="input-cumomers">
            <xsl:for-each select="smtb:listOfInputCumomers/smtb:listOfCumomers/smtb:cumomer[@weight&lt;=$weight]">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="cumomers">
            <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers/smtb:cumomer[@weight&lt;=$weight]">
                <xsl:value-of select="@id"/>
                <xsl:if test="position()&lt;last()">
                    <xsl:text> </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
	
        <header>
            <title>R�seau m�tabolique <xsl:value-of select="@id"/></title>
            <title lang="english">Metabolic network <xsl:value-of select="@id"/></title>
			<author>St�phane Mottelet</author>
			<date>
				<xsl:value-of select="concat(date:date(),', at ',date:time())"/>
			</date>
	    <script href="{$file}.sci"/>
	    <script href="sysmetab4.sci"/>
        </header>
        <parameters>
	    <!-- onglet des flux -->	
            <section>
                <title>Flux</title>
                <title lang="english">Flux</title>
			<matrix label="fluxes" rows="{count(sbml:listOfReactions/sbml:reaction)}" cols="2" stripcol="yes" save="yes" clear="yes" load="yes">
				<name/>
				<colheader>
					<col state="disabled">
						<name>Valeur en %</name>
						<name lang="english">Value in %</name>
					</col>
					<col>
						<name>Valeur impos�e</name>
						<name lang="english">Constrained value</name>
					</col>
					<col state="disabled"/>
				</colheader>
				<xsl:for-each select="sbml:listOfReactions/sbml:reaction">
					<row>
						<name><xsl:value-of select="concat(@id,', ',@name)"/></name>
						<name lang="english"><xsl:value-of select="concat(@id,', ',@name)"/></name>
						<value>0,-1</value>
					</row>
				</xsl:for-each>
			</matrix>
            </section>
		
	    <!-- onglet des flux observ�s-->	
	    <section>
               	<title>Flux observ�s</title>
                <title lang="english">Fluxes observations</title>
                <xsl:apply-templates select="sbml:listOfReactions" mode="with_ident"/>
           </section>
    	   
	   <!-- onglet des marquages connus-->	
	   <section>
        	<title>Marquages connus</title>
        	<title lang="english">Known label</title>
        	<xsl:apply-templates select="sbml:listOfSpecies/sbml:species[smtb:input]"/>
    	   </section>
	   
	   <!--onglet des marquages observ�s-->
	   <section>
        	<title>Marquages observ�s</title>
        	<title lang="english">Label observation</title>
		<xsl:call-template name="cumomer_measurement"/>
    	   </section>
		
	   <!-- onglet des concentration des m�tabolites-->	
	   <section>
		<title>M�tabolites</title>
		<title lang="english">Metabolites</title>
			<matrix label="concentrations" rows="{count(sbml:listOfSpecies/sbml:species[@type='intermediate'])}" cols="1" stripcol="yes" save="yes" clear="yes" load="yes">
				<name/>
				<colheader>
					<col>
						<name>Concentrations</name>
						<name lang="english">Concentration</name>
					</col>
						<col state="disabled"/>
				</colheader>
				<xsl:for-each select="sbml:listOfSpecies/sbml:species[@type='intermediate']">
					<row>
				        	<name><xsl:value-of select="@name"/></name>
				            	<name lang="english"><xsl:value-of select="@name"/></name>
						<value>1</value>
					</row>
				</xsl:for-each>
				</matrix>
		</section>

			
			<!--onglet avec les parametres pour l'optimisation-->
			<section>
				<title>Calcul</title>
				<title lang="english">Computation</title>
				<subsection>
					<title>Intervalle et pr�cision de calcul</title>
					<title lang="english">Interval and precision computation</title>
					<scalar label="t0" min="0" max="%inf">
						<name>Temps initial</name>
						<name lang="english">Initial time</name>
						<value>0</value>
					</scalar>
					<scalar label="t1" min="0" max="%inf">
						<name>Temps final</name>
						<name lang="english">Final time</name>
						<value>1</value>
					</scalar>
					<scalar label="d_t3" min="1" max="%inf">
						<name>Nombre de pas de temps</name>
						<name lang="english">Step number</name>
						<value>100</value>
					</scalar>
				</subsection>
				<subsection>
					<title>Optimisation (fsqp)</title>
					<title lang="english">Optimisation (fsqp)</title>
					<scalar label="epsgrad" min="1e-10" max="%inf">
						<name>Tol�rance pour la convergence (norme du gradient projet�)</name>
						<name lang="english">Tolerance for convergence (norm of projected gradient)</name>
						<value>1e-8</value>	
					</scalar>
					<scalar label="miter" min="1" max="%inf">
						<name>Nombre maximum d'it�rations pour une optimisation</name>
						<name lang="english">Maximum number of iterations for a single optimization</name>
						<value>1000</value>	
					</scalar>
					<scalar label="itmax" min="1" max="%inf">
						<name>Nombre de tirages al�atoires de conditions initiales</name>
						<name lang="english">Number of successive random initial conditions</name>
						<value>10</value>	
					</scalar>
				</subsection>
				<subsection>
					<title>Fonction cout</title>
					<title lang="english">Cost function</title>
					<scalar label="epsilon" min="0" max="%inf">
						<name>Param�tre de r�gularisation</name>
						<name lang="english">Regularization parameter</name>
						<value>1e-5</value>	
					</scalar>
					<scalar label="vmin" min="0" max="%inf">
						<name>Borne inf�rieure des fluxs</name>
						<name lang="english">Fluxes lower bound</name>
						<value>1e-5</value>	
					</scalar>
				</subsection>

			<!-- section cout-->
			<subsection>
        			<title>Cout</title>
        			<title lang="english">Cost</title>
				<matrix label="matrix_cost" rows="{$experiences}" cols="2" stripcol="yes" save="yes">
				<name/>
				<colheader>
					<col state="disabled">
						<name>Erreur sur les marquages</name>
						<name lang="english">Label Error</name>
					</col>
					<col state="disabled">
						<name>Erreur totale</name>
						<name lang="english">Total Error</name>
					</col>
					<col state="disabled"/>
				</colheader>
				<xsl:call-template name="notebook_cost"/>	
				</matrix>
				<scalar label="f_cost" state="disabled">
					<name>Erreur sur les flux</name>
					<name lang="english">fluxes error contribution</name>
				</scalar>
				<scalar label="label_error" state="disabled">
					<name>Erreur sur les marquages</name>
					<name lang="english">Label error contribution</name>
				</scalar>
    	   		</subsection>
		</section>
			
            		<actions>
                		<title>Simulation</title>
             		   	<title lang="english">Simulation</title>
				<action>
					<title>Calcul du gradient</title>
					<title lang="english">Display the gradient</title>
					<script>
					[aa,bb,cc]=costAndGrad(fluxes(:,1));
					disp("Gradient:");
					disp(bb);
					clear aa bb cc;
					<!--calc_gradient();-->
					</script>
				</action>
				<action>
				<title>Sauver les donn�es au format Excel</title>
				<title lang="english">Save data (Excel file)</title>
				<script>
				Save();
				disp("Resultats sauvegardes");
				</script>		
				</action>
				<action>
                    			<xsl:attribute name="update">
                        			<xsl:text>fluxes</xsl:text>
                   			</xsl:attribute>
                    			<title>Optimisation</title>
                    			<title lang="english">Optimization</title>
                   			<script>fluxes(:,1)=optimize()</script>
               	 		</action>
				<action>
                   			<title>Ligne de commande Scilab</title>
                    			<title lang="english">Scilab command line</title>
                    			<script>pause</script>
                		</action>
            		</actions>
				
			
        </parameters>
	<script href="{$file}_direct.sce">
		<xsl:attribute name="update">
				<xsl:text>flux_obs</xsl:text>
				<xsl:text> matrix_cost</xsl:text>
				<xsl:text> label_error</xsl:text>
				<xsl:text> cout</xsl:text>
				<xsl:text> f_cost</xsl:text>
				<xsl:call-template name="update_label_measurement"/>
				<xsl:call-template name="update_c_cost"/>
		</xsl:attribute>			
	</script>
	<graphs>
		<defdomain1d label="t">
      		<name lang="french">Intervalle de repr�sentation</name>
      		<name lang="english">Representation interval</name>
      		<interval steps="d_t3">
       		<initialvalue>t0</initialvalue>
        	<finalvalue>t1</finalvalue>
      		</interval>
    		</defdomain1d>
		<!--<xsl:call-template name="def_draw"/>-->
		<xsl:call-template name="def_draw_output"/>
	</graphs>
	<display background="white">
		<window editable="yes">
		<title>Sorties</title>
		<title lang="english">Outputs</title>				
			<xsl:call-template name="draw_output"/>
		</window>
	</display>	
    </xsl:template>
 
 <!-- template de definition des courbes de sortie du reseau-->   
  <xsl:template name="def_draw_output">
 	<xsl:param name="n_exp">1</xsl:param>
	<xsl:for-each select="sbml:listOfSpecies//smtb:measurement">
    			<parametriccurve2d label="y{$n_exp}_{position()}">
				<name><xsl:value-of select="concat(../@name,'(',@string,')')"/></name>
				<name lang="english"><xsl:value-of select="concat(../@name,'(',@string,')')"/></name>
				<x1>
				<name/>
				<value>t</value>
				</x1>
				<x2>
				<name/>
				<value><xsl:value-of select="concat('y_',$n_exp,'(',position(),',:)')"/></value>
				</x2>
			</parametriccurve2d>
	</xsl:for-each>		
		<xsl:if test="$n_exp&lt;$experiences">
			<xsl:call-template name="def_draw_output">
				<xsl:with-param name="n_exp" select="($n_exp)+1"/>
			</xsl:call-template>	
    		</xsl:if>	
 </xsl:template>

<!-- templates pour tracer les courbes de sorties-->
   <xsl:template name="draw_output">
 	<xsl:param name="n_exp">1</xsl:param>
	<xsl:variable name="colors">
			<color>blue</color>
			<color>green</color>
			<color>red</color>
			<color>cyan</color>
			<color>magenta</color>
			<color>yellow</color>
			<color>black</color>
	</xsl:variable>
	<axis2d xmin="t0" xmax="t1" ymin="0" ymax="max(y_{$n_exp}(:,$))">
		<title>
			<xsl:value-of select="concat('Experience ',$n_exp)"/>
		</title>
		<title lang="english">
			<xsl:value-of select="concat('Experience #',$n_exp)"/>
		</title>
		<xsl:for-each select="sbml:listOfSpecies//smtb:measurement">
			<xsl:variable name="n_graph" select="position()"/>
    		<drawcurve2d color="{exslt:node-set($colors)/color[(($n_graph -1) mod 7)+1]}" ref="y{$n_exp}_{position()}"/>	
		</xsl:for-each>	
	</axis2d>	
		<xsl:if test="$n_exp&lt;$experiences">
			<xsl:call-template name="draw_output">
				<xsl:with-param name="n_exp" select="($n_exp)+1"/>
			</xsl:call-template>	
    		</xsl:if>	
 </xsl:template>


<!--Templates pour la creation des matrices d'observations de cumomeres -->
<xsl:template name="cumomer_measurement">
<xsl:param name="instant">1</xsl:param>
	<subsection>
	<title><xsl:value-of select="concat('Mesure : ',$instant)"/></title>
	<title lang="english"><xsl:value-of select="concat('Measure: ',$instant)"/></title>
	<xsl:apply-templates select="sbml:listOfSpecies/sbml:species[smtb:measurement]" mode="obs">
		<xsl:with-param name="instant" select="$instant"/>
	</xsl:apply-templates>
	<scalar label="t_obs_{$instant}">
		<name>Mesure au temps :</name>
		<name lang="english">Measuring time:</name>
		<value>0</value>
	</scalar>
	</subsection>
	<xsl:if test="$instant&lt;$nb_t_obs">	
		<xsl:call-template name="cumomer_measurement">
			<xsl:with-param name="instant" select="($instant)+1"/>
		</xsl:call-template>		
	</xsl:if>
</xsl:template>

<xsl:template match="sbml:species[smtb:measurement]" mode="obs">
	<xsl:param name="instant">1</xsl:param>
	<matrix label="{@id}_obs_{$instant}" rows="{count(smtb:measurement)}" cols="{3*($experiences)}" save="yes" clear="yes" load="yes">
		<name><xsl:value-of select="@name"/></name>
		<name lang="english"><xsl:value-of select="@name"/></name>
		<colheader>
			<xsl:call-template name="iterate-observation-names"/>
		</colheader>
        	<xsl:for-each select="smtb:measurement">
		<row>
			<name><xsl:value-of select="concat('#',@string)"/></name>
			<name lang="english"><xsl:value-of select="concat('#',@string)"/></name>
			<value><xsl:call-template name="init_obs"/></value>
            	</row>
        	</xsl:for-each>
	</matrix>	
</xsl:template>

<!--templates pour le mise � jour de variable-->
<xsl:template name="update_label_measurement">
	<xsl:param name="instant">1</xsl:param>
	<xsl:for-each select="/sbml:model/sbml:listOfSpecies/sbml:species[smtb:measurement]">
		<xsl:value-of select="concat(' ',@id,'_obs_',$instant)"/>
	</xsl:for-each>
	<xsl:if test="$instant&lt;$nb_t_obs">	
	<xsl:call-template name="update_label_measurement">
		<xsl:with-param name="instant" select="($instant)+1"/>
	</xsl:call-template>		
	</xsl:if>
</xsl:template>

<xsl:template name="update_c_cost">
	<xsl:param name="exp">1</xsl:param>
		<xsl:value-of select="concat(' c_cost_',$exp)"/>
	<xsl:if test="$exp&lt;$experiences">	
	<xsl:call-template name="update_c_cost">
		<xsl:with-param name="exp" select="($exp)+1"/>
	</xsl:call-template>		
	</xsl:if>
</xsl:template>

<!--Generation de la matrice de l'onglet cout-->
<xsl:template name="notebook_cost">
	<xsl:param name="exp">1</xsl:param>
	<row>
		<name><xsl:value-of select="concat('Exp�rience : ',$exp)"/></name>
		<name lang="english"><xsl:value-of select="concat('Experience: ',$exp)"/></name>
		<value>0,0</value>
	</row>
	<xsl:if test="$exp&lt;$experiences">
		<xsl:call-template name="notebook_cost">
			<xsl:with-param name="exp" select="($exp)+1"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

<!-- Valeur 1 par defaut pour les 1/sigma des marquages observ�s-->
<xsl:template name="init_obs">
	<xsl:param name="exp">1</xsl:param>
	<xsl:text>1,0,0</xsl:text>
	<xsl:if test="$exp&lt;$experiences">
		<xsl:text>,</xsl:text>
		<xsl:call-template name="init_obs">
			<xsl:with-param name="exp" select="($exp)+1"/>
		</xsl:call-template>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>





<!-- Morceaux de code servant a l'affichage de cumom�re interm�diaire-->

<!--<subsection>
	<title>Regroup� par poids:</title>
	<title lang="english">Grouping by weight:</title>
	<scalar label="splitx_w" min="1" max="%inf">
		<name>nombre de graphes en longueur</name>
		<name lang="english">...</name>
		<value><xsl:value-of select="concat('ceil(sqrt(',$weight,'))')"/></value>
	</scalar>
	<scalar label="splity_w" min="1" max="%inf">
		<name>nombre de graphes en largeur</name>
		<name lang="english">...</name>
		<value><xsl:value-of select="concat('round(sqrt(',$weight,'))')"/></value>
	</scalar>
    </subsection>
    <subsection>
	<title>Regroup� par esp�ces:</title>
	<title lang="english">Grouping by species:</title>
		<scalar label="splitx_s" min="1" max="%inf">
			<name>nombre de graphes en longueur</name>
			<name lang="english">...</name>
			<value><xsl:value-of select="concat('ceil(sqrt(',count(smtb:listOfIntermediateCumomers//@id),'))')"/></value>
		</scalar>
		<scalar label="splity_s" min="1" max="%inf">
			<name>nombre de graphes en largeur</name>
			<name lang="english">...</name>
			<value><xsl:value-of select="concat('round(sqrt(',count(smtb:listOfIntermediateCumomers//@id),'))')"/></value>
		</scalar>
    </subsection>-->

<!--<window editable="yes" splitx="1" splity="1">	
		<title>Non regroup�</title>
		<title lang="english">Ungroup</title>			
			<axis2d xmin="t0" xmax="t1" ymin="0" ymax="1">
				<xsl:call-template name="draw_ungroup"/>
			</axis2d>	
		</window>
		<window editable="yes" splitx="splitx_exp" splity="splity_exp">
		<title>Par exp�riences</title>
		<title lang="english">By experiences</title>				
				<xsl:call-template name="draw_by_exp"/>	
		</window>
		<window editable="yes" splitx="splitx_w" splity="splity_w">
		<title>Par poids</title>
		<title lang="english">By weight</title>				
				<xsl:call-template name="draw_by_weight"/>	
		</window>
		<window editable="yes" splitx="splitx_s" splity="splity_s">
		<title>Par esp�ces</title>
		<title lang="english">By species</title>				
				<xsl:call-template name="draw_by_species"/>	
    </window>-->


<!-- template pour definir les differentes courbes du reseau-->   
 <!-- 
    <xsl:template name="def_draw">
     <xsl:param name="n_exp">1</xsl:param>
     <xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
     	<xsl:variable name="position" select="math:max(smtb:cumomer/@position)"/>
     	<xsl:for-each select="smtb:cumomer[@position&lt;=$position]">
    		<parametriccurve2d label="x{@weight}_{$n_exp}_{@position}_ungroup">
			<name><xsl:value-of select="concat('Exp�rience ',$n_exp,' ',key('species',@species)/@name,'(',key('cumomer',@id)/@pattern,')')"/></name>
			<x1>
			<name/>
			<value>t</value>
			</x1>
			<x2>
			<name/>
			<value><xsl:value-of select="concat('x',@weight,'_',$n_exp,'(',@position,',:)')"/></value>
			</x2>
		</parametriccurve2d>
		<parametriccurve2d label="x{@weight}_{$n_exp}_{@position}_exp">
		<name><xsl:value-of select="concat('Exp�rience ',$n_exp,' ',key('species',@species)/@name,'(',key('cumomer',@id)/@pattern,')')"/></name>
			<x1>
			<name/>
			<value>t</value>
			</x1>
			<x2>
			<name/>
			<value><xsl:value-of select="concat('x',@weight,'_',$n_exp,'(',@position,',:)')"/></value>
			</x2>
		</parametriccurve2d>
		<parametriccurve2d label="x{@weight}_{$n_exp}_{@position}_w">
		<name><xsl:value-of select="concat('Exp�rience ',$n_exp,' ',key('species',@species)/@name,'(',key('cumomer',@id)/@pattern,')')"/></name>
			<x1>
			<name/>
			<value>t</value>
			</x1>
			<x2>
			<name/>
			<value><xsl:value-of select="concat('x',@weight,'_',$n_exp,'(',@position,',:)')"/></value>
			</x2>
		</parametriccurve2d>
		<parametriccurve2d label="x{@weight}_{$n_exp}_{@position}_s">
		<name><xsl:value-of select="concat('Exp�rience ',$n_exp,' ',key('species',@species)/@name,'(',key('cumomer',@id)/@pattern,')')"/></name>
			<x1>
			<name/>
			<value>t</value>
			</x1>
			<x2>
			<name/>
			<value><xsl:value-of select="concat('x',@weight,'_',$n_exp,'(',@position,',:)')"/></value>
			</x2>
		</parametriccurve2d>
		</xsl:for-each>
	</xsl:for-each>
	<xsl:if test="$n_exp&lt;$experiences">
		<xsl:call-template name="def_draw">
			<xsl:with-param name="n_exp" select="($n_exp)+1"/>
		</xsl:call-template>	
    	</xsl:if>
    </xsl:template>
 -->
 
 
 <!-- templates pour tracer les courbes -->
<!-- 
 <xsl:template name="draw_ungroup">
      	<xsl:param name="n_exp">1</xsl:param>
	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
		<xsl:variable name="position" select="math:max(smtb:cumomer/@position)"/>
		<xsl:for-each select="smtb:cumomer[@position&lt;=$position]">	
    			<drawcurve2d ref="x{@weight}_{$n_exp}_{@position}_ungroup"/>
		</xsl:for-each>	
	</xsl:for-each>
	<xsl:if test="$n_exp&lt;$experiences">
		<xsl:call-template name="draw_ungroup">
			<xsl:with-param name="n_exp" select="($n_exp)+1"/>
		</xsl:call-template>	
    	</xsl:if>
  </xsl:template> 
  
  <xsl:template name="draw_by_exp">
      	<xsl:param name="n_exp">1</xsl:param>
	<axis2d xmin="t0" xmax="t1" ymin="0" ymax="1">
		<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
			<xsl:variable name="position" select="math:max(smtb:cumomer/@position)"/>
			<xsl:for-each select="smtb:cumomer[@position&lt;=$position]">	
    				<drawcurve2d ref="x{@weight}_{$n_exp}_{@position}_exp"/>
			</xsl:for-each>	
		</xsl:for-each>
	</axis2d>
	<xsl:if test="$n_exp&lt;$experiences">
		<xsl:call-template name="draw_by_exp">
			<xsl:with-param name="n_exp" select="($n_exp)+1"/>
		</xsl:call-template>	
    	</xsl:if>
  </xsl:template> 
  
  
  
  <xsl:template name="draw_by_weight">
  	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
      		<axis2d xmin="t0" xmax="t1" ymin="0" ymax="1">
		<xsl:variable name="position" select="math:max(smtb:cumomer/@position)"/>
		<xsl:for-each select="smtb:cumomer[@position&lt;=$position]">	
				
			<xsl:call-template name="draw_by_weight_aux"/>
			
		</xsl:for-each>
		</axis2d> 
	</xsl:for-each>
  </xsl:template>  
 
  <xsl:template name="draw_by_weight_aux">
  	<xsl:param name="n_exp">1</xsl:param>	
    				<drawcurve2d ref="x{@weight}_{$n_exp}_{@position}_w"/>	
	<xsl:if test="$n_exp&lt;$experiences">
		<xsl:call-template name="draw_by_weight_aux">
			<xsl:with-param name="n_exp" select="($n_exp)+1"/>
		</xsl:call-template>	
    	</xsl:if> 
</xsl:template>
 
  <xsl:template name="draw_by_species">
  	<xsl:for-each select="smtb:listOfIntermediateCumomers/smtb:listOfCumomers[@weight&lt;=$weight]">
		<xsl:variable name="position" select="math:max(smtb:cumomer/@position)"/>
		
		<xsl:for-each select="smtb:cumomer[@position&lt;=$position]">	
		<axis2d xmin="t0" xmax="t1" ymin="0" ymax="1">		
			<xsl:call-template name="draw_by_species_aux"/>
		</axis2d>	
		</xsl:for-each> 
	</xsl:for-each>
  </xsl:template>  

 <xsl:template name="draw_by_species_aux">
  	<xsl:param name="n_exp">1</xsl:param>	
    				<drawcurve2d ref="x{@weight}_{$n_exp}_{@position}_s"/>	
	<xsl:if test="$n_exp&lt;$experiences">
		<xsl:call-template name="draw_by_species_aux">
			<xsl:with-param name="n_exp" select="($n_exp)+1"/>
		</xsl:call-template>	
    	</xsl:if> 
</xsl:template> 
-->


