<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:sbml="http://www.sbml.org/sbml/level2"    version="1.0" >
  <xsl:output method="xml" indent="yes" encoding="ISO-8859-1" doctype-public="-//UTC//DTD XMLlab V1.5//FR" 
              doctype-system="http://www.xmllab.org/dtd/1.5/fr/simulation.dtd"/>
<xsl:strip-space elements="*"/>
    
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="sbml:sbml">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="sbml:model">
    <simulation>
        <header>
            <title>SBML Converted model</title>
        </header>
        <parameters>
            <section>
                <title>Param�tres des esp�ces</title>
                <xsl:apply-templates select="sbml:listOfSpecies" mode="parameters"/>                
            </section>
           <section>
                <title>Param�tres du mod�le</title>
                <xsl:apply-templates select="sbml:listOfParameters" mode="parameters"/>
                <xsl:apply-templates select="sbml:listOfCompartments" mode="parameters"/>
            </section>            
            <section>
                <title>Param�tres des r�actions</title>
                <xsl:apply-templates select="sbml:listOfReactions" mode="parameters"/>
            </section>
            <section>
                <title>Param�tres de simulation</title>
                <scalar label="Tf">
                    <name>Temps final</name>
                    <value>1</value>                
                </scalar>
                <scalar label="Tsteps">
                    <name>Nombre de points</name>
                    <value>100</value>                
                </scalar>
            </section>
        </parameters>
<!--        <compute>
            <dae label="model_dae">
                <defdomain1d label="t">
                    <name>Temps</name>
                    <interval steps="Tsteps">
                        <initialvalue>0</initialvalue>
                        <finalvalue>Tf</finalvalue>
                    </interval>
                    <states>
                        <xsl:apply-templates select="sbml:species"/>
                    </states>
                </defdomain1d>
            </dae>        
        </compute>-->
    </simulation>
  </xsl:template>

    <xsl:template match="sbml:listOfCompartments" mode="parameters">
        <xsl:apply-templates mode="parameters"/>
    </xsl:template>

    <xsl:template match="sbml:compartment[@size]" mode="parameters">
        <scalar label="{@id}" hidden="yes">
            <value><xsl:value-of select="@size"/></value>
        </scalar>
    </xsl:template>

    <xsl:template match="sbml:compartment" mode="parameters">
        <scalar label="{@id}" hidden="yes">
            <value>1</value>
        </scalar>
    </xsl:template>

  <xsl:template match="sbml:listOfSpecies" mode="parameters">
    <xsl:apply-templates mode="parameters"/>
  </xsl:template>

  <xsl:template match="sbml:species" mode="parameters">
    <subsection>
        <title>
            <xsl:value-of select="@name"/>
        </title>
        <scalar label="{@id}" min="0" max="%inf">
            <name>Condition initiale</name>
            <value>
                <xsl:choose>
                    <xsl:when test="@initialConcentration">
                        <xsl:value-of select="@initialConcentration"/>
                    </xsl:when>
                    <xsl:when test="@initialAmount">
                        <xsl:value-of select="@initialAmount"/>
                    </xsl:when>
                </xsl:choose>
            </value>
        </scalar>
    </subsection>
  </xsl:template>

  <xsl:template match="sbml:listOfReactions" mode="parameters">
      <xsl:apply-templates mode="parameters"/>
  </xsl:template>

  <xsl:template match="sbml:reaction" mode="parameters">
    <subsection>
        <title>
            <xsl:value-of select="@id"/>
        </title>
        <xsl:apply-templates select="sbml:kineticLaw/sbml:listOfParameters" mode="parameters"/>                
    </subsection>
  </xsl:template>

  
  <xsl:template match="sbml:listOfParameters" mode="parameters">
      <xsl:apply-templates mode="parameters"/>
  </xsl:template>
    
  <xsl:template match="sbml:parameter" mode="parameters">
    <scalar label="{@id}">
        <name>
            <xsl:value-of select="@id"/>
        </name>
        <value>
            <xsl:value-of select="@value"/>
        </value>
    </scalar>
  </xsl:template>
  

</xsl:stylesheet>
