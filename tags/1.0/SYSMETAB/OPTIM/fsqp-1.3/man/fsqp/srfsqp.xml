<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?> 
<!DOCTYPE MAN SYSTEM "../man.dtd">
<MAN>
  <LANGUAGE>eng</LANGUAGE>
  <TITLE>srfsqp  </TITLE>
  <TYPE>Scilab Function  </TYPE>
  <DATE>June 1997  </DATE>
  <SHORT_DESCRIPTION name="fsqp"> sequentially related FSQP solver  </SHORT_DESCRIPTION>
  <CALLING_SEQUENCE>
  <CALLING_SEQUENCE_ITEM>x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)  </CALLING_SEQUENCE_ITEM>
  <CALLING_SEQUENCE_ITEM>x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,&apos;grob&apos;,&apos;grcn&apos;)  </CALLING_SEQUENCE_ITEM>
  <CALLING_SEQUENCE_ITEM>x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],&apos;obj&apos;,&apos;cntr&apos;,&apos;grob&apos;,&apos;grcn&apos;, [cd])  </CALLING_SEQUENCE_ITEM>
  <CALLING_SEQUENCE_ITEM>x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],&apos;obj&apos;,&apos;cntr&apos;,&apos;grobfd&apos;,&apos;grcnfd&apos;, [cd])  </CALLING_SEQUENCE_ITEM>
  <CALLING_SEQUENCE_ITEM>[x,inform]=srfsqp(...)  </CALLING_SEQUENCE_ITEM>
  <CALLING_SEQUENCE_ITEM>[x,inform,f,g,lambda]=srfsqp(...)  </CALLING_SEQUENCE_ITEM>
  </CALLING_SEQUENCE>
  <PARAM>
 <PARAM_INDENT>
  <PARAM_ITEM>
  <PARAM_NAME>x0  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : real column vector (initial guess)
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
  <PARAM_ITEM>
  <PARAM_NAME>ipar  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : integer vector of size 8.
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
  <PARAM_ITEM>
  <PARAM_NAME>srpar  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : integer vector of size 3.
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
  <PARAM_ITEM>
  <PARAM_NAME>mesh_pts  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : integer vector of size nfsr+ncsrn+ncsrl
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
  <PARAM_ITEM>
  <PARAM_NAME>rpar  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : real vector of size 4.
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
  <PARAM_ITEM>
  <PARAM_NAME>[bl,bu]  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : real matrix with 2 columns and same row dimension as x.
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
  <PARAM_ITEM>
  <PARAM_NAME>obj, cntr  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : name of scilab functions or character strings (name of C functions) defining the objective function and the constraints function.
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
  <PARAM_ITEM>
  <PARAM_NAME>grob,grcn  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : name of scilab functions or character strings (name of C functions) defining the gradient of objective function and the gradient of  constraints function.
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
  <PARAM_ITEM>
  <PARAM_NAME>cd  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : optional real vector (argument passed to C functions).
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
 </PARAM_INDENT>
  </PARAM>
  <DESCRIPTION>
  <P>
    fsqp interface for SR problems. This interface uses the notations of the cfsqp
    user&apos;s guide (see c_manual.ps).
    The four functions (objective, constraints, gradient-of-objective,
    gradient-of-constraints) can be given either as C-functions dynamically
    linked with Scilab or as Scilab functions (mixtures are allowed).
    If a Scilab function is given as argument of fsqp, 
    just use its name (without quotes). If a C function is to be called,
    use a character string to define it. For instance fsqp(...,obj,...)
    invokes fsqp with the Scilab function <VERB>obj</VERB> and
    <VERB>fsqp(...,&quot;obj&quot;,...)</VERB> invokes fsqp with the C function 
    <VERB>obj</VERB>, linked with Scilab by e.g. <VERB> link(&quot;obj.o&quot;, &quot;obj&quot;) </VERB>.
  </P>
  <P>
    The chains &quot;grobfd&quot; and &quot;grcnfd&quot; can be used as C functions to compute
    the gradients of the functions grob and cntr by finite difference.
    Notations:
  </P>
  <VERBATIM><![CDATA[
ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
srpar=[nfsr,ncsrl,ncsrn];
rpar=[bigbnd,eps,epsneq,udelta];
   ]]></VERBATIM>
  <VERBATIM><![CDATA[
function fj=obj(j,x)
function  gj=cntr(j,x)
function gradf=grob(j,x)
function gradgj=grcn(j,x)
   ]]></VERBATIM>
  <P>
    obj(j,x) returns the value of the jth objective, given x.
    cntr(j,x) returns the value of the jth constraint, given x.
    grob(j,x) (resp. grcn(j,x)) is the value at x of the gradient of the 
    function x-&gt;obj(j,x) (resp.  x-&gt;cntr(j,x)).
  </P>
  <P>
    It may be useful to make use of the fsqp C variable x_is_new and to
    evaluate these functions in a vector way. For instance the function
    cntr can be replaced by the following cntr2 function:
  </P>
  <VERBATIM><![CDATA[
function cj=cntr2(j,x)
if x_is_new() then
 all_objectives=allobj(x);
 all_constraints=allcntr(x);
 all_gradobjectives=allgrob(x);
 all_gradconstraints=allgrcn(x);
 cj=all_constraints(j);
 set_x_is_new(0);  //Use resume to define global Scilab variables
 [all_objectives,all_constraints,all_gradobjectives,all_gradconstraints]=....
 resume(all_objectives,all_constraints,all_gradobjectives,all_gradconstraints);
else
  cj=all_constraints(j);
end
   ]]></VERBATIM>
  <P>
    Here, the function allcntr(x) returns a vector all_constraints whose
    jth component all_constraints(j) is cntr(j).
    A typical example comes from discretized semi-infinite program. 
    See example5.sci.
  </P>
  </DESCRIPTION>
  <EXAMPLE><![CDATA[
// see demo 2 second part
 ]]></EXAMPLE>
  <SEE_ALSO>
    <SEE_ALSO_ITEM> <LINK>fsqp</LINK> </SEE_ALSO_ITEM>     <SEE_ALSO_ITEM> <LINK>x_is_new</LINK> </SEE_ALSO_ITEM>     <SEE_ALSO_ITEM> <LINK>set_x_is_new</LINK> </SEE_ALSO_ITEM>
  </SEE_ALSO>
</MAN>
