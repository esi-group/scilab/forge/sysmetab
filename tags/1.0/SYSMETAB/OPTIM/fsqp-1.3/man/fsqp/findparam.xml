<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?> 
<!DOCTYPE MAN SYSTEM "../man.dtd">
<MAN>
  <LANGUAGE>eng</LANGUAGE>
  <TITLE>fsqp  </TITLE>
  <TYPE>Scilab Function  </TYPE>
  <DATE>June 1997  </DATE>
  <SHORT_DESCRIPTION name="findparam"> utility function for fsqp solver  </SHORT_DESCRIPTION>
  <CALLING_SEQUENCE>
  <CALLING_SEQUENCE_ITEM>[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0)  </CALLING_SEQUENCE_ITEM>
  </CALLING_SEQUENCE>
  <PARAM>
 <PARAM_INDENT>
  <PARAM_ITEM>
  <PARAM_NAME>list_objs  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : list made of two lists.
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
  <PARAM_ITEM>
  <PARAM_NAME>list_cntr  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : list made of six lists.
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
  <PARAM_ITEM>
  <PARAM_NAME>x0  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : real column vector (e.g. initial guess)
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
  <PARAM_ITEM>
  <PARAM_NAME>nf, ..., ncsrn  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : integers (see fsqp-doc).
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
  <PARAM_ITEM>
  <PARAM_NAME>mesh_pts  </PARAM_NAME>
  <PARAM_DESCRIPTION>
    : real vector.
  </PARAM_DESCRIPTION> 
  </PARAM_ITEM>
 </PARAM_INDENT>
  </PARAM>
  <DESCRIPTION>
  <P>
    Given a list of objective functions and a list of constraints
    functions, and a vector x0, findparam returns useful fsqp
    parameters.
    The list of objectives should be: (use empty list when necessary: list() )
  </P>
  <VERBATIM><![CDATA[
list_obj=list(...
               list(f_1,...,f_nf0),...     //regular objectives
        list(F_1,...,F_nfsr)        //SR objectives
              )
   ]]></VERBATIM>
  <P>
    The f_i&apos;s are functions: y=f_i(x) should return the value of the ith
    regular objective as a function of x. y can be a column vector if
    several regular objectives are stacked together.
    y=F_i(x) is the ith sequentially related objective. y is a column
    vector which contains the ith set of SR-objectives (mesh_pts(i) is
    set to size(F_i(x),1) by findparam).
  </P>
  <P>
    The list of constraints functions should be as follows:
  </P>
  <VERBATIM><![CDATA[
list_cntr=list(...
 list(g_1,...,g_ng0),...     //regular nonlinear inequality
        list(G_1,...,G_ncsrn),...   //SR      nonlinear inequality
        list(c_1,...,c_nc0),...     //regular linear    inequality
        list(C_1,...,C_ncsrl),...   //SR      linear    inequality
        list(h_1,...,h_nh0),...     //nonlinear         equality
        list(A_1,...,A_na0)         //linear            equality
             )
   ]]></VERBATIM>
  <P>
    Functions g_i&apos;s, c_i&apos;s, h_1 can return column vectors y (e.g. y=g_1(x)) 
    if several constraints are stacked together.
    Functions G_i&apos;s, C_i&apos;s, A_i&apos;s should return in a column vector the
    set of appropriate SR constraints.
    Examples are given at the end of examplei.sce files. See
    listutils.sci: generic functions obj, cntr, grob, grcn are constructed
    from the lists list_obj, list_cntr, and similar lists list_grobj
    and list_cntr which contain the gradients of objectives and constraints
    (matrices whith nparam=dim(x) columns).
  </P>
  </DESCRIPTION>
  <SEE_ALSO>
    <SEE_ALSO_ITEM> <LINK>srfsqp</LINK> </SEE_ALSO_ITEM>     <SEE_ALSO_ITEM> <LINK>x_is_new</LINK> </SEE_ALSO_ITEM>     <SEE_ALSO_ITEM> <LINK>set_x_is_new</LINK> </SEE_ALSO_ITEM>
  </SEE_ALSO>
</MAN>
