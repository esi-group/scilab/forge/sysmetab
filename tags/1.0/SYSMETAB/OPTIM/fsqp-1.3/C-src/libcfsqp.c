#include <mex.h> 
static int direct_gateway(char *fname,void F(void)) { F();return 0;};
extern Gatefunc cintfsqp2;
extern Gatefunc cintfsqp3;
extern Gatefunc cintfsqp4;
static GenericTable Tab[]={
  {(Myinterfun)sci_gateway,cintfsqp2,"fsqp"},
  {(Myinterfun)sci_gateway,cintfsqp3,"srfsqp"},
  {(Myinterfun)sci_gateway,cintfsqp4,"qld"},
};
 
int C2F(libcfsqp)()
{
  Rhs = Max(0, Rhs);
  (*(Tab[Fin-1].f))(Tab[Fin-1].name,Tab[Fin-1].F);
  return 0;
}
