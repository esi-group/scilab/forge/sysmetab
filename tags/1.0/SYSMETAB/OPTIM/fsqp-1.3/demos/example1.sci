function fj=obj32(j,x)
  fj=(x(1)+3*x(2)+x(3))^2+4*(x(1)-x(2))^2;
endfunction

function gj=cntr32(j,x)
  select j
   case 1
    gj=x(1)^3-6*x(2)-4*x(3)+3;
   case 2   
    gj=1-sum(x);
  end
endfunction

function gradf=grob32(j,x)
  fa=2*(x(1)+3*x(2)+x(3));
  fb=8*(x(1)-x(2));
  gradf=[fa+fb,3*fa-fb,fa];
endfunction

function gradgj=grcn32(j,x)
  select j
   case 1
    gradgj=[3*x(1)^2,-6,-4];
   case 2
    gradgj=[-1,-1,-1];
  end
endfunction
