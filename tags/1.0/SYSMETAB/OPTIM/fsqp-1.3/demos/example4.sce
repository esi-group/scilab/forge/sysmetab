exdir=get_absolute_file_path('example4.sce');

modefsqp=100;
iprint=1;
miter=500;
bigbnd=1.e10;
eps=1.e-7;
epsneq=0.e0;
nf=1;
neqn=0;
nineqn=3;nineq=3;ncsrn=3;
ncsrl=0;
r=100;
mesh_pts=[r,r,3*r/2];
neq=0;nfsr=0;

x0=[0.1*ones(9,1);1];
bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
bigbnd=1.e10; eps=1.e-8; epsneq=0.e0; udelta=0.e0;
 
udelta=0.00001;
rpar=[bigbnd,eps,epsneq,udelta];

srpar=[nfsr,ncsrl,ncsrn];

//[1] fsqp with Scilab functions
//=======================================================

getf(exdir+'/example4.sci');  //Loading Scilab functions obj1, grob1, cntr1, grcn1

t1=%pi*0.025*((1:r)-1)';
t2=%pi*0.025*((1:r)-1)';
t3=%pi*0.25*(1.2+0.2*((1:1.5*r)-1))';
t=[t1;t2;t3];

//Utility fcts needed by  obj1, grob1, cntr1
deff('ztx=z(t,x)','k=1:9;tk=t*k;ztx=(cos(tk)*x(k))^2+(sin(tk)*x(k))^2')
deff('grad=grz(t,x)','k=1:9;tk=t*k;ctk=cos(tk);stk=sin(t*k);grad=[diag(2*ctk*x(k))*ctk+diag(2*stk*x(k))*stk]');

//Scilab functions (Slow: BE PATIENT! 26 sec. CPU time with Pentium200)
x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],obj1,cntr1,grob1,grcn1)

//Evaluating all fcts in one pass... slower! BE PATIENT! 49sec. CPU

x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],obj2,cntr2,grob2,grcn2)

//[2] fsqp with  C functions 
//=======================================================
exec(exdir+'/example4-src/loader.sce') 

// calling C fcts, gradient of cntr computed by fd.
//=======================================================
x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],'obj','cntr','grob','grcnfd')


cd=[];
// Constraints evaluated by Scilab function cntr3 which itself calls
// a C function by the fort command.
x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],'obj',cntr3,'grob','grcnfd')

//Constraints evaluated by scilab fct cntr1
x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],'obj',cntr1,'grob','grcnfd')

//[3] fsqp with  lists 
//=======================================================

getf(exdir+'/listutils.sci');

getf(exdir+'/example4_list.sci'); //loading scilab fcts f_1, G_1,G_2,G_3
//f_1 regular objective R-valued

//G_1 first SR constraint R^100 valued non linear inequality 
//G_2 2nd   SR constraint R^100 valued non linear inequality
//G_3 3rd   SR constraint R^150 valued non linear inequality

list_obj=list(list(f_1),list()); 

list_cntr=list(list(),list(G_1,G_2,G_3),list(),list(),list(),list());

list_grobj=list(list(grf_1),list());

list_grcn=list(list(),list(grG_1,grG_2,grG_3),list(),list(),list(),list());

x0=[0.1*ones(9,1);1];

[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0);

modefsqp=100;iprint=1;miter=500;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

srpar=[nfsr,ncsrl,ncsrn];

bigbnd=1.e10;eps=1.e-7;epsneq=0.e0;udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);

x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)

