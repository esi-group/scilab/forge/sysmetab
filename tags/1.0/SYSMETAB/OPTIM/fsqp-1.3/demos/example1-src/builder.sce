// builder for a shared library for example1.c functions 
//=======================================================

// functions to be added to the call table 
link_name =['obj32','cntr32','grob32','grcn32'] ;  
flag  = "c";		// ext1c is a C function 
files = ['example1.o' ];   // objects files for ext1c 
libs  = [];		// other libs needed for linking 

// the next call generates files (Makelib,loader.sce) used
// for compiling and loading ext1c and performs the compilation

ilib_for_link(link_name,files,libs,flag);

