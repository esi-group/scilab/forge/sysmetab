exdir=get_absolute_file_path('example2.sce');

//fsqp parmeters for example 2
modefsqp=111;
iprint=1;
miter=500;
bigbnd=1.e10;
eps=1.e-8;
epsneq=0.e0;
udelta=0.e0;
nf=163;
neqn=0;
nineqn=0;
nineq=7;
neq=0;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
rpar=[bigbnd,eps,epsneq,udelta];

x0=[0.5;1;1.5;2;2.5;3]; 
bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);

//[1] fsqp with Scilab functions
//=======================================================

getf(exdir+'/example2.sci');  //Loading Scilab fcts objmad and cnmad

cd=[%pi,0.425];  //cd is a (semi-global var. known by objmad,cnmad) variable
//Scilab functions
x=fsqp(x0,ipar,rpar,[bl,bu],objmad,cnmad,'grobfd','grcnfd')

//[2] fsqp with  C functions 
//=======================================================
exec(exdir+'/example2-src/loader.sce') 
// Using C functions: cd must be given as last argument of fsqp
x=fsqp(x0,ipar,rpar,[bl,bu],'objmad','cnmad','grobfd','grcnfd',cd)

//nineqn non linear inequality
for j=1:nineqn
	cnmad(j,x)
end

//nineq-nineqn linear inequality
for j=nineqn+1:nineqn+(nineq-nineqn)
	cnmad(j,x)
end

//neqn nonlinear equality
for j=nineqn+(nineq-nineqn)+1:nineqn+(nineq-nineqn)+neqn
	cnmad(j,x)
end

//neq-neqn linear equality
for j=nineqn+(nineq-nineqn)+neqn+1:nineqn+(nineq-nineqn)+neqn+neq-neqn
	cnmad(j,x)
end


//Same example but Calling SR-fsqp (see example 2 of fsqp doc).

modefsqp=111; 
iprint=1;
miter=500;
bigbnd=1.e10;
eps=1.e-8;
epsneq=0.e0;
udelta=0.e0;
nf=1;
nfsr=1;
mesh_pts=163;
neqn=0;
nineqn=0;
nineq=7;
neq=0;
ncsrl=0;ncsrn=0;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
rpar=[bigbnd,eps,epsneq,udelta];
srpar=[nfsr,ncsrl,ncsrn];

x=srfsqp(x0,ipar,srpar, mesh_pts,rpar,[bl,bu],'objmad','cnmad','grobfd','grcnfd',cd)

//[2] fsqp with  lists 
//=======================================================

getf(exdir+'/listutils.sci');

getf(exdir+'/example2_list.sci');   //Loading user defined Scilab fcts f_1 and c_1
//f_1 returns all the objectives (vector with 163 componenets)
//c_1 returns all the constraints in a vector with 7 components.

// Variables of the problem:
I=(1:163)';
sintheta=sin(%pi*(8.5+I*0.5)/180);
un=ones(6,1);
cd=[%pi,0.425];

pi=cd(1);ss=cd(2);
A=[-1,0,0,0,0,0;
   1,-1,0,0,0,0;
   0,1,-1,0,0,0;
   0,0,1,-1,0,0;
   0,0,0,1,-1,0;
   0,0,0,0,1,-1;
   0,0,0,0,0,1;];

x0=[0.5;1;1.5;2;2.5;3]; 
bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);

// list of objectives (one objective considerd as regular or SR).
list_obj=list(list(),list(f_1))    //0 regular objective, 1 SR-objective (f_1)
//Use list_obj=list(list(f_1),list())  to consider f_1 as a set of 163 regular objs.

//list of constraints  (one linear SR inequality constraint R^7 valued)
list_cntr=list(list(),list(),list(c_1),list(),list(),list())

list_grobj=list(list(),list(grf_1))
//list_grobj=list(list(grf_1),list())

list_grcn=list(list(),list(),list(grc_1),list(),list(),list())

x0=[0.5;1;1.5;2;2.5;3]; 

//fsqp parameters obtained by findparam:
[nf,nineqn,nineq,neqn,neq,nfsr,ncsrl,ncsrn,mesh_pts,nf0,ng0,nc0,nh0,na0]=findparam(list_obj,list_cntr,x0);

modefsqp=111;iprint=1;miter=500;

ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];

srpar=[nfsr,ncsrl,ncsrn];

bigbnd=1.e10;eps=1.e-8;epsneq=0.e0;udelta=0.e0;
rpar=[bigbnd,eps,epsneq,udelta];

bl=-bigbnd*ones(x0);
bu=+bigbnd*ones(x0);

x=srfsqp(x0,ipar,srpar,mesh_pts,rpar,[bl,bu],obj,cntr,grob,grcn)





